@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('admin_page_filter')
<li class="nav-item" style="display: none"> 
<div class="btn-group">
        <select id="admin_page_filter" class="form-control custom-select">
        <option value="">Admin Pages</option>
        @if (isset($ownpage))
        @foreach($ownpage as $ownpage)
        <option value="{{$ownpage}}" id="{{$ownpage}}" >{{$ownpage}}</option>
        @endforeach
        @endif
                                                        
                                                   
      </select>
 
                                        </div>
                         </li>
                      
@endsection
@section('content')

  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                 <header class="" id="myHeader">
                  <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500">Comment</h4>
                       <!--  <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Brand List</li>
                        </ol> -->
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                      <div class='form-material input-group'>
                                    <select class="form-material form-control" id="global_senti">
                                              <option value="">Search Sentiment</option>
                                              <option value="pos">Positive</option>
                                              <option value="neg">Negative</option>
                                              <option value="neutral">Neutral</option>
                                            </select>
                            </div></div>
                               <!--  <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div> -->
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                               <input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <!-- <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
  </header>
               
             
 <div class="row" >
                        <div class="col-lg-12">
                       
                        <div class="card" >
                    
                     
                            <div class="card-body" >
                            <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <table id="tbl_comment" class="table" style="width:100%;">
                              <thead>
                                <tr>
                                  <th>Comments</th>
                                
                                </tr>
                              </thead>

                            </table>
                          </div>
                     
                  
                    
                        </div>
                    </div>
                    </div>
                <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="postModal">Post Detail</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body post_data">
                                               
                                            </div>
                                            <!-- <div class="modal-footer">
                                                <button type="button" id="btnSeeComment" class="btn btn-info waves-effect text-left" data-dismiss="modal">See Comments</button>
                                            </div> -->
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
               <div id="show-add-tag" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="tag_title" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="tag_title">Add New Tag</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body add-tag">
                                               <form method="POST" role="form" action="{{ route('tags.store') }}" aria-label="{{ __('Tags') }}">
                {{csrf_field()}}
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                   
                        <span class="invalid-name" role="alert">
                          
                        </span>
                 
                </div>
            <div class="form-group">
                <label for="keywords" class="control-label">{{ __('Keywords') }}</label>
                 <input id="keywords" type="text"
               class="form-control form-control-line{{ $errors->has('keywords') ? ' is-invalid' : '' }}"
               name="keywords" value="{{ old('keywords') }}" required>
                
                    <span class="invalid-keyword">
                       
                    </span>
               
            </div>
 
            <div class="form-group">
                <button type="button" id="add_new_tag" class="btn btn-primary">  {{ __('Save') }}</button>
              </div>
             </form> 
                                            </div>
                              
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
</div>
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
   <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
     <script src="{{asset('js/morris-data.js')}}" ></script>-->
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
    <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
   <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>
   <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
    <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
   
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var myHeaderContent = document.getElementById("myHeaderContent");
var sticky = header.offsetTop;

function myFunction() {//alert("ho");
  if (window.pageYOffset > sticky) {
    header.classList.add("s-topbar");
    header.classList.add("s-topbar-fix");
    myHeaderContent.classList.add("myHeaderContent");
  } else {
    header.classList.remove("s-topbar");
    header.classList.remove("s-topbar-fix");
    myHeaderContent.classList.remove("myHeaderContent");
  }
}
</script>
    <script type="text/javascript">
var startDate;
var endDate;
/*global mention*/
var mention_total;
var mentionLabel = [];
var mentions = [];
/*Bookmark Array*/
var bookmark_array=[];
var bookmark_remove_array=[];
/*global sentiment*/
var positive = [];
var negative = [];
var sentimentLabel = [];
var positive_total=0;
var negative_total=0;
var  colors=["#1e88e5","#dc3545","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

$(document).ready(function() {



    //initialize
 $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
/* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

 $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

      var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

    startDate = moment().subtract(1, 'month');
    endDate = moment();

           $('.singledate').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        },function(date) {
          endDate=date;
          var id =$('input[type=radio][name=period-radio]:checked').attr('id');
          if(id==="period-week")
         {
           startDate = moment(endDate).subtract(1, 'week');
           endDate = endDate;
         }
         else
         {
           startDate = moment(endDate).subtract(1, 'month');
           endDate = endDate;
         }
         // alert(startDate);
         // alert(endDate);
         var admin_page=$("#admin_page_filter").val();

         ChooseDate(startDate,endDate,admin_page,'');
       
      });



 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Dashboard'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}


   function ChooseDate(start, end,admin_page='',label='') {//alert(start);
     var filter_senti=$('#global_senti option:selected').val();
    if(start !== '')
    {
       $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
          startDate=start;
          endDate=end;
          
         getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),'',filter_senti);
    }
    else
      getAllComment('','','',filter_senti);
         
           
        }

        function GetStartDate()
{
   //alert(startDate);
 return startDate.format('YYYY MM DD');
}
function GetEndDate()
{
           // alert(endDate);
return endDate.format('YYYY MM DD');
}

 function getAllComment(fday,sday,filter_tag='',filter_senti='')
    {

      $(".popup_post").unbind('click');
      $(".edit_predict").unbind('click');
      $("#add_tag").unbind('click');
if(filter_tag == '' && filter_senti == '' && fday =='')
{
  filter_tag=GetURLParameter('search_tag');
filter_senti=GetURLParameter('CmtType');
fday=GetURLParameter('fday');
sday=GetURLParameter('sday');
}
 // alert(filter_tag);
 var oTable = $('#tbl_comment').DataTable({

        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": false,
        "destroy": true,
        "ordering": false   ,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        /*ajax: '{!! route('getallmention') !!}',*/
        "ajax": {
          "url": '{{ route('getRelatedcomment') }}',
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
           "data": {
            "fday": fday,
            "sday": sday,
            "post_id": GetURLParameter('post_id'),
            "brand_id": GetURLParameter('pid'),
            "admin_page":GetURLParameter('admin_page'),
            "is_competitor":"true",
            "tsearch_senti":filter_senti,
            "tsearch_tag":filter_tag,
          }

        },
        "initComplete": function( settings, json ) {
          //console.log(json);
       
        },
     drawCallback: function() {
     $('.select2').select2();
  },

        columns: [
        {data: 'post_div', name: 'post_div',"orderable": false},
    


        ]
        
      }).on('click', '.popup_post', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
    //alert(name);
    var post_id = $(this).attr('id');
    $("#modal-spin").show();
    $(".post_data").empty();

    //alert(post_id);alert(name);alert(GetURLParameter('pid'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getRelatedPosts") }}',
         type: 'GET',
         data: {id:post_id,brand_id:GetURLParameter('pid')},
         success: function(response) { //console.log(response);
         var res_array=JSON.parse(response);
          var data=res_array[0];

          var monitor=res_array[1];

            for(var i in data) {//alert(data[i].message);
              var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
               var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

               var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
               var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
               var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
               pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
               neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
               neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
               pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
               neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
               neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';

               var page_name=data[i].page_name;
              if(isNaN(page_name)==false)
              {
                var b = monitor.filter(item => item.indexOf(page_name) > -1);
               page_name = b[0];
              }

             var page_Link= "https://www.facebook.com/" + page_name ;                   

 var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
               '  <div class="sl-left">'+
               '<img src="'+data[i].full_picture+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
               
                ' <div class="sl-right">'+
                '<div><a href="'+page_Link+'" target="_blank">'+data[i].page_name+'</a>';
                if(parseInt(data[i].isDeleted) == 1)
                html+=' <span class="text-red">This post is no longer available on FB</span> ';
                html+=' <div class="sl-right"> '+
             ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
              ' <div style="width:55%;display: inline-block">';
              if(neg_pcent!=='')
              html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
            if(pos_pcent!=='')
              html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
            if(neutral_pcent!=='')
              html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                html+=' </div></div></div></span></p> ' +                
                  '   <p class="m-t-10" align="justify">' + 
                    data[i].message +
                  
                    '</p> </div></div>';
 $(".post_data").append(html);
     
     
        }


         $("#modal-spin").hide();
         $("#postModal").text(name);
         $('#show-post-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('click', '.edit_predict', function (e) {
  if (e.handled !== true) {

    var post_id = $(this).attr('id');
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
    var emotion = $('#emotion_'+post_id+' option:selected').val();
    var tags = $('#tags_'+post_id).val();
  //   alert(sentiment);
  // alert(tags);
  // return;
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags},
         success: function(response) {// alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });

          e.handled = true;
       }
     
    
        
  }).on('change', '.edit_senti', function (e) {

  if (e.handled !== true) {

    var senti_id = $(this).attr('id');
    var post_id  = senti_id.replace("sentiment_","");
    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
    var emotion = $('#emotion_'+post_id+' option:selected').val();
    var tags = $('#tags_'+post_id).val();
    $("#sentiment_"+post_id).removeClass('text-success'); $("#sentiment_"+post_id).removeClass('text-red'); $("#sentiment_"+post_id).removeClass('text-warning');
     if(sentiment == 'pos')
     $("#sentiment_"+post_id).addClass('text-success');
    else if (sentiment == 'neg')
      $("#sentiment_"+post_id).addClass('text-red');
    else
      $("#sentiment_"+post_id).addClass('text-warning');
    // alert(tags);

         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags},
         success: function(response) {// alert(response)
          if(response>=0)
          {
        //         swal({   
        //     title: "Updated!",   
        //     text: "Done!",   
        //     timer: 1000,   
        //     showConfirmButton: false 
        // });
          }
        }
            });

          e.handled = true;
       }
     
    
        
  }).on('change', '.edit_tag', function (e) {

  if (e.handled !== true) {


    var senti_id = $(this).attr('id');
    var post_id  = senti_id.replace("tags_","");
    var sentiment = $('#sentiment_'+post_id+' option:selected').val();
    var emotion = $('#emotion_'+post_id+' option:selected').val();
    var tags = $('#tags_'+post_id).val();

         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion,tags:tags},
         success: function(response) {// alert(response)
          if(response>=0)
          {
        //         swal({   
        //     title: "Updated!",   
        //     text: "Done!",   
        //     timer: 1000,   
        //     showConfirmButton: false 
        // });
          }
        }
            });

          e.handled = true;
       }
     
    
        
  }).on('click', '#add_tag', function (e) {
  if (e.handled !== true) {
         $('#show-add-tag').modal('show'); 
        }
          
          e.handled = true;
       
     
    
        
  });
     
    
        
  }
$('.dateranges').daterangepicker({
    locale: {
            format: 'MMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end,label) {//alert(label);
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,'',label);
      });
 ChooseDate('','','','');

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
   window.dispatchEvent(new Event('resize'));

 });

hidden_div();
tagCount();
function tagCount()
{

    var fday = moment().subtract(1, 'month');
    var sday = moment();
    fday=fday.format('YYYY MM DD');
    sday=sday.format('YYYY MM DD');
    // alert(sday);
    $("#tbl_tag_count tbody").empty();
    var brand_id = GetURLParameter('pid');
        $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getTagCount')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,limit:'no' }
    })
    .done(function( data ) {//console.log(data);
for(var i in data) 
        {
           var tagLabel=data[i].tagLabel;
           var tagCount=data[i].tagCount;
           $("#tbl_tag_count tbody").append(' <tr style="border-bottom:1px solid rgba(120, 130, 140, 0.13)"> '+
                                            ' <td style="width:40px" span="2"> <button type="button" id="'+tagLabel+'" class="btn '+
                                            ' btn-rounded btn-block btn-info btn_tag">'+tagLabel+'</button></td> '+
                                            ' <td></td> '+
                                            '<td class="text-right"> '+
                                            ' <span class="label label-light-info">'+tagCount+'</span></td> '+
                                            ' </tr>')

        }

    })
     .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    
    });
}
$("#add_new_tag").click(function(){
//alert("hihi");
  var name = $("#name").val();
  if(name == '')
  {
    $(".invalid-name").append("<strong class='text-danger'>Please fill out name!</strong>");
    return false;
  }
  var keywords = $("#keywords").val();
  if(keywords == '')
  {
  $(".invalid-keyword").append("<strong>Please fill out keyword</strong>");
  return false;
}
   $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("quick_tag") }}',
         type: 'POST',
         data: {name:name,keywords:keywords},
         success: function(response) {
          //alert(response);
          if(response !== "exist")
          {
             $('.edit_tag')
         .append($("<option></option>")
                    .attr("value",name)
                    .text(name)); 
          }
             $("#name").val('');
             $("#keywords").val('');
     $('#show-add-tag').modal('toggle');
         
        }
      });

});

  $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
       });


$('#admin_page_filter').change(function() {
    //alert($(this).val());
    var admin_page = $(this).val();
   ChooseDate(startDate,endDate,admin_page,'');
     // $(this).val() will work here
});
    

$(document).on('click', '.btn_tag', function () {
    var filter_tag=this.id;
    var filter_senti=$('#global_senti option:selected').val();
    getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),filter_tag,);
});
$(document).on('click','.dropdown-menu a',function(){
var row_id=this.id;
row_id = row_id.substring(row_id.indexOf('_')+1);
// alert(row_id);
      $("#btnaction_"+row_id+":first-child").text($(this).text());
      $("#btnaction_"+row_id+":first-child").val($(this).text());
      // $("#btnaction_"+row_id).removeClass('btn-red');
      // $("#btnaction_"+row_id).removeClass('btn-success');
      // if($(this).text() === "Require Action")
      // {
      // $("#btnaction_"+row_id).addClass('btn-red');
      // }
      // else
      // {
      //   $("#btnaction_"+row_id).addClass('btn-success');
      // }

       $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setActionUpdate") }}',
         type: 'POST',
         data: {id:row_id,brand_id:GetURLParameter('pid'),action_status:$(this).text()},
         success: function(response) { //alert(response)
          if(response>=0)
          {
       
          }
        }
            });

      //save action status and taken person in database
});
$('#global_senti').change(function(){
  var filter_senti=$('#global_senti option:selected').val();
  getAllComment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),'',filter_senti);

});

//local function

function numberWithCommas(n) {
    var parts=n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

  function kFormatter(num) {
    return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
  function readmore(message){
      // alert("hi hi ");
        var string = String(message);
        var length = string.length; 
         // alert(length);
                if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.
             
            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
        }
        return string;


        }

        

  });
    </script>
    <style type="text/css">
.table td, .table th {
    padding: .75rem;
    vertical-align: middle;
    border-top: 1px solid #dee2e6;
}
.table thead
{
  color:#1c81da;
}
.myHeaderContent
{
   margin-right:300px;
}
/*.s_topbar {
    position: relative;
    z-index: 50;
    -webkit-box-shadow: 5px 0px 10px rgba(0, 0, 0, 0.5);
    box-shadow: 2px 0px 2px rgba(0, 0, 0, 0.5);
}*/
    </style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush

