@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title,'source'=>$source])
@section('content')
@section('comparison')

<li class="nav-item dropdown">
                            <a  class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            
                          <!--     <button onclick="window.location='{{route('comparison', ['pid' => $pid])}}'" type="button" class="btn waves-effect waves-light btn-outline-warning">Comparison</button>  --> 
                                <button  onclick="window.location='{{route('comparison', ['pid' => $pid])}}'" class="btn btn-secondary waves-effect waves-light" style="background:#e9ebee;color:#263238" type="button"><span class="btn-label"><i class="ti-settings"></i></span>Comparison</button>
                            </a>
                           
 </li>
 <li class="nav-item dropdown">
                            <a  class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            <button  onclick="window.location='{{route('insight', ['pid' => $pid])}}'" class="btn btn-secondary waves-effect waves-light" style="background:#e9ebee;color:#263238" type="button"><span class="btn-label"><i class="ti-settings"></i></span>Insight</button>  
                            </a>
                           
 </li>
@endsection
  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard Test</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard Test</a></li>
                            <li class="breadcrumb-item active"> @if (isset($project_name)) {{$project_name}} @endif</li>
                        </ol>
                    </div>

                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                        <!--     <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>THIS MONTH</small></h6>
                                    <h4 class="m-t-0 text-info">$58,356</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div>
                            </div> -->
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                            <div class='input-group mb-3'>
                                <input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            </div>
                                <!-- <div class="">
                                    <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                                </div> -->
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
             
                    <div class="card-group" id='dashboard-div-1' >
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3 id="top-positive-value">0%</h3>
                                    <h6 class="card-subtitle">Positive</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div id="top-positive-progress" class="progress-bar bg-info" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3 id="top-negative-value">0%</h3>
                                    <h6 class="card-subtitle">Negative</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div id="top-negative-progress" class="progress-bar bg-info" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3 id="top-neutral-value">0%</h3>
                                    <h6 class="card-subtitle">Neutral</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div id="top-neutral-progress" class="progress-bar bg-info" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                       <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3 id="top-interest-value">0%</h3>
                                    <h6 class="card-subtitle">Interest</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div id="top-interest-progress" class="progress-bar bg-info" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <!-- <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3 id="top-like-value">0%</h3>
                                    <h6 class="card-subtitle">Like</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div id="top-like-progress" class="progress-bar btn-primary" role="progressbar" style="width: 56%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3 id="top-love-value">0%</h3>
                                    <h6 class="card-subtitle">Love</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div id="top-love-progress" class="progress-bar bg-danger" role="progressbar" style="width: 26%; height: 6px;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3 id="top-haha-value">0%</h3>
                                    <h6 class="card-subtitle">HA HA</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div id="top-haha-progress" class="progress-bar btn-warning" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3 id="top-wow-value">0%</h3>
                                    <h6 class="card-subtitle">Wow</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div id="top-wow-progress" class="progress-bar btn-success" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3 id="top-sad-value">0%</h3>
                                    <h6 class="card-subtitle">Sad</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div id="top-sad-progress" class="progress-bar btn-outline-secondary" role="progressbar" style="width: 56%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3 id="top-angry-value">0%</h3>
                                    <h6 class="card-subtitle">Total Angry</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div id="top-angry-progress" class="progress-bar btn-secondary" role="progressbar" style="width: 26%; height: 6px;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
               
                 <div class="row" id="dashboard-div-2">
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Mentions and Reactions Chart</h4>
                                 <div style="display:none"  align="center" style="vertical-align: top;" id="mentionreaction-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                 <div id="mention-reaction-bar-chart" style="width:100%; height:400px;"></div>   
                            </div>
                        </div>
                    </div>
                
                </div>  

                 <!-- Row -->
                <div class="row">
                    <!-- Column -->
                     <div class="col-lg-7 col-md-12"  id="dashboard-div-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h3 class="card-title">Social Media Reach</h3>
                                        <h6 class="card-subtitle">Monthly Data</h6>
                                    </div>
                                 
                                </div>
                                 <div style="display:none"  align="center" style="vertical-align: top;" id="socialmedia-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                     <div id="social-bar-chart" style="width:100%; height:400px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-lg-5 col-md-12"  id="dashboard-div-4">
                        <div class="card card-body">
                            <h3 class="card-title">Reaction</h3>
                            <div class="message-box">
                              <!--   <div class="message-widget m-t-20" id="reaction-count"> -->
                               <div style="display:none"  align="center" id="reaction-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div id="reaction-pie-chart" style="width:100%; height:400px;"></div>

                                </div>
                            </div>
                                

                        </div>
                    </div>
      
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-7 col-md-12"  id="dashboard-div-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h3 class="card-title">Sentiment</h3>
                                        <h6 class="card-subtitle">Monthly Data</h6>
                                    </div>
                                 
                                </div>
                                 <div style="display:none"  align="center" style="vertical-align: top;" id="sentiment-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                     <div id="sentiment-bar-chart" style="width:100%; height:400px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-12"  id="dashboard-div-6">
                        <!-- Column -->
                        
                        <!-- Column -->
                        <!-- Column -->
                        <div class="b-l p-l-0">
                            <div class="card" >
                                <div class="row">
                                 <div class="card-body">
                                   <ul class="product-review">
                                        <li>
                                            <span class="text-muted display-5"><i class="mdi mdi-emoticon-cool"></i></span>
                                            <div class="dl m-l-10">
                                                <h3 class="card-title">Positive Sentiment</h3>
                                                <h6 class="card-subtitle" id="positive-total">0%</h6>
                                            </div>
                                            <div class="progress">
                                                <div id="positive-progress" class="progress-bar bg-info" role="progressbar" style="width: 65%; height:6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="text-muted display-5"><i class="mdi mdi-emoticon-sad"></i></span>
                                            <div class="dl m-l-10">
                                                <h3 class="card-title">Negative Sentiment</h3>
                                                <h6 class="card-subtitle" id="negative-total">0%</h6>
                                            </div>
                                            <div class="progress">
                                                <div id="negative-progress" class="progress-bar bg-info" role="progressbar" style="width: 15%; height:6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="text-muted display-5"><i class="mdi mdi-emoticon-neutral"></i></span>
                                            <div class="dl m-l-10">
                                                <h3 class="card-title">Neutral Sentiment</h3>
                                                <h6 class="card-subtitle" id="neutral-total">0%</h6>
                                            </div>
                                            <div class="progress">
                                                <div id="neutral-progress" class="progress-bar bg-info" role="progressbar" style="width: 35%; height:6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                    </div>
                </div>
               

                               <!-- Row -->
                <div class="row" id="dashboard-div-7">
                    <!-- column -->
                    <div class="col-lg-12"  >
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Area of Impression</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="category-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div id="category-bar-chart" style="width:100%; height:400px;"></div>   
                            </div>
                        </div>
                    </div>
                
                </div>
    
  
       
            <div class="row" id="dashboard-chart-8">
                    <!-- Column -->
                <div class="col-lg-12 col-xlg-12 col-md-12">
                    <div class="card">
                            <!-- Nav tabs -->
                         <ul class="nav nav-tabs profile-tab" role="tablist">
                            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#Popular" role="tab">Popular Mentions</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Latest" role="tab">Latest Mentions</a> </li>
                                
                          </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="Popular" role="tabpanel">
                                    <div class="card-body">
                                        <div class="profiletimeline" id="popular">
                                      
                                        </div>
                                       <div style="display:none"  align="center" id="popular-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                    </div>
                                              </div>
                                <!--second tab-->
                                <div class="tab-pane" id="Latest" role="tabpanel">
                                    <div class="card-body">
                                         <div class="profiletimeline" id="latest">
                                      
                                        </div>
                                               <div style="display:none"  align="center" id="latest-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->

                   
                    <!-- Column -->
                </div>
                <div class="row" id="dashboard-div-9">
                    <div class="col-lg-12">
                        <div class="card">
                        
                         <ul class="nav nav-tabs profile-tab" role="tablist">
<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#influencer_page" role="tab">Influencer Page</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#influencer_profile" role="tab">Influencer Profile</a> </li>
                                
                          </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="influencer_page" role="tabpanel">
                                    <!-- <div class="card-body"> -->
                               <div class="profiletimeline" style="margin-left:10px">
                                 <div class="card-body">
                            	 <div style="display:none"  align="center" id="page-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                              
                              
                                   
                                 
                                 <div class="row" id="page_list">
                               </div>
                          </div>                      
                   
                            </div>
                         
                                       
                                    <!-- </div> -->
                                              </div>
<div class="tab-pane" id="influencer_profile" role="tabpanel">
                                    <!-- <div class="card-body"> -->
  <div class="profiletimeline" style="margin-left:10px">
  <div class="card-body">
<div style="display:none"  align="center" id="profile-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>

<div class="row" id="profile_list">
</div>


   </div>
  </div>
                                       
                                    <!-- </div> -->
                                              </div>
                                          </div>
                     

                        </div>

                    </div>
          </div>
                            <div class="row" id="dashboard-div-10">
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Highlighted Voice</h4>
                                <div style="display:none"  align="center" id="hightlighted-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div style="width:100%; height:800px;" id="hightlighted" ></div>   
                            </div>
                                   
                        </div>
                    </div>
                  
                </div>
                                 <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme working">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/1.jpg')}}" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/2.jpg')}}" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/3.jpg')}}" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/4.jpg')}}" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/5.jpg')}}" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/6.jpg')}}" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/7.jpg')}}" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/8.jpg')}}" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
    <!-- <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script> -->
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
   <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
    <script type="text/javascript">
var startDate;
var endDate;
var mention_total;
var mentionLabel = [];
    var mentions = [];
var  colors=["#98c0d8","#7b858e","#01ad9d","#cb73a9","#a1c652","#dc3545","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

  $.noConflict();

  jQuery( document ).ready(function ($) {

    //initialize
 $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
/* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

 $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

      var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

    startDate = moment().subtract(3,'month');
    endDate = moment();
     function requestinterestData(fday,sday){
     var brand_id = GetURLParameter('pid');
    
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getinterestdata')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'month' }
    })
    .done(function( data ) {
      var each_total=0;
      var all_total=0;
      

        for(var i in data) {
        each_total+=parseInt(data[i].interest_sum);
        all_total+=parseInt(data[i].comment_count);
       
      }

    

var interest_percentage=parseInt((each_total/all_total)*100);
interest_percentage = isNaN(interest_percentage)?0:interest_percentage;

 $('#top-interest-progress').css('width', interest_percentage+'%').attr('aria-valuenow', interest_percentage);   
 $("#top-interest-value").text(interest_percentage + "%");

 
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }

    function requestmentionData(fday,sday){//alert(fday);
      
/*       var mentionchart = document.getElementById('mention-bar-chart');
    var mentionChart = echarts.init(mentionchart);*/

$("#mentionreaction-spin").show();
$("#socialmedia-spin").show(); 
  $("#reaction-spin").show();       
       var brand_id = GetURLParameter('pid');
       /* var brand_id = 22;*/
       // alert (brand_id);
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getmentiondetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'month'}
    })
    .done(function( data ) {//alert(data);
 
    /*   $("#mention-total").text("Total = "+data[data.length-1].total);
       $("#social-total").text("Total = "+data[data.length-1].total);
       var positive_total= data[data.length-1].total+10;
       $("#sentiment-total").text("Positive = "+ positive_total + " && Negative = "+data[data.length-1].total);*/

      // When the response to the AJAX request comes back render the chart with new data
  


 
       mention_total=0;

       mentions=[];
       mentionLabel=[];

      for(var i in data) {//alert(data[0][i].mention);
        mentions.push(data[i].mention);
        mentionLabel.push(data[i].periodLabel);
        
      }

$.each(mentions,function(){mention_total+=parseInt(this) || 0;});


requestmentionReactData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
 
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }

  function requestsentimentbycategory(fday,sday){//alert(fday);
      
var categorychart = document.getElementById("category-bar-chart");
var categoryChart = echarts.init(categorychart);

$("#category-spin").show();
        
       var brand_id = GetURLParameter('pid');
       /* var brand_id = 22;*/
       // alert (brand_id);
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getsentimentbycategory')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id }
    })
    .done(function( data ) {//alert(data);
          
       console.log(data);//mention
    var xAxisData = [];
var data1 = [];
var data2 = [];


/*for (var i = 0; i < 10; i++) {
    xAxisData.push('Category' + i);
    data1.push((Math.random() * 2).toFixed(2));
    data2.push(-Math.random().toFixed(2));
 
}*/

for(var i in data) 
        {
    xAxisData.push(data[i].category);
    data1.push(Math.round(data[i].positive));
    if(data[i].negative >0 )
    {
    data2.push(Math.round(-data[i].negative));
    }
    else
    {
     data2.push(0);
    }
  
 
}


var itemStyle = {
    normal: {
    },
    emphasis: {
        barBorderWidth: 1,
        shadowBlur: 10,
        shadowOffsetX: 0,
        shadowOffsetY: 0,
        shadowColor: 'rgba(0,0,0,0.5)'
    }
};


option = {
      color:colors,
    backgroundColor: 'rgba(0, 0, 0, 0)',
     dataZoom:[  {
            type: 'slider',
            show: true,
            xAxisIndex: [0],
            start: 70,
            end: 100
        },
         {
            type: 'inside',
            xAxisIndex: [0],
            start: 1,
            end: 35
        }
    ],
        calculable : true,
    legend: {
        data: ['positive', 'negative'],
           x: 'center',
             y: 'top',
              padding :0,
    }/*,
    brush: {
        toolbox: ['rect', 'polygon', 'lineX', 'lineY', 'keep', 'clear'],
        xAxisIndex: 0
    }*/,

    toolbox: {
            show : true,
            feature : {
                mark : {show: true},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['stack','tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
    tooltip: {},
    calculable : true,
    dataZoom : {
        show : true,
        realtime : true,
        start : 20,
        end : 80
    },
    xAxis: {
        data: xAxisData,
        name: 'X Axis',
        silent: false,
        axisLine: {onZero: true},
        splitLine: {show: false},
        splitArea: {show: false},
       /*  axisLabel: {
            textStyle: {
                color: '#fff'
            }
        },*/
    },
    yAxis: {
        inverse: false,
        splitArea: {show: false}
    },
   grid: {
            top: '12%',
            left: '1%',
            right: '10%',
            containLabel: true
        },
  /*  visualMap: {
        type: 'continuous',
        dimension: 1,
        text: ['High', 'Low'],
        inverse: true,
        itemHeight: 200,
        calculable: true,
        min: -2,
        max: 6,
        top: 60,
        left: 10,
        inRange: {
            colorLightness: [0.4, 0.8]
        },
        outOfRange: {
            color: '#bbb'
        },
        controller: {
            inRange: {
                color: '#2f4554'
            }
        }
    },*/
    series: [
        {
            name: 'positive',
            type: 'bar',
            stack: 'one',
            color:colors[0],
            barMaxWidth:30,
            itemStyle: itemStyle,
            data: data1
        },
        {
            name: 'negative',
            type: 'bar',
            stack: 'one',
            color:colors[1],
            barMaxWidth:30,
            itemStyle: itemStyle,
            data: data2
        }
    ]
};

$("#category-spin").hide();
/*categoryChart.on('brushSelected', renderBrushed);

function renderBrushed(params) {
    var brushed = [];
    var brushComponent = params.batch[0];

    for (var sIdx = 0; sIdx < brushComponent.selected.length; sIdx++) {
        var rawIndices = brushComponent.selected[sIdx].dataIndex;
        brushed.push('[Series ' + sIdx + '] ' + rawIndices.join(', '));
    }
}
*/
    categoryChart.setOption({
        title: {
            backgroundColor: '#333',
          /*  text: 'SELECTED DATA INDICES: \n' + brushed.join('\n'),*/
            bottom: 0,
            right: 0,
            width: 100,
            textStyle: {
                fontSize: 12,
                color: '#fff'
            }
        }
    });

    categoryChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            categoryChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
   categoryChart.on('click', function (params) {
   console.log(params);
   console.log(params.name); // xaxis data = education
   console.log(params.seriesName); //bar type name ="positive"
   console.log(params.value);//count 8
   var pid = GetURLParameter('pid');
   
   window.open("{{ url('pointoutmention?')}}" +"pid="+ pid+"&category="+ params.name +"&type="+params.seriesName+"&from_graph=category" , '_blank');
   //window.location="{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName ;
   
});


   

  
    })
     .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

  }

   
   function requestmentionReactData(fday,sday){//alert(fday);
       
    
        console.log("mention");
       console.log(mentions);//mention
    
       var sachart = document.getElementById('mention-reaction-bar-chart');
    var SAChart = echarts.init(sachart);

/*$("#mentionreaction-spin").show();  */
       var brand_id = GetURLParameter('pid');
       /* var brand_id = 22;*/
       // alert (brand_id);
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getinteractiondetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'month' }
    })
    .done(function( data ) {//alert(data);
     


     var reactions = [];
     var reactions_total =0;
     var reactionLabel =[];

      var socials = [];
      var socialsfortotal = [];
       var social_total=0;

var Like_total=0;var Love_total=0;var Haha_total=0;
var Sad_total=0;var Angry_total=0;var Wow_total=0;

       for(var i in data) 
        {
          var reaction=parseInt(data[i].Like)+parseInt(data[i].Love)+parseInt(data[i].Haha)+parseInt(data[i].Wow)+parseInt(data[i].Angry)+parseInt(data[i].Sad); 
          reactions.push(reaction);
          reactionLabel.push(data[i].periodLabel);

          var mediaReach=parseInt(data[i].Like)+parseInt(data[i].Love)+parseInt(data[i].Haha)+parseInt(data[i].Wow)+parseInt(data[i].Angry)+parseInt(data[i].Sad)+parseInt(data[i].shared)+parseInt(data[i].post_count)
          socials.push(mediaReach);
         
         Like_total +=parseInt(data[i].Like);Love_total +=parseInt(data[i].Love);
         Sad_total +=parseInt(data[i].Sad);Angry_total +=parseInt(data[i].Angry);
         Haha_total +=parseInt(data[i].Haha);Wow_total +=parseInt(data[i].Wow);
          
        }

$.each(socials,function(){social_total+=parseInt(this) || 0;});

social_total = kFormatter(social_total);
$.each(reactions,function(){reactions_total+=parseInt(this) || 0;});

var formal_reactions_total=reactions_total; // pass parameter to ReactionData Function
reactions_total = kFormatter(reactions_total);





option = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross'
        },
        formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
       /* console.log(rez);*/ //quite useful for debug
        params.forEach(item => {
            /*console.log(item);
            console.log(item.data);*/
            var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName + ': ' + kFormatter(item.data)  + '</p>'
            rez += xx;
        });

        return rez;
    }
    },
    grid: {
        right: '20%'
    },
  /*  toolbox: {
        feature: {
            dataView: {show: true, readOnly: false},
            restore: {show: true},
            saveAsImage: {show: true}
        }
    },*/
     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },

    legend: {
        data:['Mentions', 'Total Reaction'],
          formatter: function (name) {
            if(name === 'Mentions')
            {
                 return name + ': ' + mention_total;
            }
            return name  + ': ' + reactions_total;

   
}
    },
    xAxis: [
        {
            type: 'category',
            axisTick: {
                alignWithLabel: true
            },
                         axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
     var date = new Date(value);
       console.log(date);
      var texts = [date.getFullYear(), monthNames[date.getMonth()]];

    return texts.join('-');

}
    },
            data: reactionLabel
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Mentions',
          /*  min: 0,
            max: 250,*/
            position: 'left',
            axisLine: {
                lineStyle: {
                    color: colors[0]
                }
            }/*,
            axisLabel: {
                formatter: '{value}'
            }*/
             /* axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }*/
    
        },
        {
            type: 'value',
            name: 'Total Reaction',
           /* min: 0,
            max: 250,*/
            position: 'right',
            offset: 80,
            axisLine: {
                lineStyle: {
                    color: colors[1]
                }
            },
            /*axisLabel: {
                formatter: '{value} ml'
            }*/
              axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }
    }
        }
    ],
    series: [
        {
            name:'Mentions',
            type:'bar',
            color:colors[0],
            barMaxWidth:30,
            data:mentions
        },
        {
            name:'Total Reaction',
            type:'line',
            color:colors[1],
            yAxisIndex: 1,
            data:reactions,
            markPoint : {
              large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        }
    ]
};
$("#mentionreaction-spin").hide();
    SAChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            SAChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
    requestsocialData(socials,social_total,reactionLabel);
    ReactionData(Like_total,Love_total,Wow_total,Sad_total,Angry_total,Haha_total,formal_reactions_total);
  
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }
    function requestsentimentData(fday,sday){//alert(fday),alert(sday);
   //alert("hihi");
  
    var sentiment_chart = document.getElementById('sentiment-bar-chart');
    var sentimentChart = echarts.init(sentiment_chart);
    $("#sentiment-spin").show();
        var brand_id = GetURLParameter('pid');
       // alert (brand_id);
      
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getsentimentdetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'month' }
    })
    .done(function( data ) {

    
       console.log(data);
    
   
     
      var positive = [];
      var negative = [];
      var neutral=[];
      var sentimentLabel = [];

      var positive_total=0;
      var negative_total=0;
      var neutral_total=0;
      var all_total=0;


        for(var i in data) {//alert(data[i].mentions);
        positive.push(data[i].positive);
        negative.push(data[i].negative);
        neutral.push(data[i].neutral);
        sentimentLabel.push(data[i].periodLabel);
        
      }


$.each(positive,function(){positive_total+=parseInt(this) || 0;});
$.each(negative,function(){negative_total+=parseInt(this) || 0;});
$.each(neutral,function(){neutral_total+=parseInt(this) || 0;});
all_total=positive_total+negative_total+neutral_total;
var positive_percentage=parseInt((positive_total/all_total)*100);
var negative_percentage= parseInt((negative_total/all_total)*100);
var neutral_percentage =  parseInt((neutral_total/all_total)*100);

          positive_percentage = isNaN(positive_percentage)?0:positive_percentage;
          negative_percentage = isNaN(negative_percentage)?0:negative_percentage;
          neutral_percentage = isNaN(neutral_percentage)?0:neutral_percentage;
         

 $('#positive-progress').css('width', positive_percentage+'%').attr('aria-valuenow', positive_percentage); 
 $('#top-positive-progress').css('width', positive_percentage+'%').attr('aria-valuenow', positive_percentage);   
 $('#negative-progress').css('width', negative_percentage+'%').attr('aria-valuenow', negative_percentage);
 $('#top-negative-progress').css('width', negative_percentage+'%').attr('aria-valuenow', negative_percentage);

 $('#top-neutral-progress').css('width', neutral_percentage+'%').attr('aria-valuenow', neutral_percentage);
 $('#neutral-progress').css('width', neutral_percentage+'%').attr('aria-valuenow', neutral_percentage);


 $("#positive-total").text(positive_percentage + "%");
 $("#top-positive-value").text(positive_percentage + "%");
 $("#negative-total").text(negative_percentage + "%");
 $("#top-negative-value").text(negative_percentage + "%");
 $("#neutral-total").text(neutral_percentage + "%");
 $("#top-neutral-value").text(neutral_percentage + "%");

    option = {
        color:colors,
      

 tooltip : {
            trigger: 'axis',
             /*formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
        console.log(rez); //quite useful for debug
        params.forEach(item => {
             console.log("hihi"); 
            console.log(item);
            console.log(item.series.color); //quite useful for debug
            var xx = '<p>'   + colorSpan(item.series.color) + ' ' + item.seriesName + ': ' + item.data  + '</p>'
            rez += xx;
        });

        return rez;
    }*/

        },

        legend: {
            data:['Positive','Negative'],
           formatter: function (name) {
            if(name === 'Positive')
            {
                 return name + ': ' + positive_total;
            }
            return name  + ': ' + negative_total;

   
}
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
        {
            type : 'category',
             axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
     var date = new Date(value);
       console.log(date);
      var texts = [date.getFullYear(), monthNames[date.getMonth()]];

    return texts.join('-');

}
    },
           data : sentimentLabel
        }
        ],
        yAxis : [
        {
            type : 'value'
        }
        ],
        series : [
        {
            name:'Positive',
            type:'bar',
            data:positive,
            barMaxWidth:30,
            color:colors[0],
            markPoint : {
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }/*,
            markLine : {
                data : [
                {type : 'average', name: 'average'}
                ]
            }*/
        },
        {
            name:'Negative',
          /*  type:effectIndex % 2 == 0 ? 'bar' : 'line',*/
            type:'bar',
            data:negative,
            barMaxWidth:30,
            color:colors[1],
            markPoint : {
              data : [
              {type : 'max', name: 'maximum'},
              {type : 'min', name: 'minimum'}
              ]
          }/*,
          markLine : {
            data : [
            {type : 'average', name : 'average value'}
            ]
        }*/
    }
    ]
};
$("#sentiment-spin").hide();
sentimentChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            sentimentChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
    sentimentChart.on('click', function (params) {
   console.log(params);
   console.log(params.name); // xaxis data = 2018-08
   console.log(params.seriesName); //bar period name ="Positive"
   console.log(params.value);//count
   var pid = GetURLParameter('pid');
   if(params.name !== 'minimum' && params.name !== 'maximum' )
   window.open("{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName+"&from_graph=sentiment" , '_blank');
   //window.location="{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName ;
   
});
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }
    function requestsocialData(socials,social_total,socialLabel){//alert(fday),alert(sday);
      
    var socialchart = document.getElementById('social-bar-chart');
    var socialChart = echarts.init(socialchart);



   
   

/* $("#mention-total").text("Total = "+mention_total);*/


    option = {
        color:colors,
      

         tooltip : {
            trigger: 'axis',
       formatter: function (params) {
            return '<b>' + params[0].name + '</b><br/><span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+params[0].color+';"></span>'+ params[0].seriesName + ' : '+  kFormatter(params[0].value);
        }

        },
        legend: {
            data:['social media reach' ],
             x: 'center',
             y: 'top',
             padding :0,
            formatter: '{name}: '+social_total,
        },
 /*       grid: {
          top:    60,
    
    left:   '10%',
    right:  '10%',
            containLabel: true
        },*/
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
        {
            type : 'category',
                         axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
     var date = new Date(value);
       console.log(date);
      var texts = [date.getFullYear(), monthNames[date.getMonth()]];

    return texts.join('-');

}
    },
            data : socialLabel
        }
        ],
        yAxis : [
        {
            type : 'value',
             axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }
    }
        }
        ],

        series : [
        {
            name:'social media reach',
            type:'line',
            data:socials,
            barMaxWidth:30,
            color:colors[0],
markPoint : {
              large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        }
        
    ]
};
$("#socialmedia-spin").hide();
        
 socialChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            socialChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

  }

  function popular_mentions(fday,sday)
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getpopularmention')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {//$("#popular").html('');
     $( "#popular-spin" ).hide();
       console.log(data);
      
        for(var i in data) {//alert(data[i].sentiment);
             var img_sentiment="";
             var img_emotion="";
                 if(data[i].sentiment !=='' && data[i].sentiment !== undefined)
                 img_sentiment = judge_sentiment_icon(data[i].sentiment);
            
                 if(data[i].emotion !=='' && data[i].emotion !== undefined)
                 {
                  var emo_code =  judge_emotion_icon(data[i].emotion) ;
                    if(! isNaN(emo_code))
                    img_emotion = String.fromCodePoint(emo_code);
            }
                 //alert(img_emotion);
                // var test= String.fromCodePoint(0x1F602);
    
     var html='<div class="sl-item">'+
               '  <div class="sl-left">'+
               '<img src="'+data[i].full_picture+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
               
                ' <div class="sl-right">'+
                '<div><a href="'+data[i].link+'" class="link" target="_blank">'+data[i].page_name+'</a> <span class="sl-date">'+data[i].created_time+'</span><a> | Influencer Score : </a><span class="label label-light-danger">'+data[i].influencer_score+'/10</span>'+
                 '</div>'+
             '<p>'+img_sentiment+'<select class="form-control custom-select sentiment-color customize-select" id="sentiment_popular_'+data[i].id+'"  >';
               if(data[i].sentiment == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="pos"';
                if(data[i].sentiment == "pos")
                html+= 'selected="selected"';
                html+= '>pos</option><option value="neg"';
                if(data[i].sentiment == "neg")
                html+= 'selected="selected"';   
                 html+= '>neg</option><option value="neutral"';
                if(data[i].sentiment == "neutral")
                html+= 'selected="selected"';    
                html+= '>neutral</option><option value="NA"';
                if(data[i].sentiment == "NA")
                html+= 'selected="selected"';  
                html+= '>NA</option></select>';

                html+= ' | '+img_emotion+'<select class="form-control custom-select emotion-color customize-select" id="emotion_popular_'+data[i].id+'"  >';
                if(data[i].emotion == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="anger"';
                if(data[i].emotion == "anger")
                html+= 'selected="selected"';
                html+= '>anger</option><option value="interest"';
                if(data[i].emotion == "interest")
                html+= 'selected="selected"';     
                html+= '>interest</option><option value="disgust"';
                if(data[i].emotion == "disgust")
                html+= 'selected="selected"';  
                html+= '>disgust</option><option value="fear"';
                if(data[i].emotion == "fear")
                html+= 'selected="selected"'; 
                html+= '>fear</option><option value="joy"';
                if(data[i].emotion == "joy")
                html+= 'selected="selected"'; 
                html+= '>joy</option><option value="like"';
                if(data[i].emotion == "like")
                html+= 'selected="selected"'; 
                html+= '>like</option><option value="love"';
                if(data[i].emotion == "love")
                html+= 'selected="selected"'; 
                html+= '>love</option><option value="neutral"';
                if(data[i].emotion == "neutral")
                html+= 'selected="selected"';
                html+= '>neutral</option><option value="sadness"';
                if(data[i].emotion == "sadness")
                html+= 'selected="selected"';
                html+= '>sadness</option><option value="surprise"';
                if(data[i].emotion == "surprise")
                html+= 'selected="selected"';
               html+= '>surprise</option><option value="trust"';
                if(data[i].emotion == "trust")
                html+= 'selected="selected"';
                html+= '>trust</option><option value="NA"';
                if(data[i].emotion == "NA")
                html+= 'selected="selected"';
                html+= '>NA</option></select>';
                if(data[i].edit_permission === 1)
                html+= ' <a class="edit_predict_popular" id="'+data[i].id+'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a> ';
                html+= ' </span></p> ' +                
                  '   <p class="m-t-10" align="justify">' + 
                       readmore(data[i].message) +
                         '...<a href="'+data[i].link+'" target="_blank">' + 
                       ' Read More</a>' + 
                    '</p> </div>';
 $("#popular").append(html);
  }
  

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}

 function latest_mentions(fday,sday)
{//alert("popular");
    var brand_id = GetURLParameter('pid');
   //var brand_id = 22;
$( "#latest-spin" ).show();
    $("#latest").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getlatestmention')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {//$("#popular").html('');
      $( "#latest-spin" ).hide();
       console.log(data);
       //latest
        for(var i in data) {//alert(data[i].mentions);
  
        // var string = readmore(data[i].message,data[i].link);
                 var img_sentiment="";
                 var img_emotion="";
                 if(data[i].sentiment !=='' && data[i].sentiment !== undefined)
                 img_sentiment = judge_sentiment_icon(data[i].sentiment);
            
            
                 if(data[i].emotion !=='' && data[i].emotion !== undefined)
                 {
                  var emo_code =  judge_emotion_icon(data[i].emotion) ;
                    if(! isNaN(emo_code))
                    img_emotion = String.fromCodePoint(emo_code);
                 }
  var html='<div class="sl-item">'+
               '  <div class="sl-left">'+
               '<img src="'+data[i].full_picture+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
               
                ' <div class="sl-right">'+
                '<div><a href="'+data[i].link+'" class="link" target="_blank">'+data[i].page_name+'</a> <span class="sl-date">'+data[i].created_time+'</span>'+
                 '</div>'+
             '<p>'+img_sentiment+'<select class="form-control custom-select sentiment-color customize-select" id="sentiment_latest_'+data[i].id+'" >';
                 if(data[i].sentiment == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="pos"';
                if(data[i].sentiment == "pos")
                html+= 'selected="selected"';
                html+= '>pos</option><option value="neg"';
                if(data[i].sentiment == "neg")
                html+= 'selected="selected"';   
                html+= '>neg</option><option value="neutral"';
                if(data[i].sentiment == "neutral")
                html+= 'selected="selected"';  
                html+= '>neutral</option><option value="NA"';
                if(data[i].sentiment == "NA")
                html+= 'selected="selected"';  
                html+= '>NA</option></select>';

                html+= ' | '+img_emotion+'<select class="form-control custom-select emotion-color customize-select" id="emotion_latest_'+data[i].id+'"  >';
                if(data[i].emotion == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="anger"';
                if(data[i].emotion == "anger")
                html+= 'selected="selected"';
                html+= '>anger</option><option value="interest"';
                if(data[i].emotion == "interest")
                html+= 'selected="selected"';     
                html+= '>interest</option><option value="disgust"';
                if(data[i].emotion == "disgust")
                html+= 'selected="selected"';  
                html+= '>disgust</option><option value="fear"';
                if(data[i].emotion == "fear")
                html+= 'selected="selected"'; 
                html+= '>fear</option><option value="joy"';
                if(data[i].emotion == "joy")
                html+= 'selected="selected"'; 
                html+= '>joy</option><option value="like"';
                if(data[i].emotion == "like")
                html+= 'selected="selected"'; 
                html+= '>like</option><option value="love"';
                if(data[i].emotion == "love")
                html+= 'selected="selected"'; 
                html+= '>love</option><option value="neutral"';
                if(data[i].emotion == "neutral")
                html+= 'selected="selected"';
                html+= '>neutral</option><option value="sadness"';
                if(data[i].emotion == "sadness")
                html+= 'selected="selected"';
                html+= '>sadness</option><option value="surprise"';
                if(data[i].emotion == "surprise")
                html+= 'selected="selected"';
                html+= '>surprise</option><option value="trust"';
                if(data[i].emotion == "trust")
                html+= 'selected="selected"';
                html+= '>trust</option><option value="NA"';
                if(data[i].emotion == "NA")
                html+= 'selected="selected"';
                html+= '>NA</option></select>';
                if(data[i].edit_permission === 1)
                html+= ' <a class="edit_predict_latest" id="'+data[i].id+'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a> ';
                html+= ' </span></p> ' +                
                  '   <p class="m-t-10" align="justify">' + 
                       readmore(data[i].message) +
                         '...<a href="'+data[i].link+'" target="_blank">' + 
                       ' Read More</a>' + 
                    '</p> </div>';
 $("#latest").append(html);
 
  }
  

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}

 function Influencer_profile(fday,sday)
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#profile-spin" ).show();
    $("#profile_list").empty();
  
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getInfluencerProfile')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {//$("#popular").html('');
     $( "#profile-spin" ).hide();
       console.log(data);
      
        for(var i in data) {//alert(data[i].sentiment);

           /* $("#profile_list").append("  <div  class='col-lg-6 col-md-6'><div class='social-widget'>"+
                                      "<div class='soc-header box-facebook'><a href='#'>"+
                                      "<div class='user-img'> <img src='"+data[i].full_picture+"' alt='user' class='img-circle img-bordered-sm'>"+
                                      " </div>"+
                                      " <h5>"+data[i].page_name+"</h5> "+
                                      " <div>"+
                                      
                                      " </div>"+
                                       "</a>"+
                                     " </div>"+
                                      "<div class='soc-content'>"+
                                      "<div class='col-6 b-r'>"+
                                      "<h3 class='font-medium'>"+numberWithCommas(data[i].fan_count)+"</h3>"+
                                      "<h5 class='text-muted'>Followers</h5></div>"+
                                      "<div class='col-6'>"+
                                      "<h3 class='font-medium'>"+numberWithCommas(data[i].shared)+"</h3>"+
                                      "<h5 class='text-muted'>Share</h5></div>"+
                                      "</div>"+
                                      "</div></div>");*/
        $("#profile_list").append("<div class='col-lg-3 col-md-6'>"+
                        "<div class='card'>"+
                        "<div class='row'>"+
                        "<div class='col-12'>"+
                        "<div class='social-widget'>"+
                        "<div class='soc-header box-facebook'><img src='"+data[i].full_picture+"' alt='user' class='img-circle img-bordered-sm' width='100' height='100'>"+
                        "<h6 style='color:#ffffff'>"+data[i].page_name+"</h6></div>"+
                        "<div class='soc-content'>"+
                        "<div class='col-12 b-r'>"+
                        "<h5 class='text-sm'>"+numberWithCommas(data[i].fan_count)+"</h5>"+
                        "<h5 class='text-muted'>Followers</h5></div>"+
                        /*"<div class='col-6'>"+
                        "<h5 class='text-sm'>"+numberWithCommas(data[i].shared)+"</h5>"+
                        "<h5 class='text-muted'>Share</h5></div>"+*/
                        "</div>"+
                        "</div>"+
                        "</div>"+
                        "</div>"+
                        "</div>"+
                    "</div>");
            
            
         
/*    $("#profile_list").append("<a href='#'>"+
    "<div class='user-img'> <img src='"+data[i].full_picture+"' alt='user' class='img-circle img-bordered-sm'> <span class='profile-status offline pull-right'></span>"+
    " </div>"+
    " <div class='mail-contnet'>"+
    " <h5>"+data[i].page_name+"</h5> <span class='mail-desc'>Follower | "+numberWithCommas(data[i].fan_count)+"</span></div>"+
    "</a>")*/
  }
  

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
function Influencer_page(fday,sday)
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
   
    $( "#page-spin" ).show();
 
    $("#page_list").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getInfluencerPage')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {//$("#popular").html('');
     $( "#page-spin" ).hide();
       console.log(data);
      
        for(var i in data) {//alert(data[i].sentiment);

          /*  $("#page_list").append("  <div  class='col-lg-6 col-md-6'><div class='social-widget'>"+
                                      "<div class='soc-header box-facebook'><a href='#'>"+
                                      "<div class='user-img'> <img src='"+data[i].full_picture+"' alt='user' class='img-circle img-bordered-sm'>"+
                                      " </div>"+
                                      " <h5>"+data[i].page_name+"</h5> "+
                                      " <div>"+
                                      
                                      " </div>"+
                                       "</a>"+
                                     " </div>"+
                                      "<div class='soc-content'>"+
                                      "<div class='col-6 b-r'>"+
                                      "<h3 class='font-medium'>"+numberWithCommas(data[i].fan_count)+"</h3>"+
                                      "<h5 class='text-muted'>Followers</h5></div>"+
                                      "<div class='col-6'>"+
                                      "<h3 class='font-medium'>"+numberWithCommas(data[i].shared)+"</h3>"+
                                      "<h5 class='text-muted'>Share</h5></div>"+
                                      "</div>"+
                                      "</div></div>");*/

            $("#page_list").append("<div class='col-lg-3 col-md-6'>"+
                        "<div class='card'>"+
                        "<div class='row'>"+
                        "<div class='col-12'>"+
                        "<div class='social-widget'>"+
                      "<div class='soc-header box-facebook'><img src='"+data[i].full_picture+"' alt='user' class='img-circle img-bordered-sm' width='100' height='100'>"+
                        "<h6 style='color:#ffffff'>"+data[i].page_name+"</h6></div>"+
                        "<div class='soc-content'>"+
                        "<div class='col-12 b-r'>"+
                        "<h5 class='text-sm'>"+numberWithCommas(data[i].fan_count)+"</h5>"+
                        "<h5 class='text-muted'>Followers</h5></div>"+
                        /*"<div class='col-6'>"+
                        "<h5 class='text-sm'>"+numberWithCommas(data[i].shared)+"</h5>"+
                        "<h5 class='text-muted'>Share</h5></div>"+*/
                        "</div>"+
                        "</div>"+
                        "</div>"+
                        "</div>"+
                        "</div>"+
                    "</div>");
            
         
/*    $("#profile_list").append("<a href='#'>"+
    "<div class='user-img'> <img src='"+data[i].full_picture+"' alt='user' class='img-circle img-bordered-sm'> <span class='profile-status offline pull-right'></span>"+
    " </div>"+
    " <div class='mail-contnet'>"+
    " <h5>"+data[i].page_name+"</h5> <span class='mail-desc'>Follower | "+numberWithCommas(data[i].fan_count)+"</span></div>"+
    "</a>")*/
  }
  

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
function ReactionData(Like_total,Love_total,Wow_total,Sad_total,Angry_total,Haha_total,total_reaction){//alert(fday),alert(sday);
 
       $("#reaction-spin").hide();
     
       var reaction=['Like','Love','HaHa','Sad','Wow','Angry'];
      

       if(total_reaction > 0)
       {
         var reaction_count=[kFormatter(Like_total),kFormatter(Love_total),kFormatter(Haha_total),kFormatter(Sad_total),kFormatter(Wow_total),kFormatter(Angry_total)];
         var like_percentage=parseInt((parseInt(Like_total)/parseInt(total_reaction))*100);
         var love_percentage=parseInt((parseInt(Love_total)/parseInt(total_reaction))*100);
         var haha_percentage=parseInt((parseInt(Haha_total)/parseInt(total_reaction))*100);
         var wow_percentage=parseInt((parseInt(Sad_total)/parseInt(total_reaction))*100);
         var sad_percentage=parseInt((parseInt(Wow_total)/parseInt(total_reaction))*100);
         var angry_percentage=parseInt((parseInt(Angry_total)/parseInt(total_reaction))*100);

          like_percentage = isNaN(like_percentage)?0:like_percentage;
          love_percentage = isNaN(love_percentage)?0:love_percentage;
          haha_percentage = isNaN(haha_percentage)?0:haha_percentage;
          wow_percentage = isNaN(wow_percentage)?0:wow_percentage;
          sad_percentage = isNaN(sad_percentage)?0:sad_percentage;
          angry_percentage = isNaN(angry_percentage)?0:angry_percentage;
     

         /*$('#top-like-progress').css('width', like_percentage+'%').attr('aria-valuenow', like_percentage);
         $("#top-like-value").text(like_percentage + "%");

         $('#top-love-progress').css('width', love_percentage+'%').attr('aria-valuenow', love_percentage);
         $("#top-love-value").text(love_percentage + "%");

         $('#top-haha-progress').css('width', haha_percentage+'%').attr('aria-valuenow', haha_percentage);
         $("#top-haha-value").text(haha_percentage + "%");

         $('#top-wow-progress').css('width', wow_percentage+'%').attr('aria-valuenow', wow_percentage);
         $("#top-wow-value").text(wow_percentage + "%");

         $('#top-sad-progress').css('width', sad_percentage+'%').attr('aria-valuenow', sad_percentage);
         $("#top-sad-value").text(sad_percentage + "%");

         $('#top-angry-progress').css('width', angry_percentage+'%').attr('aria-valuenow', angry_percentage);
         $("#top-angry-value").text(angry_percentage + "%");
*/
         var pieChart = echarts.init(document.getElementById('reaction-pie-chart'));

// specify chart configuration item and data
option = {

    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        x: 'center',
        y: 'bottom',
        data: reaction
    },
    toolbox: {
        show: true,
        feature: {

            dataView: { show: false, readOnly: false },
            magicType: {
                show: true,
                type: ['pie', 'funnel']
            },
            restore: { show: true },
            saveAsImage: { show: true }
        }
    },
    color: colors,
    calculable: true,
    series: [
        {
            name: 'Area mode',
            type: 'pie',
            radius : '30%',
            center: ['50%', '40%'],
            x: '50%', // for funnel
            max: 40, // for funnel
            sort: 'ascending', // for funnel
            data: [
                { value:Like_total, name: reaction[0] },
                { value:Love_total, name:  reaction[1]  },
                { value:Haha_total, name:  reaction[2]  },
                { value:Sad_total, name:  reaction[3]  },
                { value:Wow_total, name: reaction[4]  },
                { value:Angry_total, name:  reaction[5]  },
          
            ],

            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};


// use configuration item and data specified to show chart
pieChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            pieChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});


     }

  
  }
function highlighted(fday,sday)
{
    var chart = echarts.init(document.getElementById('hightlighted'));
    var brand_id = GetURLParameter('pid');
    //var brand_id = 22;
   $( "#hightlighted-spin" ).show();

   //$("#highlighted").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethighlighted')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {
          console.log("test");
        console.log(data);
$( "#hightlighted-spin" ).hide();
     
      if(data.length>0)
      {
     //var data =JSON.parse(data);
     var data = genData(data);


       var option = {
                tooltip: {},
                series: [ {
                    type: 'wordCloud',
                       left:'center',
                        top:'center',
                        width: '90%',
                        height: '90%',
                        right:null,
                        bottom:null,
                    sizeRange: [12, 50],
                    rotationRange: [0,0,0,0],
                    // rotationStep: 90,
                    shape: 'circle',//pentagon ratangle
                    gridSize:20,
                    height: 800,
                    drawOutOfBound: false,
                    textStyle: {
                        normal: {
                            color: function () {
                                var lightcolor = 'rgb(' + [
                                    Math.round(Math.random() * 160),
                                    Math.round(Math.random() * 160),
                                    Math.round(Math.random() * 160),
                                    2
                                ].join(',') + ')';
                                console.log(lightcolor);
                                return lightcolor;
                               /* return colors[0];*/

                            }
                        },
                        emphasis: {
                            shadowBlur: 10,
                            shadowColor: '#333'
                        }
                    },
                    data: data.seriesData
                }
                ]
            };
    function genData(data) {
    var seriesData = [];
    var xData = [];
    var yData = [];
    var i = 0;
     for(var key in data) 
        {
         var name = data[key].topic_name;
         var value=data[key].weight;
         var kvalue=parseFloat(value);
       

          seriesData.push({name: name,value:value});
        
          
        }


  /* jsonString=jsonString.split(["[","]"])*/
     return {
        
        seriesData: seriesData,
        xData:xData,
        yData:yData
        
    };

    
};



          chart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            chart.resize()
        }, 100)
    }
}
);
    chart.on('click', function (params) {
   console.log(params);
   console.log(params.name); // xaxis data =ဈခတျေမီသညျ့ကိုအသုံးပွုထားခွငျး၊
   console.log(params.seriesName); //bar period name ="Positive"
   console.log(params.value);//count
   var pid = GetURLParameter('pid');
  window.open("{{ url('pointoutmention?')}}" +"pid="+ pid+"&highlight_text="+ params.name +"&from_graph=highlighted" , '_blank');
   
   
});

      }
      


    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Dashboard'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}

   function ChooseDate(start, end) {
          $('.dateranges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          startDate=start;
          endDate=end;
        }

$('.dateranges').daterangepicker({
    locale: {
            format: 'MMMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },ChooseDate);
 ChooseDate(startDate,endDate);

hidden_div();
requestmentionData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
/*requestmentionReactData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));*/
requestsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
requestinterestData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
popular_mentions(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
latest_mentions(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
Influencer_profile(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
Influencer_page(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
ReactionData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
/*requestsocialData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));*/
requestsentimentbycategory(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
highlighted(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));

  $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
     requestmentionData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
    /* requestmentionReactData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));*/
      requestsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
      requestinterestData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
      popular_mentions(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
       latest_mentions(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
       Influencer_profile(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
       Influencer_page(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
        ReactionData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
       /*requestsocialData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));*/
       requestsentimentbycategory(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
          highlighted(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
  });

 $(document).on('click', '.edit_predict_latest', function(e) {
     var post_id = $(this).attr('id');
   
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_latest_'+post_id+' option:selected').val();
  var emotion = $('#emotion_latest_'+post_id+' option:selected').val();
 // alert(sentiment + emotion);

         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {//alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });
     
});

$(document).on('click', '.edit_predict_popular', function() {
     var post_id = $(this).attr('id');
  //   alert(post_id);
    
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_popular_'+post_id+' option:selected').val()
  var emotion = $('#emotion_popular_'+post_id+' option:selected').text()
  //alert(post_id);
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {// alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });
});
//local function

function numberWithCommas(n) {
    var parts=n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

  function kFormatter(num) {
    return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
  function readmore(message){
      // alert("hi hi ");
        var string = String(message);
        var length = string.length; 
         // alert(length);
                if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.
             
            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
        }
        return string;


        }

         function judge_sentiment_icon(sentiment)
        {
         var emojis = ['0x1F60E', '0x1F626', '0x1F610'];//
        if (sentiment ==="pos") return  String.fromCodePoint(emojis[0]) ; 
        else if(sentiment ==="neg") return  String.fromCodePoint(emojis[1]);
        else return String.fromCodePoint(emojis[2])  ;
        }

        function judge_emotion_icon(emotion)
        {
            var emojis = ['0x1F620', '0x1F604', '0x1F616', '0x1F628', '0x1F604', '0x1F44D',
            '0x2764', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

        if (emotion ==="anger") return emojis[0]; 
        else if(emotion ==="interest") return emojis[1] ;
         if (emotion ==="disgust") return emojis[2] ; 
        else if(emotion ==="fear") return emojis[3] ;
         if (emotion ==="joy") return emojis[4]; 
        else if(emotion ==="like") return emojis[5] ;
         if (emotion ==="love") return emojis[6] ; 
        else if(emotion ==="neutral") return emojis[7] ;
         if (emotion ==="sadness")  return emojis[8]; 
        else if(emotion ==="surprise") return emojis[9]  ;
         else if(emotion === "trust") return emojis[10];

        }

  });
    </script>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush

