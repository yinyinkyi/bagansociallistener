<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon1.png')}}">
    <title>{{ config('app.name', 'Laravel') }} - @if (isset($title)) {{$title}} @endif</title>
    
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
     <link href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
     <!-- range slider -->
     <link href="{{asset('assets/plugins/ion-rangeslider/css/ion.rangeSlider.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/ion-rangeslider/css/ion.rangeSlider.skinModern.css')}}" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="{{asset('assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="{{asset('assets/plugins/jquery-asColorPicker-master/css/asColorPicker.css')}}" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="{{asset('assets/plugins/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/chartist-js/dist/chartist-init.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/css-chart/css-chart.css')}}" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="{{asset('assets/plugins/morrisjs/morris.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
   
    <!-- You can change the theme colors from here -->
    <link href="{{asset('css/colors/green.css')}}" id="theme" rel="stylesheet">

    <link href="{{asset('assets/plugins/sweetalert2/sweetalert2.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href='https://mmwebfonts.comquas.com/fonts/?font=myanmar3' />
    <link href="{{asset('assets/plugins/select2/dist/css/select2.min.css')}}" id="theme" rel="stylesheet">
      <link href="{{asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet" />
     <link href="{{asset('assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
     <link href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.dataTables.min.css" rel="stylesheet"/>
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css">
.user-profile .profile-text{
    padding-top: 31px;
    position: relative; }

     .user-profile .profile-img::before {
      -webkit-animation: 2.5s blow 0s linear infinite;
      animation: 0s  0s  ;
      position: absolute;
      content: '';
      width: 50px;
      height: 50px;
      top: 35px;
      margin: 0 auto;
      border-radius: 50%;
      z-index: 0; }
      .user-profile .profile-text > a:after {
        position: absolute;
        right: 20px;
        top: 45px; }

button.btn.btn-info.dropdown-toggle.dropdown-toggle-split::after {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: .255em;
    vertical-align: .255em;
    content: "";
    border-top: .3em solid;
    border-right: .3em solid transparent;
    border-bottom: 0;
    border-left: .3em solid transparent;
}
       /* .dropdown-menu {
    position: absolute;
    top: 100%;
    left: 0;
     z-index: 1000; 
    display: none;
    float: left;
    min-width: 10rem;
    padding: .5rem 0;
    margin: .125rem 0 0;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    list-style: none;
    background-color: #383f48;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: .25rem;
}*/


/*    .label-light-success {
    background-color: #e8fdeb;
    color: #fcb22e;
}*/

.search { position: relative; }
.search input { text-indent: 135px;}
.search .text { 
    font-size: 1rem;
  position: absolute;
  top: 9px;
  left: 7px;
  font-size: 15px;
}
</style>
</head>

<body class="fix-header fix-sidebar card-no-border" >
<button type="button" id="print" class="btn waves-effect waves-light btn-block btn-info noprint">Print</button>
                <div class="coverpage print">
                <div class="innerDiv">
                <img src="{{asset('Logo/companyLogo.png')}}" style="width:100px;height:100px"/>
                <div class="cover_title">
                @if(isset($companyData)) {{$companyData['name'] }} @endif REPORT
                </div>
                <div class="cover_subtitle">
                period : <span>2018-11-10 - 2018-12-10 </span>
                </div>
                </div>
                </div>
                
                <div class="page">
                    <div class="row header_row">
                            <div class="col-lg-12 header_title"  style="width:100%;text-align:center">
                            
                                    @if(isset($companyData)) {{$companyData['name'] }} @endif REPORT
                        
                            </div>
                            <div class="col-lg-12 header_subtitle" style="width:100%;text-align:center">
                        
                            period : <span>2018-11-10 - 2018-12-10 </span>
                        
                            </div>
                     </div>
                <div class="row contentDiv" >
                  
                    <div class="col-lg-12">
                            <div class="card">
                        
                                <div class="card-body">
                                    <h4 class="card-title graph-title">Fan Growth</h4>
                                    <div style="display:none"  align="center" style="vertical-align: top;" id="dashboardfan-growth-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                <div class="noprint" id="dashboardfan-growth-chart" style="width:100%;height:350px"></div>  
                                <div class="print" id="dashboardfan-growth-chart-print" style="width:297mm;height:500px"></div>  
                                </div>
                            </div>
                    </div>
                </div>
                <div class="row contentDiv" style="padding-top:50px">
                        <div class="col-lg-12">
                            <!-- Column -->
                            <div class="card earning-widget" style="height:500px">
                                <div class="card-header">
                                  <h4 class="card-title graph-title">Most Frequently Tags</h4>
                                </div>
                                <div class="card-body b-t collapse show">
                                    <table id="tbl_tag_count" class="table v-middle no-border">
                                        <tbody>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
              
                </div>
                <div class="page">
                    <div class="row header_row">
                            <div class="col-lg-12 header_title"  style="width:100%;text-align:center">
                            
                                    @if(isset($companyData)) {{$companyData['name'] }} @endif REPORT
                        
                            </div>
                            <div class="col-lg-12 header_subtitle" style="width:100%;text-align:center">
                        
                            period : <span>2018-11-10 - 2018-12-10 </span>
                        
                            </div>
                     </div>
                        <div class="row contentDiv" >
                        <div class="col-lg-12">
                        <div class="card">
                      
                            <div class="card-body">
                            <h4 class="card-title graph-title">Total Reach</h4>
                                <div style="display:none"  align="center" id="reach-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                     <div id="total-reach-chart" style="width:100%; height:150px"></div>
                                 </div>
                        </div>
                    </div>
                    </div>
                    <div class="row contentDiv" style="padding-top:80px">
                    <div class="col-lg-12">
                        <div class="card">
                       
                            <div class="card-body" style="padding:0.5rem !important">
                            <h4 class="card-title graph-title">Number of Posts</h4>
                                             <div style="display:none"  align="center" id="post_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                    <div class="card-body text-center " style="padding:0.5rem !important">
                                        <h1 class="card-title m-t-10" style="font-size:65px" id="post_total">-</h1>
                                        
                                    </div>
                                    <div class="card-body text-center ">
                                      
                                        <ul class="list-inline m-b-0">
                                            <li>
                                                <h6 id="post_neg"class="text-red" style="font-size:20px">-%</h6>
                                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnPost" value="neg">Negative</button></div> </li>
                                           <li>
                                                 <h6 id="post_neutral" class="text-warning" style="font-size:20px">-%</h6>
                                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnPost" value="neutral">Neutral</button></div> </li>
                                            <li>
                                                 <h6 id="post_pos" class="text-success" style="font-size:20px">-%</h6>
                                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnPost" value="pos">Positive</button></div> </li>
                                        </ul>
                                    </div>
                                </div>
                        </div>
                    </div>
                  </div>
                  <div class="row contentDiv" style="padding-top:80px">
                    <div class="col-lg-12">
                        <div class="card">
                                
                            <div class="card-body" style="padding:0.5rem !important">
                            <h4 class="card-title graph-title">Number Of Comments</h4>
                                             <div style="display:none"  align="center" id="cmt_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                    <div class="card-body text-center " style="padding:0.5rem !important">
                                        <h1 class="card-title m-t-10" style="font-size:65px" id="cmt_total">-</h1>
                                        
                                    </div>
                                    <div class="card-body text-center ">
                                      
                                        <ul class="list-inline m-b-0">
                                            <li>
                                                <h6 id="cmt_neg"class="text-red" style="font-size:20px">-%</h6>
                                                <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnComment" value="neg">Negative</button></div> </li>
                                            <li>
                                                 <h6 id="cmt_neutral" class="text-warning" style="font-size:20px">-%</h6>
                                                <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnComment" value="neutral">Neutral</button></div> </li>
                                            <li>
                                                 <h6 id="cmt_pos" class="text-success" style="font-size:20px">-%</h6>
                                                <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnComment" value="pos">Positive</button></div> </li>
                                        </ul>
                                    </div>
                                </div>
                        </div>
                    </div>
                         </div>
              
                </div>
                <div class="page">
                    <div class="row header_row">
                            <div class="col-lg-12 header_title"  style="width:100%;text-align:center">
                            
                                    @if(isset($companyData)) {{$companyData['name'] }} @endif REPORT
                        
                            </div>
                            <div class="col-lg-12 header_subtitle" style="width:100%;text-align:center">
                        
                            period : <span>2018-11-10 - 2018-12-10 </span>
                        
                            </div>
                     </div>
                <div class="row contentDiv" >
                  
                    <div class="col-lg-12">
                            <div class="card">
                        
                                <div class="card-body">
                                    <h4 class="card-title graph-title">Top 5: Fan's City</h4>
                                    <div style="display:none"  align="center" id="city_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                    <table id="tbl_city_reach" class="table v-middle no-border">
                                        <tbody>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="row contentDiv" style="padding-top:50px">
                        <div class="col-lg-12">
                            <!-- Column -->
                            <div class="card earning-widget" style="height:500px">
                                <div class="card-header">
                                  <h4 class="card-title graph-title">People Reach</h4>
                                </div>
                                <div class="card-body">
                                <div style="display:none"  align="center" style="vertical-align: top;" id="reach-gender-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                 <div style="width:100%; height:500px;">
                                <div style="width:100%;">
                                <div id="div_legend_Women" style="width:20%; height:200px;float:left;padding-left:20px;padding-top:50px">
                                    Women<br><span class="fa fa-square m-r-10" style="color:#1e88e5"> </span><span id="pcent_legend_Women" style="font-weight:bold"> 0</span>%<br><span style="font-size:12px">People Reached</span></div>
                                <div id="reach-age-gender-chart" style="width:80%; height:200px;float:right;padding-top:10px"></div>
                                </div>
                                <div style="width:100%;">
                                <div id="div_legend_Men" style="width:20%; height:200px;float:left;vertical-align: middle;padding-left:20px;">
                                    Men<br><span class="fa fa-square m-r-10" style="color:#4da1f0"> </span><span id="pcent_legend_Men" style="font-weight:bold"> 0</span>%<br> <span style="font-size:12px">People Reached</span></div>
                                <div id="reach-age-gender-chart-1" style="width:80%; height:200px;float:right"></div>
                                </div>
                                </div>
                                </div>
                            </div>
                    </div>
                </div>
              
                </div>
                <div class="page">
                    <div class="row header_row">
                            <div class="col-lg-12 header_title"  style="width:100%;text-align:center">
                            
                                    @if(isset($companyData)) {{$companyData['name'] }} @endif REPORT
                        
                            </div>
                            <div class="col-lg-12 header_subtitle" style="width:100%;text-align:center">
                        
                            period : <span>2018-11-10 - 2018-12-10 </span>
                        
                            </div>
                     </div>
                <div class="row contentDiv" >
                  
                    <div class="col-lg-12">
                            <div class="card">
                        
                                <div class="card-body">
                                    <h4 class="card-title graph-title">Page Fan Vs Online Fan</h4>
                                    <div style="display:none"  align="center" style="vertical-align: top;" id="fan-page-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div id="fan-page-chart" style="width:100%;height:500px"></div> 
                                </div>
                            </div>
                    </div>
                </div>
                <div class="row contentDiv" style="padding-top:50px">
                        <div class="col-lg-12">
                            <!-- Column -->
                            <div class="card">
                                
                                <div class="card-body">
                                <h4 class="card-title graph-title">Tag Sentiment</h4>
                                <div style="display:none"  align="center" style="vertical-align: top;" id="tag-senti-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div id="tag-senti-chart" style="width:100%;height:500px"></div>
                                </div>
                            </div>
                    </div>
                </div>
              
                </div>

                <!-- Comparison -->
                <div class="page">
                    <div class="row header_row">
                            <div class="col-lg-12 header_title"  style="width:100%;text-align:center">

                                    @if(isset($companyData)) {{$companyData['name'] }} @endif REPORT
                        
                            </div>
                            <div class="col-lg-12 header_subtitle" style="width:100%;text-align:center">
                        
                            period : <span>2018-11-10 - 2018-12-10 </span>
                        
                            </div>
                     </div>
                <div class="row contentDiv" >
                  
                    <div class="col-lg-12">
                        <div class="card">
                     
                            <div class="card-body">
                                <h4 class="card-title graph-title">Fan Growth</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="fan-growth-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div class="noprint" id="fan-growth-chart" style="width:100%;height:350px"></div>  
                              <div class="print" id="fan-growth-chart-print" style="width:297mm;height:500px"></div>  
                            </div>
                        </div>
                    </div>
                    </div>
                <div class="row contentDiv" style="padding-top:50px">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title graph-title">Page Summary</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="summary_chart_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div class="noprint" id="page-summary-chart" style="width:100%;height:350px"></div>  
                              <div class="print" id="page-summary-chart-print" style="width:297mm;height:500px"></div>  
                            </div>
                        </div>
                    </div>
                
                </div>
                </div>
                <div class="page">
                <div class="row header_row">
                            <div class="col-lg-12 header_title"  style="width:100%;text-align:center">
                        
                                    @if(isset($companyData)) {{$companyData['name'] }} @endif REPORT
                        
                            </div>
                            <div class="col-lg-12 header_subtitle" style="width:100%;text-align:center">
                        
                            period : <span>2018-11-10 - 2018-12-10 </span>
                        
                            </div>
                     </div>
                 <div class="row contentDiv">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title graph-title">Post Sentiment</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="post_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div class="noprint" id="post-sentiment-chart" style="width:100%;height:350px"></div>  
                              <div class="print" id="post-sentiment-chart-print" style="width:297mm;height:500px"></div>  
                            </div>
                        </div>
                    </div>
                
                </div>
                <div class="row contentDiv" style="padding-top:50px">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title graph-title">Comment Sentiment</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="comment_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div class="noprint" id="comment-sentiment-chart" style="width:100%;height:350px"></div>
                              <div class="print" id="comment-sentiment-chart-print" style="width:297mm;height:500px"></div>  
                            </div>
                        </div>
                    </div>
                
                </div>
                </div>
            <!--        <div class="row" id="page-div-2">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Total Reach</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="reach-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div id="reach-chart" style="width:100%; height:400px;"></div>   
                            </div>
                        </div>
                    </div>
                
                </div> -->
                <div class="page">
                <div class="row header_row">
                            <div class="col-lg-12 header_title"  style="width:100%;text-align:center">
                        
                                    @if(isset($companyData)) {{$companyData['name'] }} @endif REPORT
                        
                            </div>
                            <div class="col-lg-12 header_subtitle" style="width:100%;text-align:center">
                        
                            period : <span>2018-11-10 - 2018-12-10 </span>
                        
                            </div>
                     </div>
              <div class="row contentDiv">

              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                          <div style="display:none"  align="center" style="vertical-align: top;" id="reaction-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             <h4 class="card-title graph-title">Social Reaction</h4>
                              <div id="div_reaction">
                                   <!--  <table  class="table table-bordered">
                                        <thead>
                                            <tr id="tr_header">
                                                <th>Reaction</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="tr_like">
                                               <td><a href="javascript:void(0)">Like</td>
                                              
                                                
                                            </tr>
                                            <tr id="tr_love">
                                                <td><a href="javascript:void(0)">Love</a></td>
                                           
                                              
                                            </tr>
                                            <tr id="tr_wow">
                                                <td><a href="javascript:void(0)">/a></td>
                                           
                                            </tr>
                                            <tr id="tr_haha">
                                                <td><a href="javascript:void(0)">HA HA</a></td>
                                                
                                            </tr>
                                            <tr id="tr_sad">
                                                <td><a href="javascript:void(0)">Sad</a></td>
                                               
                                            </tr>
                                            <tr id="tr_angry">
                                                <td><a href="javascript:void(0)">Angry</a></td>
                                               
                                            </tr>
                                             <tr id="tr_share">
                                                <td><a href="javascript:void(0)">Share</a></td>
                                              
                                            </tr>
                                        </tbody>
                                    </table> -->
                                </div>
                           
                            </div>
                        </div>
                    </div>
               
                </div>
                </div>
              <!--    <div class="row" id='comparison-div-4'>
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                          <div style="display:none"  align="center" style="vertical-align: top;" id="summary-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             <h4 class="card-title">Page Summary</h4>
                              <div id="div_page_summary" class="table-responsive">
                                 
                                </div>
                           
                            </div>
                        </div>
                    </div>
               
                </div> -->
         <!--          <div class="row" id='comparison-div-4'>
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                          <div style="display:none"  align="center" style="vertical-align: top;" id="ranking-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             <h4 class="card-title">Page Ranking</h4>
                              <div id="div_page_ranking" class="table-responsive">
                                 
                              </div>
                           
                            </div>
                        </div>
                    </div>
               
                </div> -->

<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>
   <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script> -->
   <!--  <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script> -->
 
    
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   
   <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript')}}" defer ></script>
    <script src="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript')}}" defer></script>
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
    
    <script type="text/javascript">
var startDate;
var endDate;
var labelDate;
var colors=[ "#4267b2","#382448","#01ad9d","#cb73a9","#a1c652","#7b858e","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];
var colors_senti=["#28a745","#fb3a3a","#ffb22b"];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/
$(document).ready(function() {
  //generate legend data
 
  Clear_reaction_table();
  //Clear_page_summary_table();
 // Clear_page_ranking_table();
var compare_count=0;
var Legend_Data=[];var brand_id_arr=[];var reachseriesList=[];var reachLabel = [];var arr_reach_total=[];var arr_reach_label=[];
var fanseriesList=[];var fanLabel = [];var arr_fan_total=[];var arr_fan_label=[];var fan_abs_diff=[];// var min_fan_array=0;
var  post=[];var share=[];var comment=[];var summaryLabel=[];
var  arr_ov_positive=[];var arr_ov_negative=[];var arr_ov_neutral=[];var OverallLabel=[];
var  arr_cmt_positive=[];var arr_cmt_negative=[];var arr_cmt_neutral=[];var CommentLabel=[];
var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_social_total=[];
var Senti_xAxisData=[];var Senti_data1=[];var Senti_data2=[]; var Senti_seriesList=[];var Senti_pos_total=0;
var Senti_neg_total=0;
var arr_post=[];var arr_reaction=[];var arr_comment=[]; var arr_shared=[];var arr_ov_pos=[];
var arr_ov_neg=[];



    startDate = moment().subtract(1, 'month');
    endDate = moment();
    var periodType= '';
       
            if(periodType == '')
            {
              periodType='month';
            }
                  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};



function Bind_Pages()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#brand_id").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getallpages')}}", // This is the URL to the API
      data: { brand_id:brand_id}
    })
    .done(function( data ) {//   <option value="">Choose</option>
           $('#brand_id').append($('<option>', {
            value: '',
            text: 'Choose'
        }));
     for(var i in data) {

      $('#brand_id').append($('<option>', {
            value: data[i],
            text: data[i]
        }));
        if(i < 5 )
        {
            brand_id_arr.push(data[i]);
            Legend_Data.push(data[i]);
        }
       
     }


     ChooseDate(GetURLParameter('fday'),GetURLParameter('sday'),data[0],0,labelDate);
    
     
    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
Bind_Pages();
 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Comparison'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
hidden_div();
function ChooseDate(start,end,brand_id,sr_of_brand,label)
{
//    $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
          startDate=start;
          endDate=end;
         var brand_id = brand_id;
         var split_name = brand_id.split("-");
          if(split_name.length > 0)
          {
            if(isNaN(split_name[split_name.length-1]) == false )
              brand_id = split_name[split_name.length-1];
          }
         // alert(brand_id);
         var date_preset='this_month'; 
         var start = new Date(startDate);
         var end = new Date(endDate);
         var current_Date = new Date();
         var current_year =new Date().getFullYear();
         var timeDiff = Math.abs(current_Date.getTime() - start.getTime());
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        if(label === 'Today' )  date_preset = 'today';
        else if (label === 'Yesterday')  date_preset = 'yesterday';
        else if (label === 'Last 7 Days' || diffDays <=7)  date_preset = 'last_7d';
        else if (label === 'Last 30 Days' ||  diffDays <=30 )  date_preset = 'last_30d';
        else if (label === 'This Quarter')  date_preset = 'this_quarter';
        else if (label === 'Last 90 Days' || diffDays <=90)  date_preset = 'last_90d';
        else if (label === 'This Year')  date_preset = 'this_year';
        else if (label === 'Last Year')  date_preset = 'last_year';
        else if (diffDays >90 && current_year === end.getFullYear() ) date_preset = 'this_year';

        socialdetail(brand_id,sr_of_brand);
       // requestsentimentbycategory(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand)
       // TotalReach(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand,date_preset);
     //   appendtoSummarytable(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
        FanGrowth(brand_id,sr_of_brand,date_preset);
       // appendtoRankingtable(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
       PageSummary(brand_id,sr_of_brand);
       PostSentiment(brand_id,sr_of_brand);
       CommentSentiment(brand_id,sr_of_brand);
       FanPage(brand_id,date_preset);
       TotalReach(brand_id,date_preset);
       posting_status(brand_id);
       CityReach(brand_id);
       GenderReachData(brand_id);
       tagsentimentData(brand_id);
       tagCount(brand_id);
}
function PostSentiment(brand_id,post_sr_of_brand)
    {
      var PostSentiChart = echarts.init(document.getElementById('post-sentiment-chart'));
      var PostSentiChart_print = echarts.init(document.getElementById('post-sentiment-chart-print'));
      $('#post_spin').show();
      $("#fan-growth-spin").show();
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: GetURLParameter('fday'),sday:GetURLParameter('sday'),brand_id:GetURLParameter('pid'),filter_page_name:brand_id}
    })
    .done(function(data) {
       
                var ov_total=parseInt(data[2][0]['ov_positive'])+parseInt(data[2][0]['ov_negative'])+parseInt(data[2][0]['ov_neutral'])
                var pcent_positive=parseFloat((parseInt(data[2][0]['ov_positive'])/ov_total)*100).toFixed(2);
                pcent_positive = isNaN(pcent_positive)?0:pcent_positive;
                arr_ov_positive.push(pcent_positive);
                var pcent_negative=parseFloat((parseInt(data[2][0]['ov_negative'])/ov_total)*100).toFixed(2);
                pcent_negative = isNaN(pcent_negative)?0:pcent_negative;
                arr_ov_negative.push(pcent_negative);
                var pcent_neutrual=parseFloat((parseInt(data[2][0]['ov_neutral'])/ov_total)*100).toFixed(2);
                pcent_neutrual = isNaN(pcent_neutrual)?0:pcent_neutrual;
                arr_ov_neutral.push(pcent_neutrual);
                 
                if(jQuery.inArray(Legend_Data[post_sr_of_brand], OverallLabel)=='-1')
                OverallLabel.push(Legend_Data[post_sr_of_brand]);

        option= null;
option = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    grid: {
      left: '3%',
      right: '12%',
      bottom: '3%',
      containLabel: true
    },
    xAxis:  {
      type: 'value',
      name:'Senti Percent',
       axisLabel: {
            formatter: '{value}%'
        },
        max:100
    },
    yAxis: {
      type: 'category', 
      name:'Page Name',
      data:OverallLabel,

},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_neutral
}
]
};
 
    $( "#post_spin" ).hide();
    
    PostSentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            PostSentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

     PostSentiChart_print.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            PostSentiChart_print.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

     var arrayLength=brand_id_arr.length;
     post_sr_of_brand = post_sr_of_brand +1;
     if(post_sr_of_brand<arrayLength )
     {
      PostSentiment(brand_id_arr[post_sr_of_brand],post_sr_of_brand);
      $("#fan-growth-spin").show();

     }
     else
     {
       $("#fan-growth-spin").hide();
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function CommentSentiment(brand_id,comment_sr_of_brand)
    {
      var CommentSentiChart = echarts.init(document.getElementById('comment-sentiment-chart'));
      var CommentSentiChart_print = echarts.init(document.getElementById('comment-sentiment-chart-print'));
      $('#comment_spin').show();
      $("#fan-growth-spin").show();
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: GetURLParameter('fday'),sday:GetURLParameter('sday'),brand_id:GetURLParameter('pid'),filter_page_name:brand_id}
    })
    .done(function(data) {
       
                var comment_total=parseInt(data[1][0]['cmt_positive'])+parseInt(data[1][0]['cmt_negative'])+parseInt(data[1][0]['cmt_neutral'])+parseInt(data[1][0]['cmt_NA']);
                 // alert(comment_total);
                var neutralTotal=parseInt(data[1][0]['cmt_neutral'])+parseInt(data[1][0]['cmt_NA']);
                var pcent_positive=parseFloat((parseInt(data[1][0]['cmt_positive'])/comment_total)*100).toFixed(2);
                pcent_positive = isNaN(pcent_positive)?0:pcent_positive;
                arr_cmt_positive.push(pcent_positive);
                var pcent_negative=parseFloat((parseInt(data[1][0]['cmt_negative'])/comment_total)*100).toFixed(2);
                pcent_negative = isNaN(pcent_negative)?0:pcent_negative;
                arr_cmt_negative.push(pcent_negative);
                var pcent_neutrual=parseFloat((neutralTotal/comment_total)*100).toFixed(2);
                pcent_neutrual = isNaN(pcent_neutrual)?0:pcent_neutrual;
                arr_cmt_neutral.push(pcent_neutrual);
                 
                if(jQuery.inArray(Legend_Data[comment_sr_of_brand], CommentLabel)=='-1')
                CommentLabel.push(Legend_Data[comment_sr_of_brand]);

        option= null;
option = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    grid: {
      left: '3%',
      right: '12%',
      bottom: '3%',
      containLabel: true
    },
    xAxis:  {
      type: 'value',
      name:'Senti Percent',
       axisLabel: {
            formatter: '{value}%'
        },
        max:100
    },
    yAxis: {
      type: 'category', 
      name:'Page Name',
      data:CommentLabel
},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_neutral
}
]
};
 
  $( "#comment_spin" ).hide();

    CommentSentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            CommentSentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
    CommentSentiChart_print.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            CommentSentiChart_print.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

      var arrayLength=brand_id_arr.length;
     comment_sr_of_brand = comment_sr_of_brand +1;
     if(comment_sr_of_brand<arrayLength )
     {
      CommentSentiment(brand_id_arr[comment_sr_of_brand],comment_sr_of_brand);
      $("#fan-growth-spin").show();
     }
     else
     {
      $("#fan-growth-spin").hide();
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function PageSummary(brand_id,summary_sr_of_brand)
    {
      var SummaryChart = echarts.init(document.getElementById('page-summary-chart'));
      var SummaryChart_print = echarts.init(document.getElementById('page-summary-chart-print'));
      $('#summary_chart_spin').show();
      $("#fan-growth-spin").show();
     
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: GetURLParameter('fday'),sday:GetURLParameter('sday'),brand_id:GetURLParameter('pid'),filter_page_name:brand_id}
    })
    .done(function(data) {
       
        
      
                post.push(data[0][0]['total_post']);
                share.push(data[0][0]['shared']);
                comment.push(data[1][0]['total_comment']);
          
                if(jQuery.inArray(Legend_Data[summary_sr_of_brand], summaryLabel)=='-1')
                  summaryLabel.push(Legend_Data[summary_sr_of_brand]);

        option= null;
option = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross'
        },
      
    },
          grid: {
          top:    60,
    
    left:   '5%',
    right:  '10%',
    bottom:  '5%',
            containLabel: true
        },
         legend: {
        data:['post','comment','share'],
      
   
         padding :0,
       
        
        },

     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

    xAxis: [
        {
        
            type: 'category',
            name: 'Page Name',
             boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },
        
            data: summaryLabel
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Count',
            scale:true,
            position: 'left',
         
                    axisLabel: {
        formatter: function (e) {
            return kFormatter(e,1);
        }
    }
    
     }
    ],
    series: [{
            name:'post',
            type:'bar',
            smooth: 0.2,
            color:colors[0],
            barMaxWidth:30,
            barMinHeight:2,
            data:post
          
        },{
            name:'comment',
            type:'bar',
            smooth: 0.2,
            color:colors[1],
            barMaxWidth:30,
            barMinHeight:2,
            data:comment
          
        },{
            name:'share',
            type:'bar',
            smooth: 0.2,
            color:colors[2],
            barMaxWidth:30,
            barMinHeight:2,
            data:share
          
        }]
};
 
    $( "#summary_chart_spin" ).hide();
   
    SummaryChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            SummaryChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
     SummaryChart_print.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            SummaryChart_print.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
    
      var arrayLength=brand_id_arr.length;
     summary_sr_of_brand = summary_sr_of_brand +1;
     if(summary_sr_of_brand<arrayLength )
     {
      PageSummary(brand_id_arr[summary_sr_of_brand],summary_sr_of_brand);
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function FanGrowth(page_name,fan_sr_of_brand,date_preset)
{
    var FanGrowthChart = echarts.init(document.getElementById('fan-growth-chart'));
    var FanGrowthChart_print = echarts.init(document.getElementById('fan-growth-chart-print'));

      $('#fan-growth-spin').show();
    var brand_id = GetURLParameter('pid');
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getPageFanDif')}}", // This is the URL to the API
      data: { date_preset: date_preset,page_name:page_name,fday:GetURLParameter('fday'),sday:GetURLParameter('sday'),brand_id:brand_id}
    })
    .done(function( data ) {//alert(data.length);

     var fan=[];
     var fan_abs=[];
       var fan_diff=[];
       var fan_temp_diff=[];
       var fan_total=0;
       var new_date_arr=[];
       // var fanLabel=[];
  
 // console.log('pagediff');
 //          console.log(data);
         for(var i in data) {//alert(data[i].mentions);

          if(parseInt(i) !== 0)
        {
                fan.push(data[i].fan);
                fan_abs.push(data[i].fan_abs_diff);
                fan_temp_diff.push(data[i].fan_diff);
                if(jQuery.inArray(data[i].end_time, fanLabel)=='-1')
                fanLabel.push(data[i].end_time);
                 
                new_date_arr.push(data[i].end_time);
                
       }
    
        
      }
      fanLabel.sort(function(a, b){
    var dateA=new Date(a), dateB=new Date(b)
    return dateA-dateB //sort by date ascending
})
       for(var j in fanLabel) {//alert(fanLabel[j]);
        var found =jQuery.inArray(fanLabel[j], new_date_arr);
      //  alert(found + fanLabel[j]);
        if(found=='-1')
          fan_diff.push('');
        else
          fan_diff.push(fan_temp_diff[found]);
       }
      // console.log(new_date_arr);
      // console.log(fan_temp_diff);
    //   console.log(fan_diff);
    //   console.log(fanLabel);
     //  if(min_fan_array == 0)
     //  {
     //   min_fan_array = Math.min.apply(Math, fan);
     //   min_fan_array = min_fan_array-10000;
     //  }
     //  else  if(Math.min.apply(Math, fan)<min_fan_array)
     // {
     //   min_fan_array = Math.min.apply(Math, fan);
     //   min_fan_array = min_fan_array-10000;
     //  }
     
      // console.log(fanLabel);
      $.each(fan,function(){fan_total+= parseInt(this) || 0;});
      arr_fan_label.push(Legend_Data[fan_sr_of_brand]+' : '+ formatNumber(fan_total));
      arr_fan_total.push(fan_total);
      fan_abs_diff.push(fan_abs);
      fanseriesList.push({
            name:Legend_Data[fan_sr_of_brand],
            type:'line',
            smooth: 0.2,
            color:colors[fan_sr_of_brand],
            barMinHeight:2,
            data:fan_diff
          
        },

                           );

      // console.log("fanseriesList");
      // console.log(fanseriesList);
      // console.log(fan_abs_diff);

        option_growth= null;

option_growth = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        axisPointer: {
           // type: 'cross'
        },
        formatter: function (params) {

        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        // let rez = '<p>' + params[0].name + '</p>';
        let rez = '';
        //console.log("test");
        //quite useful for debug
        params.forEach(item => {
            
            //  console.log(item);
            //  console.log(item.seriesIndex);
            //  console.log(item.dataIndex);
          var diff_amount = 0;
           var item_data=item.data==='-'?'-':formatNumber(item.data);
           // var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName +'<br>' + 'Fun Growth: ' + fan_abs_diff[item.dataIndex]  + '</p>'
           if(typeof fan_abs_diff[item.seriesIndex] !== 'undefined')
           {
            diff_amount = fan_abs_diff[item.seriesIndex][item.dataIndex];
           }
        var xx = '<p>'+params[0].name+'<br><span >' +item.seriesName +': '+item_data+'</span><br></p>'
              // var xx = '<p>'+params[0].name+'<br><span >' +item.seriesName +': '+item_data+'</span><br>' + 'Fun Growth: ' + diff_amount  + '</p>'

              // var xx = '<p>'+params[0].name+'<br><span >' +item.seriesName +': '+item_data+'</span></p>'
              rez += xx;
       
         }
           
        );

        return rez;
    }
    },
          grid: {
          top:    60,
    
    left:   '5%',
    right:  '10%',
    bottom:  '5%',
            containLabel: true
        },
         legend: {
        data:Legend_Data,
        // formatter: '{name}: '+ formatNumber(arr_reach_total[sr_of_brand]),
   
         padding :0,
       
        
        },

     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

    xAxis: [
        {
        
            type: 'category',
            name : 'Day',
             boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },
                  axisLabel: {
                      
      formatter: function (value, index) {
       const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

     var date = new Date(value);
       var xx= date.getDate() + '\n' + monthNames[date.getMonth()];
       return xx;

},

    },
            data: fanLabel
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Fan Count',
            scale:true,
            // max: 250,
            position: 'left',
            // axisLine: {
            //     lineStyle: {
            //         color: colors[0]
            //     }
            // }
                    axisLabel: {
        formatter: function (e) {
            return formatNumber(e);
        }
    }
    
     }
    ],
    series: fanseriesList
};
//$("#fan-growth-spin").hide();

 FanGrowthChart.setOption(option_growth, true), $(function() {
    function resize() {
        setTimeout(function() {
            FanGrowthChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
 FanGrowthChart_print.setOption(option_growth, true), $(function() {
    function resize() {
        setTimeout(function() {
            FanGrowthChart_print.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

     var arrayLength=brand_id_arr.length;
     fan_sr_of_brand = fan_sr_of_brand +1;
     if(fan_sr_of_brand<arrayLength )
     {
      FanGrowth(brand_id_arr[fan_sr_of_brand],fan_sr_of_brand,date_preset);
     }
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in FAN API" );
            });
}

        function socialdetail(brand_id,social_sr_of_brand)
    {//alert(keyword);
     /* var socialchart = document.getElementById('social-chart');
      var socialChart = echarts.init(socialchart);*/

      // $("#social-spin").show();
      
      $("#fan-growth-spin").show();
      $("#reaction-spin").show();
      
     
        //var brand_id = 22;
       /* alert (brand_id);
       alert(periodType);*/
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getinboundReaction')}}", // This is the URL to the API
      data: { fday: GetURLParameter('fday'),sday:GetURLParameter('sday'),brand_id:GetURLParameter('pid'),filter_page_name:brand_id,periodType:'month'}
    })
    .done(function(data) {//alert(data);alert("hhi");
//var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_mention_total=[];

       var socials = [];
       var social_total=0; var like_total=0; var love_total=0; var wow_total=0; var haha_total=0; var sad_total=0; var angry_total=0; 
       var share_total=0;var post_total=0;



       for(var i in data) 
       {
   
        var mediaReach=parseInt(data[i].Like)+parseInt(data[i].Love)+parseInt(data[i].Haha)+parseInt(data[i].Wow)+parseInt(data[i].Angry)+parseInt(data[i].Sad)+parseInt(data[i].shared)+parseInt(data[i].post_count)//add share
         socials.push(Math.round(mediaReach));
         if(jQuery.inArray(data[i].periodLabel, socialLabel)=='-1')
         socialLabel.push(data[i].periodLabel);
         like_total+=parseInt(data[i].Like);love_total+=parseInt(data[i].Love);wow_total+=parseInt(data[i].Wow);
         haha_total+=parseInt(data[i].Haha);sad_total+=parseInt(data[i].Sad);angry_total+=parseInt(data[i].Angry);
         share_total+=parseInt(data[i].shared);
         //console.log(data[i].post_count);
         post_total+=parseInt(data[i].post_count);

       }

      
       appendtoReactiontable(Legend_Data[social_sr_of_brand],like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,social_sr_of_brand,post_total);
     var arrayLength=brand_id_arr.length;
     social_sr_of_brand = social_sr_of_brand +1;
     if(social_sr_of_brand<arrayLength )
     {
      socialdetail(brand_id_arr[social_sr_of_brand],social_sr_of_brand);
     }
       $("#reaction-spin").hide();
       


 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}


// $('.dateranges').daterangepicker({
//     locale: {
//             format: 'MMM D, YYYY'
//         },
//             ranges: {
//                 'Today': [moment(), moment()],
//                 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
//                 'Last 7 Days': [moment().subtract(6, 'days'), moment()],
//                 'Last 30 Days': [moment().subtract(29, 'days'), moment()],
//                 'This Month': [moment().startOf('month'), moment().endOf('month')],
//                 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
//             },
//               startDate: startDate,
//               endDate: endDate,

//         },function(start, end,label) {//alert(label);
//         var startDate;
//         var endDate;
//         var labelDate;
//         startDate = start;
//         endDate = end;
//         labelDate = label;
//         reachseriesList=[];reachLabel = [];arr_reach_total=[];arr_reach_label=[];
//         fanseriesList=[]; fanLabel = []; arr_fan_total=[]; arr_fan_label=[]; fan_abs_diff=[];
        
//         post=[]; share=[]; comment=[]; summaryLabel=[];
//         arr_ov_positive=[]; arr_ov_negative=[]; arr_ov_neutral=[]; OverallLabel=[];
//         arr_cmt_positive=[]; arr_cmt_negative=[]; arr_cmt_neutral=[]; CommentLabel=[];
//         Social_seriesList=[]; socialLabel = [];arr_social_total=[];
//         Senti_data1=[];Senti_data2=[];Senti_xAxisData=[];
  
//         Clear_reaction_table();
//         //Clear_page_summary_table();
//         //Clear_page_ranking_table();
//         compare_count=0;

//         refreshGraph(startDate,endDate,labelDate);
//       });


// function refreshGraph(startDate,endDate,labelDate)
// {//alert(brand_id_arr);
//   // var arrayLength=brand_id_arr.length;
//   // if(arrayLength >1)
//   // {
//   // for (var i = 0; i < arrayLength; i++) {
//   //   compare_count=i;
//   //   // alert(Legend_Data[compare_count]);
//   //   ChooseDate(startDate,endDate,brand_id_arr[i],compare_count,labelDate);
//   // }
    
//   // }
//   // else
//   // {
//     ChooseDate(startDate,endDate,brand_id_arr[0],0,labelDate);
//   //}
   

// }




   

// $("#btn_add_project").click(function(){
// var arrayLength=brand_id_arr.length;
//   if(arrayLength >0)
//   {
//     for (var i = 0; i < arrayLength; i++) {
//            var brand_id= brand_id_arr[i];
//     //Do something
//     // $('#'+brand_id).prop('disabled', true);
//      $('#brand_id').children('option[value="'+brand_id+'"]').css('display','none');
// }   
//    /* $("select#brand_id").val(''); */
//    $("select#brand_id").prop('selectedIndex', 0);
//    $('#add-project').modal('show'); 
//   }
  
// });

// $("#btn_add_brand").click(function(){
//    $('#add-project').modal('hide'); 
//   var selected_text=$( "#brand_id option:selected" ).text();
//   var selected_value=$( "#brand_id option:selected" ).val();
//  // alert(selected_value);
//   if(selected_value !== '')
//   {
//     compare_count=compare_count+1;
//     Legend_Data.push(selected_value);
//     brand_id_arr.push(selected_value);
//     ChooseDate(startDate,endDate,selected_value,compare_count,labelDate);

//   }
 
  
//   //add to legend array

// });



function appendtoReactiontable(brand_name,like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,sr_no,total_mention)
{
    // total_mention=0;
  $("#tr_header").append("<th style='color:"+colors[sr_no]+"'><div>"+brand_name+"</div><div>Post: "+total_mention+"</div></th>");
  $("#tr_like").append("<td>"+kFormatter(like_total)+"</td>");
  $("#tr_love").append("<td>"+kFormatter(love_total)+"</td>");
  $("#tr_wow").append("<td>"+kFormatter(wow_total)+"</td>");
  $("#tr_haha").append("<td>"+kFormatter(haha_total)+"</td>");
  $("#tr_sad").append("<td>"+kFormatter(sad_total)+"</td>");
  $("#tr_angry").append("<td>"+kFormatter(angry_total)+"</td>");
 // $("#tr_share").append("<td>"+kFormatter(share_total)+"</td>");
}




function Clear_reaction_table()
{
 $("#div_reaction").empty();

 $("#div_reaction").append(" <table  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_header'>"+
                           "<th>Reaction</th>"+
                          "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                           "<tr id='tr_like'>"+
                           "<td>Like</td>"+
                            "</tr>"+
                             "<tr id='tr_love'>"+
                             "<td>Love</td>"+
                             "</tr>"+
                             "<tr id='tr_wow'>"+
                             "<td>WOW</td>"+
                             " </tr>"+
                             "<tr id='tr_haha'>"+
                             "<td>HA HA</td>"+
                             "</tr>"+
                             "<tr id='tr_sad'>"+
                             "<td>Sad</td>"+
                             "</tr>"+
                             "<tr id='tr_angry'>"+
                             "<td>Angry</td>"+
                              "</tr>"+
                              // "<tr id='tr_share'>"+
                              // "<td><a href='javascript:void(0)'>⤴ Share</a></td>"+
                              // "</tr>"+
                              "</tbody>"+
                              "</table>");
}

//Dashboard
function tagCount(admin_page)
{
    $("#tbl_tag_count tbody").empty();
    var brand_id = GetURLParameter('pid');
        $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getTagCount')}}", // This is the URL to the API
      data: { fday: GetURLParameter('fday'),sday:GetURLParameter('sday'),brand_id:brand_id,admin_page:admin_page }
    })
    .done(function( data ) {//console.log(data);
for(var i in data) 
        {
           var tagLabel=data[i].tagLabel;
           var tagCount=data[i].tagCount;
           $("#tbl_tag_count tbody").append(' <tr style="border-bottom:1px solid rgba(120, 130, 140, 0.13)"> '+
                                            ' <td style="width:40px" span="2"> <button type="button" class="btn '+
                                            ' btn-rounded btn-block btn-info btntag" value="'+tagLabel+'">'+tagLabel+'</button></td> '+
                                            ' <td></td> '+
                                            '<td class="text-right"> '+
                                            ' <span class="label label-light-info">'+tagCount+'</span></td> '+
                                            ' </tr>')

        }

    })
     .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    
    });
}
function FanPage(page_name,date_preset)
{//alert(startDate);alert(endDate);alert(date_preset);
    var pageFanChart = echarts.init(document.getElementById('fan-page-chart'));
    var FanGrowthChart = echarts.init(document.getElementById('dashboardfan-growth-chart'));
    var FanGrowthChart_print = echarts.init(document.getElementById('dashboardfan-growth-chart-print'));
  
    $('#dashboardfan-growth-spin').show();
    var brand_id = GetURLParameter('pid');
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getPageFan')}}", // This is the URL to the API
      data: { date_preset: date_preset,page_name:page_name,fday:GetURLParameter('pid'),sday:GetURLParameter('sday'),brand_id:brand_id}
    })
    .done(function( data ) {//alert(data);
     console.log(data);
     console.log("Page");
      var fan = [];
      var fan_online = [];
      var endtime = [];

         for(var i in data) {//alert(data[i].mentions);
        fan.push(data[i].fan);
        fan_online.push(data[i].fan_online);
        endtime.push(data[i].end_time);
        
      }
      // var min_of_array = Math.min.apply(Math, fan);
      // var min_of_array = min_of_array-10000;
        option_growth= null;
option_growth = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        axisPointer: {
            //type: 'cross'
        },
        formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
       /* console.log(rez);*/ //quite useful for debug
        params.forEach(item => {
            // console.log("item");
            // console.log(item.data);
            var item_data=item.data==='-'?'-':kFormatter(item.data);
            var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName + ': ' + item_data  + '</p>'
            rez += xx;
        });

        return rez;
    }
    },
          grid: {
          top:    60,
    
    left:   '5%',
    right:  '10%',
    bottom:  '5%',
            containLabel: true
        },
  
     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },


    xAxis: [
        {
        
            type: 'category',
             boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },
                  axisLabel: {
      formatter: function (value, index) {
       const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

     var date = new Date(value);
       var xx= date.getDate() + '\n' + monthNames[date.getMonth()];
       return xx;

},

    },
            data: endtime
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Fan Count',
          //  min: min_of_array,
            scale:true,
            // max: 250,
            position: 'left',
            // axisLine: {
            //     lineStyle: {
            //         color: colors[0]
            //     }
            // }
                    axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }
    }
    
     }
    ],
    series: [
        {
            name:'Fan',
            type:'line',
            smooth: 0.2,
            color:colors[0],
            barMaxWidth:30,
            data:fan
        }
    ]
};
$("#dashboardfan-growth-spin").hide();
        
 FanGrowthChart.setOption(option_growth, true), $(function() {
    function resize() {
        setTimeout(function() {
            FanGrowthChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
FanGrowthChart_print.setOption(option_growth, true), $(function() {
    function resize() {
        setTimeout(function() {
            FanGrowthChart_print.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

      option = null;
     option = {
    color: ['#e5e5e5',colors[0]],
    tooltip : {
        trigger: 'axis',

        
    },

    grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top: '2%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            axisTick: {
                alignWithLabel: true
            },
               boundaryGap : true,
              axisLabel: {
      formatter: function (value, index) {
         const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

     var date = new Date(value);
       var xx= date.getDate() + '\n' + monthNames[date.getMonth()];
       return xx;

},

    },
            data : endtime,

        }
    ],
    yAxis : [
        {
            
             // inverse:true,
            type : 'value',
                     axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }
    }
        }
    ],
    series : [
        {
            name:'Fan',
            type:'bar',
            barGap:'-88%',
            barWidth: '80%',
            data:fan
        },
        {
            name:'Online',
            type:'bar',
            barWidth: '60%',
            data:fan_online
        }
    ]
};
 if (option && typeof option === "object") {
    pageFanChart.setOption(option, true);

}
 $('#fan-page-spin').hide();
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in FAN API" );
            });
  }
  function GenderReachData(page_name){//alert(startDate);alert(endDate);alert(date_preset);
    var reachgenderchart = document.getElementById('reach-age-gender-chart');
    var reachgenderchart_1=document.getElementById('reach-age-gender-chart-1')
    var reachGenderChart = echarts.init(reachgenderchart);
    var reachGenderChartBottom = echarts.init(reachgenderchart_1);
    var brand_id = GetURLParameter("pid");
    $('#reach-gender-spin').show();
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('get-agegender-Reach')}}", // This is the URL to the API
      data: {brand_id:brand_id,page_name:page_name}
    })
    .done(function( data ) {
     
       var count = Object.keys(data).length;//last record id
       var keys = Object.keys(data);//get only key
       var last_key = keys[count-1]; //to get last key's value total
        
      var F = [];
      var M = [];
      var F_val_arr = [];
      var M_val_arr = [];
      var Label =[];
      var Total = 0;
      var Ftotal = 0;
      var Mtotal =0;
        for(var i in data) {
        Total=data[last_key].T;
        var F_val=data[i].F;
        var M_val=data[i].M;
        F.push((parseFloat(F_val)/parseFloat(Total)*100).toFixed(3));
        M.push((parseFloat(M_val)/parseFloat(Total)*100).toFixed(3));
        F_val_arr.push(F_val);
        M_val_arr.push(M_val);
        Label.push(data[i].Label);
              
      }
       $.each( F_val_arr,function(){Ftotal+=parseInt(this) || 0;});
       $.each( M_val_arr,function(){Mtotal+=parseInt(this) || 0;});

     var FLegend=(parseFloat(Ftotal)/parseFloat(Total)*100).toFixed(2);
     var MLegend=(parseFloat(Mtotal)/parseFloat(Total)*100).toFixed(2);
     $("#pcent_legend_Women").text(FLegend);
     $("#pcent_legend_Men").text(MLegend);

    option = null;
    option_bottom = null;
    option = {
    color: ['#1e88e5'],//"#4da1f0", "#f1c54b"
    tooltip : {
        trigger: 'axis',
        axisPointer : {            
            type : 'shadow'       
        },
         formatter: function (params) { console.log(params);
            return "Women " + params[0].name + " make up" + '</br>' + params[0].value +  "% of people reach";
         }
        
    },
//      legend: {
//         data:['Women'],
//         position: 'bottom',
//         formatter: function (name) {
//     return  name + " " + FLegend +'%';
// }
//     },
    grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top: '12%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : Label,
            axisTick: {
                alignWithLabel: true
            }
        }
    ],
    yAxis : [
        {
            show: false,
             // inverse:true,
            type : 'value'
        }
    ],
    series : [
        {
            name:'Women',
            type:'bar',
              label: {
                normal: {
                    show: true,
                    position: 'top',
                    formatter: '{c}%',
                },
              
            },
            barMinHeight:2,
            barWidth: '60%',
            data:F
        }
    ]
};
option_bottom = {
    color: ['#4da1f0'],
    tooltip : {
        trigger: 'axis',
        axisPointer : {            
            type : 'shadow'       
        },
         formatter: function (params) { console.log(params);
            return "Men " + params[0].name + " make up" + '</br>' + params[0].value +  "% of people reach";
         }
        
    },
//      legend: {
//         data:['Men'],
//         position: 'bottom',
//         formatter: function (name) {
//     return  name + " " + MLegend +'%';
// }
//     },
    grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top: '2%',
        containLabel: true
    },
    xAxis : [
        {
            //show: false,
          
            type : 'category',
            data : Label,
            axisTick: {
                alignWithLabel: true
            },
            axisLabel: {
      formatter: function (value, index) {
return ;

},
 rotate:45
    },
        }
    ],
    yAxis : [
        {
            show: false,
            inverse:true,
            type : 'value'
        }
    ],
    series : [
        {
            name:'Men',
            type:'bar',
              label: {
                normal: {
                    show: true,
                    position: 'bottom',
                    formatter: '{c}%',
                },
              
            },
            barMinHeight:2,
            barWidth: '60%',
            data:M
        }
    ]
};
 if (option && typeof option === "object") {
    reachGenderChart.setOption(option, true);

}
if (option_bottom && typeof option_bottom === "object") {
    reachGenderChartBottom.setOption(option_bottom, true);

}
$('#reach-gender-spin').hide();
    
      
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in Reach API" );
            });
  }
  function tagsentimentData(admin_page){//alert(fday);
      
      var sentichart = document.getElementById("tag-senti-chart");
      var sentiChart = echarts.init(sentichart);
      
      $("#tag-senti-spin").show();
              
          var brand_id = GetURLParameter('pid');
        
          $.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
            url: "{{route('getTagSentiment')}}", // This is the URL to the API
            data: { fday: GetURLParameter('fday'),sday:GetURLParameter('sday'),brand_id:brand_id,admin_page:admin_page }
          })
          .done(function( data ) {//alert(data);
                console.log("tag senti");
          console.log(data);//mention
          var xAxisData = [];
          var data1 = [];
          var data2 = [];
      
      
      for(var i in data) 
              {
          xAxisData.push(data[i].tagLabel);
          data1.push(Math.round(data[i].positive));
          if(data[i].negative >0 )
          {
          data2.push(Math.round(-data[i].negative));
          }
          else
          {
           data2.push(0);
          }
        
       
      }
      
      
      var itemStyle = {
          normal: {
          },
          emphasis: {
              barBorderWidth: 1,
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowOffsetY: 0,
              shadowColor: 'rgba(0,0,0,0.5)'
          }
      };
      
      
      option = {
            color:colors,
          backgroundColor: 'rgba(0, 0, 0, 0)',
           dataZoom:[  {
                  type: 'slider',
                  show: true,
                  xAxisIndex: [0],
                  start: 70,
                  end: 100
              },
               {
                  type: 'inside',
                  xAxisIndex: [0],
                  start: 1,
                  end: 35
              }
          ],
              calculable : true,
          legend: {
              data: ['positive', 'negative'],
                 x: 'center',
                   y: 'top',
                    padding :0,
          }/*,
          brush: {
              toolbox: ['rect', 'polygon', 'lineX', 'lineY', 'keep', 'clear'],
              xAxisIndex: 0
          }*/,
      
          toolbox: {
                  show : true,
                  feature : {
                      mark : {show: true},
                      dataView : {show: false, readOnly: false},
                      magicType : {show: false, type: ['stack','tiled']},
                      restore : {show: false},
                      saveAsImage : {show: false}
                  }
              },
          tooltip: {},
          calculable : true,
          dataZoom : {
              show : true,
              realtime : true,
              start : 0,
              end : 100
          },
          xAxis: {
              type: 'category',
              data: xAxisData,
              name: 'Tags',
              silent: false,
              axisLine: {onZero: true},
              splitLine: {show: false},
              splitArea: {show: false},
             /*  axisLabel: {
                  textStyle: {
                      color: '#fff'
                  }
              },*/
          },
          yAxis: {
              inverse: false,
              splitArea: {show: false}
          },
         grid: {
                  top: '12%',
                  left: '1%',
                  right: '10%',
                  containLabel: true
              },
      
          series: [
              {
                  name: 'positive',
                  type: 'bar',
                  stack: 'one',
                  color:colors[0],
                  barMaxWidth:30,
                  itemStyle: itemStyle,
                  data: data1
              },
              {
                  name: 'negative',
                  type: 'bar',
                  stack: 'one',
                  color:colors[1],
                  barMaxWidth:30,
                  itemStyle: itemStyle,
                  data: data2
              }
          ]
      };
      
      $("#tag-senti-spin").hide();
    
          sentiChart.setOption({
              title: {
                  backgroundColor: '#333',
                /*  text: 'SELECTED DATA INDICES: \n' + brushed.join('\n'),*/
                  bottom: 0,
                  right: 0,
                  width: 100,
                  textStyle: {
                      fontSize: 12,
                      color: '#fff'
                  }
              }
          });
      
          sentiChart.setOption(option, true), $(function() {
          function resize() {
              setTimeout(function() {
                  sentiChart.resize()
              }, 100)
          }
          $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
      });
         sentiChart.on('click', function (params) {
         console.log("tags");
         console.log(params);
         console.log(params.name); // xaxis data = tag name
         console.log(params.seriesName); //bar type name ="positive"
         console.log(params.value);//count 8
         var pid = GetURLParameter('pid');
         var source = GetURLParameter('source');
         var CmtType ='';
         if(params.seriesName == 'positive')
          CmtType = 'pos'
          else
         CmtType = 'neg'
      
         // window.open("{{ url('relatedcomment?')}}" +"pid="+ pid +"&source="+ source+"&fday="+ GetStartDate()+
         // "&sday="+GetEndDate()+"&type="+ params.seriesName +"&tag="+ params.name +"&from_graph=sentiment&admin_page="+admin_page  , '_blank');
         window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+params.name+"&CmtType="+CmtType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate() , '_blank');
      });
      
      
         
      
        
          })
           .fail(function(xhr, textStatus, error) {
             console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
            // If there is no communication between the server, show an error
           // alert( "error occured" );
          });
      
        }
 
function TotalReach(admin_page,date_preset){//alert(fday);

var brand_id = GetURLParameter('pid');
$("#reach-spin").show();
$.ajax({
type: "GET",
dataType:'json',
contentType: "application/json",
url: "{{route('getFbReach')}}", // This is the URL to the API
data: {date_preset:date_preset,period:'day',brand_id:brand_id,fday:GetURLParameter('fday'),sday:GetURLParameter('sday')}
})
.done(function( data ) {console.log(data);
$("#reach-spin").hide();
var organic = [];
var paid = [];
var organic_total=0;
var paid_total=0;

for(var i in data) 
 {
 organic.push(data[i].organic);
 paid.push(data[i].paid);
   
 }
 $.each(organic,function(){organic_total+=parseInt(this) || 0;});
 $.each(paid,function(){paid_total+=parseInt(this) || 0;});

var ReachChart = echarts.init(document.getElementById('total-reach-chart'));

option = {
tooltip: {
 trigger: 'item',
 formatter: "{a} <br/>{b}: {d}% " // formatter: "{a} <br/>{b}: {c} ({d}%)"
},
legend: {
 orient: 'vertical',
 x:'right',

 data:['Organic','Paid']
},
color: ["#4da1f0", "#f1c54b"],
series: [
 {
     name:'Total Reach',
     type:'pie',
     radius: ['65%', '80%'],
     center : ['25%', '50%'],
     avoidLabelOverlap: false,
     label: {
        
         normal: {
             show: false,
             position: 'center',
           
         },
         emphasis: {
             show: true,
             position : 'center',
             formatter:"{d}%",
              // formatter : function (params){
              //                    return params.name +'\n' + params.value + '\n'
              //                },
             textStyle: {
                 fontSize: '20',
                 fontWeight: 'bold'
             }
         }
     },
     labelLine: {
         normal: {
             show: false
         }
     },
     data:[
     { value: organic_total, name: 'Organic' },
     { value: paid_total, name: 'Paid' },
   
     ]
 }
]
};


// use configuration item and data specified to show chart
ReachChart.setOption(option, true), $(function() {
function resize() {
 setTimeout(function() {
     ReachChart.resize()
 }, 100)
}
$(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

})
.fail(function() {

});
}

     function kFormatter(num,get_zero=0) {
      if(num<=0 && get_zero==0)
      {
        return '-';
      }
      else
      {
         //return num > 999 ? (num/1000).toFixed(1) + 'k' : Math.round(num)
         return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
      }
   
}
function CityReach(admin_page)
{
var brand_id = GetURLParameter('pid');
 // var fday = moment(endDate).subtract(1, 'week');
 // var sday = endDate;
 // fday=fday.format('YYYY MM DD');
 // sday=sday.format('YYYY MM DD');

    $( "#city-spin" ).show();
    
 $("#tbl_city_reach tbody").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getcityReach')}}", // This is the URL to the API
      data: { fday: GetURLParameter('fday'),sday:GetURLParameter('sday'),brand_id:brand_id,admin_page:admin_page}
    })
    .done(function( data ) { //console.log(data);
     $( "#city-spin" ).hide();
     var html="";
      $.each(data, function( index, value ) {

             html +='<tr>'+
                   
                    ' <td style="font-size:15px">'+index+'</td> '+
                    ' <td align="right"><span class="label label-light-info">'+value+'</span></td> '+
                    ' </tr> ';
        
        
  // alert( index + ": " + value +":" +i);
});
    
     $("#tbl_city_reach tbody").append(html);

        

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });
}
function posting_status(admin_page)

{
 var brand_id = GetURLParameter('pid');
 // var fday = moment(endDate).subtract(1, 'week');
 // var sday = endDate;
 // fday=fday.format('YYYY MM DD');
 // sday=sday.format('YYYY MM DD');

    $( "#post_spin" ).show();
    $( "#cmt_spin" ).show();

  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getPostingStatus')}}", // This is the URL to the API
      data: { fday: GetURLParameter('fday'),sday:GetURLParameter('sday'),brand_id:brand_id,admin_page:admin_page}
    })
    .done(function( data ) {
     $( "#post_spin" ).hide();
     $( "#cmt_spin" ).hide();
    

        var total_post =data[5][0]['total'] ;
        var overall_pos =data[3][0]['ov_positive'] ;
        var overall_neg = data[3][0]['ov_negative'] ;
        var overall_neutral = data[3][0]['ov_neutral'] ;
   
        var all_post_total=parseInt(overall_pos)+parseInt(overall_neg)+parseInt(overall_neutral);
  
        var post_pos_pcent=parseFloat((overall_pos/all_post_total)*100).toFixed(2);
        var post_neg_pcent=parseFloat((overall_neg/all_post_total)*100).toFixed(2);
         var post_neutral_pcent=parseFloat((overall_neutral/all_post_total)*100).toFixed(2);
        
        post_pos_pcent = isNaN(post_pos_pcent)?'-':post_pos_pcent;
        post_neg_pcent = isNaN(post_neg_pcent)?'-':post_neg_pcent;
        post_neutral_pcent = isNaN(post_neutral_pcent)?'-':post_neutral_pcent;
        total_post = isNaN(total_post)?'-':total_post;
        
          $("#post_total").text(total_post==0?'-':total_post);
          $("#post_pos").text(post_pos_pcent==0?'- %':post_pos_pcent.replace(".00", "") + "%");
          $("#post_neg").text(post_neg_pcent==0?'- %':post_neg_pcent.replace(".00", "") + "%");
          $("#post_neutral").text(post_neutral_pcent==0?'- %':post_neutral_pcent.replace(".00", "") + "%");

        var pos =data[1][0]['positive'] ;
        var neg =data[1][0]['negative'] ;
        var neutral = data[1][0]['neutral'] ;
        var NA = data[1][0]['NA'] ;
            neutral=parseInt(neutral)+parseInt(NA);
            
        var total_comment =  data[1][0]['total'] ;
        var all_total=parseInt(pos)+parseInt(neg)+parseInt(neutral);
        var pos_pcent=parseFloat((pos/all_total)*100).toFixed(2);
        var neg_pcent=parseFloat((neg/all_total)*100).toFixed(2);
        var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);

        pos_pcent = isNaN(pos_pcent)?'-':pos_pcent;
        neg_pcent = isNaN(pos_pcent)?'-':neg_pcent;
        neutral_pcent = isNaN(neutral_pcent)?'-':neutral_pcent;
        total_comment = isNaN(total_comment)?'-':total_comment;

          $("#cmt_total").text(total_comment==0?'-':total_comment);
          $("#cmt_pos").text(pos_pcent==0?'- %':pos_pcent.replace(".00", "") + "%");
          $("#cmt_neg").text(neg_pcent==0?'- %':neg_pcent.replace(".00", "") + "%");
          $("#cmt_neutral").text(neutral_pcent==0?'- %':neutral_pcent.replace(".00", "") + "%");

        // var w_total_post = parseInt(data[5][0]['total'])=== 0 ? '-' : data[5][0]['total'] ;
        // var w_overall_pos = parseInt(data[3][0]['ov_positive'])=== 0 ? '-' : data[3][0]['ov_positive'] ;
        // var w_overall_neg = parseInt(data[3][0]['ov_negative'])=== 0 ? '-' : data[3][0]['ov_negative'] ;
        // var w_total_comment = parseInt(data[1][0]['total'])=== 0 ? '-' : data[1][0]['total'] ;
        // var w_pos = parseInt(data[1][0]['positive'])=== 0 ? '-' : data[1][0]['positive'] ;
        // var w_neg = parseInt(data[1][0]['negative'])=== 0 ? '-' : data[1][0]['negative'] ;


        // for(var i in data) {//alert(data[i].sentiment);
          
        //                   }
  

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });
}


 function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

 function convertKtoThousand(s)
{

    var str=s;
    str=str.toUpperCase();
    if(str === '-')
    {
      return 0;
    }
    if (str.indexOf("K") !== -1) {
    str = str.replace("K",'');
    return parseFloat(str) * 1000;
  } else if (str.indexOf("M") !== -1) {
    str = str.replace("M",'');
     return parseFloat(str) * 1000000;
  } else {
    return parseFloat(str);
  }
  
}
 
$("#print").click(function(){
    window.print();
});
 function LevelCheck(s)
{

    if(parseInt(s) === 1) return "1st";else if(parseInt(s) === 2) return "2nd";else if(parseInt(s) === 3) return "3rd";
    else if(parseInt(s) > 3) return s+"th";
  
}
    function judge_emotion_icon(emotion)
        {
            var emojis = ['0x1F620', '0x1F604', '0x1F616', '0x1F628', '0x1F604', '0x1F44D',
            '0x1F60D', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

        if (emotion ==="anger") return emojis[0]; 
        else if(emotion ==="interest") return emojis[1] ;
         if (emotion ==="disgust") return emojis[2] ; 
        else if(emotion ==="fear") return emojis[3] ;
         if (emotion ==="joy") return emojis[4]; 
        else if(emotion ==="like") return emojis[5] ;
         if (emotion ==="love") return emojis[6] ; 
        else if(emotion ==="neutral") return emojis[7] ;
         if (emotion ==="sadness")  return emojis[8]; 
        else if(emotion ==="surprise") return emojis[9]  ;
         else if(emotion === "trust") return emojis[10];

        }
 

       
  });
    </script>
    
<style>
.modal-dialog {
    position: absolute;
    top: 200px;
    right: 100px;
    bottom: 0;
    left: 100px;
    z-index: 10040;
    /* overflow: auto; */
}
.table td, .table th {
    padding: .75rem .5rem .75rem .5rem;
    }
    
.myHeaderContent
{
   margin-right:300px;
}
  .print {display:none;}
.page {
        width: 297mm;
        min-height: 210mm;
        padding: 6mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        padding-top:0px;
    }
.coverpage
{
    width: 297mm;
    min-height: 210mm;
    /* padding: 10mm; */
    margin: 10mm auto;
    border: 1px #D3D3D3 solid;
    border-radius: 5px;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    background:white;
   
}
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    
  .cover_title {
  text-align: center;
  font-size: 1.5em;
  margin-top: 10px;
  text-transform: uppercase;
  letter-spacing: 3px;
  font-weight: bold;
  color: white !important;;
  text-shadow: 0 1px 2px rgba(black,.15);
}
.cover_subtitle {
  text-align: center;
  font-size: 1em;
  margin-bottom: 10px;
  font-weight: bold;
  color: white !important;;
  text-shadow: 0 1px 2px rgba(black,.15);
}
.header_row { 

  background:#1f759c;
  padding:20px 0 20px 20px;
}
.header_title { 
  font-size: 1em;
  /* margin-top: 10px; */
  text-transform: uppercase;
  letter-spacing: 1px;
  font-weight: bold;
  color: white !important;
  
  text-shadow: 0 1px 2px rgba(black,.15);
 
}
.header_subtitle {
  text-align: center;
  font-size: 1em;
  color: white !important;;
  text-shadow: 0 1px 2px rgba(black,.15);
}
.innerDiv {
 padding-top:50%;
 text-align:center;
 background:#51b7e6;
 height: 372mm;

 }

.headerDiv
{
 margin:0%;
}
.graph-title
{
    font-family : "Times New Roman";
    font-weight: bold;
    text-decoration:underline;
}

.contentDiv
{
 padding:10px;
 padding-top:30px;
}
@page {
        size: A4;
        margin: 0;
       
    }
    @media print {
       .noprint {display:none;}
        .print {display:block;}
        
        html, body {
        width: 297mm;
        height: 210mm;
        margin:0;
        padding:0;
        overflow:visible;
        height: auto;
        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
            page-break-before: always;
        }
        .coverpage {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
            page-break-before: always;
            background:initial;
            background-size: cover;
        }
        body {
      -webkit-print-color-adjust: exact !important;
      /* background:#51b7e6 !important; */
    
   }
            .innerDiv {
            padding-top:50%;
            text-align:center;
            background:#51b7e6;
            height: 445mm;
            width:340mm;
            }
            .header_row
            {
                width:340mm;
                background:#1f759c;
                padding:20px 0 20px 20px;
            }
            
            .contentDiv
            {
            padding:10px;
            padding-top:30px;
            width:315mm;
            }
    
    }
</style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">

</body>



