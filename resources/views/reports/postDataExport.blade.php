@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')

  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                 <header class="" id="myHeader">
                  <div class="row page-titles" style="padding-bottom:15px;padding-top:15px">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500">Post Data Export</h4>
                       <!--  <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Brand List</li>
                        </ol> -->
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                            <!--     <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>THIS MONTH</small></h6>
                                    <h4 class="m-t-0 text-info">$58,356</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div> -->
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                               <!-- <input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div> -->
                            </div>
                            <!-- <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
  </header>
             

                  
 <div class="row ">
                    <div class="col-12">
                       
                        <div class="card" >
                   
                            <div class="card-body b-t collapse show" align="center">
                            <table>
                            <tr class="spaceUnder">
                            <td style="font-weight:bold;text-align:right">Page Name : </td>
                            <td> <div class='form-material input-group'>
                                    <select class="form-material form-control" id="page_name">
                                    <option value="" id="" disabled selected>Select Page</option>
                                              <!-- <option value="">Choose Page</option> -->
                                                  @if (isset($monitor))
                                                  @foreach($monitor as $index =>$monitor)
                                                  <option value="{{$monitor}}" id="{{$monitor}}" >{{$monitor}}</option>
                                                  @endforeach
                                                  @endif
                                            </select>
                            </div></td>
                            </tr>
                            <tr class="spaceUnder">
                            <td style="font-weight:bold;text-align:right">Date : </td>
                            <td><div class="d-flex m-r-20 m-l-10 hidden-md-down"><input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div></div></td>
                            </tr>
                            <tr>
                            <td colspan='2' align="middle">
                              <div class="form-group">
                                <button type="button" class="btn btn-primary" id="btn_export"> Export </button>
                              </div>
                            </td>
                           
                            </tr>
                            </table>
                            
                            </div>
                        </div>
                    </div>
 </div>
         
               
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
   <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
     <script src="{{asset('js/morris-data.js')}}" ></script>-->
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
    <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
   <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}" defer></script>
   <script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var myHeaderContent = document.getElementById("myHeaderContent");
var sticky = header.offsetTop;

function myFunction() {//alert("ho");
  if (window.pageYOffset > sticky) {
    header.classList.add("s-topbar");
    header.classList.add("s-topbar-fix");
    myHeaderContent.classList.add("myHeaderContent");
  } else {
    header.classList.remove("s-topbar");
    header.classList.remove("s-topbar-fix");
    myHeaderContent.classList.remove("myHeaderContent");
  }
}
</script>
    <script type="text/javascript">
var startDate;
var endDate;
 $(document).ready(function() {
    startDate = moment().subtract(1, 'month');
    endDate = moment();
    var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

    $('.dateranges').daterangepicker({
    locale: {
            format: 'MMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end,label) {//alert(label);
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
    });
    function GetStartDate()
    {
    //alert(startDate);
    return startDate.format('YYYY MM DD');
    }
    function GetEndDate()
    {
            // alert(endDate);
    return endDate.format('YYYY MM DD');
    }
    $( "#btn_export" ).click(function() {
            // alert(startDate);
            var db_name=$("#db_selection").val();
           data_table();

          });

          function data_table(fday,sday,db_name)
    { 
        // alert(fday);
        $.ajax({
      type: "GET",
   
     headers: { 
           'X-CSRF-TOKEN': '{{csrf_token()}}' 
    },
     url: "{{route('getPostExcelData')}}", // This is the URL to the API
      data: {
             page_name: $('#page_name option:selected').val(),
             brand_id:GetURLParameter('pid'),
             fday:GetStartDate(),
             sday:GetEndDate()
             
            }
    })
    .done(function( response ) {
      // console.log("test");
     
      
// console.log (response);

      location.href ='reports/'+response.title+'.xlsx';
     




    })
    .fail(function(xhr, textStatus, error) {
      console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

 
    }

});
    </script>
    <style type="text/css">
.table td, .table th {
    padding: .50rem;
    vertical-align: middle;
    border-top: 1px solid #dee2e6;
}
.table thead
{
  color:#1c81da;
}
table.fixedHeader-floating {
   clear: both;
  top: 114px !important;
  z-index:5;
   top : 0;
   left:0 !important;
   width: 100% !important;
/*   margin: 0 auto;
     padding: 0 15px;*/
 /*    overflow-x: hidden;
      overflow-y: auto;*/
  
    box-sizing: border-box;
/*
  background: red;*/
/*  left:150px !important;*/
/*  margin-right:30px;*/
 
/*  width: 80% !important;*/
  /*background: transparent;*/
}
tr.spaceUnder>td {
  padding-bottom: 1em;
}

.myHeaderContent
{
   margin-right:300px;
}

    </style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush

