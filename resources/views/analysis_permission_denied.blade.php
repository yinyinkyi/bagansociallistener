@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')
@section('comparison')

<li class="nav-item dropdown">
                            <a  class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            
                              <button onclick="window.location='{{route('comparison', ['pid' => $pid])}}'" type="button" class="btn waves-effect waves-light btn-outline-warning">Comparison</button>  
                            </a>
  <a></a>                         
 </li>
 <li class="nav-item dropdown">
                            <a  class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            
                              <button onclick="window.location='{{route('comparison', ['pid' => $pid])}}'" type="button" class="btn waves-effect waves-light btn-outline-warning">Insight</button>  
                            </a>
                           

 </li>
@endsection

  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Analysis</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Analysis</a></li>
                            <li class="breadcrumb-item active"> @if (isset($project_name)) {{$project_name}} @endif</li>
                        </ol>
                    </div>
                     <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                            <div class="btn-group btn-group-lg  mb-3" role="group" aria-label="Basic example">
                                            <button id="day" type="button" class="btn btn-success">Day</button>
                                            <button id="week" type="button" class="btn btn-success">Week</button>
                                            <button  id="month" type="button" class="btn btn-success">Month</button>
                                        </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                            <div class='input-group mb-3'>
                                <input type='text' class="form-control dateranges" style="color:#01c0c8 !important;width: 17em; padding: .2em .2em 0 ;" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            </div>
                          <!--   <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                           <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Mentions</h3>
                                          
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                               <div style="display:none"  align="center" style="vertical-align: top;" id="mention-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             
                              <div id="mention-chart" style="width:100%; height:400px;"></div>   
                           
                            </div>
                        </div>
                    </div>
               
                </div>
                <div class="row">
                    <!-- Column -->
                       <div class="col-lg-4 col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h3 class="card-title">Gender</h3>
                                       
                                    </div>
                                 
                                </div>
                                   <div style="display:none"  align="center" id="gender-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           
                           <div id="gender-pie-chart" style="width:100%; height:200px;"></div>
                            </div>
                        </div>
                    </div>
                                  <!-- Column -->
                    <!-- Column -->
                   <div class="col-lg-4 col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h3 class="card-title">Location</h3>
                                       
                                    </div>
                                 
                                </div>
                                   <div style="display:none"  align="center" id="location-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           
                           <div id="location-pie-chart" style="width:100%; height:200px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h3 class="card-title">Age</h3>
                                       
                                    </div>
                                 
                                </div>
                                   <div style="display:none"  align="center" id="age-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           
                           <div id="age-pie-chart" style="width:100%; height:200px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- Row -->
                <div class="row">
               <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Sentiment</h3>
                                          
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                                <div style="display:none" width="100%" align="center" style="vertical-align: top;" id="sentiment-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                               <div id="sentiment-bar-chart" style="width:100%; height:400px;"></div>
                         
                            </div>
                        </div>
                    </div>
                </div>
                      <div class="row">
               <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Social Media Reach</h3>
                                        
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                                <div style="display:none"  align="center" style="vertical-align: top;" id="socialmedia-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                               <div id="socialmedia-bar-chart" style="width:100%; height:400px;">

                               </div>

                         
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
               <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Reaction</h3>
                                         
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                                 <div style="display:none" width="100%" align="center" id="reaction-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                               <div id="reaction-bar-chart" style="width:100%; height:400px;"></div>
                         
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                
                <!-- Row -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                       <div class="col-lg-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Popular Negative Post</a> </li>
                              
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home" role="tabpanel">
                                        <div style="display:none"  align="center" id="popular-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>

                                    <div class="card-body">
                                        <div class="profiletimeline" id="popular-negative">
                                      
                                            
                                        </div>
                                    </div>
                                </div>
                          
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
               <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Net Promotor Score</h3>
                                         
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                 <div style="display:none"  align="center" id="promotor-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                 <div id="promotor-pie-chart" style="width:100%; height:400px;"></div>
                         
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme working">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/1.jpg')}}" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/2.jpg')}}" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/3.jpg')}}" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/4.jpg')}}" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/5.jpg')}}" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/6.jpg')}}" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/7.jpg')}}" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/8.jpg')}}" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}"></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
   <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script> -->
   <!--  <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script> -->
 
    
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
   
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
    <script type="text/javascript">
var startDate;
var endDate;
var mention_total;


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

$(function () {
    startDate = moment().subtract(3, 'month');
    endDate = moment();
    var periodType= '';
            $('.btn-group .active').each(function(){
                periodType= $(this).attr('id'); 
                
            });
            if(periodType == '')
            {
              periodType='month';
            }
                  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function ChooseDate(start,end,calType)
{
   $('.dateranges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          startDate=start;
          endDate=end;
      var brand_id = GetURLParameter('pid');
          var start = new Date(startDate);
           var end = new Date(endDate);
           var timeDiff = Math.abs(end.getTime() - start.getTime());
           var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
      
           
          if(parseInt(diffDays) < 7) //in terms of day
          { 
            //alert("7");
             $("#day").show();
             $("#week").hide();
             $("#month").hide();
          }
         
          
         else if (parseInt(diffDays) == 7)//in terms of week
          {
            
           //alert("==7");
             $("#day").show();
             $("#week").show();
             $("#month").hide();
              
   
          }
          else if (parseInt(diffDays) > 7)//in terms of month
          {
            //alert(">7");
         
             $("#day").show();
             $("#week").show();
             $("#month").show();
                 
        }

        mentiondetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,periodType);
        sentimentdetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,periodType);
        socialmediadetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,periodType);
        GenderData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
        if(calType === '')
        {
        popular_negative(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
        PromotorData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
        }
}
 function mentiondetail(fday,sday,brand_id,periodType)
          {//alert(fday + "" + periodType + "" + brand_id);
              var mentionchart = document.getElementById('mention-chart');
    var mentionChart = echarts.init(mentionchart);

  $( "#mention-spin" ).show();


    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getmentiondetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:periodType}
    })
    .done(function( data ) {//alert("hihi");
    
    //  $("#sentiment-spin").hide();
      console.log(data);//mention
     // console.log(data[1]);//sentiment
/*       if(data.length > 0)
       {
       $("#mention-total").text("Total = "+data[data.length-1].total);
      // When the response to the AJAX request comes back render the chart with new data
    }*/
      var mentions = [];
      var positive = [];
      var negative = [];
      var mentionLabel = [];
      var sentimentLabel = [];
       var mention_total=0;
        
        for(var i in data) 
        {
        //alert(data[i].mentions); 
          mentions.push( Math.round(data[i].mention));
          mentionLabel.push(data[i].periodLabel);
          
        }
$.each(mentions,function(){mention_total+= parseInt(this) || 0;});
 
option = {
        color:[
        "#3c8cbc"
       
        ],
      

        tooltip : {
            trigger: 'axis',
       formatter: function (params) {
            return '<b>' + params[0].name + '</b><br/><span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:#3c8cbc;"></span>'+ params[0].seriesName + ' : '+  kFormatter(params[0].value);
        }

        },
      
        legend: {
        data:['mention'],
        formatter: '{name}: '+ kFormatter(mention_total),
         padding :0,
       
        
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
       /* xAxis : [
        {
           
            data : mentionLabel,
         formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    var date = new Date(value);
    var texts = [(date.getMonth() + 1), date.getDate()];
    if (idx === 0) {
        texts.unshift(date.getYear());
    }
    return texts.join('/');
}
            
        }
      
        ],*/
        xAxis: [{
    type: 'category', 
    axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
  
     var arr = value.split(' - ');
    var date = new Date(arr[0]);
       console.log(date);
if(periodType === 'day')
{
    var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];
   
}
else 
{

      var texts = [date.getFullYear(), monthNames[date.getMonth()]];
}



    return texts.join('-');

}
    },
         data : mentionLabel,
}],
        
        yAxis : [
        {
            type : 'value'
        }
        ],
        series : [
        {
              xAxes: [{ 
                  ticks: {
                  fontColor: "#f5f2f2", // this here
                },
            }],

            name:'mention',
            type:'line',
            data:mentions,
            color:["#3c8cbc"],

            markPoint : {
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        }
        
    ]
};
 
    $( "#mention-spin" ).hide();
    mentionChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            mentionChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
   
         })
            .fail(function() {
              // If there is no communication between the server, show an error
             console.log( "error occured in mentions" );
            });

        }

    function sentimentdetail(fday,sday,brand_id,periodType){//alert(fday),alert(sday);
   //alert("hihi");
  
    var sentiment_chart = document.getElementById('sentiment-bar-chart');
    var sentimentChart = echarts.init(sentiment_chart);

$("#sentiment-spin").show();


        var brand_id = GetURLParameter('pid');
       // alert (brand_id);
      
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getsentimentdetail')}}", // This is the URL to the API
      data: {fday:fday,sday:sday,brand_id:brand_id,periodType:periodType}
    })
    .done(function( data ) {


       console.log(data);
    
    /*   $("#mention-total").text("Total = "+data[data.length-1].total);
       $("#social-total").text("Total = "+data[data.length-1].total);
       var positive_total= data[data.length-1].total+10;
       $("#sentiment-total").text("Positive = "+ positive_total + " && Negative = "+data[data.length-1].total);*/

      // When the response to the AJAX request comes back render the chart with new data
     
      var positive = [];
      var negative = [];
     
      var sentimentLabel = [];

      var positive_total=0;
      var negative_total=0;



        for(var i in data) {//alert(data[i].mentions);
        positive.push( Math.round(data[i].positive));
        negative.push( Math.round(data[i].negative));
        sentimentLabel.push(data[i].periodLabel);
        
      }


$.each(positive,function(){positive_total+=parseInt(this) || 0;});
$.each(negative,function(){negative_total+=parseInt(this) || 0;});
var positive_percentage= (positive_total/mention_total)*100;
var negative_percentage= (negative_total/mention_total)*100;
var neutral_percentage = 100 - (positive_percentage+negative_percentage);



 $('#positive-progress').css('width', positive_percentage+'%').attr('aria-valuenow', positive_percentage);  
 $('#negative-progress').css('width', negative_percentage+'%').attr('aria-valuenow', negative_percentage);
 $('#neutral-progress').css('width', neutral_percentage+'%').attr('aria-valuenow', neutral_percentage);

 $("#positive-total").text(positive_percentage + "%");
 $("#negative-total").text(negative_percentage + "%");
 $("#neutral-total").text(neutral_percentage + "%");

    option = {
        color:[
        "#00a55a",
        "#de4b3a",

        ],
      

        tooltip : {
            trigger: 'axis'
                  },
      

        legend: {
            data:['Positive','Negative'],
           formatter: function (name) {
            if(name === 'Positive')
            {
                 return name + ': ' + positive_total;
            }
            return name  + ': ' + negative_total;

   
}
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
        {
            type : 'category',
              axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
  
     var arr = value.split(' - ');
    var date = new Date(arr[0]);
       console.log(date);
if(periodType === 'day')
{
    var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];
   
}
else 
{

      var texts = [date.getFullYear(), monthNames[date.getMonth()]];
}



    return texts.join('-');

}
    },
            data : sentimentLabel
        }
        ],
        yAxis : [
        {
            type : 'value'
        }
        ],
        series : [
        {
            name:'Positive',
            type:'bar',
            data:positive,
            color:'#00a55a', // "#00a55a","#de4b3a",

          /*  markPoint : {
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }*//*,
            markLine : {
                data : [
                {type : 'average', name: 'average'}
                ]
            }*/
        },
        {
            name:'Negative',
          /*  type:effectIndex % 2 == 0 ? 'bar' : 'line',*/
            type:'bar',
            data:negative,
             color:'#de4b3a',
           /* markPoint : {
              data : [
              {type : 'max', name: 'maximum'},
              {type : 'min', name: 'minimum'}
              ]
          }*//*,
          markLine : {
            data : [
            {type : 'average', name : 'average value'}
            ]
        }*/
    }
    ]
};
    
$("#sentiment-spin").hide();

   // sentimentChart.setOption(option);
sentimentChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            sentimentChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }
    function socialmediadetail(fday,sday,brand_id,periodType)
    {
      var socialchart = document.getElementById('socialmedia-bar-chart');
    var socialChart = echarts.init(socialchart);

      var reactionchart = document.getElementById('reaction-bar-chart');
    var ReactionChart = echarts.init(reactionchart);

$("#socialmedia-spin").show();
        $("#reaction-spin").show();
        var brand_id = GetURLParameter('pid');
        //var brand_id = 22;
       // alert (brand_id);
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getinteractiondetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:periodType }
    })
    .done(function( data ) {//alert(data);
  
    
        
       console.log(data);//mention

      var Like = [];
      var Love = [];
      var Haha = [];
      var Wow = [];
      var Angry = [];
      var Sad = [];
    

      var Like_total =0;
       var Love_total =0;
       var Haha_total =0;
       var Wow_total =0;
       var Angry_total =0;
       var Sad_total =0;

   
    var socials = [];

      var socialLabel = [];
      var socialsfortotal = [];
 
      var social_total=0;



       for(var i in data) 
        {
          Like.push( Math.round(data[i].Like));
          Love.push( Math.round(data[i].Love));
          Haha.push( Math.round(data[i].Haha));
          Wow.push( Math.round(data[i].Wow));
          Angry.push( Math.round(data[i].Angry));
          Sad.push( Math.round(data[i].Sad));


          var mediaReach=parseInt(data[i].Like)+parseInt(data[i].Love)+parseInt(data[i].Haha)+parseInt(data[i].Wow)+parseInt(data[i].Angry)+parseInt(data[i].Sad)+parseInt(data[i].post_count)
             socials.push( Math.round(mediaReach));
          socialsfortotal.push( Math.round(mediaReach));
          socialLabel.push(data[i].periodLabel);
          
        }

$.each(Like,function(){Like_total+=parseInt(this) || 0;});
$.each(Love,function(){Love_total+=parseInt(this) || 0;});
$.each(Haha,function(){Haha_total+=parseInt(this) || 0;});
$.each(Wow,function(){Wow_total+=parseInt(this) || 0;});
$.each(Angry,function(){Angry_total+=parseInt(this) || 0;});
$.each(Sad,function(){Sad_total+=parseInt(this) || 0;});
$.each(socialsfortotal,function(){social_total+=parseInt(this) || 0;});

social_total = kFormatter(social_total);

/* $("#mention-total").text("Total = "+mention_total);*/
reaction_option = {
        color:[
        "#3c8cbc","#00c0ef","#de4b3a","#f39c12","#00a55a","#f48024"
       
        ],
      

       tooltip : {
            trigger: 'axis'/*,
             formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
        console.log(rez); //quite useful for debug
        params.forEach(item => {
             console.log("hihi"); 
            console.log(item);
            console.log(item.series.color); //quite useful for debug
            var xx = '<p>'   + colorSpan(item.series.color) + ' ' + item.seriesName + ': ' + item.data  + '</p>'
            rez += xx;
        });

        return rez;
    }*/

        },
        legend: {
            data:['Like','Love','Ha Ha','Wow','Angry','Sad' ],
                formatter: function (name) {
            if(name === 'Like')return name + ': ' + kFormatter(Like_total) ;
            if(name === 'Love')return name + ': ' + kFormatter(Love_total);
            if(name === 'Ha Ha')return name + ': ' + kFormatter(Haha_total);
            if(name === 'Wow')return name + ': ' + kFormatter(Wow_total);
            if(name === 'Angry')return name + ': ' + kFormatter(Angry_total);
            if(name === 'Sad')return name + ': ' + kFormatter(Sad_total);
         
            },
             padding :0,
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
        {
            type : 'category',
              axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
  
     var arr = value.split(' - ');
    var date = new Date(arr[0]);
       console.log(date);
if(periodType === 'day')
{
    var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];
   
}
else 
{

      var texts = [date.getFullYear(), monthNames[date.getMonth()]];
}



    return texts.join('-');

}
    },
            data : socialLabel
        }
        ],
        yAxis : [
        {
            type : 'value',
              
        }
        ],

        series : [
        {
            name:'Like',
            type:'bar',
            data:Like,
            color:["#3c8cbc"],
    markPoint : {
                  large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },

        {
            name:'Love',
            type:'bar',
            data:Love,
            color:["#00c0ef"],
            markPoint : {
                large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },

        {
            name:'Ha Ha',
            type:'bar',
            data:Haha,
            color:["#de4b3a"],
            markPoint : {
                large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },

        {
            name:'Wow',
            type:'bar',
            data:Wow,
            color:["#f39c12"],
             markPoint : {
                large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },
 
        {
            name:'Angry',
            type:'bar',
            data:Angry,
            color:["#00a55a"],
             markPoint : {
                large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },

        {
            name:'Sad',
            type:'bar',
            data:Sad,
            color:["#f48024"],
         markPoint : {
                large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        }
        
    ]
};

    social_option = {
        color:[
        "#6f096f"
       
        ],
      

     
        tooltip : {
            trigger: 'axis',
       formatter: function (params) {
            return '<b>' + params[0].name + '</b><br/><span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:#6f096f;"></span>'+ params[0].seriesName + ' : '+  kFormatter(params[0].value);
        }

        },
      
        legend: {
            data:['social media reach' ],
            formatter: '{name}: '+ social_total,
             padding :0,
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
        {
            type : 'category',
              axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
  
     var arr = value.split(' - ');
    var date = new Date(arr[0]);
       console.log(date);
if(periodType === 'day')
{
    var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];
   
}
else 
{

      var texts = [date.getFullYear(), monthNames[date.getMonth()]];
}



    return texts.join('-');

}
    },
            data : socialLabel
        }
        ],
        yAxis : [
        {
            type : 'value',
            axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }
    }
        }
        ],

        series : [
        {
            name:'social media reach',
            type:'line',
            data:socials,
 markPoint : {
              large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        }
        
    ]
};
$("#socialmedia-spin").hide();
$("#reaction-spin").hide();
   // socialChart.setOption(option);
 socialChart.setOption(social_option, true), $(function() {
    function resize() {
        setTimeout(function() {
            socialChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
 ReactionChart.setOption(reaction_option, true), $(function() {
    function resize() {
        setTimeout(function() {
            ReactionChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
  
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }
 function popular_negative(fday,sday)
{
    var brand_id = GetURLParameter('pid');
    $( "#popular-spin" ).show();
    $("#popular-negative").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getpopularnegative')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {//$("#popular-negative").html("");
      $( "#popular-spin" ).hide();
       console.log(data);
       //latest
        for(var i in data) {//alert(data[i].mentions);
       // alert(data[i].message);
     
      $("#popular-negative").append("<div class='sl-item'>"+
                                    " <div class='sl-left'> <img src='"+data[i].full_picture+"' alt='user' class='img-circle  img-bordered-sm'> </div> "+
                                    " <div class='sl-right'> <div><a href='"+data[i].link+"' class='link'  target='_blank'>"+data[i].page_name+"</a> "+
                                    " <span class='sl-date'>"+data[i].created_time+"</span> "+
                                   /* " <p>Sentiment |  <a href='#'> " + data[i].sentiment+" </a>  Emotion |  <a href='#'> " + data[i].sentiment+" </a></p>"+*/
                                    " <p class='m-t-10'>"+ readmore(data[i].message) +"...<a href='"+data[i].link+"' target='_blank'> Read More"+
                                      " </a></p></div>");
                            }
  

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}

function GenderData(fday,sday){//alert(fday),alert(sday);

    var brand_id = GetURLParameter('pid');
     //var brand_id = 22;
     $("#gender-spin").show();
     $("#location-spin").hide();
     $("#age-spin").hide();
      //$("#reaction-count").empty();


      var colors=["#f39c12","#00a55a","#de4b39","#00c0ef","#3c8cbc"];
      var town=['Yangon','Mandalay','NayPyiTaw','Ayarwaddy','Shan'];
      var age=['10-19 years','20-45 years','46-60 years','> 60'];
     

         var pieChart = echarts.init(document.getElementById('gender-pie-chart'));
         var locationChart = echarts.init(document.getElementById('location-pie-chart'));
         var ageChart=echarts.init(document.getElementById('age-pie-chart'));

// specify chart configuration item and data
option = {

    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        x: 'center',
        y: 'bottom',
        data: ['male','female']
    },
    color: colors,
    calculable: true,
    series: [
        {
            name: 'Area mode',
            type: 'pie',
            radius : '50%',
            center: ['50%', '40%'],
            x: '50%', // for funnel
            max: 40, // for funnel
            sort: 'ascending', // for funnel
            data: [
                { value:20, name: 'male' },
                { value:90, name:  'female'  },
          
            ],

            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};

locationoption = {

    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        x: 'center',
        y: 'bottom',
        data: town
    },
    color: colors,
    calculable: true,
    series: [
        {
            name: 'Area mode',
            type: 'pie',
            radius : '50%',
            center: ['50%', '40%'],
            x: '50%', // for funnel
            max: 40, // for funnel
            sort: 'ascending', // for funnel
            data: [
                { value:20, name: town[0] },
                { value:90, name: town[1]  },
                { value:90, name: town[2]  },
                { value:90, name: town[3]  },
                { value:90, name: town[4]  },
          
            ],

            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
ageoption = {

    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        x: 'center',
        y: 'bottom',
        data: age
    },
    color: colors,
    calculable: true,
    series: [
        {
            name: 'Area mode',
            type: 'pie',
            radius : '50%',
            center: ['50%', '40%'],
            x: '50%', // for funnel
            max: 40, // for funnel
            sort: 'ascending', // for funnel
            data: [
                { value:20, name: age[0] },
                { value:30, name: age[1]  },
                { value:70, name: age[2]  },
                { value:20, name: age[3]  },
            
          
            ],

            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};

  $("#gender-spin").hide();
   $("#location-spin").hide();
     $("#age-spin").hide();
// use configuration item and data specified to show chart
pieChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            pieChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

locationChart.setOption(locationoption, true), $(function() {
    function resize() {
        setTimeout(function() {
            locationChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

ageChart.setOption(ageoption, true), $(function() {
    function resize() {
        setTimeout(function() {
            ageChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});


     }

function PromotorData(fday,sday){//alert(fday),alert(sday);

    var brand_id = GetURLParameter('pid');
     //var brand_id = 22;
     $("#promotor-spin").show();
      //$("#reaction-count").empty();


    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getnetpromotorscore')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function( jsondata ) {/*alert(data);alert("hihi");*/
      
       console.log(jsondata);
       var pieChart = echarts.init(document.getElementById('promotor-pie-chart'));
       var data = genData(jsondata);
 console.log("legend");
 console.log(data)
/*option = {
    title : {
        text: 'Score of the page',
         x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: data.legendData,

        selected: data.selected
    },
    series : [
        {
            name: 'Name',
            type: 'pie',
            radius : '55%',
            center: ['40%', '50%'],
            data: data.seriesData,
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};*/


// use configuration item and data specified to show chart

var itemStyle = {
    normal: {
    },
    emphasis: {
        barBorderWidth: 1,
        shadowBlur: 10,
        shadowOffsetX: 0,
        shadowOffsetY: 0,
        shadowColor: 'rgba(0,0,0,0.5)'
    }
};
var bar_option={
      color:[
        "#dd4b3a"
        ],
    backgroundColor: 'rgba(0, 0, 0, 0)',
     dataZoom:[ {
        show: true,
        start : 50
    },
    {
                type: 'inside',
                start: 50,
                end: 100
            }
    ],
        calculable : true,
    legend: {},

    toolbox: {
            show : true,
            feature : {
                mark : {show: true},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
    tooltip: {},
    xAxis: {
        data: data.legendData,
        name: 'Highlight Text',
        silent: false,
        axisLine: {onZero: true},
        splitLine: {show: false},
        splitArea: {show: false},
         axisLabel: {
            textStyle: {
                color: '#fff'
            },
            rotate:90,
        },
    },
    yAxis: {
        inverse: false,
        name: 'weight',
        splitArea: {show: false},

    },
   grid: {
            top: '12%',
            left: '1%',
            right: '10%',
         
            containLabel: true
        },
  
    series: [
        {
            name: 'Promotor Score',
            type: 'bar',
            stack: 'one',
            color:["#dd4b3a"],
            itemStyle: itemStyle,
            data: data.yData
        }
        
    ]
};

function genData(data) {
    var legendData = [];
    var yData = [];
    var seriesData = [];
    var selected = {};
    var i = 0;
     for(var key in data) 
        {
         var name = data[key].page_name;
         var value=data[key].netscore;
          legendData.push(name);
          yData.push(value);
          seriesData.push({name: name,value:value});
          if( parseInt(i) < 6)
          {
          selected[name] =true;
            i +=1;
        }
          else
          selected[name] =false;
          
        }
console.log("dataforcal");console.log(legendData);
   
     return {
        legendData: legendData,
        seriesData: seriesData,
        selected: selected,
        yData:yData
    };

    
};
 $("#promotor-spin").hide();
pieChart.setOption(bar_option, true), $(function() {
    function resize() {
        setTimeout(function() {
            pieChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});




   
    })
    .fail(function() {
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });
  }
  

$('.dateranges').daterangepicker({
    locale: {
            format: 'MMMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end) {//alert("hihi")
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,'');
      });



      $(".btn-group > .btn").click(function(){
       periodType= $(this).attr('id'); 
       //$(this).addClass("active");
         ChooseDate(startDate,endDate,'cal_graph');
      
 
});


    ChooseDate(startDate,endDate,'');



//local customize function



     function kFormatter(num) {
    return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
  function readmore(message){
      // alert("hi hi ");
        var string = String(message);
        var length = string.length; 
         // alert(length);
                if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.
             
            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
        }
        return string;


        }

         function judge_sentiment_icon(sentiment)
        {
         var emojis = ['0x1F60E', '0x1F626', '0x1F610'];//
        if (sentiment ==="pos") return  String.fromCodePoint(emojis[0]) + " positive"; 
        else if(sentiment ==="neg") return  String.fromCodePoint(emojis[1])+ " negative" ;
        else return String.fromCodePoint(emojis[2]) + " neutral" ;
        }

        function judge_emotion_icon(emotion)
        {
            var emojis = ['0x1F620', '0x1F604', '0x1F61D', '0x1F628', '0x1F604', '0x1F44D',
            '0x2764', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

        if (emotion ==="anger") return emojis[0]; 
        else if(emotion ==="anticipation") return emojis[1] ;
         if (emotion ==="disgust") return emojis[2] ; 
        else if(emotion ==="fear") return emojis[3] ;
         if (emotion ==="joy") return emojis[4]; 
        else if(emotion ==="like") return emojis[5] ;
         if (emotion ==="love") return emojis[6] ; 
        else if(emotion ==="neutral") return emojis[7] ;
         if (emotion ==="sadness")  return emojis[8]; 
        else if(emotion ==="surprise") return emojis[9]  ;
         else if(emotion === "trust") return emojis[10];

        }

  });
    </script>

@endpush


<!-- var data = genData(50);

option = {
    title : {
        text: 'Statistics of the same name',
        subtext: '纯属虚构',
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: data.legendData,

        selected: data.selected
    },
    series : [
        {
            name: 'Name',
            type: 'pie',
            radius : '55%',
            center: ['40%', '50%'],
            data: data.seriesData,
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};




function genData(count) {
    var nameList = [
        '赵', '钱', '孙', '李', '周', '吴', '郑', '王', '冯', '陈', '褚', '卫', '蒋', '沈', '韩', '杨', '朱', '秦', '尤', '许', '何', '吕', '施', '张', '孔', '曹', '严', '华', '金', '魏', '陶', '姜', '戚', '谢', '邹', '喻', '柏', '水', '窦', '章', '云', '苏', '潘', '葛', '奚', '范', '彭', '郎', '鲁', '韦', '昌', '马', '苗', '凤', '花', '方', '俞', '任', '袁', '柳', '酆', '鲍', '史', '唐', '费', '廉', '岑', '薛', '雷', '贺', '倪', '汤', '滕', '殷', '罗', '毕', '郝', '邬', '安', '常', '乐', '于', '时', '傅', '皮', '卞', '齐', '康', '伍', '余', '元', '卜', '顾', '孟', '平', '黄', '和', '穆', '萧', '尹', '姚', '邵', '湛', '汪', '祁', '毛', '禹', '狄', '米', '贝', '明', '臧', '计', '伏', '成', '戴', '谈', '宋', '茅', '庞', '熊', '纪', '舒', '屈', '项', '祝', '董', '梁', '杜', '阮', '蓝', '闵', '席', '季', '麻', '强', '贾', '路', '娄', '危'
    ];
    var legendData = [];
    var seriesData = [];
    var selected = {};
    for (var i = 0; i < 50; i++) {
        name = Math.random() > 0.65
            ? makeWord(4, 1) + '·' + makeWord(3, 0)
            : makeWord(2, 1);
        legendData.push(name);
        seriesData.push({
            name: name,
            value: Math.round(Math.random() * 100000)
        });
        selected[name] = i < 6;
    }

    return {
        legendData: legendData,
        seriesData: seriesData,
        selected: selected
    };

    function makeWord(max, min) {
        var nameLen = Math.ceil(Math.random() * max + min);
        var name = [];
        for (var i = 0; i < nameLen; i++) {
            name.push(nameList[Math.round(Math.random() * nameList.length - 1)]);
        }
        return name.join('');
    }
}
 -->