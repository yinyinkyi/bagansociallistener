@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')

  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Analysis</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Analysis</a></li>
                            <li class="breadcrumb-item active"> @if (isset($project_name)) {{$project_name}} @endif</li>
                        </ol>
                    </div>
                     <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                            <!-- <div class="btn-group btn-group-lg  mb-3" role="group" aria-label="Basic example">
                                            <button id="day" type="button" class="btn btn-success">Day</button>
                                            <button id="week" type="button" class="btn btn-success">Week</button>
                                            <button  id="month" type="button" class="btn btn-success">Month</button>
                            </div> -->
                            <div class="btn-group btn-group-lg  mb-3" data-toggle="buttons">
                                            <label class="btn btn-primary">
                                                <input type="radio" name="options" value="day" autocomplete="off" class="btn btn-success" > Day
                                            </label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="options" value="week" autocomplete="off" class="btn btn-success"> Week
                                            </label>
                                            <label class="btn btn-primary active">
                                                <input type="radio" name="options" value="month" autocomplete="off" class="btn btn-success" checked> Month
                                            </label>
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                            <div class='input-group mb-3'>
                                <input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            </div>
                          <!--   <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                  <!-- interest graph -->
                <div class="row" id="analysis-div-1">
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Enquiries</h4>
                                 <div style="display:none"  align="center" style="vertical-align: top;" id="interest-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                 <div id="interest-bar-chart" style="width:100%; height:400px;"></div>   
                            </div>
                        </div>
                    </div>
                
                </div>
                <!-- Row -->
            <!--     <div class="row">
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                           <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Mentions</h3>
                                          
                                      
                            
                                    </div>
                         
                                </div>
                               <div style="display:none"  align="center" style="vertical-align: top;" id="mention-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             
                              <div id="mention-chart" style="width:100%; height:400px;"></div>   
                           
                            </div>
                        </div>
                    </div>
               
                </div> -->
                <div class="row" >
                    <!-- Column -->
                       <div class="col-lg-4 col-md-4" id="analysis-div-2">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h3 class="card-title">Gender</h3>
                                       
                                    </div>
                                 
                                </div>
                                   <div style="display:none"  align="center" id="gender-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           
                           <div id="gender-pie-chart" style="width:100%; height:200px;"></div>
                            </div>
                        </div>
                    </div>
                                  <!-- Column -->
                    <!-- Column -->
                   <div class="col-lg-4 col-md-4" id="analysis-div-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h3 class="card-title">Location</h3>
                                       
                                    </div>
                                 
                                </div>
                                   <div style="display:none"  align="center" id="location-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           
                           <div id="location-pie-chart" style="width:100%; height:200px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-4" id="analysis-div-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h3 class="card-title">Age</h3>
                                       
                                    </div>
                                 
                                </div>
                                   <div style="display:none"  align="center" id="age-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           
                           <div id="age-pie-chart" style="width:100%; height:200px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- Row -->
                <div class="row" id="analysis-div-5">
               <div class="col-lg-12" >
                        <div class="card">
                            <div class="card-body">

                                <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Social Media Reach</h3>
                                          
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                                <div style="display:none" width="100%" align="center" style="vertical-align: top;" id="sentiment-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                               <div id="sentiment-bar-chart" style="width:100%; height:400px;"></div>
                         
                            </div>
                        </div>
                    </div>
                </div>
               <!--  <div class="row" id="analysis-div-6">
               <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Social Media Reach</h3>
                                        
                                                         
                                    </div>
                         
                                </div>
                                <div style="display:none"  align="center" style="vertical-align: top;" id="socialmedia-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                               <div id="socialmedia-bar-chart" style="width:100%; height:400px;">

                               </div>

                         
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row" id="analysis-div-7">
               <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Reaction</h3>
                                         
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                                 <div style="display:none" width="100%" align="center" id="reaction-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                               <div id="reaction-bar-chart" style="width:100%; height:400px;"></div>
                         
                            </div>
                        </div>
                    </div>
                </div>
                  <div class="row" id="dashboard-div-9">
                    <div class="col-lg-12">
                        <div class="card">
                        
                         <ul class="nav nav-tabs profile-tab" role="tablist">
<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#interest_highlighted" role="tab">Interest Hilighted Voice</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#all_highlighted" role="tab">All</a> </li>
                                
                          </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="interest_highlighted" role="tabpanel">
                                    <!-- <div class="card-body"> -->
                                <div class="card-body">
                        
                                <div style="display:none"  align="center" id="hightlighted-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div style="width:100%; height:800px;" id="hightlighted" ></div>   
                            </div>
                         
                                       
                                    <!-- </div> -->
                                              </div>
<div class="tab-pane" id="all_highlighted" role="tabpanel">
                                    <!-- <div class="card-body"> -->
  <div class="card-body">
  <div style="display:none"  align="center" id="highlighted-all-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
  <div style="width:100%; height:800px;" id="highlighted-all" ></div>   
  </div>
                                       
                                    <!-- </div> -->
                                              </div>
                                          </div>
                     

                        </div>

                    </div>
          </div>
           <div class="row" id="dashboard-div-9">
                    <div class="col-lg-12">
                        <div class="card">
                        
                         <ul class="nav nav-tabs profile-tab" role="tablist">
<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#positive_wc" role="tab">Positive Voice</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#negative_wc" role="tab">Negative Voice</a> </li>
                                
                          </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="positive_wc" role="tabpanel">
                                    <!-- <div class="card-body"> -->
                                <div class="card-body">
                        
                                <div style="display:none"  align="center" id="positiveWC-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div style="width:100%; height:800px;" id="positive_wc" ></div>   
                            </div>
                         
                                       
                                    <!-- </div> -->
                                              </div>
<div class="tab-pane" id="negative_wc" role="tabpanel">
                                    <!-- <div class="card-body"> -->
  <div class="card-body">
  <div style="display:none"  align="center" id="negativeWC-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
  <div style="width:100%; height:800px;" id="negative_wc" ></div>   
  </div>
                                       
                                    <!-- </div> -->
                                              </div>
                                          </div>
                     

                        </div>

                    </div>
          </div>
                <!-- Row -->
                
                <!-- Row -->
                <!-- Row -->
                <!-- <div class="row" id="analysis-div-8">
                  
                       <div class="col-lg-12">
                        <div class="card">
                       <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Popular Negative Post</a> </li>
                              
                            </ul>
                          
                            <div class="tab-content">
                                <div class="tab-pane active" id="home" role="tabpanel">
                                        <div style="display:none"  align="center" id="popular-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>

                                    <div class="card-body">
                                        <div class="profiletimeline" id="popular-negative">
                                      
                                            
                                        </div>
                                    </div>
                                </div>
                          
                            </div>
                        </div>
                    </div>
                </div> -->

            <!--     <div class="row" id="analysis-div-9">
               <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Net Promotor Score</h3>
                         
                                    </div>
                         
                                </div>
                 <div style="display:none"  align="center" id="promotor-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                 <div id="promotor-pie-chart" style="width:100%; height:600px;"></div>
                         
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row" id="analysis-div-10">
                    <!-- Column -->
                     <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h3 class="card-title">Highlighted Voice</h3>
                                 </div>
                                 
                                </div>
                                 <div style="display:none"  align="center" style="vertical-align: top;" id="highlight-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                     <div id="highlight-bar-chart" style="width:100%; height:1500px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme working">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/1.jpg')}}" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/2.jpg')}}" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/3.jpg')}}" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/4.jpg')}}" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/5.jpg')}}" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/6.jpg')}}" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/7.jpg')}}" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/8.jpg')}}" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
   <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script> -->
   <!--  <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script> -->
 
    
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
<!--   <style type="text/css">
    .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show>.btn-primary.dropdown-toggle {
    color: #fff;
    background-color: #043e38;
    border-color: #141515;    
  </style>
 -->    <script type="text/javascript">
var startDate;
var endDate;
var mention_total;
var socials = [];
var socialLabel = [];
var social_total = 0;
var colors =["#98c0d8","#7b858e","#01ad9d","#cb73a9","#a1c652","#dc3545","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];

/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/
$(function() {
    startDate = moment().subtract(3, 'month');
    endDate = moment();
    var periodType= '';
           /* $('.btn-group .active').each(function(){
                periodType= $(this).attr('id'); 
                
            });*/
            periodType=$('input[name=options]:checked').val();
            //alert(periodType);
            if(periodType == '')
            {
              periodType='month';
            }
                  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
   window.dispatchEvent(new Event('resize'));

 });
 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Analysis'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
hidden_div();


function ChooseDate(start,end,calType)
{
   $('.dateranges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          startDate=start;
          endDate=end;
      var brand_id = GetURLParameter('pid');
          var start = new Date(startDate);
           var end = new Date(endDate);
           var timeDiff = Math.abs(end.getTime() - start.getTime());
           var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
      
           
          if(parseInt(diffDays) < 7) //in terms of day
          { 
            //alert("7");
             $("#day").show();
             $("#week").hide();
             $("#month").hide();
          }
         
          
         else if (parseInt(diffDays) == 7)//in terms of week
          {
            
           //alert("==7");
             $("#day").show();
             $("#week").show();
             $("#month").hide();
              
   
          }
          else if (parseInt(diffDays) > 7)//in terms of month
          {
            //alert(">7");
         
             $("#day").show();
             $("#week").show();
             $("#month").show();
                 
        }

       // mentiondetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,periodType);
       // sentimentdetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,periodType);
        requestinterestData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,periodType);
        socialmediadetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,periodType);
        GenderData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
        if(calType === '')
        {
       // popular_negative(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
       // PromotorData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
       highlighted_all(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
        highlighted_interest(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
        highlighted(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
        }

}


// interest graph
function requestinterestData(fday,sday){//alert(fday);
      
    var sachart = document.getElementById('interest-bar-chart');
    var SAChart = echarts.init(sachart);

/*$("#mentionreaction-spin").show();  */
       var brand_id = GetURLParameter('pid');
       // dd($brand_id);
       // var brand_id = 59;
       // alert (brand_id);
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getInboundinterest')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:periodType }
    })
    .done(function( data ) {//alert(data);
     


     var comment_count = [];
     var comments_total =0;
     var interest_total =0;
     var interest_count = [];
     var interestLabel =[];


       for(var i in data) 
        {
        
          comment_count.push(data[i].comment_count);
          interestLabel.push(data[i].periodLabel);
          interest_count.push(parseInt(data[i].interest_sum));
          
        }
        console.log(interest_count);
        // console.log(comment_count);
$.each(comment_count,function(){comments_total+=parseInt(this) || 0;});

var formal_comments_total=comments_total; // pass parameter to ReactionData Function
comments_total = kFormatter(comments_total);

$.each(interest_count,function(){interest_total+=parseInt(this) || 0;});
// console.log(interest_count);
var formal_interest_total=interest_total; // pass parameter to ReactionData Function
interest_total = kFormatter(interest_total);
// console.log(interest_total);



option = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross'
        },
        formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
       /* console.log(rez);*/ //quite useful for debug
        params.forEach(item => {
            /*console.log(item);
            console.log(item.data);*/
            var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName + ': ' + kFormatter(item.data)  + '</p>'
            rez += xx;
        });

        return rez;
    }
    },
    grid: {
        right: '20%'
    },
  /*  toolbox: {
        feature: {
            dataView: {show: true, readOnly: false},
            restore: {show: true},
            saveAsImage: {show: true}
        }
    },*/
     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },

    legend: {
        data:['Comments', 'Enquiries'],
          formatter: function (name) {
            if(name === 'Comments')
            {
                 return name + ': ' + comments_total;
            }
            return name  + ': ' + interest_total;

   
}
    },
    xAxis: [
        {
            type: 'category',
            axisTick: {
                alignWithLabel: true
            },
                         axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
     var date = new Date(value);
       console.log(date);
      var texts = [date.getFullYear(), monthNames[date.getMonth()]];

    return texts.join('-');

}
    },
            data: interestLabel
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Comments',
            /*  min: 0,
            max: 250,*/

            position: 'left',
            axisLine: {
                lineStyle: {
                    color: colors[0]
                }
            }
        },
  
    ],
    series: [
        {
            name:'Comments',
            type:'bar',
            barMaxWidth:30,
            color:colors[0],
            data:comment_count,
             markPoint : {
              large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },
        {
            name:'Enquiries',
            type:'bar',
            barMaxWidth:30,
            color:colors[1],
            // yAxisIndex: 1,
            data:interest_count,
            markPoint : {
              large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        }
    ]
};
$("#interest-spin").hide();
    SAChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            SAChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
    // requestsocialData(socials,social_total,reactionLabel);
    // ReactionData(Like_total,Love_total,Wow_total,Sad_total,Angry_total,Haha_total,formal_reactions_total);
   SAChart.on('click', function (params) {
   console.log(params);
   console.log(params.name); // xaxis data = 2018-08
   console.log(params.seriesName); //bar period name ="Comments"
   console.log(params.value);//count
   var pid = GetURLParameter('pid');
   if(params.name !== 'minimum' && params.name !== 'maximum' && params.seriesName !=='Comments' )
   window.open("{{ url('pointoutcomment?')}}" +"pid="+ pid+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&period="+ params.name +"&type=interest&from_graph=enquiry" , '_blank');
   //window.location="{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName ;
   
});
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }








    function sentimentdetail(fday,sday,brand_id,periodType){//alert(fday),alert(sday);
   //alert("hihi");
  
    var sentiment_chart = document.getElementById('sentiment-bar-chart');
    var sentimentChart = echarts.init(sentiment_chart);

$("#sentiment-spin").show();


        var brand_id = GetURLParameter('pid');
       // alert (brand_id);
      
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getinboundsentiment')}}", // This is the URL to the API
      data: {fday:fday,sday:sday,brand_id:brand_id,periodType:periodType}
    })
    .done(function( data ) {


       console.log(data);
    
    /*   $("#mention-total").text("Total = "+data[data.length-1].total);
       $("#social-total").text("Total = "+data[data.length-1].total);
       var positive_total= data[data.length-1].total+10;
       $("#sentiment-total").text("Positive = "+ positive_total + " && Negative = "+data[data.length-1].total);*/

      // When the response to the AJAX request comes back render the chart with new data
     
      var positive = [];
      var negative = [];
     
      var sentimentLabel = [];

      var positive_total=0;
      var negative_total=0;



        for(var i in data) {//alert(data[i].mentions);
        positive.push( Math.round(data[i].positive));
        negative.push( Math.round(data[i].negative));
        sentimentLabel.push(data[i].periodLabel);
        
      }


$.each(positive,function(){positive_total+=parseInt(this) || 0;});
$.each(negative,function(){negative_total+=parseInt(this) || 0;});
var positive_percentage= (positive_total/mention_total)*100;
var negative_percentage= (negative_total/mention_total)*100;
var neutral_percentage = 100 - (positive_percentage+negative_percentage);



 $('#positive-progress').css('width', positive_percentage+'%').attr('aria-valuenow', positive_percentage);  
 $('#negative-progress').css('width', negative_percentage+'%').attr('aria-valuenow', negative_percentage);
 $('#neutral-progress').css('width', neutral_percentage+'%').attr('aria-valuenow', neutral_percentage);

 $("#positive-total").text(positive_percentage + "%");
 $("#negative-total").text(negative_percentage + "%");
 $("#neutral-total").text(neutral_percentage + "%");


    option = {
        color:colors,
      

        tooltip : {
            trigger: 'axis'
                  },
            grid: {
          top:    60,
    
    left:   '5%',
    right:  '15%',
            containLabel: true
        },

        legend: {
            data:['Positive','Negative','social media reach'],
            formatter: function (name) {
            if(name === 'Positive')
            return name + ': ' + positive_total;
            if(name === 'Negative')
            return name  + ': ' + negative_total;
            else
            return name  + ': ' + social_total;
},
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
        {
            type : 'category',
              axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
  
     var arr = value.split(' - ');
    var date = new Date(arr[0]);
       console.log(date);
if(periodType === 'day')
{
    var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];
   
}
else 
{

      var texts = [date.getFullYear(), monthNames[date.getMonth()]];
}



    return texts.join('-');

}
    },
            data : sentimentLabel
        }
        ],
         yAxis: [
        {
            type: 'value',
            name: 'Sentiment',
          /*  min: 0,
            max: 250,*/
            position: 'left',
            axisLine: {
                lineStyle: {
                    color: colors[0]
                }
            }
    
        },
        {
            type: 'value',
            name: 'social media reach',
           /* min: 0,
            max: 250,*/
            position: 'right',
            offset: 80,
            axisLine: {
                lineStyle: {
                    color: colors[2]
                }
            },
            /*axisLabel: {
                formatter: '{value} ml'
            }*/
              axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }
    }
        }
    ],
        series : [
        {
            name:'Positive',
            type:'bar',
            barMaxWidth:30,
            data:positive,
            color:colors[0], // "#00a55a","#de4b3a",

          /*  markPoint : {
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }*//*,
            markLine : {
                data : [
                {type : 'average', name: 'average'}
                ]
            }*/
        },
        {
            name:'Negative',
          /*  type:effectIndex % 2 == 0 ? 'bar' : 'line',*/
            type:'bar',
            barMaxWidth:30,
            data:negative,
             color:colors[1],
           /* markPoint : {
              data : [
              {type : 'max', name: 'maximum'},
              {type : 'min', name: 'minimum'}
              ]
          }*//*,
          markLine : {
            data : [
            {type : 'average', name : 'average value'}
            ]
        }*/
    },
        {
            name:'social media reach',
            type:'line',
            color:colors[2],
            yAxisIndex: 1,
            data:socials,
            markPoint : {
              large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        }
    ]
};
    
$("#sentiment-spin").hide();

   // sentimentChart.setOption(option);
sentimentChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            sentimentChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

    sentimentChart.on('click', function (params) {
  // console.log(params);
   console.log(params.name); // xaxis data = 2018-08
   console.log(params.seriesName); //bar period name ="Positive"
   console.log(params.value);//count
   var pid = GetURLParameter('pid');
   var source= GetURLParameter('source');
  if(params.name !== 'minimum' && params.name !== 'maximum' )
window.open("{{ url('pointoutcomment?')}}" +"pid="+ pid+"&source="+ source+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&period="+ params.name +"&type="+params.seriesName+"&from_graph=sentiment" , '_blank');
   
});

    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }
    function socialmediadetail(fday,sday,brand_id,periodType)
    {
    //   var socialchart = document.getElementById('socialmedia-bar-chart');
    // var socialChart = echarts.init(socialchart);

      var reactionchart = document.getElementById('reaction-bar-chart');
    var ReactionChart = echarts.init(reactionchart);

$("#socialmedia-spin").show();
        $("#reaction-spin").show();
        var brand_id = GetURLParameter('pid');
        //var brand_id = 22;
       // alert (brand_id);
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getinboundReaction')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:periodType }
    })
    .done(function( data ) {//alert(data);
  
    
        
       console.log(data);//mention

      var Like = [];
      var Love = [];
      var Haha = [];
      var Wow = [];
      var Angry = [];
      var Sad = [];
    

      var Like_total =0;
       var Love_total =0;
       var Haha_total =0;
       var Wow_total =0;
       var Angry_total =0;
       var Sad_total =0;

   
       socials = [];

       socialLabel = [];
       socialsfortotal = [];
 
      social_total=0;



       for(var i in data) 
        {
          Like.push( Math.round(data[i].Like));
          Love.push( Math.round(data[i].Love));
          Haha.push( Math.round(data[i].Haha));
          Wow.push( Math.round(data[i].Wow));
          Angry.push( Math.round(data[i].Angry));
          Sad.push( Math.round(data[i].Sad));


          var mediaReach=parseInt(data[i].Like)+parseInt(data[i].Love)+parseInt(data[i].Haha)+parseInt(data[i].Wow)+parseInt(data[i].Angry)+parseInt(data[i].Sad)+parseInt(data[i].post_count)
             socials.push( Math.round(mediaReach));
          socialsfortotal.push( Math.round(mediaReach));
          socialLabel.push(data[i].periodLabel);
          
        }

$.each(Like,function(){Like_total+=parseInt(this) || 0;});
$.each(Love,function(){Love_total+=parseInt(this) || 0;});
$.each(Haha,function(){Haha_total+=parseInt(this) || 0;});
$.each(Wow,function(){Wow_total+=parseInt(this) || 0;});
$.each(Angry,function(){Angry_total+=parseInt(this) || 0;});
$.each(Sad,function(){Sad_total+=parseInt(this) || 0;});
$.each(socialsfortotal,function(){social_total+=parseInt(this) || 0;});

social_total = kFormatter(social_total);

/* $("#mention-total").text("Total = "+mention_total);*/
reaction_option = {
        color:colors,
      

       tooltip : {
            trigger: 'axis'

        },
        legend: {
            data:['Like','Love','Ha Ha','Wow','Angry','Sad' ],
                formatter: function (name) {
            if(name === 'Like')return name + ': ' + kFormatter(Like_total) ;
            if(name === 'Love')return name + ': ' + kFormatter(Love_total);
            if(name === 'Ha Ha')return name + ': ' + kFormatter(Haha_total);
            if(name === 'Wow')return name + ': ' + kFormatter(Wow_total);
            if(name === 'Angry')return name + ': ' + kFormatter(Angry_total);
            if(name === 'Sad')return name + ': ' + kFormatter(Sad_total);
         
            },
             padding :0,
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
        {
            type : 'category',
              axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
  
     var arr = value.split(' - ');
    var date = new Date(arr[0]);
       console.log(date);
if(periodType === 'day')
{
    var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];
   
}
else 
{

      var texts = [date.getFullYear(), monthNames[date.getMonth()]];
}



    return texts.join('-');

}
    },
            data : socialLabel
        }
        ],
        yAxis : [
        {
            type : 'value',
           
         

                      axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }
    }
              
        }
        ],

        series : [
        {
            name:'Like',
            type:'bar',
            data:Like,
            color:colors[0],
    markPoint : {
                  large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },

        {
            name:'Love',
            type:'bar',
            data:Love,
            color:colors[5],
            markPoint : {
                large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },

        {
            name:'Ha Ha',
            type:'bar',
            data:Haha,
            color:colors[2],
            markPoint : {
                large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },

        {
            name:'Wow',
            type:'bar',
            data:Wow,
            color:colors[3],
             markPoint : {
                large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },
 
        {
            name:'Angry',
            type:'bar',
            data:Angry,
            color:colors[4],
             markPoint : {
                large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },

        {
            name:'Sad',
            type:'bar',
            data:Sad,
            color:colors[1],
         markPoint : {
                large:true,
               label: {
                                    normal: {
                                        formatter: function (param) {
                                            return kFormatter(param.value);
                                        },
                                        textStyle: {
                                            color: '#f8fbfb'
                                        },
                                        position: 'inside'
                                    }
                                },
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        }
        
    ]
};


$("#reaction-spin").hide();

 sentimentdetail(fday,sday,brand_id,periodType);
 ReactionChart.setOption(reaction_option, true), $(function() {
    function resize() {
        setTimeout(function() {
            ReactionChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
   ReactionChart.on('click', function (params) {
   console.log(params);
   console.log(params.name); // 2018-07
   console.log(params.seriesName); //bar period name ="Positive"
   console.log(params.value);//count
   var pid = GetURLParameter('pid');
   var source= GetURLParameter('source');
    if(params.name !== 'minimum' && params.name !== 'maximum' )
    window.open("{{ url('pointoutpost?')}}" +"pid="+ pid+"&source="+ source+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&period="+ params.name +"&emotion_type="+ params.seriesName +"&from_graph=reaction" , '_blank');
   
   
});
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }
 function popular_negative(fday,sday)
{
    var brand_id = GetURLParameter('pid');
    $( "#popular-spin" ).show();
    $("#popular-negative").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getpopularnegative')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {//$("#popular-negative").html("");
      $( "#popular-spin" ).hide();
       console.log(data);
       //latest
        for(var i in data) {//alert(data[i].mentions);
       // alert(data[i].message);

       var html='<div class="sl-item">'+
               '  <div class="sl-left">'+
               '<img src="'+data[i].full_picture+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
               
                ' <div class="sl-right">'+
                '<div><a href="'+data[i].link+'" class="link" target="_blank">'+data[i].page_name+'</a> <span class="sl-date">'+data[i].created_time+'</span><a> '+
                 '</div>'+
             '<p>Sentiment: <select class="form-control custom-select sentiment-color customize-select" id="sentiment_popular_'+data[i].id+'"  >';
               if(data[i].sentiment == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="pos"';
                if(data[i].sentiment == "pos")
                html+= 'selected="selected"';
                html+= '>pos</option><option value="neg"';
                if(data[i].sentiment == "neg")
                html+= 'selected="selected"';
                html+= '>neg</option><option value="neutral"';
                if(data[i].sentiment == "neutral")
                html+= 'selected="selected"';     
                html+= '>neutral</option><option value="NA"';
                if(data[i].sentiment == "NA")
                html+= 'selected="selected"';  
                html+= '>NA</option></select>';

                html+= ' | Emotion: <select class="form-control custom-select emotion-color customize-select" id="emotion_popular_'+data[i].id+'"  >';
                if(data[i].emotion == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="anger"';
                if(data[i].emotion == "anger")
                html+= 'selected="selected"';
                html+= '>anger</option><option value="interest"';
                if(data[i].emotion == "interest")
                html+= 'selected="selected"';     
                html+= '>interest</option><option value="disgust"';
                if(data[i].emotion == "disgust")
                html+= 'selected="selected"';  
                html+= '>disgust</option><option value="fear"';
                if(data[i].emotion == "fear")
                html+= 'selected="selected"'; 
                html+= '>fear</option><option value="joy"';
                if(data[i].emotion == "joy")
                html+= 'selected="selected"'; 
                html+= '>joy</option><option value="like"';
                if(data[i].emotion == "like")
                html+= 'selected="selected"'; 
                html+= '>like</option><option value="love"';
                if(data[i].emotion == "love")
                html+= 'selected="selected"'; 
                html+= '>love</option><option value="neutral"';
                if(data[i].emotion == "neutral")
                html+= 'selected="selected"';
                html+= '>neutral</option><option value="sadness"';
                if(data[i].emotion == "sadness")
                html+= 'selected="selected"';
                html+= '>sadness</option><option value="surprise"';
                if(data[i].emotion == "surprise")
                html+= 'selected="selected"';
                html+= '>surprise</option><option value="trust"';
                if(data[i].emotion == "trust")
                html+= 'selected="selected"';
                html+= '>trust</option><option value="NA"';
                if(data[i].emotion == "NA")
                html+= 'selected="selected"';
                html+= '>NA</option></select>';
                if(data[i].edit_permission === 1)
                html+= ' <a class="edit_predict_popular" id="'+data[i].id+'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a> ';
                html+=' </span></p> ' +                
                  '   <p class="m-t-10" align="justify">' + 
                       readmore(data[i].message) +
                         '...<a href="'+data[i].link+'" target="_blank">' + 
                       ' Read More</a>' + 
                    '</p> </div>';
 $("#popular-negative").append(html);
     

                            }
  

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}

function GenderData(fday,sday){//alert(fday),alert(sday);

    var brand_id = GetURLParameter('pid');
     //var brand_id = 22;
     $("#gender-spin").show();
     $("#location-spin").hide();
     $("#age-spin").hide();
      //$("#reaction-count").empty();


      
      var town=['Yangon','Mandalay','NayPyiTaw','Ayarwaddy','Shan'];
      var age=['10-19 years','20-45 years','46-60 years','> 60'];
     

         var pieChart = echarts.init(document.getElementById('gender-pie-chart'));
         var locationChart = echarts.init(document.getElementById('location-pie-chart'));
         var ageChart=echarts.init(document.getElementById('age-pie-chart'));

// specify chart configuration item and data
option = {

    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        x: 'center',
        y: 'bottom',
        data: ['male','female']
    },
    color: colors,
    calculable: true,
    series: [
        {
            name: 'Area mode',
            type: 'pie',
            radius : '50%',
            center: ['50%', '40%'],
            x: '50%', // for funnel
            max: 40, // for funnel
            sort: 'ascending', // for funnel
            data: [
                { value:20, name: 'male' },
                { value:90, name:  'female'  },
          
            ],

            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};

locationoption = {

    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        x: 'center',
        y: 'bottom',
        data: town
    },
    color: colors,
    calculable: true,
    series: [
        {
            name: 'Area mode',
            type: 'pie',
            radius : '50%',
            center: ['50%', '40%'],
            x: '50%', // for funnel
            max: 40, // for funnel
            sort: 'ascending', // for funnel
            data: [
                { value:20, name: town[0] },
                { value:90, name: town[1]  },
                { value:90, name: town[2]  },
                { value:90, name: town[3]  },
                { value:90, name: town[4]  },
          
            ],

            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
ageoption = {

    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        x: 'center',
        y: 'bottom',
        data: age
    },
    color: colors,
    calculable: true,
    series: [
        {
            name: 'Area mode',
            type: 'pie',
            radius : '50%',
            center: ['50%', '40%'],
            x: '50%', // for funnel
            max: 40, // for funnel
            sort: 'ascending', // for funnel
            data: [
                { value:20, name: age[0] },
                { value:30, name: age[1]  },
                { value:70, name: age[2]  },
                { value:20, name: age[3]  },
            
          
            ],

            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};

  $("#gender-spin").hide();
   $("#location-spin").hide();
     $("#age-spin").hide();
// use configuration item and data specified to show chart
pieChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            pieChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

locationChart.setOption(locationoption, true), $(function() {
    function resize() {
        setTimeout(function() {
            locationChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

ageChart.setOption(ageoption, true), $(function() {
    function resize() {
        setTimeout(function() {
            ageChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});


     }
   
     function GetStartDate()
{
   //alert(startDate);
 return startDate.format('YYYY MM DD');
}
function GetEndDate()
{
           // alert(endDate);
return endDate.format('YYYY MM DD');
}

$('.dateranges').daterangepicker({
    locale: {
            format: 'MMMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end) {//alert("hihi")
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,'');
      });



/*      $(".btn-group > .btn").click(function(){//alert("hihi");
      // periodType= $(this).attr('id'); 
       periodType=$('input[name=options]:checked').val();
       alert(periodType);
       //$(this).addClass("active");
         ChooseDate(startDate,endDate,'cal_graph');
      
 
});*/

      $('input[name=options]').mouseup(function(){
    //alert("Before change "+$('input[name=options]:checked').val());
}).change(function(){
     periodType=$('input[name=options]:checked').val();
     ChooseDate(startDate,endDate,'cal_graph');
    //alert("After change "+$('input[name=options]:checked').val());
})


    ChooseDate(startDate,endDate,'');


$(document).on('click', '.edit_predict_popular', function() {
     var post_id = $(this).attr('id');
  //   alert(post_id);
    
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_popular_'+post_id+' option:selected').val()
  var emotion = $('#emotion_popular_'+post_id+' option:selected').text()
  //alert(post_id);
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {// alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });
});
//local customize function
function highlighted_all(fday,sday)
{
    var chart_all = echarts.init(document.getElementById('highlighted-all'));
    var brand_id = GetURLParameter('pid');
    //var brand_id = 22;
   $( "#highlighted-all-spin" ).show();

   //$("#highlighted").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getInHighlighted')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,type:'all'}
    })
    .done(function( data ) {
          console.log("test");
        console.log(data);
$( "#highlighted-all-spin" ).hide();
     
      if(data.length>0)
      {
     //var data =JSON.parse(data);
     var data = genData(data);


       var option = {
                tooltip: {},
                series: [ {
                    type: 'wordCloud',
                       left:'center',
                        top:'center',
                        width: '90%',
                        height: '90%',
                        right:null,
                        bottom:null,
                    sizeRange: [12, 50],
                    rotationRange: [0,0,0,0],
                    // rotationStep: 90,
                    shape: 'circle',//pentagon ratangle
                    gridSize:20,
                    height: 800,
                    drawOutOfBound: false,
                    textStyle: {
                        normal: {
                            color: function () {
                                var lightcolor = 'rgb(' + [
                                    Math.round(Math.random() * 160),
                                    Math.round(Math.random() * 160),
                                    Math.round(Math.random() * 160),
                                    2
                                ].join(',') + ')';
                                console.log(lightcolor);
                                return lightcolor;
                               /* return colors[0];*/

                            }
                        },
                        emphasis: {
                            shadowBlur: 10,
                            shadowColor: '#333'
                        }
                    },
                    data: data.seriesData
                }
                ]
            };
    function genData(data) {
    var seriesData = [];
    var xData = [];
    var yData = [];
    var i = 0;
     for(var key in data) 
        {
         var name = data[key].topic_name;
         var value=data[key].weight;
         var kvalue=parseFloat(value);
       

          seriesData.push({name: name,value:value});
        
          
        }


  /* jsonString=jsonString.split(["[","]"])*/
     return {
        
        seriesData: seriesData,
        xData:xData,
        yData:yData
        
    };

    
};


 chart_all.setOption(option, true), $(function() {
            function resize() {
              setTimeout(function() {
                chart_all.resize()
              }, 100)
            }
            $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
          });
  chart_all.resize();
    chart_all.on('click', function (params) {
   console.log(params);
   console.log(params.name); // xaxis data =ဈခတျေမီသညျ့ကိုအသုံးပွုထားခွငျး၊
   console.log(params.seriesName); //bar period name ="Positive"
   console.log(params.value);//count
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
  window.open("{{ url('pointoutcomment?')}}" +"pid="+ pid+"&source="+ source+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&highlight_text="+ params.name +"&from_graph=highlighted" , '_blank');
   
   
});

      }
       


    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}

function highlighted_interest(fday,sday)
{
    var chart = echarts.init(document.getElementById('hightlighted'));
    var brand_id = GetURLParameter('pid');
    //var brand_id = 22;
   $( "#hightlighted-spin" ).show();

   //$("#highlighted").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getInHighlighted')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,type:'interest'}
    })
    .done(function( data ) {
          console.log("test");
        console.log(data);
$( "#hightlighted-spin" ).hide();
     
      if(data.length>0)
      {
     //var data =JSON.parse(data);
     var data = genData(data);


       var option = {
                tooltip: {},
                series: [ {
                    type: 'wordCloud',
                       left:'center',
                        top:'center',
                        width: '90%',
                        height: '90%',
                        right:null,
                        bottom:null,
                    sizeRange: [12, 50],
                    rotationRange: [0,0,0,0],
                    // rotationStep: 90,
                    shape: 'circle',//pentagon ratangle
                    gridSize:20,
                    height: 800,
                    drawOutOfBound: false,
                    textStyle: {
                        normal: {
                            color: function () {
                                var lightcolor = 'rgb(' + [
                                    Math.round(Math.random() * 160),
                                    Math.round(Math.random() * 160),
                                    Math.round(Math.random() * 160),
                                    2
                                ].join(',') + ')';
                                console.log(lightcolor);
                                return lightcolor;
                               /* return colors[0];*/

                            }
                        },
                        emphasis: {
                            shadowBlur: 10,
                            shadowColor: '#333'
                        }
                    },
                    data: data.seriesData
                }
                ]
            };
    function genData(data) {
    var seriesData = [];
    var xData = [];
    var yData = [];
    var i = 0;
     for(var key in data) 
        {
         var name = data[key].topic_name;
         var value=data[key].weight;
         var kvalue=parseFloat(value);
       

          seriesData.push({name: name,value:value});
        
          
        }


  /* jsonString=jsonString.split(["[","]"])*/
     return {
        
        seriesData: seriesData,
        xData:xData,
        yData:yData
        
    };

    
};


 chart.setOption(option, true), $(function() {
            function resize() {
              setTimeout(function() {
                chart.resize()
              }, 100)
            }
            $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
          });
  chart.resize();
    chart.on('click', function (params) {
   console.log(params);
   console.log(params.name); // xaxis data =ဈခတျေမီသညျ့ကိုအသုံးပွုထားခွငျး၊
   console.log(params.seriesName); //bar period name ="Positive"
   console.log(params.value);//count
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
  window.open("{{ url('pointoutcomment?')}}" +"pid="+ pid+"&source="+ source+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&highlight_text="+ params.name +"&from_graph=highlighted" , '_blank');
   
   
});

      }
       


    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}

 function highlighted(fday,sday)
{
   var highlight_chart= echarts.init(document.getElementById('highlight-bar-chart'));
    var brand_id = GetURLParameter('pid');
    //var brand_id = 22;

   $("#highlight-spin").show();
   //$("#highlighted").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getInHighlighted')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {
    $( "#highlight-spin" ).hide();

      if(data.length>0)
      {
       //var data =JSON.parse(data);
     var data = genData(data);

   

var labelRight = {

    normal: {
        position: 'right',
       
    }
};
var bar_option = {
    title: {
       
    },
    tooltip : {
        trigger: 'axis',
        axisPointer : {            
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    grid: {
        top: 30,
        bottom: 30,
         left: 150
    },
    xAxis: {
        type : 'value',
        position: 'top',
        splitLine: {lineStyle:{type:'dashed'}},
    },
    yAxis: {
        type : 'category',
        axisLine: {show: false},
        axisLabel: {show: false},
        axisTick: {show: false},
        splitLine: {show: false},
        data : data.legendData
    },
    series : [
        {
            name:'Weight',
            type:'bar',
            barMaxWidth:30,
            stack: 'Total Amount',
            label: {
                normal: {
                    show: true,
                    formatter: '{b}'
                }
            },
            data:data.seriesData
        }
    ]
};

    function genData(data) {
    var legendData = [];
    var seriesData = [];
  
    var i = 0;
     for(var key in data) 
        {
              var yData1={};
              var yData2={};
         var name = data[key].topic_name;
         var value=data[key].weight;
         var fvalue=parseFloat(value);
        
          legendData.push(name);
       
          if(fvalue <0)
          {

          yData1['value']=fvalue;
          yData1['label']='labelRight';
          yData1['itemStyle']={color: '#86140e'};
          seriesData.push(yData1);
          
          }
          else
          {
      
          yData2['name']=name;
          yData2['value']=value;
          yData2['itemStyle']={color: '#3c8cbc'};
          seriesData.push(yData2);
          
          }

          
        }
 console.log("dataforcal");console.log(legendData);
   
     return {
        legendData: legendData,
        seriesData: seriesData
      
    };

    
};

 
 highlight_chart.setOption(bar_option, true), $(function() {
    function resize() {
        setTimeout(function() {
            highlight_chart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
   highlight_chart.on('click', function (params) {
   console.log(params);
   console.log(params.name); // xaxis data =ဈခတျေမီသညျ့ကိုအသုံးပွုထားခွငျး၊
   console.log(params.seriesName); //bar period name ="Positive"
   console.log(params.value);//count
   var pid = GetURLParameter('pid');
  window.open("{{ url('pointoutmention?')}}" +"pid="+ pid+"&highlight_text="+ params.name +"&from_graph=highlighted" , '_blank');
   
   
});
      }
      


    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}



     function kFormatter(num) {
    return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
  function readmore(message){
      // alert("hi hi ");
        var string = String(message);
        var length = string.length; 
         // alert(length);
                if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.
             
            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
        }
        return string;


        }

         function judge_sentiment_icon(sentiment)
        {
         var emojis = ['0x1F60E', '0x1F626', '0x1F610'];//
        if (sentiment ==="pos") return  String.fromCodePoint(emojis[0]) + " positive"; 
        else if(sentiment ==="neg") return  String.fromCodePoint(emojis[1])+ " negative" ;
        else return String.fromCodePoint(emojis[2]) + " neutral" ;
        }

        function judge_emotion_icon(emotion)
        {
            var emojis = ['0x1F620', '0x1F604', '0x1F61D', '0x1F628', '0x1F604', '0x1F44D',
            '0x2764', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

        if (emotion ==="anger") return emojis[0]; 
        else if(emotion ==="anticipation") return emojis[1] ;
         if (emotion ==="disgust") return emojis[2] ; 
        else if(emotion ==="fear") return emojis[3] ;
         if (emotion ==="joy") return emojis[4]; 
        else if(emotion ==="like") return emojis[5] ;
         if (emotion ==="love") return emojis[6] ; 
        else if(emotion ==="neutral") return emojis[7] ;
         if (emotion ==="sadness")  return emojis[8]; 
        else if(emotion ==="surprise") return emojis[9]  ;
         else if(emotion === "trust") return emojis[10];

        }

  });
    </script>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush


<!-- var data = genData(50);

option = {
    title : {
        text: 'Statistics of the same name',
        subtext: '纯属虚构',
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: data.legendData,

        selected: data.selected
    },
    series : [
        {
            name: 'Name',
            type: 'pie',
            radius : '55%',
            center: ['40%', '50%'],
            data: data.seriesData,
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};




function genData(count) {
    var nameList = [
        '赵', '钱', '孙', '李', '周', '吴', '郑', '王', '冯', '陈', '褚', '卫', '蒋', '沈', '韩', '杨', '朱', '秦', '尤', '许', '何', '吕', '施', '张', '孔', '曹', '严', '华', '金', '魏', '陶', '姜', '戚', '谢', '邹', '喻', '柏', '水', '窦', '章', '云', '苏', '潘', '葛', '奚', '范', '彭', '郎', '鲁', '韦', '昌', '马', '苗', '凤', '花', '方', '俞', '任', '袁', '柳', '酆', '鲍', '史', '唐', '费', '廉', '岑', '薛', '雷', '贺', '倪', '汤', '滕', '殷', '罗', '毕', '郝', '邬', '安', '常', '乐', '于', '时', '傅', '皮', '卞', '齐', '康', '伍', '余', '元', '卜', '顾', '孟', '平', '黄', '和', '穆', '萧', '尹', '姚', '邵', '湛', '汪', '祁', '毛', '禹', '狄', '米', '贝', '明', '臧', '计', '伏', '成', '戴', '谈', '宋', '茅', '庞', '熊', '纪', '舒', '屈', '项', '祝', '董', '梁', '杜', '阮', '蓝', '闵', '席', '季', '麻', '强', '贾', '路', '娄', '危'
    ];
    var legendData = [];
    var seriesData = [];
    var selected = {};
    for (var i = 0; i < 50; i++) {
        name = Math.random() > 0.65
            ? makeWord(4, 1) + '·' + makeWord(3, 0)
            : makeWord(2, 1);
        legendData.push(name);
        seriesData.push({
            name: name,
            value: Math.round(Math.random() * 100000)
        });
        selected[name] = i < 6;
    }

    return {
        legendData: legendData,
        seriesData: seriesData,
        selected: selected
    };

    function makeWord(max, min) {
        var nameLen = Math.ceil(Math.random() * max + min);
        var name = [];
        for (var i = 0; i < nameLen; i++) {
            name.push(nameList[Math.round(Math.random() * nameList.length - 1)]);
        }
        return name.join('');
    }
}
 -->