@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('admin_page_filter')
<li class="nav-item" style="display: none"> 
<div class="btn-group">
        <select id="admin_page_filter" class="form-control custom-select">
        <option value="">Admin Pages</option>
        @if (isset($monitorpage))
        @foreach($monitorpage as $monitorpage)
        <option value="{{$monitorpage}}" id="{{$monitorpage}}" >{{$monitorpage}}</option>
        @endforeach
        @endif
                                                        
                                                   
      </select>
 
                                        </div>
                         </li>
@endsection
@section('content')

  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               <!--   <header class="" id="myHeader">
                <div class="row page-titles" >
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px">Dashboard</h4>
                      <ol class="breadcrumb" style="padding-left:20px">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">last Crawl Date</a></li>
                            <li class="breadcrumb-item active"> @if (isset($last_crawl_date)) {{$last_crawl_date}} @endif</li>
                        </ol>
                    </div>

                    <div class="col-md-7 col-4 align-self-center">
                    <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                      
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down" >
                            <div class='input-group mb-3'>
                                <input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            </div>
                             
                        </div>
                    </div>
                </div>
            </header> -->
            <header class="" id="myHeader">
                  <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500">Dashboard</h4>
                       <ol class="breadcrumb" style="padding-left:20px">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Last Crawl Date</a></li>
                            <li class="breadcrumb-item active"> @if (isset($last_crawl_date)) {{$last_crawl_date}} @endif</li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                            <!--     <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>THIS MONTH</small></h6>
                                    <h4 class="m-t-0 text-info">$58,356</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div> -->
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                               <input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <!-- <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
  </header>
             
                    <!-- just for second top fixed bar -->
             
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
             
                    <!-- <div class="row" id='dashboard-div-1' >
                         <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                     <span class="text-muted display-5 text-warning"><i class="mdi mdi-emoticon-cool"></i></span>
                                    <div class="m-l-10 align-self-center" style="width: 100%;">
                                        <h3 class="m-b-0" >Positive</h3>
                                        <div style="float: left;width: 100%;margin-top:10px;margin-bottom: 5px;">
                                            <h5 class="text-muted m-b-0" id="top-positive-num" style="float: left">0</h5>
                                            <h5 class="text-muted m-b-0" id="top-positive-value" style="float: right">0%</h5>    
                                        </div>
                                        <div class="progress" style="float: left;width: 100%;">
                                        <div id="top-positive-progress" class="progress-bar bg-info" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>

                                    </div>
                                   
                                </div>
                                
                            </div>
                             <div align="right" style="padding:5px"><a href="#" class="top_div" ><span class="label label-success" id="positive">Detail</span></a></div>
                        </div>
                    </div> -->
                    <!-- Column -->
                    <!-- Column -->
                    <!-- <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                     <span class="text-muted display-5 text-warning"><i class="mdi mdi-emoticon-sad"></i></span>
                                    <div class="m-l-10 align-self-center" style="width: 100%;">
                                        <h3 class="m-b-0">Negative</h3>
                                        <div style="float: left;width: 100%;margin-top:10px;margin-bottom: 5px;">
                                            <h5 class="text-muted m-b-0" id="top-negative-num" style="float: left">0</h5>
                                            <h5 class="text-muted m-b-0" id="top-negative-value" style="float: right">0%</h5>
                                        </div>

                                        <div class="progress" style="float: left;width: 100%;">
                                        <div id="top-negative-progress" class="progress-bar bg-info" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div></div>
                                </div>
                              
                            </div>
                             <div align="right" style="padding:5px"><a href="#" class="top_div"  ><span class="label label-success" id="negative">Detail</span></a></div>
                        </div>
                    </div> -->
                    <!-- Column -->
                    <!-- Column -->
                    <!-- <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <span class="text-muted display-5 text-warning"><i class="mdi mdi-emoticon-neutral"></i></span>
                                    <div class="m-l-10 align-self-center" style="width: 100%;">
                                        <h3 class="m-b-0">Neutral</h3>
                                        <div style="float: left;width: 100%;margin-top:10px;margin-bottom: 5px;">
                                            <h5 class="text-muted m-b-0" id="top-neutral-num" style="float: left">0</h5>
                                            <h5 class="text-muted m-b-0" id="top-neutral-value" style="float: right">0%</h5>
                                        </div>
                   
                                        <div class="progress" style="float: left;width: 100%;">
                                        <div id="top-neutral-progress" class="progress-bar bg-info" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div></div>
                                </div>
                                
                            </div>
                             <div align="right" style="padding:5px"><a href="#" class="top_div"   ><span class="label label-success" id="neutral">Detail</span></a></div>
                        </div>
                    </div> -->
                    <!-- Column -->
                    <!-- Column -->
                   <!--  <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                 <span class="text-muted display-5 text-warning"><i class="mdi mdi-emoticon-cool"></i></span>
                                    <div class="m-l-10 align-self-center" style="width: 100%;">
                                        <h3 class="m-b-0">Enquiry</h3>
                                        <div style="float: left;width: 100%;margin-top:10px;margin-bottom: 5px;">
                                            <h5 class="text-muted m-b-0" id="top-interest-num" style="float: left">0</h5>
                                            <h5 class="text-muted m-b-0" id="top-interest-value" style="float: right">0%</h5>
                                        </div>

                                        <div class="progress" style="float: left;width: 100%;">
                                        <div id="top-interest-progress" class="progress-bar bg-info" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="0"  aria-valuemin="0" aria-valuemax="100"></div>
                                    </div></div>
                                </div>
                               
                            </div>
                             <div align="right" style="padding:5px"><a href="#"   class="top_div" ><span class="label label-success" id="interest">Detail</span></a></div>
                        </div>
                    </div> 
                   
               </div>-->
            <!--    <div class="row">
                 <div class="col-lg-12"  id="dashboard-div-3">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Line Chart</h4>
                                <div id="morris-line-chart"></div>
                            </div>
                        </div>
                </div>
               </div> -->
                  <!-- Row -->
                   <div class="row">
                    <div class="col-lg-8">
                       
                       
                        <div class="card" style="height:425px">
                           <div class="card-header">
                                <!-- <div class="card-actions">
                                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                                </div> -->
                                <h4 class="card-title m-b-0">Fan Growth</h4>
                            </div>
                            <div class="card-body b-t collapse show">
                               <div style="display:none"  align="center" style="vertical-align: top;" id="fan-growth-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div id="fan-growth-chart" style="width:100%;height:350px"></div>
                          </div>
                        </div>
                    </div>
                     <div class="col-lg-4">
                        <!-- Column -->
                        <div class="card earning-widget" style="height:425px">
                            <div class="card-header">
                               <!--  <div class="card-actions">
                                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                                </div> -->
                                <h4 class="card-title m-b-0">Most Frequently Tags</h4>
                            </div>
                            <div class="card-body b-t collapse show">
                                <table id="tbl_tag_count" class="table v-middle no-border">
                                    <tbody>
                                      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
                </div>
                 <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-md-4">
                        <div class="card" style="height: 249px">
                           <div class="card-header">
                                <!-- <div class="card-actions">
                                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                                </div> -->
                                <h4 class="card-title m-b-0">Total Reach</h4>
                            </div>
                            <div class="card-body b-t collapse show">
                                <div style="display:none"  align="center" id="reach-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                     <div id="total-reach-chart" style="width:100%; height:150px"></div>
                                 </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-4">
                        <div class="card">
                       
                                <div class="card-header">
                               <!--  <div class="card-actions">
                                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                                </div> -->
                                <h4 class="card-title m-b-0">Number Of Posts</h4>
                            </div>
                            <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                                             <div style="display:none"  align="center" id="post_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                    <div class="card-body text-center " style="padding:0.5rem !important">
                                        <h1 class="card-title m-t-10" style="font-size:65px" id="post_total">-</h1>
                                        
                                    </div>
                                    <div class="card-body text-center ">
                                      
                                        <ul class="list-inline m-b-0">
                                            <li>
                                                <h6 id="post_neg"class="text-red" style="font-size:20px">-%</h6>
                                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnPost" value="neg">Negative</button></div> </li>
                                           <li>
                                                 <h6 id="post_neutral" class="text-warning" style="font-size:20px">-%</h6>
                                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnPost" value="neutral">Neutral</button></div> </li>
                                            <li>
                                                 <h6 id="post_pos" class="text-success" style="font-size:20px">-%</h6>
                                                <div><button type="button" class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnPost" value="pos">Positive</button></div> </li>
                                        </ul>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-4">
                        <div class="card">
                                <div class="card-header">
                                <!-- <div class="card-actions">
                                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                                </div> -->
                                <h4 class="card-title m-b-0">Number Of Comments</h4>
                            </div>
                            <div class="card-body b-t collapse show" style="padding:0.5rem !important">
                                             <div style="display:none"  align="center" id="cmt_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                    <div class="card-body text-center " style="padding:0.5rem !important">
                                        <h1 class="card-title m-t-10" style="font-size:65px" id="cmt_total">-</h1>
                                        
                                    </div>
                                    <div class="card-body text-center ">
                                      
                                        <ul class="list-inline m-b-0">
                                            <li>
                                                <h6 id="cmt_neg"class="text-red" style="font-size:20px">-%</h6>
                                                <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-red btnComment" value="neg">Negative</button></div> </li>
                                            <li>
                                                 <h6 id="cmt_neutral" class="text-warning" style="font-size:20px">-%</h6>
                                                <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-warning btnComment" value="neutral">Neutral</button></div> </li>
                                            <li>
                                                 <h6 id="cmt_pos" class="text-success" style="font-size:20px">-%</h6>
                                                <div><button type="button"  class="btn waves-effect waves-light btn-rounded btn-sm btn-success btnComment" value="pos">Positive</button></div> </li>
                                        </ul>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <div class="row">
                 <div class="col-lg-4">
                        <!-- Column -->
                        <div class="card earning-widget">
                            <div class="card-header">
                              <!--   <div class="card-actions">
                                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                                </div> -->
                                <h4 class="card-title m-b-0">Top 5: Fan's City</h4>
                            </div>
                            <div class="card-body b-t collapse show">
                                      <div style="display:none"  align="center" id="city-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                <table id="tbl_city_reach" class="table v-middle no-border">
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
                 <div class="col-lg-8">
                       
                       
                        <div class="card" style="height:357px">
                           <div class="card-header">
                             <!--    <div class="card-actions">
                                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                                </div> -->
                                <h4 class="card-title m-b-0">People Reach</h4>
                            </div>
                            <div class="card-body b-t collapse show">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="reach-gender-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                 <div style="width:100%; height:350px;">
                                <div style="width:100%;">
                                <div id="div_legend_Women" style="width:20%; height:150px;float:left;padding-left:20px;padding-top:50px">
                                    Women<br><span class="fa fa-square m-r-10" style="color:#1e88e5"> </span><span id="pcent_legend_Women" style="font-weight:bold"> 0</span>%<br><span style="font-size:12px">People Reached</span></div>
                                <div id="reach-age-gender-chart" style="width:80%; height:150px;float:right;padding-top:10px"></div>
                                </div>
                                <div style="width:100%;">
                                <div id="div_legend_Men" style="width:20%; height:150px;float:left;vertical-align: middle;padding-left:20px;">
                                    Men<br><span class="fa fa-square m-r-10" style="color:#4da1f0"> </span><span id="pcent_legend_Men" style="font-weight:bold"> 0</span>%<br> <span style="font-size:12px">People Reached</span></div>
                                <div id="reach-age-gender-chart-1" style="width:80%; height:150px;float:right"></div>
                                </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
 <div class="row">
                        <div class="col-12">
                       
                        <div class="card" >
                           <div class="card-header">
                              <!--   <div class="card-actions">
                                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                                </div> -->
                                <h4 class="card-title m-b-0">Page Fan Vs Online Fan </h4>
                            </div>
                            <div class="card-body b-t collapse show">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="fan-page-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div id="fan-page-chart" style="width:100%;height:320px"></div>
                          </div>
                        </div>
                    </div>
                    </div>
                <!-- <div class="row">
                 
                     <div class="col-lg-12"  id="dashboard-div-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h3 class="card-title">Sentiment Statistic</h3>
                                      
                                    </div>
                                   <div class="ml-auto">
                                        <ul class="list-inline text-right icheck-list">
                                            <li>
                                                 <input tabindex="7" type="radio" class="check" id="period-week" name="period-radio" checked>
                                                                <label for="period-week">Week</label>
                                            </li>
                                         <li>
                                                                <input tabindex="8" type="radio" class="check" id="period-month" name="period-radio" >
                                                                <label for="period-month">Month</label>
                                                            </li>
                                          <li>
                                                        <div class='input-group mb-3' style="width:60%">
                                <input type='text' class="form-control singledate" id="up_to_date"  />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                                 
                            </div>
                                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                 <div style="display:none"  align="center" style="vertical-align: top;" id="socialmedia-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                     <div id="social-bar-chart" style="width:100%; height:320px;"></div>
                            </div>
                        </div>
                    </div>
                 
                  
                    </div>  -->
                  
                    <div class="row">
                        <div class="col-12">
                       
                        <div class="card" >
                           <div class="card-header">
                              <!--   <div class="card-actions">
                                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                    <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                                </div> -->
                                <h4 class="card-title m-b-0">Tag Sentiment</h4>
                            </div>
                            <div class="card-body b-t collapse show">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="tag-senti-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div id="tag-senti-chart" style="width:100%;height:320px"></div>
                          </div>
                        </div>
                    </div>
                    </div>
                    <!--  <div class="row">
                        <div class="col-12">
                       
                        <div class="card" >
                           <div class="card-header">
                          
                                <h4 class="card-title m-b-0">Tag Sentiment</h4>
                            </div>
                            <div class="card-body b-t collapse show">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="hightlighted_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                            <div id="hightlighted_chart" style="width:100%;height:320px"></div>
                          </div>
                        </div>
                    </div>
                    </div> -->
                  <!--   <div class="row">
                    
                     <div class="col-lg-8 col-md-12"  id="dashboard-div-3">
                        <div class="card" style="height:367px">
                             <ul class="nav nav-tabs profile-tab" role="tablist">
<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#posting" role="tab">Posting Status</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#engagement" role="tab">Engagement</a> </li>
                                
                          </ul>
                          
                            <div class="tab-content">
                                <div class="tab-pane active" id="posting" role="tabpanel">
                                
                                 
                                <div class="card-body">
                        
                                <div style="display:none"  align="center" id="posting_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
  <div align="center">
  <u><h6 class="card-title" style="margin-bottom:2rem;display: none" >Today Posting Status</h6></u>
                                      
                                    </div>
                               <table id="tbl_today_post" class="table" style="width:100%;margin-top:-1.5em;display: none">
          <thead >
            <tr>
              <th width="20%">Category</th>
              <th width="20%">Total</th>
              <th width="20%">positive</th>
              <th width="20%">Negative</th>
            
            </tr>
          </thead>
            <tbody>
            </tbody>


        </table>

        <div align="center">
 <u><h6 class="card-title" style="margin-bottom:2rem">Weekly Posting Status</h6></u>
                                      
                                    </div>
                               <table id="tbl_week_post" class="table" style="width:100%;margin-top:-1.5em;">
          <thead >
            <tr>
             <th width="20%">Category</th>
              <th width="20%">Total</th>
              <th width="20%">positive</th>
              <th width="20%">Negative</th>
             
            </tr>
          </thead>
           <tbody>
            </tbody>
        </table>
                            </div>
                    
                                              </div>
<div class="tab-pane" id="engagement" role="tabpanel">
                           
  <div class="card-body">
  <div style="display:none"  align="center" id="engagement-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
  <div align="center">
  <u> <h6 class="card-title" style="margin-bottom:2rem;display: none">Today Engagement Status</h6></u>
                                      
                                    </div>
                                      
                                  
                               <table id="tbl_today_engagement" class="table" style="width:100%;margin-top:-1.5em;display: none">
          <thead >
            <tr>
              <th width="15%">Category</th>
              <th width="15%">Total</th>
              <th width="15%">Reaction</th>
              <th width="15%">Share</th>
              <th width="15%">Comment</th>
              
            </tr>
          </thead>
<tbody></tbody>
        </table>

        <div align="center">
 <u><h6 class="card-title" style="margin-bottom:2rem">Weekly Engagement Status</h6></u>
                                      
                                    </div>
                               <table id="tbl_week_engagement" class="table" style="width:100%;margin-top:-1.5em;">
          <thead >
            <tr>
             <th width="15%">Category</th>
              <th width="15%">Total</th>
              <th width="15%">Reaction</th>
              <th width="15%">Share</th>
              <th width="15%">Comment</th>
             
            </tr>
          </thead>
          <tbody></tbody>

        </table> 
  </div>
                                       
                         
                                              </div>
                                          </div>
                        </div>
                    </div>
                  
                    <div class="col-lg-4 col-md-12"  id="dashboard-div-4">

                         <div class="card">
                            <div class="card-body">
                            <h3 class="card-title">Reaction</h3>
                             <div id="reaction-chart" style="width:100%; height:200px"></div>
                                     <div class="social-widget" style="padding:10px 0 10px 0 ;display:none">
                                          <div class="soc-content"  >
                                            <div class="col-6 b-r b-b">
                                              <span class="text-muted display-5" id="reaction_pic"><i class="mdi mdi-emoticon-cool"></i></span>
                                            </div>
                                            <div class="col-6 b-b">
                                                <a href="#" class="react_div"    id="angry"><h3 class="font-medium" id="reaction_angry_num">0</h3>
                                                <h5 class="text-danger react_type" name="angry">Angry</h5></a></div>
                                        </div>
                                        <div class="soc-content">
                                            <div class="col-6 b-r ">
                                             <a href="#" class="react_div"  id="haha"><h3 class="font-medium" id="reaction_haha_num">0</h3>
                                                <h5 class="text-danger react_type" name="haha">HaHa</h5></a></div>
                                            <div class="col-6">
                                             <a href="#" class="react_div"   id="sad">
                                                <h3 class="font-medium" id="reaction_sad_num">0</h3>
                                                <h5 class="text-danger react_type" name="sad">Sad</h5></a></div>
                                        </div>
                                    </div>


                            </div>
                               <div >
                                <hr class="m-t-0 m-b-0">
                            </div>
                           
                               <div class="social-widget" >
                                          <div class="soc-content" id="reaction_pic_b">
                                            <div class="col-6 b-r "  style="padding:0px">
                                              <span class="text-mute display-5" style="color:#e7eef1" ><i class="mdi mdi-emoticon-sad"></i></span>
                                            </div>
                                            <div class="col-6 "  style="padding:0px">
                                                <span class="text-warning display-5"><i class="mdi mdi-emoticon-cool"></i></span>
                                              </div>
                                        </div>
                                    
                                    </div>
                            
                    </div>
                        </div>
                    </div> -->
      
                           <!-- <div class="row" id="page-div-2">

  <div class="col-lg-12">
    <div class="card">
 
        <ul class="nav nav-tabs profile-tab" role="tablist">
         <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#top_post" role="tab">Highlighted Comments</a> </li>
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Latest" role="tab">Latest Post</a> </li>
        </ul>
         <div class="tab-content">
         <div class="tab-pane active" id="top_post" role="tabpanel">
          <div class="card-body">
            <div class="d-flex no-block align-items-center" >
           <div class="ml-auto" style="float:right;">
            <ul class="list-inline">
              <li>
               <button type="button" class="btn btn-success" name="btn_high_bookmark" id="btn_high_bookmark">Bookmark</button>
               
              </li>

            </ul>
          </div>
        </div>
         <div style="display:none"  align="center" style="vertical-align: top;" id="modal-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
     
        <table id="tbl_top_post" class="table" style="width:100%;margin-top:-1.5em">
          <thead style='display:none;'>
            <tr>
              <th>Rendering engine</th>
              <th>Setting</th>
            </tr>
          </thead>

        </table>
         </div>
            </div>
            <div id="show-task" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Comment Type</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="message-box">
                                <div class="comment-widgets message-scroll  comment_data">
                             
                                   
                                </div>
                            </div>
                                                            </div>
                                                     
                                                        </div>
                                                  
                                                    </div>
                                                 
                                                </div>
           <div class="tab-pane" id="Latest" role="tabpanel">
         <div class="card-body">
          <div class="d-flex no-block align-items-center">
           <div class="ml-auto" style="float:right;">
            <ul class="list-inline">
              <li>
               <button type="button" class="btn btn-success" name="btn_bookmark" id="btn_bookmark">Bookmark</button>
               
              </li>

            </ul>
          </div>
        </div>
        <table id="tbl_latest" class="table" style="width:100%;margin-top:-1.5em">
          <thead style='display:none;'>
            <tr>
              <th>Rendering engine</th>
              <th>Setting</th>
            </tr>
          </thead>

        </table>
        </div>
           </div>
       </div>
     
    </div>
  </div>

</div> -->
               <!--  <div class="row" id="dashboard-div-9">
                    <div class="col-lg-12">
                        <div class="card">
                        
                         <ul class="nav nav-tabs profile-tab" role="tablist">
<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#interest_highlighted" role="tab">Interest Hilighted Voice</a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#all_highlighted" role="tab">All</a> </li>
                                
                          </ul>
                          
                            <div class="tab-content">
                                <div class="tab-pane active" id="interest_highlighted" role="tabpanel">
                             
                                <div class="card-body">
                        
                                <div style="display:none"  align="center" id="hightlighted-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div style="width:100%; height:800px;" id="hightlighted" ></div>   
                            </div>
                    
                                              </div>
<div class="tab-pane" id="all_highlighted" role="tabpanel">
                           
  <div class="card-body">
  <div style="display:none"  align="center" id="hightlighted-all-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
  <div style="width:100%; height:800px;" id="hightlighted-all" ></div>   
  </div>
                                       
                         
                                              </div>
                                          </div>
                     

                        </div>

                    </div>
          </div> -->
                            
                                 <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme working">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/1.jpg')}}" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/2.jpg')}}" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/3.jpg')}}" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/4.jpg')}}" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/5.jpg')}}" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/6.jpg')}}" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/7.jpg')}}" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="{{asset('assets/images/users/8.jpg')}}" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
   <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
     <script src="{{asset('js/morris-data.js')}}" ></script>-->
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
    <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
   <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>
   <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
    <script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var myHeaderContent = document.getElementById("myHeaderContent");
var sticky = header.offsetTop;

function myFunction() {//alert("ho");
  if (window.pageYOffset > sticky) {
    header.classList.add("s-topbar");
    header.classList.add("s-topbar-fix");
    myHeaderContent.classList.add("myHeaderContent");
  } else {
    header.classList.remove("s-topbar");
    header.classList.remove("s-topbar-fix");
    myHeaderContent.classList.remove("myHeaderContent");
  }
}
</script>
    <script type="text/javascript">
var startDate;
var endDate;
/*global mention*/
var mention_total;
var mentionLabel = [];
var mentions = [];
/*Bookmark Array*/
var bookmark_array=[];
var bookmark_remove_array=[];
/*global sentiment*/
var positive = [];
var negative = [];
var sentimentLabel = [];
var positive_total=0;
var negative_total=0;
var  colors=["#1e88e5","#dc3545","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

$(document).ready(function() {



    //initialize
 $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
/* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

 $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

      var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

    startDate = moment().subtract(1, 'month');
    endDate = moment();

           $('.singledate').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        },function(date) {
          endDate=date;
          var id =$('input[type=radio][name=period-radio]:checked').attr('id');
          if(id==="period-week")
         {
           startDate = moment(endDate).subtract(1, 'week');
           endDate = endDate;
         }
         else
         {
           startDate = moment(endDate).subtract(1, 'month');
           endDate = endDate;
         }
         // alert(startDate);
         // alert(endDate);
         var admin_page=$("#admin_page_filter").val();

         ChooseDate(startDate,endDate,admin_page,'');
       
      });

     function requestinterestData(fday,sday,admin_page){
     var brand_id = GetURLParameter('pid');
    
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getInboundinterest')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'day' ,admin_page:admin_page}
    })
    .done(function( data ) {
      var each_total=0;
      var all_total=0;
      

        for(var i in data) {
        each_total+=parseInt(data[i].interest_sum);
        all_total+=parseInt(data[i].comment_count);
       
      }

    

var interest_percentage=parseInt((each_total/all_total)*100);
interest_percentage = isNaN(interest_percentage)?0:interest_percentage;

 $('#top-interest-progress').css('width', interest_percentage+'%').attr('aria-valuenow', interest_percentage);   
 if(interest_percentage !== 0)
 {

  $("#top-interest-value").text(interest_percentage + "%");
  $("#top-interest-num").text("count - "+each_total);
 }
 else
 {
  $("#top-interest-value").text("- %");
 $("#top-interest-num").text("count - ");
 }


 
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }

    function requestmentionData(fday,sday,admin_page){//alert(fday);
      
/*       var mentionchart = document.getElementById('mention-bar-chart');
    var mentionChart = echarts.init(mentionchart);*/

$("#mentionreaction-spin").show();

  $("#reaction-spin").show();       
       var brand_id = GetURLParameter('pid');
       /* var brand_id = 22;*/
       // alert (brand_id);
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getmentiondetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'day'}
    })
    .done(function( data ) {//alert(data);
 
    /*   $("#mention-total").text("Total = "+data[data.length-1].total);
       $("#social-total").text("Total = "+data[data.length-1].total);
       var positive_total= data[data.length-1].total+10;
       $("#sentiment-total").text("Positive = "+ positive_total + " && Negative = "+data[data.length-1].total);*/

      // When the response to the AJAX request comes back render the chart with new data
  


 
       mention_total=0;

       mentions=[];
       mentionLabel=[];

      for(var i in data) {//alert(data[0][i].mention);
        mentions.push(data[i].mention);
        mentionLabel.push(data[i].periodLabel);
        
      }

$.each(mentions,function(){mention_total+=parseInt(this) || 0;});


//requestmentionReactData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
 
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }

   
   function TotalReach(fday,sday,admin_page,date_preset){//alert(fday);

       var brand_id = GetURLParameter('pid');
    $("#reach-spin").show();
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getFbReach')}}", // This is the URL to the API
      data: {date_preset: date_preset,period:'day',brand_id:brand_id,fday:fday,sday:sday}
    })
    .done(function( data ) {console.log(data);
     $("#reach-spin").hide();
      var organic = [];
      var paid = [];
      var organic_total=0;
      var paid_total=0;

       for(var i in data) 
        {
        organic.push(data[i].organic);
        paid.push(data[i].paid);
          
        }
        $.each(organic,function(){organic_total+=parseInt(this) || 0;});
        $.each(paid,function(){paid_total+=parseInt(this) || 0;});

    var ReachChart = echarts.init(document.getElementById('total-reach-chart'));

    option = {
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {d}% " // formatter: "{a} <br/>{b}: {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        x:'right',

        data:['Organic','Paid']
    },
    color: ["#4da1f0", "#f1c54b"],
    series: [
        {
            name:'Total Reach',
            type:'pie',
            radius: ['65%', '80%'],
            center : ['25%', '50%'],
            avoidLabelOverlap: false,
            label: {
               
                normal: {
                    show: false,
                    position: 'center',
                  
                },
                emphasis: {
                    show: true,
                    position : 'center',
                    formatter:"{d}%",
                     // formatter : function (params){
                     //                    return params.name +'\n' + params.value + '\n'
                     //                },
                    textStyle: {
                        fontSize: '20',
                        fontWeight: 'bold'
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data:[
            { value: organic_total, name: 'Organic' },
            { value: paid_total, name: 'Paid' },
          
            ]
        }
    ]
};


// use configuration item and data specified to show chart
ReachChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            ReachChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }
    function requestsentimentData(fday,sday,admin_page){//alert(fday),alert(sday);
   //alert("hihi");
    var brand_id = GetURLParameter('pid');
       // alert (brand_id);
      
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getinboundsentiment')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'day',admin_page:admin_page}
    })
    .done(function( data ) {

    
       
        // console.log("setiment");
        // console.log(data);
    $("#socialmedia-spin").show(); 
 
   
     positive = [];
       negative = [];
      var neutral=[];
       sentimentLabel = [];

       positive_total=0;
       negative_total=0;
      var neutral_total=0;
      var all_total=0;


        for(var i in data) {//alert(data[i].mentions);
           if(parseInt(data[i].positive)===0)
           {
             positive.push('-');
            
           }
           else
           {
            positive.push(data[i].positive);
           }
              if(parseInt(data[i].negative)===0)
           {
            negative.push('-');
           }
           else
           {
            negative.push(data[i].negative);
            
           }
        
        
        neutral.push(data[i].neutral);
        // if(parseInt(data[i].positive) !==0 || parseInt(data[i].negative) !==0)
        // {

        sentimentLabel.push(data[i].periodLabel);
        // }
      
        
      }

// console.log("positive");
// console.log(negative);
$.each(positive,function(){positive_total+=parseInt(this) || 0;});
$.each(negative,function(){negative_total+=parseInt(this) || 0;});
$.each(neutral,function(){neutral_total+=parseInt(this) || 0;});
all_total=positive_total+negative_total+neutral_total;
var positive_percentage=parseFloat((positive_total/all_total)*100).toFixed(2);
var negative_percentage= parseFloat((negative_total/all_total)*100).toFixed(2);
var neutral_percentage =  parseFloat((neutral_total/all_total)*100).toFixed(2);

          positive_percentage = isNaN(positive_percentage)?0:positive_percentage;
          negative_percentage = isNaN(negative_percentage)?0:negative_percentage;
          neutral_percentage = isNaN(neutral_percentage)?0:neutral_percentage;
         

 $('#positive-progress').css('width', positive_percentage+'%').attr('aria-valuenow', positive_percentage); 
 $('#top-positive-progress').css('width', positive_percentage+'%').attr('aria-valuenow', positive_percentage);   
 $('#negative-progress').css('width', negative_percentage+'%').attr('aria-valuenow', negative_percentage);
 $('#top-negative-progress').css('width', negative_percentage+'%').attr('aria-valuenow', negative_percentage);

 $('#top-neutral-progress').css('width', neutral_percentage+'%').attr('aria-valuenow', neutral_percentage);
 $('#neutral-progress').css('width', neutral_percentage+'%').attr('aria-valuenow', neutral_percentage);

 // positive_percentage = positive_percentage.replace(".00", "");

 $("#positive-total").text(positive_percentage + "%");

 if(positive_percentage !== 0)
 {
  $("#top-positive-value").text(positive_percentage.replace(".00", "") + "%");
  $("#top-positive-num").text("count - "+positive_total);
 }
 else
 {
  $("#top-positive-value").text("- %");
  $("#top-positive-num").text("count - ");
 }
 
 $("#negative-total").text(negative_percentage + "%");
 
  if(negative_percentage !== 0)
 {
  $("#top-negative-value").text(negative_percentage.replace(".00", "") + "%");
   $("#top-negative-num").text("count - "+ negative_total);

 }
 else
 {
    $("#top-negative-value").text("- %");
     $("#top-negative-num").text("count - ");
 }

 $("#neutral-total").text(neutral_percentage + "%");
   
   if(neutral_percentage !== 0)
 {
  $("#top-neutral-value").text(neutral_percentage.replace(".00", "") + "%");
  $("#top-neutral-num").text("count - "+neutral_total);
 }
 
 else
 {
  $("#top-neutral-value").text("- %");
   $("#top-neutral-num").text("count - ");
 }
 
      
 // if(negative_total > 0 )
 // {
 //     $("#reaction_pic").empty();
 //     $("#reaction_pic").append('<i class="mdi mdi-emoticon-sad"></i>');
 // }
requestmentionReactData(fday,sday,admin_page);
    })
    .fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
    });
  }
//     function requestsocialData(socials,social_total,socialLabel){//alert(fday),alert(sday);
//       console.log(sentimentLabel.length);
//       console.log(socialLabel.length);
//       var Labels=sentimentLabel;
//       // if(socialLabel.length>sentimentLabel.length)
//       // Labels = socialLabel;

      

//     var socialchart = document.getElementById('social-bar-chart');
//     var socialChart = echarts.init(socialchart);
   

  
// option = {
//     color: colors,

//     tooltip: {
//         trigger: 'axis',
//         axisPointer: {
//             type: 'cross'
//         },
//         formatter: function (params) {
//         var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
//         let rez = '<p>' + params[0].name + '</p>';
//        /* console.log(rez);*/ //quite useful for debug
//         params.forEach(item => {
//             console.log("item");
//             console.log(item.data);
//             var item_data=item.data==='-'?'-':kFormatter(item.data);
//             var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName + ': ' + item_data  + '</p>'
//             rez += xx;
//         });

//         return rez;
//     }
//     },
//           grid: {
//           top:    60,
    
//     left:   '5%',
//     right:  '10%',
//     bottom:  '5%',
//             containLabel: true
//         },
//   /*  toolbox: {
//         feature: {
//             dataView: {show: true, readOnly: false},
//             restore: {show: true},
//             saveAsImage: {show: true}
//         }
//     },*/
//      toolbox: {
//             show : true,
//             feature : {
//                 mark : {show: false},
//                 dataView : {show: false, readOnly: false},
//                 magicType : {show: true, type: ['line','bar']},
//                 restore : {show: true},
//                 saveAsImage : {show: true}
//             }
//         },

//     legend: {
//         data:['Positive', 'Negative'],//,'social media reach'

//           formatter: function (name) {
//             if(name === 'Positive')
//             return name + ': ' + positive_total;
//             if(name === 'Negative')
//             return name  + ': ' + negative_total;
//             else
//             return name  + ': ' + social_total;

   
// },

//     },
//     xAxis: [
//         {
        
//             type: 'category',
//              boundaryGap: true,
//             axisTick: {
//                 alignWithLabel: true
//             },
//                          axisLabel: {
//       formatter: function (value, index) {
//     // Formatted to be month/day; display year only in the first label
//     const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
//   "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
//      var date = new Date(value);
//        console.log(date);
//       var texts = [date.getFullYear(), monthNames[date.getMonth()],date.getDate()];

//     return texts.join('-');

// },
//  rotate:45
//     },
//             data: Labels
//         }
//     ],
//     yAxis: [
//         {
//             type: 'value',
//             name: 'Sentiment',
//           /*  min: 0,
//             max: 250,*/
//             position: 'left',
//             axisLine: {
//                 lineStyle: {
//                     color: colors[0]
//                 }
//             }
    
//     //     },
//     //     {
//     //         type: 'value',
//     //         name: 'social media reach',
//     //        /* min: 0,
//     //         max: 250,*/
//     //         position: 'right',
//     //         offset: 80,
//     //         axisLine: {
//     //             lineStyle: {
//     //                 color: colors[2]
//     //             }
//     //         },
//     //         /*axisLabel: {
//     //             formatter: '{value} ml'
//     //         }*/
//     //           axisLabel: {
//     //     formatter: function (e) {
//     //         return kFormatter(e);
//     //     }
//     // }
//      }
//     ],
//     series: [
//         {
//             name:'Positive',
//             type:'line',
//             smooth: 0.2,
//             color:colors[0],
//             barMaxWidth:30,
//             data:positive
//         },
//         {
//             name:'Negative',
//             type:'line',
//             smooth:0.2,
//             color:colors[1],
//             barMaxWidth:30,
//             data:negative
//          }//,
//         // {
//         //     name:'social media reach',
//         //     type:'line',
//         //     color:colors[2],
//         //     yAxisIndex: 1,
//         //     data:socials,
//         //     markPoint : {
//         //       large:true,
//         //        label: {
//         //                             normal: {
//         //                                 formatter: function (param) {
//         //                                     return kFormatter(param.value);
//         //                                 },
//         //                                 textStyle: {
//         //                                     color: '#f8fbfb'
//         //                                 },
//         //                                 position: 'inside'
//         //                             }
//         //                         },
//         //         data : [
//         //         {type : 'max', name: 'maximum'},
//         //         {type : 'min', name: 'minimum'},

//         //         ]
//         //     }
//         // }
//     ]
// };
// $("#socialmedia-spin").hide();
        
//  socialChart.setOption(option, true), $(function() {
//     function resize() {
//         setTimeout(function() {
//             socialChart.resize()
//         }, 100)
//     }
//     $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
// });
//   socialChart.on('click', function (params) {
//    console.log(params);
//    console.log(params.name); // xaxis data = 2018-08
//    console.log(params.seriesName); //bar period name ="Positive"
//    console.log(params.value);//count
//    var pid = GetURLParameter('pid');
//    var source = GetURLParameter('source');
//   var admin_page = $( "#admin_page_filter" ).val();
//   // alert(admin_page);
//    if(params.name !== 'minimum' && params.name !== 'maximum' )

//    window.open("{{ url('pointoutcomment?')}}" +"pid="+ pid+"&source="+ source+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&period="+ params.name +"&type="+params.seriesName+"&from_graph=sentiment&admin_page="+admin_page , '_blank');
//    //window.location="{{ url('pointoutmention?')}}" +"pid="+ pid+"&period="+ params.name +"&type="+params.seriesName ;
   
// });
//   }

  function popular_mentions(fday,sday)
{

}
function GenderReachData(page_name){//alert(startDate);alert(endDate);alert(date_preset);
    var reachgenderchart = document.getElementById('reach-age-gender-chart');
    var reachgenderchart_1=document.getElementById('reach-age-gender-chart-1')
    var reachGenderChart = echarts.init(reachgenderchart);
    var reachGenderChartBottom = echarts.init(reachgenderchart_1);
    var brand_id = GetURLParameter("pid");
    $('#reach-gender-spin').show();
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('get-agegender-Reach')}}", // This is the URL to the API
      data: {brand_id:brand_id,page_name:page_name}
    })
    .done(function( data ) {
     
       var count = Object.keys(data).length;//last record id
       var keys = Object.keys(data);//get only key
       var last_key = keys[count-1]; //to get last key's value total
        
      var F = [];
      var M = [];
      var F_val_arr = [];
      var M_val_arr = [];
      var Label =[];
      var Total = 0;
      var Ftotal = 0;
      var Mtotal =0;
        for(var i in data) {
        Total=data[last_key].T;
        var F_val=data[i].F;
        var M_val=data[i].M;
        F.push((parseFloat(F_val)/parseFloat(Total)*100).toFixed(3));
        M.push((parseFloat(M_val)/parseFloat(Total)*100).toFixed(3));
        F_val_arr.push(F_val);
        M_val_arr.push(M_val);
        Label.push(data[i].Label);
              
      }
       $.each( F_val_arr,function(){Ftotal+=parseInt(this) || 0;});
       $.each( M_val_arr,function(){Mtotal+=parseInt(this) || 0;});

     var FLegend=(parseFloat(Ftotal)/parseFloat(Total)*100).toFixed(2);
     var MLegend=(parseFloat(Mtotal)/parseFloat(Total)*100).toFixed(2);
     $("#pcent_legend_Women").text(FLegend);
     $("#pcent_legend_Men").text(MLegend);

    option = null;
    option_bottom = null;
    option = {
    color: ['#1e88e5'],//"#4da1f0", "#f1c54b"
    tooltip : {
        trigger: 'axis',
        axisPointer : {            
            type : 'shadow'       
        },
         formatter: function (params) { console.log(params);
            return "Women " + params[0].name + " make up" + '</br>' + params[0].value +  "% of people reach";
         }
        
    },
//      legend: {
//         data:['Women'],
//         position: 'bottom',
//         formatter: function (name) {
//     return  name + " " + FLegend +'%';
// }
//     },
    grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top: '12%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : Label,
            axisTick: {
                alignWithLabel: true
            }
        }
    ],
    yAxis : [
        {
            show: false,
             // inverse:true,
            type : 'value'
        }
    ],
    series : [
        {
            name:'Women',
            type:'bar',
              label: {
                normal: {
                    show: true,
                    position: 'top',
                    formatter: '{c}%',
                },
              
            },
            barMinHeight:2,
            barWidth: '60%',
            data:F
        }
    ]
};
option_bottom = {
    color: ['#4da1f0'],
    tooltip : {
        trigger: 'axis',
        axisPointer : {            
            type : 'shadow'       
        },
         formatter: function (params) { console.log(params);
            return "Men " + params[0].name + " make up" + '</br>' + params[0].value +  "% of people reach";
         }
        
    },
//      legend: {
//         data:['Men'],
//         position: 'bottom',
//         formatter: function (name) {
//     return  name + " " + MLegend +'%';
// }
//     },
    grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top: '2%',
        containLabel: true
    },
    xAxis : [
        {
            //show: false,
          
            type : 'category',
            data : Label,
            axisTick: {
                alignWithLabel: true
            },
            axisLabel: {
      formatter: function (value, index) {
return ;

},
 rotate:45
    },
        }
    ],
    yAxis : [
        {
            show: false,
            inverse:true,
            type : 'value'
        }
    ],
    series : [
        {
            name:'Men',
            type:'bar',
              label: {
                normal: {
                    show: true,
                    position: 'bottom',
                    formatter: '{c}%',
                },
              
            },
            barMinHeight:2,
            barWidth: '60%',
            data:M
        }
    ]
};
 if (option && typeof option === "object") {
    reachGenderChart.setOption(option, true);

}
if (option_bottom && typeof option_bottom === "object") {
    reachGenderChartBottom.setOption(option_bottom, true);

}
$('#reach-gender-spin').hide();
    
      
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in Reach API" );
            });
  }
function FanPage(fday,sday,page_name,date_preset)
{//alert(startDate);alert(endDate);alert(date_preset);
    var pageFanChart = echarts.init(document.getElementById('fan-page-chart'));
    var FanGrowthChart = echarts.init(document.getElementById('fan-growth-chart'));
    $('#fan-page-spin').show();
    $('#fan-growth-spin').show();
    var brand_id = GetURLParameter('pid');
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getPageFan')}}", // This is the URL to the API
      data: { date_preset: date_preset,page_name:page_name,fday:fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {//alert(data);
        
      var fan = [];
      var fan_online = [];
      var endtime = [];

         for(var i in data) {//alert(data[i].mentions);
        fan.push(data[i].fan);
        fan_online.push(data[i].fan_online);
        endtime.push(data[i].end_time);
        
      }
      // var min_of_array = Math.min.apply(Math, fan);
      // var min_of_array = min_of_array-10000;
        option_growth= null;
option_growth = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        axisPointer: {
            //type: 'cross'
        },
        formatter: function (params) {
        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        let rez = '<p>' + params[0].name + '</p>';
       /* console.log(rez);*/ //quite useful for debug
        params.forEach(item => {
            // console.log("item");
            // console.log(item.data);
            var item_data=item.data==='-'?'-':kFormatter(item.data);
            var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName + ': ' + item_data  + '</p>'
            rez += xx;
        });

        return rez;
    }
    },
          grid: {
          top:    60,
    
    left:   '5%',
    right:  '10%',
    bottom:  '5%',
            containLabel: true
        },
  /*  toolbox: {
        feature: {
            dataView: {show: true, readOnly: false},
            restore: {show: true},
            saveAsImage: {show: true}
        }
    },*/
     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: false}
            }
        },

//     legend: {
//         data:['Fan'],//,'social media reach'

// //           formatter: function (name) {
// //             if(name === 'Positive')
// //             return name + ': ' + positive_total;
// //             if(name === 'Negative')
// //             return name  + ': ' + negative_total;
// //             else
// //             return name  + ': ' + social_total;

   
// // },

//     },
    xAxis: [
        {
        
            type: 'category',
             boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },
                  axisLabel: {
      formatter: function (value, index) {
       const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

     var date = new Date(value);
       var xx= date.getDate() + '\n' + monthNames[date.getMonth()];
       return xx;

},

    },
            data: endtime
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Fan Count',
          //  min: min_of_array,
            scale:true,
            // max: 250,
            position: 'left',
            // axisLine: {
            //     lineStyle: {
            //         color: colors[0]
            //     }
            // }
                    axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }
    }
    
     }
    ],
    series: [
        {
            name:'Fan',
            type:'line',
            smooth: 0.2,
            color:colors[0],
            barMaxWidth:30,
            data:fan
        }
    ]
};
$("#fan-growth-spin").hide();
        
 FanGrowthChart.setOption(option_growth, true), $(function() {
    function resize() {
        setTimeout(function() {
            FanGrowthChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

      option = null;
     option = {
    color: ['#e5e5e5',colors[0]],
    tooltip : {
        trigger: 'axis',

        
    },

    grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top: '2%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            axisTick: {
                alignWithLabel: true
            },
               boundaryGap : true,
              axisLabel: {
      formatter: function (value, index) {
         const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

     var date = new Date(value);
       var xx= date.getDate() + '\n' + monthNames[date.getMonth()];
       return xx;

},

    },
            data : endtime,

        }
    ],
    yAxis : [
        {
            
             // inverse:true,
            type : 'value',
                     axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }
    }
        }
    ],
    series : [
        {
            name:'Fan',
            type:'bar',
            barGap:'-88%',
            barWidth: '80%',
            data:fan
        },
        {
            name:'Online',
            type:'bar',
            barWidth: '60%',
            data:fan_online
        }
    ]
};
 if (option && typeof option === "object") {
    pageFanChart.setOption(option, true);

}
$('#fan-page-spin').hide();
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in FAN API" );
            });
  }
function CityReach(fday,sday,admin_page)
{
var brand_id = GetURLParameter('pid');
 // var fday = moment(endDate).subtract(1, 'week');
 // var sday = endDate;
 // fday=fday.format('YYYY MM DD');
 // sday=sday.format('YYYY MM DD');

    $( "#city-spin" ).show();
    
 $("#tbl_city_reach tbody").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getcityReach')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page}
    })
    .done(function( data ) { //console.log(data);
     $( "#city-spin" ).hide();
     var html="";
      $.each(data, function( index, value ) {

             html +='<tr>'+
                   
                    ' <td style="font-size:15px">'+index+'</td> '+
                    ' <td align="right"><span class="label label-light-info">'+value+'</span></td> '+
                    ' </tr> ';
        
        
  // alert( index + ": " + value +":" +i);
});
    
     $("#tbl_city_reach tbody").append(html);

        

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });
}
  function posting_status(fday,sday,admin_page)

{
 var brand_id = GetURLParameter('pid');
 // var fday = moment(endDate).subtract(1, 'week');
 // var sday = endDate;
 // fday=fday.format('YYYY MM DD');
 // sday=sday.format('YYYY MM DD');

    $( "#post_spin" ).show();
    $( "#cmt_spin" ).show();

  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getPostingStatus')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page}
    })
    .done(function( data ) {
     $( "#post_spin" ).hide();
     $( "#cmt_spin" ).hide();
    

        var total_post =data[5][0]['total'] ;
        var overall_pos =data[3][0]['ov_positive'] ;
        var overall_neg = data[3][0]['ov_negative'] ;
        var overall_neutral = data[3][0]['ov_neutral'] ;
   
        var all_post_total=parseInt(overall_pos)+parseInt(overall_neg)+parseInt(overall_neutral);
  
        var post_pos_pcent=parseFloat((overall_pos/all_post_total)*100).toFixed(2);
        var post_neg_pcent=parseFloat((overall_neg/all_post_total)*100).toFixed(2);
         var post_neutral_pcent=parseFloat((overall_neutral/all_post_total)*100).toFixed(2);
        
        post_pos_pcent = isNaN(post_pos_pcent)?'-':post_pos_pcent;
        post_neg_pcent = isNaN(post_neg_pcent)?'-':post_neg_pcent;
        post_neutral_pcent = isNaN(post_neutral_pcent)?'-':post_neutral_pcent;
        total_post = isNaN(total_post)?'-':total_post;
        
          $("#post_total").text(total_post==0?'-':total_post);
          $("#post_pos").text(post_pos_pcent==0?'- %':post_pos_pcent.replace(".00", "") + "%");
          $("#post_neg").text(post_neg_pcent==0?'- %':post_neg_pcent.replace(".00", "") + "%");
          $("#post_neutral").text(post_neutral_pcent==0?'- %':post_neutral_pcent.replace(".00", "") + "%");

        var pos =data[1][0]['positive'] ;
        var neg =data[1][0]['negative'] ;
        var neutral = data[1][0]['neutral'] ;
        var NA = data[1][0]['NA'] ;
            neutral=parseInt(neutral)+parseInt(NA);
            
        var total_comment =  data[1][0]['total'] ;
        var all_total=parseInt(pos)+parseInt(neg)+parseInt(neutral);
        var pos_pcent=parseFloat((pos/all_total)*100).toFixed(2);
        var neg_pcent=parseFloat((neg/all_total)*100).toFixed(2);
        var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);

        pos_pcent = isNaN(pos_pcent)?'-':pos_pcent;
        neg_pcent = isNaN(pos_pcent)?'-':neg_pcent;
        neutral_pcent = isNaN(neutral_pcent)?'-':neutral_pcent;
        total_comment = isNaN(total_comment)?'-':total_comment;

          $("#cmt_total").text(total_comment==0?'-':total_comment);
          $("#cmt_pos").text(pos_pcent==0?'- %':pos_pcent.replace(".00", "") + "%");
          $("#cmt_neg").text(neg_pcent==0?'- %':neg_pcent.replace(".00", "") + "%");
          $("#cmt_neutral").text(neutral_pcent==0?'- %':neutral_pcent.replace(".00", "") + "%");

        // var w_total_post = parseInt(data[5][0]['total'])=== 0 ? '-' : data[5][0]['total'] ;
        // var w_overall_pos = parseInt(data[3][0]['ov_positive'])=== 0 ? '-' : data[3][0]['ov_positive'] ;
        // var w_overall_neg = parseInt(data[3][0]['ov_negative'])=== 0 ? '-' : data[3][0]['ov_negative'] ;
        // var w_total_comment = parseInt(data[1][0]['total'])=== 0 ? '-' : data[1][0]['total'] ;
        // var w_pos = parseInt(data[1][0]['positive'])=== 0 ? '-' : data[1][0]['positive'] ;
        // var w_neg = parseInt(data[1][0]['negative'])=== 0 ? '-' : data[1][0]['negative'] ;


        // for(var i in data) {//alert(data[i].sentiment);
          
        //                   }
  

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });
}

  function engagement_status(fday,sday,admin_page)
{
   var brand_id = GetURLParameter('pid');
  var fday = moment(endDate).subtract(1, 'week');
     var sday = endDate;
     fday=fday.format('YYYY MM DD');
     sday=sday.format('YYYY MM DD');
  // var brand_id = 22;
    $( "#engagement-spin" ).show();
    $("#tbl_today_engagement tbody").empty();
    $("#tbl_week_engagement tbody").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getEngagementStatus')}}", // This is the URL to the API
      data: {fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page}
    })
    .done(function( data ) {
     $( "#engagement-spin" ).hide();
      $("#tbl_today_engagement tbody").empty();
     $("#tbl_week_engagement tbody").empty();
      var pid = GetURLParameter('pid');
      var source= GetURLParameter('source');

       var total_post = parseInt(data[0][0]['total_post'])=== 0 ? '-' : data[0][0]['total_post'] ;
       var reaction = parseInt(data[0][0]['total_reaction'])=== 0 ? '-' : data[0][0]['total_reaction'] ;
       var share = parseInt(data[0][0]['shared'])=== 0 ? '-' : data[0][0]['shared'] ;
       var total_comment = parseInt(data[2][0]['total_comment'])=== 0 ? '-' : data[2][0]['total_comment'] ;

       var w_total_post = parseInt(data[1][0]['total_post'])=== 0 ? '-' : data[1][0]['total_post'] ;
       var w_reaction = parseInt(data[1][0]['total_reaction'])=== 0 ? '-' : data[1][0]['total_reaction'] ;
       var w_share = parseInt(data[1][0]['shared'])=== 0 ? '-' : data[1][0]['shared'] ;
       var w_total_comment = parseInt(data[3][0]['total_comment'])=== 0 ? '-' : data[3][0]['total_comment'] ;
       

      $("#tbl_today_engagement tbody").append(
        "<tr><td>Post</td><td>"+total_post+"</td><td>"+reaction+"</td> " +
        " <td>"+share+"</td><td>"+total_comment+"</td></tr>"

        );
      $("#tbl_week_engagement tbody").append(
        "<tr><td>Post</td><td>"+w_total_post+"</td><td>"+w_reaction+"</td> " +
        " <td>"+w_share+"</td><td>"+w_total_comment+"</td></tr>"
        );
        // for(var i in data) {//alert(data[i].sentiment);
          
        //                   }
  

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });
}

  function latest_posts(fday,sday)
{//alert("popular");
   $(".popup").unbind('click');
 var oTable = $('#tbl_latest').DataTable({
        "pageLength": 5,
        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": false,
        "destroy": true,
        "ordering": false,
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        /*ajax: '{!! route('getallmention') !!}',*/
        "ajax": {
          "url": '{{ route('getTopAndLatestPost') }}',
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
          "data": {
            "fday": fday,
            "sday": sday,
            "keyword": '',
            "sentiment": '',
            "format_type": 'latest_post',
            "limit":5,
            "brand_id": GetURLParameter('pid'),
          }

        },
        "initComplete": function( settings, json ) {
          // console.log(json);
        },

        columns: [
        {data: 'post_div', name: 'post_div',orderable: false,searchable: true},
        {data: 'action', name: 'action', orderable: false, searchable: false}

        ]
        
      }).on('click', '.btn-bookmark', function () {
       //change css to bookmark star

       var current = $(this).attr('class');
       var id = $(this).attr('id');
       var name = $(this).attr('name');
  //if no bookmark prior 1- Change it to Bookmark css 2- Push to bookmark_array 3- remove from bookmark remove array if it is exist
       if(current == 'mdi mdi-star-outline text-yellow btn-bookmark')
       {
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star text-yellow btn-bookmark');
        if (jQuery.inArray(id, bookmark_array)=='-1') {
         bookmark_array.push(id) ;
        //need to remove it is exist in remove array
           if (jQuery.inArray(id, bookmark_remove_array)!='-1') {
        
              bookmark_remove_array.splice(jQuery.inArray(id,bookmark_remove_array), 1);
            

          } 
    
        } 

      }
//if  bookmark prior 1- Change it to No Bookmark css 2- Push to bookmark_remove_array 3- remove from bookmark array if it is exist
      else
      {
      /*  console.log("remove");
        console.log(name);*/
        $(this).removeClass(current);
        $(this).addClass('mdi mdi-star-outline text-yellow btn-bookmark');
           if (jQuery.inArray(id, bookmark_remove_array)=='-1') {
            bookmark_remove_array.push(id) ;
        if (jQuery.inArray(id, bookmark_array)!='-1') {
        
              bookmark_array.splice(jQuery.inArray(id,bookmark_array), 1);
            

          } 
    

          }
    
      

        
     }

      
    
  }).on('click', '.popup', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
    //alert(name);
    var post_id = $(this).attr('id');
    $("#modal-spin").show();
    $(".comment_data").empty();

    //alert(post_id);alert(name);alert(GetURLParameter('pid'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getInboundcomments") }}',
         type: 'GET',
         data: {id:post_id,cmt_type:name,brand_id:GetURLParameter('pid')},
         success: function(response) { //console.log(response);
          var data=JSON.parse(response);
         // console.log(data);

            for(var i in data) {
          var html ='<div class="d-flex flex-row comment-row"> '+
                  ' <div class="user-img"> <span class="round">A</span> <span class="profile-status away pull-right"></span> </div> '+
                    '   <div class="comment-text w-100" style="padding:15px 0px 15px 10px;"> '+
               '<span class="text-muted pull-right">'+data[i].created_time+'</span>';
               html+='<p>Sentiment: <select class="form-control custom-select sentiment-color comment-select" id="sentiment_comment_'+data[i].id+'"  >';
                 if(data[i].sentiment == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="pos"';
                if(data[i].sentiment == "pos")
                html+= 'selected="selected"';
                html+= '>pos</option><option value="neg"';
                if(data[i].sentiment == "neg")
                html+= 'selected="selected"';
                html+= '>neg</option><option value="neutral"';
                if(data[i].sentiment == "neutral")
                html+= 'selected="selected"';  
                html+= '>neutral</option><option value="NA"';
                if(data[i].sentiment == "NA")
                html+= 'selected="selected"';  
                html+= '>NA</option></select>';

                html+= ' Emotion: <select class="form-control custom-select emotion-color comment-select" id="emotion_comment_'+data[i].id+'"  >';
                if(data[i].emotion == "")
                html+= '<option value="" selected="selected"></option>';
                html+='<option value="anger"';
                if(data[i].emotion == "anger")
                html+= 'selected="selected"';
                html+= '>anger</option><option value="interest"';
                if(data[i].emotion == "interest")
                html+= 'selected="selected"';     
                html+= '>interest</option><option value="disgust"';
                if(data[i].emotion == "disgust")
                html+= 'selected="selected"';  
                html+= '>disgust</option><option value="fear"';
                if(data[i].emotion == "fear")
                html+= 'selected="selected"'; 
                html+= '>fear</option><option value="joy"';
                if(data[i].emotion == "joy")
                html+= 'selected="selected"'; 
                html+= '>joy</option><option value="like"';
                if(data[i].emotion == "like")
                html+= 'selected="selected"'; 
                html+= '>like</option><option value="love"';
                if(data[i].emotion == "love")
                html+= 'selected="selected"'; 
                html+= '>love</option><option value="neutral"';
                if(data[i].emotion == "neutral")
                html+= 'selected="selected"';
                html+= '>neutral</option><option value="sadness"';
                if(data[i].emotion == "sadness")
                html+= 'selected="selected"';
                html+= '>sadness</option><option value="surprise"';
                if(data[i].emotion == "surprise")
                html+= 'selected="selected"';
                html+= '>surprise</option><option value="trust"';
                if(data[i].emotion == "trust")
                html+= 'selected="selected"';
                html+= '>trust</option><option value="NA"';
                if(data[i].emotion == "NA")
                html+= 'selected="selected"';
                html+= '>NA</option></select>';
                if(data[i].edit_permission === 1)
                html+= ' <a class="edit_predict_comment" id="'+data[i].id+'" href="javascript:void(0)" style="max-width:10%;"><i class="ti-pencil-alt"></i></a> ';
                html+=' </span></p> ' + 
                  ' <div class="m-b-5">'+data[i].message+'</div>'+
                 '<div class="comment-footer">'+
                  
                 '</div>'+
                 '</div>'+
                  '</div>';
       
                $(".comment_data").append(html);
        }


         $("#modal-spin").hide();
         $("#myModalLabel").text(name);
         $('#show-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('click', '.edit_post_latest_post_predict', function (e) {
  if (e.handled !== true) {

    var post_id = $(this).attr('id');
  var sentiment = $('#sentiment_latest_post_'+post_id+' option:selected').val();
  var emotion = $('#emotion_latest_post_'+post_id+' option:selected').val();
 /* alert(post_id);*/
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) { /*alert(response)*/
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });

          e.handled = true;
       }
     
    
        
  });
}

function ReactionData(Like_total,Love_total,Wow_total,Sad_total,Angry_total,Haha_total,total_reaction){
   //alert(Angry_total);alert(Haha_total);alert(Sad_total);

    // if(parseInt(Angry_total) > 0 || parseInt(Sad_total)>0 || parseInt(Haha_total)>0 )
    //          {
                
    //              $("#reaction_pic").empty();
    //              $("#reaction_pic").append('<i class="mdi mdi-emoticon-sad"></i>');

    //               $("#reaction_angry_num").text(Angry_total);
    //               $("#reaction_sad_num").text(Sad_total);
    //               $("#reaction_haha_num").text(Haha_total);
    //          }
    //          else
    //          {

    //              $("#reaction_pic").empty();
    //              $("#reaction_pic").append('<i class="mdi mdi-emoticon-cool"></i>');
    //              $("#reaction_angry_num").text("0");
    //               $("#reaction_sad_num").text("0");
    //               $("#reaction_haha_num").text("0");
                  
             
    //          }

                       $("#reaction-spin").hide();
     
       var reaction=['Like','Love','HaHa','Sad','Wow','Angry'];
      

       if(total_reaction > 0)
       {
         var reaction_count=[kFormatter(Like_total),kFormatter(Love_total),kFormatter(Haha_total),kFormatter(Sad_total),kFormatter(Wow_total),kFormatter(Angry_total)];
         var like_percentage=parseInt((parseInt(Like_total)/parseInt(total_reaction))*100);
         var love_percentage=parseInt((parseInt(Love_total)/parseInt(total_reaction))*100);
         var haha_percentage=parseInt((parseInt(Haha_total)/parseInt(total_reaction))*100);
         var wow_percentage=parseInt((parseInt(Sad_total)/parseInt(total_reaction))*100);
         var sad_percentage=parseInt((parseInt(Wow_total)/parseInt(total_reaction))*100);
         var angry_percentage=parseInt((parseInt(Angry_total)/parseInt(total_reaction))*100);

          like_percentage = isNaN(like_percentage)?0:like_percentage;
          love_percentage = isNaN(love_percentage)?0:love_percentage;
          haha_percentage = isNaN(haha_percentage)?0:haha_percentage;
          wow_percentage = isNaN(wow_percentage)?0:wow_percentage;
          sad_percentage = isNaN(sad_percentage)?0:sad_percentage;
          angry_percentage = isNaN(angry_percentage)?0:angry_percentage;

          
   
     
         /*$('#top-like-progress').css('width', like_percentage+'%').attr('aria-valuenow', like_percentage);
         $("#top-like-value").text(like_percentage + "%");

         $('#top-love-progress').css('width', love_percentage+'%').attr('aria-valuenow', love_percentage);
         $("#top-love-value").text(love_percentage + "%");

         $('#top-haha-progress').css('width', haha_percentage+'%').attr('aria-valuenow', haha_percentage);
         $("#top-haha-value").text(haha_percentage + "%");

         $('#top-wow-progress').css('width', wow_percentage+'%').attr('aria-valuenow', wow_percentage);
         $("#top-wow-value").text(wow_percentage + "%");

         $('#top-sad-progress').css('width', sad_percentage+'%').attr('aria-valuenow', sad_percentage);
         $("#top-sad-value").text(sad_percentage + "%");

         $('#top-angry-progress').css('width', angry_percentage+'%').attr('aria-valuenow', angry_percentage);
         $("#top-angry-value").text(angry_percentage + "%");
*/
        // var pieChart = echarts.init(document.getElementById('reaction-pie-chart'));

// specify chart configuration item and data



     }

  
  }


 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Dashboard'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
function tagCount(fday,sday,admin_page)
{
    $("#tbl_tag_count tbody").empty();
    var brand_id = GetURLParameter('pid');
        $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getTagCount')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page }
    })
    .done(function( data ) {//console.log(data);
for(var i in data) 
        {
           var tagLabel=data[i].tagLabel;
           var tagCount=data[i].tagCount;
           $("#tbl_tag_count tbody").append(' <tr style="border-bottom:1px solid rgba(120, 130, 140, 0.13)"> '+
                                            ' <td style="width:40px" span="2"> <button type="button" class="btn '+
                                            ' btn-rounded btn-block btn-info btntag" value="'+tagLabel+'">'+tagLabel+'</button></td> '+
                                            ' <td></td> '+
                                            '<td class="text-right"> '+
                                            ' <span class="label label-light-info">'+tagCount+'</span></td> '+
                                            ' </tr>')

        }

    })
     .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
    
    });
}
function tagsentimentData(fday,sday,admin_page){//alert(fday);
      
var sentichart = document.getElementById("tag-senti-chart");
var sentiChart = echarts.init(sentichart);

$("#tag-senti-spin").show();
        
    var brand_id = GetURLParameter('pid');
       /* var brand_id = 22;*/
       // alert (brand_id);
    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getTagSentiment')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,admin_page:admin_page }
    })
    .done(function( data ) {//alert(data);
          console.log("tag senti");
    console.log(data);//mention
    var xAxisData = [];
    var data1 = [];
    var data2 = [];


for(var i in data) 
        {
    xAxisData.push(data[i].tagLabel);
    data1.push(Math.round(data[i].positive));
    if(data[i].negative >0 )
    {
    data2.push(Math.round(-data[i].negative));
    }
    else
    {
     data2.push(0);
    }
  
 
}


var itemStyle = {
    normal: {
    },
    emphasis: {
        barBorderWidth: 1,
        shadowBlur: 10,
        shadowOffsetX: 0,
        shadowOffsetY: 0,
        shadowColor: 'rgba(0,0,0,0.5)'
    }
};


option = {
      color:colors,
    backgroundColor: 'rgba(0, 0, 0, 0)',
     dataZoom:[  {
            type: 'slider',
            show: true,
            xAxisIndex: [0],
            start: 70,
            end: 100
        },
         {
            type: 'inside',
            xAxisIndex: [0],
            start: 1,
            end: 35
        }
    ],
        calculable : true,
    legend: {
        data: ['positive', 'negative'],
           x: 'center',
             y: 'top',
              padding :0,
    }/*,
    brush: {
        toolbox: ['rect', 'polygon', 'lineX', 'lineY', 'keep', 'clear'],
        xAxisIndex: 0
    }*/,

    toolbox: {
            show : true,
            feature : {
                mark : {show: true},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['stack','tiled']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },
    tooltip: {},
    calculable : true,
    dataZoom : {
        show : true,
        realtime : true,
        start : 0,
        end : 100
    },
    xAxis: {
        data: xAxisData,
        name: 'Tags',
        silent: false,
        axisLine: {onZero: true},
        splitLine: {show: false},
        splitArea: {show: false},
       /*  axisLabel: {
            textStyle: {
                color: '#fff'
            }
        },*/
    },
    yAxis: {
        inverse: false,
        splitArea: {show: false}
    },
   grid: {
            top: '12%',
            left: '1%',
            right: '10%',
            containLabel: true
        },
  /*  visualMap: {
        type: 'continuous',
        dimension: 1,
        text: ['High', 'Low'],
        inverse: true,
        itemHeight: 200,
        calculable: true,
        min: -2,
        max: 6,
        top: 60,
        left: 10,
        inRange: {
            colorLightness: [0.4, 0.8]
        },
        outOfRange: {
            color: '#bbb'
        },
        controller: {
            inRange: {
                color: '#2f4554'
            }
        }
    },*/
    series: [
        {
            name: 'positive',
            type: 'bar',
            stack: 'one',
            color:colors[0],
            barMaxWidth:30,
            itemStyle: itemStyle,
            data: data1
        },
        {
            name: 'negative',
            type: 'bar',
            stack: 'one',
            color:colors[1],
            barMaxWidth:30,
            itemStyle: itemStyle,
            data: data2
        }
    ]
};

$("#tag-senti-spin").hide();
/*categoryChart.on('brushSelected', renderBrushed);

function renderBrushed(params) {
    var brushed = [];
    var brushComponent = params.batch[0];

    for (var sIdx = 0; sIdx < brushComponent.selected.length; sIdx++) {
        var rawIndices = brushComponent.selected[sIdx].dataIndex;
        brushed.push('[Series ' + sIdx + '] ' + rawIndices.join(', '));
    }
}
*/
    sentiChart.setOption({
        title: {
            backgroundColor: '#333',
          /*  text: 'SELECTED DATA INDICES: \n' + brushed.join('\n'),*/
            bottom: 0,
            right: 0,
            width: 100,
            textStyle: {
                fontSize: 12,
                color: '#fff'
            }
        }
    });

    sentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            sentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
   sentiChart.on('click', function (params) {
   console.log("tags");
   console.log(params);
   console.log(params.name); // xaxis data = tag name
   console.log(params.seriesName); //bar type name ="positive"
   console.log(params.value);//count 8
   var pid = GetURLParameter('pid');
   var source = GetURLParameter('source');
   var CmtType ='';
   if(params.seriesName == 'positive')
    CmtType = 'pos'
    else
   CmtType = 'neg'

   // window.open("{{ url('relatedcomment?')}}" +"pid="+ pid +"&source="+ source+"&fday="+ GetStartDate()+
   // "&sday="+GetEndDate()+"&type="+ params.seriesName +"&tag="+ params.name +"&from_graph=sentiment&admin_page="+admin_page  , '_blank');
   window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+params.name+"&CmtType="+CmtType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate() , '_blank');
});


   

  
    })
     .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

  }

   function ChooseDate(start, end,admin_page='',label='') {//alert(start);
          $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
          startDate=start;
          endDate=end;
         var date_preset='this_month'; 
         var start = new Date(startDate);
         var end = new Date(endDate);
         var current_Date = new Date();
         var current_year =new Date().getFullYear();
         var timeDiff = Math.abs(current_Date.getTime() - start.getTime());
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
         var admin_page = $( "#admin_page_filter" ).val();
         

        if(label === 'Today' )  date_preset = 'today';
        else if (label === 'Yesterday')  date_preset = 'yesterday';
        else if (label === 'Last 7 Days' || diffDays <=7)  date_preset = 'last_7d';
        else if (label === 'Last 30 Days' ||  diffDays <=30 )  date_preset = 'last_30d';
        else if (label === 'This Quarter')  date_preset = 'this_quarter';
        else if (label === 'Last 90 Days' || diffDays <=90)  date_preset = 'last_90d';
        else if (label === 'This Year')  date_preset = 'this_year';
        else if (label === 'Last Year')  date_preset = 'last_year';
        else if (diffDays >90 && current_year === end.getFullYear() ) date_preset = 'this_year';

            TotalReach(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),admin_page,date_preset);
            posting_status(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),admin_page);
            tagCount(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),admin_page);
            CityReach(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),admin_page);
            GenderReachData(admin_page);
            FanPage(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),admin_page,date_preset);
            tagsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),admin_page);
        }

        function GetStartDate()
{
   //alert(startDate);
 return startDate.format('YYYY MM DD');
}
function GetEndDate()
{
           // alert(endDate);
return endDate.format('YYYY MM DD');
}
$(document).on('click', '.btntag', function(e) {
    var search_tag = $(this).attr('value');
    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&search_tag="+search_tag+"&fday="+ GetStartDate()+"&sday="+ GetEndDate() , '_blank');
   
});
$(".btnComment").click(function(e){
    var commentType = $(this).attr('value');
    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&CmtType="+commentType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate() , '_blank');
})
$(".btnPost").click(function(e){
    var postType = $(this).attr('value');
    window.open("{{ url('relatedpost?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&overall="+postType+"&fday="+ GetStartDate()+"&sday="+ GetEndDate() , '_blank');
})
$(document).on('click', '.edit_predict_comment', function(e) {
     var comment_id = $(this).attr('id');
   
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_comment_'+comment_id+' option:selected').val();
  var emotion = $('#emotion_comment_'+comment_id+' option:selected').val();
  

         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPredict") }}',
         type: 'POST',
         data: {id:comment_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {//alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });
     
});

$('.dateranges').daterangepicker({
    locale: {
            format: 'MMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end,label) {//alert(label);
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,'',label);
      });
 ChooseDate(startDate,endDate,'','');

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
   window.dispatchEvent(new Event('resize'));

 });

$(".react_div").click(function(event) {
  //window.open("http://cms.baganintel.com/BaganSocialListener/pointoutpost?pid=17&source=in&fday=2018 10 10&sday=2018 10 17&reaction_type=angry&from_graph=reaction", '_blank');
    var class_id =$(this).attr('id');
      //alert(class_id);
      // alert(GetStartDate());
      // alert(GetEndDate());
        var pid= GetURLParameter('pid');
        var reaction_type =$(".react_type").attr("name");
         var admin_page = $( "#admin_page_filter" ).val();
     //   alert(reaction_type);
        var source= GetURLParameter('source');
        // alert("{{ url('pointoutpost?')}}" +"pid="+ pid +"&source="+source+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&reaction_type="+ class_id +"&from_graph=reaction");
      	window.open("{{ url('pointoutpost?')}}" +"pid="+ pid +"&source="+source+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&reaction_type="+ class_id +"&from_graph=reaction&admin_page="+admin_page , '_blank');


	})
$(".top_div").click(function(event) {
      var class_id = $(event.target).attr('id');
      var pid = GetURLParameter('pid');
         var source= GetURLParameter('source');
          var admin_page = $( "#admin_page_filter" ).val();
      if(class_id !== "interest")
      {
       window.open("{{ url('pointoutcomment?')}}" +"pid="+ pid +"&source="+ source+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&type="+ class_id +"&from_graph=sentiment&admin_page="+admin_page  , '_blank');
      }
      
      else
      {
         window.open("{{ url('pointoutcomment?')}}" +"pid="+ pid +"&source="+ source+"&fday="+ GetStartDate()+"&sday="+ GetEndDate()+"&type="+ class_id +"&from_graph=enquiry&admin_page="+admin_page  , '_blank');
      }
     
        

  
     
    });

    $('input:radio').change(function(){
       // alert('changed');   
         var name = $(this).attr("name");
         var id =$('input[type=radio][name='+name+']:checked').attr('id');
         // alert( moment(endDate));
         if(id==="period-week")
         {
           startDate = moment(endDate).subtract(1, 'week');
           endDate = endDate;
         }
         else
         {
           startDate = moment(endDate).subtract(1, 'month');
           endDate = endDate;
         }
         // alert(startDate);
         // alert(endDate);
         var admin_page=$("#admin_page_filter").val();

         ChooseDate(startDate,endDate,admin_page,'');
    });   

hidden_div();




  $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
     // requestmentionData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
   
     //  requestsentimentData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
     //  requestinterestData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
     //  popular_mentions(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
     //  latest_posts(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
     //  posting_status(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
     //  engagement_status(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
     //  ReactionData(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
  
  });

 $(document).on('click', '.edit_predict_latest', function(e) {
     var post_id = $(this).attr('id');
   
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_latest_'+post_id+' option:selected').val();
  var emotion = $('#emotion_latest_'+post_id+' option:selected').val();
 // alert(sentiment + emotion);

         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {//alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });
     
});

$(document).on('click', '.edit_predict_popular', function() {
     var post_id = $(this).attr('id');
  //   alert(post_id);
    
/*    var sentiment = $('input[name=sentiment]').val();
    var emotion = $('input[name=emotion_'+post_id+']').val();*/
  var sentiment = $('#sentiment_popular_'+post_id+' option:selected').val()
  var emotion = $('#emotion_popular_'+post_id+' option:selected').text()
  //alert(post_id);
   
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("setUpdatedPostPredict") }}',
         type: 'POST',
         data: {id:post_id,brand_id:GetURLParameter('pid'),sentiment:sentiment,emotion:emotion},
         success: function(response) {// alert(response)
          if(response>=0)
          {
                swal({   
            title: "Updated!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
          }
        }
            });
});

$('#admin_page_filter').change(function() {
    //alert($(this).val());
    var admin_page = $(this).val();
   ChooseDate(startDate,endDate,admin_page,'');
     // $(this).val() will work here
});

$( "#btn_bookmark" ).click(function() {
  //alert(bookmark_array);
  //alert(bookmark_remove_array);
  if(Object.keys(bookmark_array).length > 0 || Object.keys(bookmark_remove_array).length > 0)
  {
        //save bookmark data;
        //console.log(bookmark_remove_array);
        event.preventDefault();

        $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("in_postbookmark") }}',
         type: 'POST',
         data: {bookmark_array:bookmark_array,bookmark_remove_array:bookmark_remove_array,id:GetURLParameter('pid')},
         success: function(response) {//alert(response);
         swal({   
            title: "Bookmark!",   
            text: "Done!",   
            timer: 1000,   
            showConfirmButton: false 
        });
         
        }
      });


      }
    });
//local function

function numberWithCommas(n) {
    var parts=n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

  function kFormatter(num) {
    return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
  function readmore(message){
      // alert("hi hi ");
        var string = String(message);
        var length = string.length; 
         // alert(length);
                if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.
             
            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
        }
        return string;


        }

         function judge_sentiment_icon(sentiment)
        {
         var emojis = ['0x1F60E', '0x1F626', '0x1F610'];//
        if (sentiment ==="pos") return  String.fromCodePoint(emojis[0]) ; 
        else if(sentiment ==="neg") return  String.fromCodePoint(emojis[1]);
        else return String.fromCodePoint(emojis[2])  ;
        }

        function judge_emotion_icon(emotion)
        {
            var emojis = ['0x1F620', '0x1F604', '0x1F616', '0x1F628', '0x1F604', '0x1F44D',
            '0x2764', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

        if (emotion ==="anger") return emojis[0]; 
        else if(emotion ==="interest") return emojis[1] ;
         if (emotion ==="disgust") return emojis[2] ; 
        else if(emotion ==="fear") return emojis[3] ;
         if (emotion ==="joy") return emojis[4]; 
        else if(emotion ==="like") return emojis[5] ;
         if (emotion ==="love") return emojis[6] ; 
        else if(emotion ==="neutral") return emojis[7] ;
         if (emotion ==="sadness")  return emojis[8]; 
        else if(emotion ==="surprise") return emojis[9]  ;
         else if(emotion === "trust") return emojis[10];

        }

  });
    </script>
    <style type="text/css">
/*.table thead th, .table th {
    border: 1px solid;
}
.table td, .table th {
    border-color: #4267b2;
}

.table td, .table th {
    padding: .3rem;
    vertical-align: top;
    border: 1px solid #4267b2;
}*/
.table td, .table th {
    padding: .75rem .5rem .75rem .5rem;
    }
    
.myHeaderContent
{
   margin-right:300px;
}

    </style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush

