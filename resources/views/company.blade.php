@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')

                 <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:5px;padding-top:5px;font-weight: 500">Company Information</h4>
                       <!--  <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Brand List</li>
                        </ol> -->
                    </div>



                </div>


@if(count($data)<=0)
<!-- <div class="box box-warning"> -->
            <div class="">
             <!--  <h3 class="">{{ __('Register') }}</h3> -->
            </div>
              @if(session()->has('message'))
            <div class="">
              {{ session()->get('message') }}
            </div>
          @endif
              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                       <form enctype="multipart/form-data" class="form-material m-t-10" method="POST" role="form" action="{{ route('companyInfo.store') }}" aria-label="{{ __('CompanyInfo') }}"> 
                       <!--  {!! Form::open(array('route' => 'companyInfo.store','class'=>'form-material m-t-40','method'=>'POST','enctype'=>'multipart/form-data')) !!} -->
                {{csrf_field()}}
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            <div class="form-group">
                <label for="phone" class="control-label">{{ __('Phone') }}</label>
                 <input id="phone" type="text"
               class="form-control form-control-line{{ $errors->has('phone') ? ' is-invalid' : '' }}"
               name="phone" value="{{ old('phone') }}" required>
                 @if ($errors->has('phone'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="address" class="control-label">{{ __('Address') }}</label>
                 <textarea id="address" type="text" rows="3"
               class="form-control form-control-line{{ $errors->has('address') ? ' is-invalid' : '' }}"
               name="address" value="{{ old('address') }}" required> </textarea>
                 @if ($errors->has('address'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="logo_path" class="control-label">{{ __('Upload Logo') }}</label>
                  <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                            <input id="logo_path" type="file" name="logo_path"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
              <!--   <input type="file" name="logo_path" id="logo_path" class="form-control"> -->
                 @if ($errors->has('logo_path'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('logo_path') }}</strong>
                    </span>
                @endif
            </div>
 
            <div class="form-group">
                <button type="submit" class="btn btn-primary">  {{ __('Save') }}</button>
              </div>
             </form>
                            </div>

                        </div>
                    </div>
                </div>
            <!-- </div> -->
            @else
            <div class="">
             <!--  <h3 class="">{{ __('Register') }}</h3> -->
            </div>
              @if(session()->has('message'))
            <div class="">
              {{ session()->get('message') }}
            </div>
          @endif
              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        {!! Form::model($data, ['method' => 'PATCH','class'=>'form-material m-t-10','enctype'=>'multipart/form-data','route' => ['companyInfo.update', $data->id]]) !!}
                {{csrf_field()}}
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old( 'name', $data->name) }}"  required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
           <div class="form-group">
                <label for="phone" class="control-label">{{ __('Phone') }}</label>
                 <input id="phone" type="text"
               class="form-control form-control-line{{ $errors->has('phone') ? ' is-invalid' : '' }}"
               name="phone" value="{{ old('phone', $data->phone) }}" required>
                 @if ($errors->has('phone'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="address" class="control-label">{{ __('Address') }}</label>
                 <textarea id="address" type="text" rows="3"
               class="form-control form-control-line{{ $errors->has('address') ? ' is-invalid' : '' }}"
               name="address"  required> {{$data->address}} </textarea>
                 @if ($errors->has('address'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>
               <div class="form-group">
                <label for="logo_path" class="control-label">{{ __('Upload Logo') }}</label>
                  <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                            <input id="logo_path" type="file" name="logo_path" > </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
              <!--   <input type="file" name="logo_path" id="logo_path" class="form-control"> -->
                 @if ($errors->has('logo_path'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('logo_path') }}</strong>
                    </span>
                @endif
            </div>
         
            <div class="form-group">
                <button type="submit" class="btn btn-primary">  {{ __('Register') }}</button>
              </div>
             <!-- {!! Form::close() !!} -->
           </form>
                            </div>

                        </div>
                    </div>
                </div>

@endif
              
                  <!-- Row -->
           
              
                
       
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<script src="{{asset('js/jasny-bootstrap.js')}}"></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
  <script type="text/javascript">

$(document).ready(function() {
   


    });

</script>
@endpush

