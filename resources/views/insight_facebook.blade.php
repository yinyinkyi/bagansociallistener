@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('admin_page_filter')
<li class="nav-item"> 
<div class="btn-group">
        <select id="admin_page_filter" class="form-control custom-select">
        <!-- <option value="">Admin Pages</option> -->
        @if (isset($ownpage))
        @foreach($ownpage as $ownpage)
        <option value="{{$ownpage}}" id="{{$ownpage}}" >{{$ownpage}}</option>
        @endforeach
        @endif
                                                        
                                                   
      </select>
 
                                        </div>
                         </li>
@endsection
@section('content')
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Insight</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Insight</li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class='input-group mb-3'>
                         <input type='text' class="form-control dateranges" style="color:#01c0c8 !important;width: 18.5em; padding: .2em .2em 0 ;" />
                         <div class="input-group-append">
                         <span class="input-group-text">
                         <span class="ti-calendar"></span>
                         </span>
                         </div>
                         </div>
                            </div>
                       <!--      <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>LAST MONTH</small></h6>
                                    <h4 class="m-t-0 text-primary">$48,356</h4></div>
                                <div class="spark-chart">
                                    <div id="lastmonthchart"></div>
                                </div>
                            </div> -->
                          
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-3 col-xlg-2 col-md-4">
                        <div class="stickyside">
                            <div class="list-group" id="top-menu">
                                <a href="#1" class="list-group-item active">Total Reach</a>
                                <a href="#22" class="list-group-item">People Reach</a>
                                <a href="#3" class="list-group-item">City Reach</a>
                                <a href="#4" class="list-group-item">Page Fans</a>
                                <a href="#5" class="list-group-item">Fan's Cities</a>
                                <a href="#6" class="list-group-item">Fan's Age Gender</a>
                                <!-- <a href="#7" class="list-group-item">Title will be 7</a>
                                <a href="#8" class="list-group-item">Title will be 8</a>
                                <a href="#9" class="list-group-item">Title will be 7</a>
                                <a href="#10" class="list-group-item">Title will be 8</a> -->
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-xlg-10 col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="page-section card-title" id="1">Reach
                                <div style="display:none"  align="center" style="vertical-align: top;" id="reach-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                <div id="total-reach-chart" style="width:100%; height:320px;"></div>
                                </div>
                                   <div class="page-section card-title m-t-40" id="22">People Reach
                               
                                <div style="display:none"  align="center" style="vertical-align: top;" id="reach-gender-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                 <div style="width:100%; height:320px;">
                                <div style="width:100%;">
                                <div id="div_legend_Women" style="width:20%; height:150px;float:left;padding-left:20px;padding-top:50px">
                                    Women<br><span class="fa fa-square m-r-10" style="color:#899bc1"> </span><span id="pcent_legend_Women" style="font-weight:bold"> 0</span>%<br><span style="font-size:12px">People Reached</span></div>
                                <div id="reach-age-gender-chart" style="width:80%; height:150px;float:right"></div>
                                </div>
                                <div style="width:100%;">
                                <div id="div_legend_Men" style="width:20%; height:150px;float:left;vertical-align: middle;padding-left:20px;">
                                    Men<br><span class="fa fa-square m-r-10" style="color:#3c6399"> </span><span id="pcent_legend_Men" style="font-weight:bold"> 0</span>%<br> <span style="font-size:12px">People Reached</span></div>
                                <div id="reach-age-gender-chart-1" style="width:80%; height:150px;float:right"></div>
                                </div>
                                </div>
                                </div>
                                <div class="page-section card-title m-t-40" id="3">City Reach
                                <div class="table-responsive">
                                    <table class="table color-table info-table" id="tbl_cityReach" style="width:80%;margin-left:50px">
                                        <thead>
                                            <tr>
                                              
                                                <th>City</th>
                                                <th>Your Fans</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                               
                              
                                <div class="page-section card-title m-t-40" id="4">Page Fans
                                <div style="display:none"  align="center" style="vertical-align: top;" id="fan-page-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                <div id="fan-page-chart" style="width:100%; height:320px"></div>
                                </div>
                                <div class="page-section card-title m-t-40" id="5">Fan's Cities
                                   <div class="table-responsive">
                                    <table class="table color-table info-table" id="tbl_cityFan" style="width:80%;margin-left:50px">
                                        <thead>
                                            <tr>
                                              
                                                <th>City</th>
                                                <th>Your Fans</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                                <div class="page-section card-title m-t-40" id="6">Fans Age Gender
                                <div style="display:none"  align="center" style="vertical-align: top;" id="fan-gender-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                                 <div style="width:100%; height:320px;">
                                <div style="width:100%;">
                                <div style="width:20%; height:150px;float:left;padding-left:20px;padding-top:50px">
                                    Women<br><span class="fa fa-square m-r-10" style="color:#899bc1"> </span><span id="pcent_Fan_Women" style="font-weight:bold"> 0</span>%<br><span style="font-size:12px">Your Fans</span></div>
                                <div id="fan-age-gender-chart" style="width:80%; height:150px;float:right"></div>
                                </div>
                                <div style="width:100%;">
                                <div id="div_legend_Men" style="width:20%; height:150px;float:left;vertical-align: middle;padding-left:20px;">
                                    Men<br><span class="fa fa-square m-r-10" style="color:#3c6399"> </span><span id="pcent_Fan_Men" style="font-weight:bold"> 0</span>%<br> <span style="font-size:12px">Your Fans</span></div>
                                <div id="fan-age-gender-chart-1" style="width:80%; height:150px;float:right"></div>
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                

              
                
       
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" ></script>
   <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script> -->
   <!--  <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script> -->
 
    
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   
   <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript')}}" defer></script>
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
   <script>
   $(document).ready(function() {
    startDate = moment().subtract(1, 'month');
    endDate = moment();
    var  colors=["#ffca87","#f79600"];
    // This is for the sticky sidebar    


    $(".stickyside").stick_in_parent({
        offset_top: 100
    });
    $('.stickyside a').click(function() {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top -100
        }, 600);
        return false;
    });
    // This is auto select left sidebar
    // Cache selectors
    // Cache selectors
    // var lastId,
    //     topMenu = $(".stickyside"),
    //     topMenuHeight = topMenu.outerHeight(),

    //     // All list items
    //     menuItems = topMenu.find("a"),
    //     // Anchors corresponding to menu items
    //     scrollItems = menuItems.map(function() {
    //         var item = $($(this).attr("href"));
    //         if (item.length) {
    //             return item;
    //         }
    //     });

        $(window).scroll(function() {
        var scrollDistance = $(window).scrollTop();

        // Show/hide menu on scroll
        //if (scrollDistance >= 850) {
        //      $('nav').fadeIn("fast");
        //} else {
        //      $('nav').fadeOut("fast");
        //}
    
        // Assign active class to nav links while scolling
        $('.page-section').each(function(i) {
                if ($(this).position().top <= scrollDistance) {
                        $('.list-group a.active').removeClass('active');
                        $('.list-group a').eq(i).addClass('active');
                }
        });
}).scroll();
      
    // Bind click handler to menu items
  
    // Bind to scroll
    // $(window).scroll(function() {
    //     // Get container scroll position
    //     var fromTop = $(this).scrollTop() + topMenuHeight - 140;

    //     // Get id of current scroll item
    //     var cur = scrollItems.map(function() {
    //         if ($(this).offset().top < fromTop)
    //             return this;
    //     });
        
    //     // console.log(cur);

    //     // Get the id of the current element
    //     cur = cur[cur.length - 1];
    //     // alert(cur.length);
    //     var id = cur && cur.length ? cur[0].id : "";

    //     if (lastId !== id) {//alert(id);
    //         lastId = id;
    //         // Set/remove active class
    //         menuItems
    //             .removeClass("active")
    //             .filter("[href='#" + id + "']").addClass("active");
    //     }
    // });


function ChoosePage()
{
 var admin_page = $( "#admin_page_filter" ).val();
 CityReach(admin_page);
 GenderReachData(admin_page);
 CityFans(admin_page);
 GenderFansData(admin_page);
}

    function ChooseDate(start,end,label)
{//alert(start);alert(end),alert(keyword)
 $('.dateranges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
 startDate=start;
 endDate=end;

 var date_preset='this_month'; 
 var start = new Date(startDate);
 var end = new Date(endDate);
 var current_Date = new Date();
 var current_year =new Date().getFullYear();
 var timeDiff = Math.abs(current_Date.getTime() - start.getTime());
 var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
 var admin_page = $( "#admin_page_filter" ).val();
 

if(label === 'Today' )  date_preset = 'today';
else if (label === 'Yesterday')  date_preset = 'yesterday';
else if (label === 'Last 7 Days' || diffDays <=7)  date_preset = 'last_7d';
else if (label === 'Last 30 Days' ||  diffDays <=30 )  date_preset = 'last_30d';
else if (label === 'This Quarter')  date_preset = 'this_quarter';
else if (label === 'Last 90 Days' || diffDays <=90)  date_preset = 'last_90d';
else if (label === 'This Year')  date_preset = 'this_year';
else if (label === 'Last Year')  date_preset = 'last_year';
else if (diffDays >90 && current_year === end.getFullYear() ) date_preset = 'this_year';

 ReachData(date_preset,admin_page,startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
 FanPage(date_preset,admin_page,startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));


        }
        function ReachData(date_preset,page_name,startDate,endDate){//alert(startDate);alert(endDate);alert(date_preset);
    var reachchart = document.getElementById('total-reach-chart');
    var reachChart = echarts.init(reachchart);
    $('#reach-spin').show();
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getFbReach')}}", // This is the URL to the API
      data: { date_preset: date_preset,period:'day',page_name:page_name,fday:startDate,sday:endDate}
    })
    .done(function( data ) {console.log(data);
        
      var organic = [];
      var paid = [];
      var total=[];
      var endtime = [];

         for(var i in data) {//alert(data[i].mentions);
        organic.push(data[i].organic);
        paid.push(data[i].paid);
        total.push(data[i].total);
        endtime.push(data[i].end_time);
        
      }
      console.log(endtime);
      option = null;
      option = {
        color: colors,
    title: {
        text: ''
    },
    tooltip : {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
              },
               formatter: function (params) {
                    let total = 0;
            var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
            let rez = '<p>' + params[0].name + '</p>';
       /* console.log(rez);*/ //quite useful for debug
       params.forEach(item => {
            /*console.log(item);
            console.log(item.data);*/
            total += item.data;
            console.log("total");
            console.log(total);
            var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName + ': ' + formatNumber(item.data)  + '</p>'
            rez += xx;
          });
          rez += '<p>'   + colorSpan("#ff5722") + ' Total: ' + formatNumber(total)  + '</p>';
       return rez;
   
        },

            label: {
                backgroundColor: '#6a7985'
            }
    },
    legend: {
        data:['Organic','Paid']
    },
    toolbox: {
        // feature: {
        //     saveAsImage: {}
        // }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            // position: 'top',
            boundaryGap : true,
              axisLabel: {
      formatter: function (value, index) {
     var date = new Date(value);
       console.log(date);
       return date.getDate();

},

    },
            data : endtime,

        }
    ],
    yAxis : [
        {
            // show: false,
             // inverse:true,
            type : 'value',
             axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }
    }
        }
    ],
    series : [
        {
            name:'Organic',
            type:'line',
            stack: 'Total Amount',
            areaStyle: {},
            data:organic
        },
        {
            name:'Paid',
            type:'line',
            stack: 'Total Amount',
            // label: {
            //     normal: {
            //         show: true,
            //         position: 'top'
            //     }
            // },
            areaStyle: {normal: {}},
            data:paid
        }
    ]
};
 if (option && typeof option === "object") {
    reachChart.setOption(option, true);

}
$('#reach-spin').hide();
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in Reach API" );
            });
  }
function FanPage(date_preset,page_name,startDate,endDate)
{//alert(startDate);alert(endDate);alert(date_preset);
    var pagefanchart = document.getElementById('fan-page-chart');
    var pageFanChart = echarts.init(pagefanchart);
    $('#fan-page-spin').show();
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getPageFan')}}", // This is the URL to the API
      data: { date_preset: date_preset,page_name:page_name,fday:startDate,sday:endDate}
    })
    .done(function( data ) {
        
      var fan = [];
      var fan_online = [];
      var endtime = [];

         for(var i in data) {//alert(data[i].mentions);
        fan.push(data[i].fan);
        fan_online.push(data[i].fan_online);
        endtime.push(data[i].end_time);
        
      }

      option = null;
     option = {
    color: ['#e5e5e5','#899bc1'],
    tooltip : {
        trigger: 'axis',
       
         // formatter: function (params) { console.log(params);
         //    return "Women " + params[0].name + " make up" + '</br>' + params[0].value +  "% of people reach";
         // }
        
    },
//      legend: {
//         data:['Women'],
//         position: 'bottom',
//         formatter: function (name) {
//     return  name + " " + FLegend +'%';
// }
//     },
    grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top: '2%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            axisTick: {
                alignWithLabel: true
            },
               boundaryGap : true,
              axisLabel: {
      formatter: function (value, index) {
     var date = new Date(value);
       console.log(date);
       return date.getDate();

},

    },
            data : endtime,

        }
    ],
    yAxis : [
        {
            
             // inverse:true,
            type : 'value',
                     axisLabel: {
        formatter: function (e) {
            return kFormatter(e);
        }
    }
        }
    ],
    series : [
        {
            name:'Fan',
            type:'bar',
            barGap:'-88%',
            barWidth: '80%',
            data:fan
        },
        {
            name:'Online',
            type:'bar',
            barWidth: '60%',
            data:fan_online
        }
    ]
};
 if (option && typeof option === "object") {
    pageFanChart.setOption(option, true);

}
$('#fan-page-spin').hide();
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in FAN API" );
            });
  }
 function CityReach(page_name){//alert(startDate);alert(endDate);alert(date_preset);
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
            url: "{{route('getcityReach')}}", // This is the URL to the API
            data: {page_name:page_name}
    })
    .done(function( data ) {
        
      var organic = [];
      var paid = [];
      var total=[];
      var endtime = [];
      var newRowContent = '<tr  class="header" style="cursor:pointer;display: none">'+
                          '<td colspan="2">See More</td>'+
                          '</tr>'+
                          '<tr>'+
                          '<td>1</td>'+
                          '<td>Nigam</td>'+
                          '</tr>'+
                          '<tr>'+
                          '<td>2</td>'+
                          '<td>Deshmukh</td>'+
                          '</tr>'+
                          '<tr  class="header" style="cursor:pointer;">'+
                          '<td colspan="2">See More</td>'+
                          '</tr>'+
                          '<tr  style="display: none;">'+
                          '<td>3</td>'+
                          '<td>Roshan</td>'+
                          '</tr>"';

     
      var i=0;
      var newRowContent ='<tr  class="header" style="cursor:pointer;display: none">'+
                          '<td colspan="2">See More</td>'+
                          '</tr>';
      var newRowContent_expend='<tr  class="header" style="cursor:pointer;">'+
                          '<td colspan="2">See More</td>'+
                          '</tr>';
      $.each(data, function( index, value ) {
        i=i+1;
        if(i<=10)
        {
             newRowContent +='<tr>'+
                          '<td>'+index+'</td>'+
                          '<td>'+value+'</td>'+
                          '</tr>';
               
        }
        else
        {
         newRowContent_expend +='<tr  style="display: none;">'+
                          '<td>'+index+'</td>'+
                          '<td>'+value+'</td>'+
                          '</tr>"';
        
        }
        
  // alert( index + ": " + value +":" +i);
});
        $("#tbl_cityReach tbody").append(newRowContent);
        $("#tbl_cityReach tbody").append(newRowContent_expend);
      //    for(var i in data) {//alert(data[i].mentions);
      //   organic.push(data[i].organic);
      //   paid.push(data[i].paid);
      //   total.push(data[i].total);
      //   endtime.push(data[i].end_time);
        
      // }
      
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in Reach API" );
            });
  }

$(document).on("click", ".header", function () {
$(this).nextUntil('tr.header').slideToggle(1000);
$(this).hide();

});

function CityFans(page_name)
{
    $.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
            url: "{{route('getcityFan')}}", // This is the URL to the API
            data: {page_name:page_name}
    })
    .done(function( data ) {
        
      var organic = [];
      var paid = [];
      var total=[];
      var endtime = [];
      var newRowContent = '<tr  class="fan_header" style="cursor:pointer;display: none">'+
                          '<td colspan="2">See More</td>'+
                          '</tr>'+
                          '<tr>'+
                          '<td>1</td>'+
                          '<td>Nigam</td>'+
                          '</tr>'+
                          '<tr>'+
                          '<td>2</td>'+
                          '<td>Deshmukh</td>'+
                          '</tr>'+
                          '<tr  class="fan_header" style="cursor:pointer;">'+
                          '<td colspan="2">See More</td>'+
                          '</tr>'+
                          '<tr  style="display: none;">'+
                          '<td>3</td>'+
                          '<td>Roshan</td>'+
                          '</tr>"';

     
      var i=0;
      var newRowContent ='<tr  class="fan_header" style="cursor:pointer;display: none">'+
                          '<td colspan="2">See More</td>'+
                          '</tr>';
      var newRowContent_expend='<tr  class="fan_header" style="cursor:pointer;">'+
                          '<td colspan="2">See More</td>'+
                          '</tr>';
      $.each(data, function( index, value ) {
        i=i+1;
        if(i<=10)
        {
             newRowContent +='<tr>'+
                          '<td>'+index+'</td>'+
                          '<td>'+value+'</td>'+
                          '</tr>';
               
        }
        else
        {
         newRowContent_expend +='<tr  style="display: none;">'+
                          '<td>'+index+'</td>'+
                          '<td>'+value+'</td>'+
                          '</tr>"';
        
        }
        
  // alert( index + ": " + value +":" +i);
});
        $("#tbl_cityFan tbody").append(newRowContent);
        $("#tbl_cityFan tbody").append(newRowContent_expend);
      //    for(var i in data) {//alert(data[i].mentions);
      //   organic.push(data[i].organic);
      //   paid.push(data[i].paid);
      //   total.push(data[i].total);
      //   endtime.push(data[i].end_time);
        
      // }
      
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in Reach API" );
            });
}
$(document).on("click", ".fan_header", function () {
$(this).nextUntil('tr.header').slideToggle(1000);
$(this).hide();

});
  function GenderReachData(page_name){//alert(startDate);alert(endDate);alert(date_preset);
    var reachgenderchart = document.getElementById('reach-age-gender-chart');
    var reachgenderchart_1=document.getElementById('reach-age-gender-chart-1')
    var reachGenderChart = echarts.init(reachgenderchart);
    var reachGenderChartBottom = echarts.init(reachgenderchart_1);
    $('#reach-gender-spin').show();
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('get-agegender-Reach')}}", // This is the URL to the API
      data: {page_name:page_name}
    })
    .done(function( data ) {
     
       var count = Object.keys(data).length;//last record id
       var keys = Object.keys(data);//get only key
       var last_key = keys[count-1]; //to get last key's value total
        
      var F = [];
      var M = [];
      var F_val_arr = [];
      var M_val_arr = [];
      var Label =[];
      var Total = 0;
      var Ftotal = 0;
      var Mtotal =0;
        for(var i in data) {
        Total=data[last_key].T;
        var F_val=data[i].F;
        var M_val=data[i].M;
        F.push((parseFloat(F_val)/parseFloat(Total)*100).toFixed(3));
        M.push((parseFloat(M_val)/parseFloat(Total)*100).toFixed(3));
        F_val_arr.push(F_val);
        M_val_arr.push(M_val);
        Label.push(data[i].Label);
              
      }
       $.each( F_val_arr,function(){Ftotal+=parseInt(this) || 0;});
       $.each( M_val_arr,function(){Mtotal+=parseInt(this) || 0;});

     var FLegend=(parseFloat(Ftotal)/parseFloat(Total)*100).toFixed(2);
     var MLegend=(parseFloat(Mtotal)/parseFloat(Total)*100).toFixed(2);
     $("#pcent_legend_Women").text(FLegend);
     $("#pcent_legend_Men").text(MLegend);

    option = null;
    option_bottom = null;
    option = {
    color: ['#899bc1'],
    tooltip : {
        trigger: 'axis',
        axisPointer : {            
            type : 'shadow'       
        },
         formatter: function (params) { console.log(params);
            return "Women " + params[0].name + " make up" + '</br>' + params[0].value +  "% of people reach";
         }
        
    },
//      legend: {
//         data:['Women'],
//         position: 'bottom',
//         formatter: function (name) {
//     return  name + " " + FLegend +'%';
// }
//     },
    grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top: '2%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : Label,
            axisTick: {
                alignWithLabel: true
            }
        }
    ],
    yAxis : [
        {
            show: false,
             // inverse:true,
            type : 'value'
        }
    ],
    series : [
        {
            name:'Women',
            type:'bar',
              label: {
                normal: {
                    show: true,
                    position: 'top',
                    formatter: '{c}%',
                },
              
            },
            barMinHeight:2,
            barWidth: '60%',
            data:F
        }
    ]
};
option_bottom = {
    color: ['#3c6399'],
    tooltip : {
        trigger: 'axis',
        axisPointer : {            
            type : 'shadow'       
        },
         formatter: function (params) { console.log(params);
            return "Men " + params[0].name + " make up" + '</br>' + params[0].value +  "% of people reach";
         }
        
    },
//      legend: {
//         data:['Men'],
//         position: 'bottom',
//         formatter: function (name) {
//     return  name + " " + MLegend +'%';
// }
//     },
    grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top: '2%',
        containLabel: true
    },
    xAxis : [
        {
            //show: false,
          
            type : 'category',
            data : Label,
            axisTick: {
                alignWithLabel: true
            },
            axisLabel: {
      formatter: function (value, index) {
return ;

},
 rotate:45
    },
        }
    ],
    yAxis : [
        {
            show: false,
            inverse:true,
            type : 'value'
        }
    ],
    series : [
        {
            name:'Men',
            type:'bar',
              label: {
                normal: {
                    show: true,
                    position: 'bottom',
                    formatter: '{c}%',
                },
              
            },
            barMinHeight:2,
            barWidth: '60%',
            data:M
        }
    ]
};
 if (option && typeof option === "object") {
    reachGenderChart.setOption(option, true);

}
if (option_bottom && typeof option_bottom === "object") {
    reachGenderChartBottom.setOption(option_bottom, true);

}
$('#reach-gender-spin').hide();
    
      
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in Reach API" );
            });
  }

  function GenderFansData(page_name){//alert(startDate);alert(endDate);alert(date_preset);
    var fangenderchart = document.getElementById('fan-age-gender-chart');
    var fangenderchart_1=document.getElementById('fan-age-gender-chart-1')
    var fanGenderChart = echarts.init(fangenderchart);
    var fanGenderChartBottom = echarts.init(fangenderchart_1);
    $('#fan-gender-spin').show();
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('get-agegender-Fan')}}", // This is the URL to the API
      data: {page_name:page_name}
    })
    .done(function( data ) {
     
       var count = Object.keys(data).length;//last record id
       var keys = Object.keys(data);//get only key
       var last_key = keys[count-1]; //to get last key's value total
        
      var F = [];
      var M = [];
      var F_val_arr = [];
      var M_val_arr = [];
      var Label =[];
      var Total = 0;
      var Ftotal = 0;
      var Mtotal =0;
        for(var i in data) {
        Total=data[last_key].T;
        var F_val=data[i].F;
        var M_val=data[i].M;
        F.push((parseFloat(F_val)/parseFloat(Total)*100).toFixed(3));
        M.push((parseFloat(M_val)/parseFloat(Total)*100).toFixed(3));
        F_val_arr.push(F_val);
        M_val_arr.push(M_val);
        Label.push(data[i].Label);
              
      }
       $.each( F_val_arr,function(){Ftotal+=parseInt(this) || 0;});
       $.each( M_val_arr,function(){Mtotal+=parseInt(this) || 0;});

     var FLegend=(parseFloat(Ftotal)/parseFloat(Total)*100).toFixed(2);
     var MLegend=(parseFloat(Mtotal)/parseFloat(Total)*100).toFixed(2);
     $("#pcent_Fan_Women").text(FLegend);
     $("#pcent_Fan_Men").text(MLegend);

    option = null;
    option_bottom = null;
    option = {
    color: ['#899bc1'],
    tooltip : {
        trigger: 'axis',
        axisPointer : {            
            type : 'shadow'       
        },
         formatter: function (params) { console.log(params);
            return "Your Fans " + '</br>' + params[0].value +  "%" + '</br>' + params[0].name;
         }
        
    },
//      legend: {
//         data:['Women'],
//         position: 'bottom',
//         formatter: function (name) {
//     return  name + " " + FLegend +'%';
// }
//     },
    grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top: '2%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : Label,
            axisTick: {
                alignWithLabel: true
            }
        }
    ],
    yAxis : [
        {
            show: false,
             // inverse:true,
            type : 'value'
        }
    ],
    series : [
        {
            name:'Women',
            type:'bar',
              label: {
                normal: {
                    show: true,
                    position: 'top',
                    formatter: '{c}%',
                },
              
            },
            barMinHeight:2,
            barWidth: '60%',
            data:F
        }
    ]
};
option_bottom = {
    color: ['#3c6399'],
    tooltip : {
        trigger: 'axis',
        axisPointer : {            
            type : 'shadow'       
        },
         formatter: function (params) { console.log(params);
            return "Men " + params[0].name + " make up" + '</br>' + params[0].value +  "% of people reach";
         }
        
    },
//      legend: {
//         data:['Men'],
//         position: 'bottom',
//         formatter: function (name) {
//     return  name + " " + MLegend +'%';
// }
//     },
    grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top: '2%',
        containLabel: true
    },
    xAxis : [
        {
            //show: false,
          
            type : 'category',
            data : Label,
            axisTick: {
                alignWithLabel: true
            },
            axisLabel: {
      formatter: function (value, index) {
return ;

},
 rotate:45
    },
        }
    ],
    yAxis : [
        {
            show: false,
            inverse:true,
            type : 'value'
        }
    ],
    series : [
        {
            name:'Men',
            type:'bar',
              label: {
                normal: {
                    show: true,
                    position: 'bottom',
                    formatter: '{c}%',
                },
              
            },
            barMinHeight:2,
            barWidth: '60%',
            data:M
        }
    ]
};
 if (option && typeof option === "object") {
    fanGenderChart.setOption(option, true);

}
if (option_bottom && typeof option_bottom === "object") {
    fanGenderChartBottom.setOption(option_bottom, true);

}
$('#fan-gender-spin').hide();
    
      
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in Reach API" );
            });
  }


     $('.dateranges').daterangepicker({
    locale: {
            format: 'MMMM D, YYYY'
        },
        'minDate': moment().startOf('year'),
        'maxDate': moment(),
            ranges: {

                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'Last 90 Days': [moment().subtract(89, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('day')],
                // 'Last Year': [moment().subtract(1, 'year').startOf('year'),moment().subtract(1, 'year').endOf('year')],
                'This Quarter': [ moment().startOf('quarter'),moment().endOf('quarter')],
               // 'Last Quarter': [ moment().subtract({ months: (moment().month() % 3) + 1 }).endOf('month'), lastQuarterEndDate.clone().subtract({ months: 3 }).startOf('month')],
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end,label) {//alert(label);
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,label);
      });
  ChooseDate(startDate,endDate,'');
  ChoosePage();
function kFormatter(num) {
    return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}
    });
    </script>
<style>
.table td, .table th{
    padding:.35rem;
    font-size: 0.8rem;

}
html body .m-t-40 {
    margin-top: 20px;
}
</style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush


