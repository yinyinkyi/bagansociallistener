<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon1.png')}}">
    <title>{{ config('app.name', 'Laravel') }} - @if (isset($title)) {{$title}} @endif</title>
    
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
     <link href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
     <!-- range slider -->
     <link href="{{asset('assets/plugins/ion-rangeslider/css/ion.rangeSlider.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/ion-rangeslider/css/ion.rangeSlider.skinModern.css')}}" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="{{asset('assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="{{asset('assets/plugins/jquery-asColorPicker-master/css/asColorPicker.css')}}" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="{{asset('assets/plugins/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/chartist-js/dist/chartist-init.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/css-chart/css-chart.css')}}" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="{{asset('assets/plugins/morrisjs/morris.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
   
    <!-- You can change the theme colors from here -->
    <link href="{{asset('css/colors/green.css')}}" id="theme" rel="stylesheet">

    <link href="{{asset('assets/plugins/sweetalert2/sweetalert2.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href='https://mmwebfonts.comquas.com/fonts/?font=myanmar3' />
    <link href="{{asset('assets/plugins/select2/dist/css/select2.min.css')}}" id="theme" rel="stylesheet">
      <link href="{{asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet" />
     <link href="{{asset('assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
     <link href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.dataTables.min.css" rel="stylesheet"/>
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css">
.user-profile .profile-text{
    padding-top: 31px;
    position: relative; }

     .user-profile .profile-img::before {
      -webkit-animation: 2.5s blow 0s linear infinite;
      animation: 0s  0s  ;
      position: absolute;
      content: '';
      width: 50px;
      height: 50px;
      top: 35px;
      margin: 0 auto;
      border-radius: 50%;
      z-index: 0; }
      .user-profile .profile-text > a:after {
        position: absolute;
        right: 20px;
        top: 45px; }

button.btn.btn-info.dropdown-toggle.dropdown-toggle-split::after {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: .255em;
    vertical-align: .255em;
    content: "";
    border-top: .3em solid;
    border-right: .3em solid transparent;
    border-bottom: 0;
    border-left: .3em solid transparent;
}
       /* .dropdown-menu {
    position: absolute;
    top: 100%;
    left: 0;
     z-index: 1000; 
    display: none;
    float: left;
    min-width: 10rem;
    padding: .5rem 0;
    margin: .125rem 0 0;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    list-style: none;
    background-color: #383f48;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: .25rem;
}*/


/*    .label-light-success {
    background-color: #e8fdeb;
    color: #fcb22e;
}*/

.search { position: relative; }
.search input { text-indent: 135px;}
.search .text { 
    font-size: 1rem;
  position: absolute;
  top: 9px;
  left: 7px;
  font-size: 15px;
}
</style>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
  <div id="app">
    <div id="main-wrapper">
      
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" style="background-color:#1c81da">
                    <a class="navbar-brand" href="">
                        <!-- Logo icon -->
                        <b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <!-- <img src="{{asset('assets/images/favicon.png')}}" alt="homepage" class="dark-logo" /> -->
                            <!-- Light Logo icon -->
                            <img src="{{asset('assets/images/logo-icon1.png')}}" style ="width:34px;height:33px" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span style="color:#FFFFFF;font-size:18px" >
                         <!-- dark Logo text -->
                        <!--  <img src="{{asset('assets/images/logo-text.png')}}" alt="homepage" class="dark-logo" /> -->
                         <!-- Light Logo text -->  
                         <img src="{{asset('assets/images/logo-light-text-own.png')}}" class="light-logo" style="width:180px" alt="" /></span> </a>
                </div>

                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
            


                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                      @yield('filter')
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
           
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                     <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                   @yield('admin_page_filter')
                        <li class="nav-item" style="display:none"> 
<div class="btn-group">
                                            <button type="button" class="btn btn-info">  @if (isset($source) and $source === 'out' )
                                                Other Pages
                                                @else
                                                My Pages
                                                @endif</button>
                                            <button value="{{$source}}" id="btn_source" type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="{{route('brandList', ['source' => 'in'])}}">My Pages</a>
                                                <a class="dropdown-item" href="{{route('brandList', ['source' => 'out'])}}">Other Pages</a>
                                              
                                               
                                            </div>
                                        </div>
                         </li>
                       
                    </ul>
                   
                       
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">

                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                         @yield('comparison')
   <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset('assets/images/users/9.jpg')}}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="{{asset('assets/images/users/9.jpg')}}" alt="user"></div>
                                            <div class="u-text">
                                                <h4>{{ Auth::user()->name }}</h4>
                                                <p class="text-muted">{{ Auth::user()->email }}</p><a href="{{route('users.edit', ['id' => Auth::user()->id,'source'=>$source])}}" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                        </div>
                                    </li>
                                <!--     <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                    <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{route('companyInfo.index')}}"><i class="ti-settings"></i> Company Information</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" ><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
        
                        <!-- ============================================================== -->
                        <!-- Language -->
                        <!-- ============================================================== -->
                
                    </ul>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
              <div class="user-profile" style="background: url({{asset('Logo/companyLogo.png')}}) no-repeat;">
               <!--  <div class="user-profile" id="user-profile" style=""> -->
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="" /> </div>
                    <!-- User profile text-->
                    <!-- class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" -->
                    <div class="profile-text" > <a href="#" > @if (isset($project_data) && count($project_data)>0) {{$project_data[0]['name']}} @endif</a>
                        <div class="dropdown-menu animated flipInY">
                            <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                            <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                            <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                            <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                            <div class="dropdown-divider"></div> <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout</a>
                        </div>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->

                <nav class="sidebar-nav">
                    <ul id="sidebarnav">

                      
       

        @if (isset($project_data))
        @foreach($project_data as $project_data)
                     <!--  <li class="{{ request()->is($project_data->id) ? 'active' : '' }}">
           <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu"> {{ $project_data->name }} </span></a> -->
                        <!--     <ul aria-expanded="false" class="collapse"> -->
             <li class="{{ request()->is('dashboard') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('dashboard', ['pid' => $project_data->id,'source'=>$source])}}"><i class="mdi mdi-home"></i><span class="hide-menu"> Dashboard</span></a></li>
                   
            <li class="{{ request()->is('post') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('post', ['pid' => $project_data->id,'source'=>$source])}}"><i class="mdi mdi-view-module"></i><span class="hide-menu">Post</span></a></li>

            <li class="{{ request()->is('comment') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('comment', ['pid' => $project_data->id,'source'=>$source])}}"><i class="mdi mdi-tooltip-text"></i><span class="hide-menu">Comment</span></a></li>
          
           
            <li class="{{ request()->is('competitor') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('competitor', ['pid' => $project_data->id,'source'=>$source])}}"><i class="mdi  mdi-human-greeting"></i><span class="hide-menu">Competitor</span></a></li>
            <li class="{{ request()->is('comparison') ? 'active' : '' }}"  class="nav-small-cap"><a href="{{route('comparison', ['pid' => $project_data->id,'source'=>$source])}}"><i class="mdi mdi-compare"></i><span class="hide-menu">Comparison</span></a></li>
      
           
            <!--  <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i>Setting</a></li> -->
                         <!--    </ul> -->
                       <!--  </li> -->
       

                        
        @endforeach

        @endif  

         
            @if ($permission_data['setting'] === 1)
                   <!--      <li class="nav-devider"></li>
                        <li class="nav-small-cap">Administration</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Authentication</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('register', ['source'=>$source])}}">User Registration</a></li>
                                
                            </ul>
                        </li> -->
                     
                    <!-- {{ Request::is('tags*')}} -->       
                      
                             <li  class="nav-devider"></li>
                    <!--     <li class="nav-small-cap">Setting</li> -->
                        <li class="@if (Request::is('brandList/*') || Request::is('tags/*') || Request::is('users/*')) {{'active'}}  @endif"> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Setting</span></a>
                            <ul aria-expanded="false"  class="collapse">
                               
                                  <!-- <li class="{{ request()->is('brandList/*') ? 'active' : '' }}" class="nav-small-cap">  <a class="{{ request()->is('brandList/*') ? 'active' : '' }}" href="{{route('brandList', ['source'=>$source])}}">
                                    <i class="mdi mdi-plus-box"></i><span class="hide-menu"> Brand List</span>
                                  </a></li> -->
                                  <li class="nav-item {{ str_is('brandList*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap"><a class=" {{ str_is('brandList*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('brandList', ['source'=>$source])}}"><i class="mdi mdi-tag"></i> Brand List </a></li>
                                <li class="nav-item {{ str_is('tags*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap"><a class=" {{ str_is('tags*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('tags.index')}}"><i class="mdi mdi-tag"></i> Tags Entry </a></li>

                                <li class="nav-item {{ str_is('users*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap"><a class="nav-item {{ str_is('users*', Request::route()->getName()) ? 'active' : '' }}"  href="{{route('register', ['source'=>$source])}}"><i class="mdi mdi-account-plus"></i> User Registration</a></li>

                                <li class="nav-item {{ str_is('companyInfo*', Request::route()->getName()) ? 'active' : '' }}"  class="nav-small-cap"><a class="nav-item {{ str_is('companyInfo*', Request::route()->getName()) ? 'active' : '' }}" href="{{route('companyInfo.index')}}"><i class="mdi mdi-bank"></i> Company Info</a></li>

                            </ul>
                        </li>

                             <li class="nav-devider"></li>
                    <!--     <li class="nav-small-cap">Setting</li> -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-chart-line"></i><span class="hide-menu">Report</span></a>
                            @if (isset($project_data) && count($project_data)>0)
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('rpt-sentipredict', ['pid' => $project_data->id,'source'=>$source])}}"><i class="mdi mdi-note-text"></i> Sentiment Predict</a></li>
                                <li><a href="{{route('rpt-humanpredict', ['pid' => $project_data->id,'source'=>$source])}}"><i class="mdi mdi-note-text"></i> Human Predict</a></li>
                                <li><a href="{{route('rpt-postDetail-export', ['pid' => $project_data->id,'source'=>$source])}}"><i class="mdi mdi-note-text"></i> Export Data</a></li>
                                <li><a href="{{route('rpt-analysis', ['pid' => $project_data->id,'source'=>$source])}}"><i class="mdi mdi-note-text"></i> Print Data</a></li>
                            </ul>
                            @endif
                        </li>
                        @endif
                       
                     
                
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer" style="display: none">
                <!-- item-->
                <a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
                <!-- item-->
                <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
                <!-- item-->
                <a href="{{ route('logout') }}" class="link" data-toggle="tooltip" title="Logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="mdi mdi-power"></i></a>
            </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                @yield('content')
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2018 Bagan Social Listener by Bagan Innovation Technology</footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
</div>
    <!-- ============================================================== -->
   <!-- defer -->
   <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

// function getCompanyInfo()
// {

//      $.ajax({
//       type: "GET",
//       dataType:'json',
//       contentType: "application/json",
//       url: "{{route('getCompanyInfo')}}"
//     })
//     .done(function( data ) {//$("#popular").html('');
//         console.log(data);
//        for(var i in data) {
//       var image_name=data[i].logo_path;
    
//         $('#user-profile').css({"background-image": "url({{asset('Logo')}}/"+image_name+")", 'background-repeat': 'no-repeat'});
//      }
//     })
//     .fail(function(xhr, textStatus, error) {

//     });

// }
// getCompanyInfo();
 
// $('#user-profile').css({"background-image":"url(../storage/app/   Logo/1543393588.wdXssepUmwyY8aAsuZDoj4jUmAswJv2mjam5v5UX.png)  "});

    });

    </script>
     @stack('scripts')

</body>

</html>