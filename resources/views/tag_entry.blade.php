@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')

            <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:5px;padding-top:5px;font-weight: 500">Tag Entry</h4>
                       <!--  <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Brand List</li>
                        </ol> -->
                    </div>



                </div>

@if(!isset($data))
<!-- <div class="box box-warning"> -->
            <div class="">
             <!--  <h3 class="">{{ __('Register') }}</h3> -->
            </div>
              @if(session()->has('message'))
            <div class="">
              {{ session()->get('message') }}
            </div>
          @endif
              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        <form method="POST" role="form" class="form-material m-t-10"  action="{{ route('tags.store') }}" aria-label="{{ __('Tags') }}">
                {{csrf_field()}}
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            <div class="form-group">
                <label for="keywords" class="control-label">{{ __('Keywords') }}</label>
                 <!-- <input id="keywords" type="text"
               class="form-control form-control-line{{ $errors->has('keywords') ? ' is-invalid' : '' }}"
               name="keywords" value="{{ old('keywords') }}" required> -->
               <div class="tags-default">
                <input type="text" id="keywords" data-role="tagsinput" placeholder="add tags" class="form-control form-control-line{{ $errors->has('keywords') ? ' is-invalid' : '' }}"
               name="keywords" value="{{ old('keywords') }}" required /> </div>
                 @if ($errors->has('keywords'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('keywords') }}</strong>
                    </span>
                @endif
            </div>
 
            <div class="form-group">
                <button type="submit" class="btn btn-primary">  {{ __('Save') }}</button>
              </div>
             </form>
                            </div>

                        </div>
                    </div>
                </div>
            <!-- </div> -->
            @else
            <div class="">
             <!--  <h3 class="">{{ __('Register') }}</h3> -->
            </div>
              @if(session()->has('message'))
            <div class="">
              {{ session()->get('message') }}
            </div>
          @endif
              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                        {!! Form::model($data, ['method' => 'PATCH','class'=>'form-material m-t-10','enctype'=>'multipart/form-data','route' => ['tags.update', $data->id]]) !!}
                {{csrf_field()}}
            <div class="form-group">
                 <label for="name" class="control-label">{{ __('Name') }}</label>
                   <input id="name" type="text" class="form-control form-control-line{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old( 'name', $data->name) }}"  required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            <div class="form-group">
                <label for="keywords" class="control-label">{{ __('Keywords') }}</label>
                <div class="tags-default">
                <input type="text" id="keywords" data-role="tagsinput" placeholder="add tags" class="form-control form-control-line{{ $errors->has('keywords') ? ' is-invalid' : '' }}"
               name="keywords" value="{{ old( 'keywords', $data->keywords) }}" required /> </div>
                 @if ($errors->has('keywords'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('keywords') }}</strong>
                    </span>
                @endif
            </div>
         
            <div class="form-group">
                <button type="submit" class="btn btn-primary">  {{ __('Register') }}</button>
              </div>
             {!! Form::close() !!}
                            </div>

                        </div>
                    </div>
                </div>

@endif
              
                  <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">Tags List</h4>
                            
                                 <table id="tags_table" class="table">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Keywords</th>
                    <th></th>
                   
                  </tr>
                  </thead>
                 <!--  <tbody>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Apple</a></td>
                    <td>2018-07-06</td>

                    <td><a class="btn btn-primary btn-xs">Edit</a><span> </span><a class="btn btn-danger btn-xs">Delete</a></td>
                  </tr>
                  <tr>
                    <td><a href="pages/examples/invoice.html">Eleven</a></td>
                    <td>2018-07-25</td>
                  <td><a class="btn btn-primary btn-xs">Edit</a><span> </span><a class="btn btn-danger btn-xs">Delete</a></td>
                    
                  </tr>
             
                  
                  </tbody> -->
                </table>
                            </div>

                        </div>
                    </div>
                </div>  
              
                
       
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
  <script type="text/javascript">

$(document).ready(function() {
        $('#tags_table').DataTable({
       "lengthChange": true,"info": false, "searching": true,
        processing: true,
        serverSide: false,

        ajax: '{!! route('gettaglist') !!}',

         columns: [
            {data: 'name', name: 'name'},
            {data: 'keywords', name: 'keywords'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
        
    })


    });

</script>
<style>
  .bootstrap-tagsinput{
    width: 100%
  }
</style>
@endpush

