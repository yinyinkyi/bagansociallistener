@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('content')
  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                   <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:5px;padding-top:5px;font-weight: 500">Brand Create</h4>
                       <!--  <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Brand List</li>
                        </ol> -->
                    </div>



                </div>

              
                <!-- Row -->
             <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form role="form" class="" action="{{route('brand_store')}}" method="post" id="myform">
          {{csrf_field()}}
 <input  type="hidden" id="chk_val" name="chk_val" value=""  readonly="true" class="form-control" style="background:red"/>
 <input  type="hidden" id="unchk_val" name="unchk_val" value=""  readonly="true" class="form-control" style="background:red"/>
 <input type="hidden" class="form-control form-control-line" name="source" id="source"  placeholder="Enter brand name" value="{{$source}}"/> 
      <div class="box-body">
        <div class="form-group">
            <label class="control-label">Brand Name: </label>
               <input type="text" class="form-control form-control-line" name="brand_name" id="brand_name"  placeholder="Enter brand name"> 
        </div>
               
        <div class="demo-checkbox">
           <label for="sources" class="control-label"> Sources: </label>
              <input type="checkbox"  name="facebook_check" id="chk-mention" value="Facebook" class="filled-in chk-col-red" checked />
           <label for="facebook">Facebook</label>
              <input type="checkbox"  name="twitter_check" value="Twitter" class="filled-in chk-col-red" checked />
           <label for="twitter">Twitter</label>
              <input type="checkbox" name="instagram_check" value="Instagram" class="filled-in chk-col-red" checked />
           <label for="instagram">Instagram</label>
        </div>

        <table class="table table-condensed input_fields_table" id="input_fields_table">
          <tr>
            <th><button name="add" class="add_field_button btn btn-flat btn-success"><span class="fa fa-plus"></span> ADD </button></th>
          </tr>
          <tr>
          <th>Key Word</th>
              <th>Word Included</th>
              <th>Word Excluded</th>
              <th></th>
           </tr>
           <tr>
              <td>
              <div class="form-group">
                  <input type="text" class="form-control" name="main_key[]" id="main_key_1" placeholder="Enter keyword" >
                </div>
              </td>
              <td> 
                <div class="form-group">
                  <input type="text" class="form-control" name="include_key[]" id="include_key_1" placeholder="Enter include key">
                  <br><h6>separate words with a comma</h6>
                </div> 
              </td>
              <td>
                <div class="form-group">
                  <input type="text" class="form-control" name="exclude_key[]" id="exclude_key_1" placeholder="Enter exlude key">
                  <br><h6>separate words with a comma</h6>
                </div>
              </td>
              <td>
                 <a href="#" class="remove_field">
                          <i class="icon-trash "></i>Remove
                        </a>
                      </td>
              
                    </tr>
                </table>

          <table class="table table-condensed input_fields_table2" id="input_fields_table2">
          <tr>
            <th><button name="add" class="add_field_button2 btn btn-flat btn-success"><span class="fa fa-plus"></span> ADD </button></th>
          </tr>
          <tr>
          <th>Monitor Pages</th>
          <th>My Page</th>
              <th></th>
           </tr>
           <tr>
              <td>
              <div class="search">
                <span class="text">www.facebook.com/</span>
                  <input type="text" class="form-control" name="monitor_pages[]">
                </div>
              </td>
             <td>
           <div class="demo-checkbox">
                                    
                                    <input type="checkbox" id="chk_1" name="chk[]" class="filled-in my_page" />
                                    <label for="chk_1">Admin</label>
                                   
                                </div>
             </td>
              <td>
                 <a href="#" class="remove_field2">
                          <i class="icon-trash "></i>Remove
                        </a>
                      </td>

              
                    </tr>
                </table>

         

                <div class="form-group">
                  <button type="button" class="btn btn-flat btn-info pull-right" id="btn_save" value="">Save</button>
                  
                </div>
            </div> <!-- endof BoxBody -->
        </form> 
                            </div>
            
                        </div>
                    </div>
                </div>

              
                
              
                
       
@endsection
@push('scripts')
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}" defer></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}" defer></script>
<!--stickey kit -->
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}" defer></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->



<!-- Chart JS -->
<script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>

<!-- Flot Charts JavaScript -->
<script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
<script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>

<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
<!-- Date range Plugin JavaScript -->
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>

<!-- DataTables -->
  <!-- <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script> -->
  <!-- This is data table -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>

  <!-- ============================================================== -->
  <!-- Style switcher -->
  <!-- ============================================================== -->
  <!-- <script src="{{asset('assets/plugins/echarts/echarts.js')}}"></script> -->
  <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" defer></script>
  <script src="{{asset('assets/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider-init.js')}}" defer></script>
  <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
     <script src="{{asset('plugins/iCheck/icheck.min.js')}}" defer></script>
     <script type="text/javascript">
     $(document).ready(function() {
      // var input = $("#aa").val;
      // alert(input);
      //   var len = input.val().length;
      //   input[0].focus();
      //   input[0].setSelectionRange(len, len);


        $('#btn_save').on('click',function(e){
    var chk_val=[];
    var unchk_val=[];
    var monitor_arr = document.getElementsByName('monitor_pages[]');

    $('#input_fields_table2 tr').has('td').each(function(tr_index) {//alert(tr_index);
    
    $('td', $(this)).each(function(index, item) {//alert(item);
      //if column is equal to Action (index is 5) we will not save this column to database . So we don't need to 
      
     
     
      if(parseInt(index) === 1)
      {
        var monitor_page=monitor_arr[tr_index].value;
        //alert(monitor_page);
        var $chkbox = $(this).find('input[type="checkbox"]');
         // alert($chkbox.prop('checked'));
         if($chkbox.prop('checked')===true)
         {
           chk_val.push(monitor_page);
         }
         else
         {
          unchk_val.push(monitor_page);
         }
          
        

      }
       
        

    });

  
});
// alert(chk_val);
// alert(unchk_val);
 $("#chk_val").val(chk_val);
 $("#unchk_val").val(unchk_val);



    var form = $(this).parents('#myform');
   // alert("hihi");
          swal({
            title: 'Comfirmation',
            text: "Are you sure?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, save it!'
          }).then((result) => {
            if (result.value) {
                 form.submit();
              swal({title:'Collecting Data....',  allowOutsideClick: false});
              swal.showLoading();
            
            }
          })

  });
      



        var max_fields      = 100; //maximum input boxes allowed
        var wrapper         = $(".input_fields_table"); //Fields wrapper
        var wrapper2         = $(".input_fields_table2");
        var add_button      = $(".add_field_button"); //Add button ID
        var add_button2      = $(".add_field_button2");
        var page_name        = $(".test");
       
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
          // alert("hello you clicked add button");
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<tr><td><div class="form-group"><input type="text" name="main_key[]" class="form-control"placeholder="Enter keyword"/></div></td><td><div class="form-group"><input type="text" name="include_key[]" class="form-control" placeholder="Enter include key"/><br><h6>separate keywords with a comma</h6></div></td><td><div class="form-group"><input type="text" class="form-control" name="exclude_key[]" placeholder="Enter exclude key"/><br><h6>separate keywords with a comma</h6></div></td><td><a href="#" class="remove_field"><i class="icon-trash"></i>Remove</a></td></tr>'); //add input box
            }
        });
       
        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); 
            // $(this).parent('tr').remove(); 
            $(this).closest("tr").remove();
            x--;

        })
          var y = 1;

         $(add_button2).click(function(e){ //on add input button click
          // alert("hello you clicked add button");
            e.preventDefault();
            if(y < max_fields){ //max input box allowed

                y++; //text box increment
                //alert(y);
               var id_gen="chk_"+y;
               // alert('<tr><td><div class="form-group"><input type="text" name="monitor_pages[]" class="form-control" placeholder="Enter Page Name"/></div></td><td>       <div class="demo-checkbox">'+
               //    '<input type="checkbox" id="'+id_gen+'" name="chk[]" class="filled-in my_page" />'+
               //    '<label for="'+id_gen+'">Own</label>'+
               //    '</div></td><td><a href="#" class="remove_field2"><i class="icon-trash"></i>'+
               //    ' Remove</a></td></tr>');
                $(wrapper2).append('<tr><td><div class="search"><span class="text">www.facebook.com/</span><input type="text" name="monitor_pages[]" class="form-control"></div></td><td>       <div class="demo-checkbox">'+
                  '<input type="checkbox" id="'+id_gen+'" name="chk[]" class="filled-in my_page" />'+
                  '<label for="'+id_gen+'">Admin</label>'+
                  '</div></td><td><a href="#" class="remove_field2"><i class="icon-trash"></i>'+
                  ' Remove</a></td></tr>'); //add input box
                // $(wrapper2).append('<tr><td><div class="gmail_box"><div class="first_div"><input type="text" name="monitor_pages[]" class="form-control t_class" />www.facebook.com/</div></div></td><td>       <div class="demo-checkbox">'+
                //   '<input type="checkbox" id="'+id_gen+'" name="chk[]" class="filled-in my_page" />'+
                //   '<label for="'+id_gen+'">Admin</label>'+
                //   '</div></td><td><a href="#" class="remove_field2"><i class="icon-trash"></i>'+
                //   ' Remove</a></td></tr>'); //add input box
            }
        });
       
        $(wrapper2).on("click",".remove_field2", function(e){ //user click on remove text
            e.preventDefault(); 
            // $(this).parent('tr').remove(); 
            $(this).closest("tr").remove();
            y--;

        })

        // for icheck

        //      $('.my_page').click(function () {
        //     if ($(this).attr('checked')) {
        //         alert('is checked');
        //     } else {
        //         alert('is not checked');
        //     }
        // })
        
       $("#example1").DataTable({"lengthChange": false,"info": false, "searching": false});
       // alert($('#test').val());
        var input = $("#test");
    var len = input.val().length;
    input[0].focus();
    input[0].setSelectionRange(len, len);



      });
  </script>
@endpush

