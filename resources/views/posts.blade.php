@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('admin_page_filter')
<li class="nav-item" style="display: none"> 
<div class="btn-group">
        <select id="admin_page_filter" class="form-control custom-select">
        <option value="">Admin Pages</option>
        @if (isset($ownpage))
        @foreach($ownpage as $ownpage)
        <option value="{{$ownpage}}" id="{{$ownpage}}" >{{$ownpage}}</option>
        @endforeach
        @endif
                                                        
                                                   
      </select>
 
                                        </div>
                         </li>
                      
@endsection
@section('content')

  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                 <header class="" id="myHeader">
                  <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500">Post</h4>
                       <!--  <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Brand List</li>
                        </ol> -->
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                            <!--     <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>THIS MONTH</small></h6>
                                    <h4 class="m-t-0 text-info">$58,356</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div> -->
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                               <input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <!-- <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
  </header>
             

                  
 <div class="row ">
                        <div class="col-12">
                       
                        <div class="card" >
                   
                            <div class="card-body b-t collapse show">
                            <div style="display:none"  align="center" style="vertical-align: top;" id="post-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <table id="tbl_post"  class="table" style="margin-top:4px;">
                              <thead>
                                <tr>
                                  <th ></th>
                                  <th >Reaction</th>
                                  <th >Comments</th>
                                  <th>Shares</th>
                                  <th >Overall</th>
                                </tr>
                              </thead>

                            </table>
                          </div>
                        </div>
                    </div>
                    </div>
                <div id="show-post-task" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="postModal" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="postModal">Post Detail</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body post_data">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" id="btnSeeComment" class="btn btn-info waves-effect text-left" data-dismiss="modal">See Comments</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
               
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
   <!--  <script src="{{asset('assets/plugins/morrisjs/morris.js')}}" defer></script>
     <script src="{{asset('js/morris-data.js')}}" ></script>-->
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/plugins/echarts/echarts-wordcloud.js')}}"></script>
    <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script>-->
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}" defer></script>
   <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
   <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}" defer></script>
   <script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var myHeaderContent = document.getElementById("myHeaderContent");
var sticky = header.offsetTop;

function myFunction() {//alert("ho");
  if (window.pageYOffset > sticky) {
    header.classList.add("s-topbar");
    header.classList.add("s-topbar-fix");
    myHeaderContent.classList.add("myHeaderContent");
  } else {
    header.classList.remove("s-topbar");
    header.classList.remove("s-topbar-fix");
    myHeaderContent.classList.remove("myHeaderContent");
  }
}
</script>
    <script type="text/javascript">
var startDate;
var endDate;
/*global mention*/
var mention_total;
var mentionLabel = [];
var mentions = [];
/*Bookmark Array*/
var bookmark_array=[];
var bookmark_remove_array=[];
/*global sentiment*/
var positive = [];
var negative = [];
var sentimentLabel = [];
var positive_total=0;
var negative_total=0;
var  colors=["#1e88e5","#dc3545","#01ad9d","#cb73a9","#a1c652","#7b858e","#e79b5f","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

$(document).ready(function() {



    //initialize
 $('#positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-positive-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-negative-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-interest-progress').css('width', 0+'%').attr('aria-valuenow', 0);
/* $('#top-like-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-love-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-haha-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-wow-progress').css('width', 0+'%').attr('aria-valuenow', 0);
 $('#top-sad-progress').css('width', 0+'%').attr('aria-valuenow', 0);  
 $('#top-angry-progress').css('width', 0+'%').attr('aria-valuenow', 0);*/

 $('#neutral-progress').css('width', 0+'%').attr('aria-valuenow', 0);

      var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

    startDate = moment().subtract(1, 'month');
    endDate = moment();

           $('.singledate').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        },function(date) {
          endDate=date;
          var id =$('input[type=radio][name=period-radio]:checked').attr('id');
          if(id==="period-week")
         {
           startDate = moment(endDate).subtract(1, 'week');
           endDate = endDate;
         }
         else
         {
           startDate = moment(endDate).subtract(1, 'month');
           endDate = endDate;
         }
         // alert(startDate);
         // alert(endDate);
         var admin_page=$("#admin_page_filter").val();

         ChooseDate(startDate,endDate,admin_page,'');
       
      });



 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Dashboard'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}


   function ChooseDate(start, end,admin_page='',label='') {//alert(start);
          $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
          startDate=start;
          endDate=end;
       
         getAllPost(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'));
           
        }

        function GetStartDate()
{
   //alert(startDate);
 return startDate.format('YYYY MM DD');
}
function GetEndDate()
{
           // alert(endDate);
return endDate.format('YYYY MM DD');
}

 function getAllPost(fday,sday)
    {
      $(".popup_post").unbind('click');
      $(".see_comment").unbind('click');

 var oTable = $('#tbl_post').DataTable({
  
       // "responsive": true,
        "paging": true,
         "pageLength": 20,
        "lengthChange": false,
        "searching": false,
        "processing": false,
        "serverSide": false,
        "destroy": true,
        "ordering": true   ,
        "autoWidth": true,
       /* "columnDefs" : [
             { "width": "60%", "targets": 0 },
             { "width": "10%", "targets": 1 },
             { "width": "10%", "targets": 2 },
             { "width": "10%", "targets": 3 },
             { "width": "10%", "targets": 4 }
        ] ,*/
        "order": [],
        "headers": {
          'X-CSRF-TOKEN': '{{csrf_token()}}' 
        },
        /*ajax: '{!! route('getallmention') !!}',*/
        "ajax": {
          "url": '{{ route('getInboundPost') }}',
   /*       data: function ( d ) {
            d.fday = mailingListName;
            d.sday = mailingListName;
            d.brand_id = mailingListName;
          }*/
           "data": {
            "fday": fday,
            "sday": sday,
            "brand_id": GetURLParameter('pid'),
          
          }

        },
        "initComplete": function( settings, json ) {
          //console.log(json);
        $('.tbl_post thead tr').removeAttr('class');
        },
 

        columns: [
        {data: 'post', name: 'post',"orderable": false},
        {data: 'reaction', name: 'action', orderable: true,className: "text-center"},
        {data: 'comment', name: 'action', orderable: true, className: "text-center"},
        {data: 'share', name: 'action', orderable: true, className: "text-center"},
        {data: 'overall', name: 'overall', orderable: true,className: "text-center"},


        ]
        
      }).on('click', '.popup_post', function (e) {
  if (e.handled !== true) {
    var name = $(this).attr('name');
    //alert(name);
    var post_id = $(this).attr('id');
    $("#modal-spin").show();
    $(".post_data").empty();

    //alert(post_id);alert(name);alert(GetURLParameter('pid'));
         $.ajax({
         headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
         url:'{{ route("getRelatedPosts") }}',
         type: 'GET',
         data: {id:post_id,brand_id:GetURLParameter('pid')},
         success: function(response) { //console.log(response);
          var res_array=JSON.parse(response);
          var data=res_array[0];

          var monitor=res_array[1];

            for(var i in data) {//alert(data[i].message);
              var neutral = parseInt(data[i].neutral) + parseInt(data[i].NA);
               var all_total=parseInt(data[i].positive)+parseInt(data[i].negative)+parseInt(neutral);

               var pos_pcent=parseFloat((data[i].positive/all_total)*100).toFixed(2);
               var neg_pcent=parseFloat((data[i].negative/all_total)*100).toFixed(2);
               var neutral_pcent=parseFloat((neutral/all_total)*100).toFixed(2);
               pos_pcent=isNaN(pos_pcent)?0:pos_pcent;
               neg_pcent=isNaN(neg_pcent)?0:neg_pcent;
               neutral_pcent=isNaN(neutral_pcent)?0:neutral_pcent;
               pos_pcent=pos_pcent==0?'':pos_pcent.replace(".00", "")+'%';
               neg_pcent=neg_pcent==0?'':neg_pcent.replace(".00", "")+'%';
               neutral_pcent=neutral_pcent==0?'':neutral_pcent.replace(".00", "")+'%';

               var page_name=data[i].page_name;
              if(isNaN(page_name)==false)
              {
                var b = monitor.filter(item => item.indexOf(page_name) > -1);
               page_name = b[0];
              }

             var page_Link= "https://www.facebook.com/" + page_name ;                   

 var html='<input type="hidden" id="popup_id" value="'+data[i].id+'"/><div class="profiletimeline"><div class="sl-item">'+
               '  <div class="sl-left">'+
               '<img src="'+data[i].full_picture+'"  alt="user" class="img-circle img-bordered-sm" /></div>'+
               
                ' <div class="sl-right">'+
                '<div><a href="'+page_Link+'" target="_blank">'+data[i].page_name+'</a> '+
                ' <div class="sl-right"> '+
             ' <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '+data[i].created_time+'</span> </div> '+
              ' <div style="width:55%;display: inline-block">';
              if(neg_pcent!=='')
              html +='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'+neg_pcent;
            if(pos_pcent!=='')
              html +=' <span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'+pos_pcent;
            if(neutral_pcent!=='')
              html +='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'+neutral_pcent;

                html+=' </div></div></div></span></p> ' +                
                  '   <p class="m-t-10" align="justify">' + 
                    data[i].message +
                  
                    '</p> </div></div>';
 $(".post_data").append(html);
     
     
        }


         $("#modal-spin").hide();
         $("#postModal").text(name);
         $('#show-post-task').modal('show'); 
        }
            });
          e.handled = true;
       }
     
    
        
  }).on('click', '.see_comment', function (e) {//alert("hihi");
  if (e.handled !== true) {
    var attr_id = $(this).attr('id');
    var post_id  = attr_id.replace("seeComment_","");
  

    window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() , '_blank');
     
     
        }
        


          e.handled = true;
       });
    new $.fn.dataTable.FixedHeader( oTable );
   // oTable.columns.adjust().draw();
   // $('#tbl_post').css('width', '100%');
   // $("#tbl_post").attr("class", "table dataTable no-footer dtr-inline fixedHeader-nonfloating");
        
  }

    $(window).resize( function() {

     //  new $.fn.dataTable.FixedHeader( oTable );
      $('#tbl_post').css('width', '100%');
      //$("#tbl_post").DataTable().columns.adjust().draw();
      //$('table.fixedHeader-floating').css('left', '200px');
  });
$('.dateranges').daterangepicker({
    locale: {
            format: 'MMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end,label) {//alert(label);
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        ChooseDate(startDate,endDate,'',label);
      });
 ChooseDate(startDate,endDate,'','');

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
   window.dispatchEvent(new Event('resize'));

 });

hidden_div();




  $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MMM D, YYYY') + ' - ' + picker.endDate.format('MMM D, YYYY'));
       });


$('#admin_page_filter').change(function() {
    //alert($(this).val());
    var admin_page = $(this).val();
   ChooseDate(startDate,endDate,admin_page,'');
     // $(this).val() will work here
});
  $("#btnSeeComment").click( function()
           {
             var post_id=$("#popup_id").val();
              window.open("{{ url('relatedcomment?')}}" +"pid="+ GetURLParameter('pid') +"&source="+GetURLParameter('source')+"&post_id="+post_id+"&fday="+GetStartDate()+"&sday="+GetEndDate() , '_blank');
           }
        );

//local function

function numberWithCommas(n) {
    var parts=n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
}

  function kFormatter(num) {
    return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
}
  function readmore(message){
      // alert("hi hi ");
        var string = String(message);
        var length = string.length; 
         // alert(length);
                if (length > 500) {
          // alert("length is greater than 500");

            // truncate

            var stringCut = string.substr(0, 500);
             // alert(stringCut);
            // var endPoint = stringCut.indexOf(" ");

            //if the string doesn't contain any space then it will cut without word basis.
             
            // string =endPoint? stringCut.substr(0, endPoint):stringCut.substr(0);
            string =stringCut.substr(0,length);
            // string = string + "...<a href='"+readmore_link+"'>Read More</a>";
            // alert(string);
        }
        return string;


        }

        

  });
    </script>
    <style type="text/css">
.table td, .table th {
    padding: .50rem;
    vertical-align: middle;
    border-top: 1px solid #dee2e6;
}
.table thead
{
  color:#1c81da;
}
table.fixedHeader-floating {
   clear: both;
  top: 114px !important;
  z-index:5;
   top : 0;
   left:0 !important;
   width: 100% !important;
/*   margin: 0 auto;
     padding: 0 15px;*/
 /*    overflow-x: hidden;
      overflow-y: auto;*/
  
    box-sizing: border-box;
/*
  background: red;*/
/*  left:150px !important;*/
/*  margin-right:30px;*/
 
/*  width: 80% !important;*/
  /*background: transparent;*/
}


.myHeaderContent
{
   margin-right:300px;
}

    </style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush

