@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])

@section('content')

                 <header class="" id="myHeader">
                  <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h4 class="text-themecolor m-b-0 m-t-0" style="padding-left:20px;font-weight:500">Comparison</h4>
                    
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end" id="myHeaderContent">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                               
                                  <button type="button" id="btn_add_project" class="btn waves-effect waves-light btn-block btn-info">More Brands</button>
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                               <input type='text' class="form-control dateranges" style="datepicker" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <!-- <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div> -->
                        </div>
                    </div>
                </div>
  </header>

                <div id="add-project" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                                                    
                                                        <!-- /.modal-content -->
                       <div class="modal-content">
                                                           
                           <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Add Brand</h4>
                                                                <button type="button"  class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                         
                            <div class="message-box">
                            <div class="form-group has-info">
                                                    <label class="control-label">Brand</label>
                                                    <select id="brand_id" class="form-control custom-select">
                                                   
                                                   
                                          </select>
                                                    <small class="form-control-feedback"> Choose a brand to compare </small> </div>
                                                     <hr>
                                         
                                        <button type="button" id="btn_add_brand" class="btn btn-info"><i class="fa fa-check"></i> Add </button>
                            </div>
                  
                                                            </div>                              
                       </div>
                    </div>
                                                    <!-- /.modal-dialog -->
                       
               </div>

                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
               
                <!-- Row -->
                    <div class="row" class="row s-topbar-u-div" id="page-div-1">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Fan Growth</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="fan-growth-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div id="fan-growth-chart" style="width:100%;height:350px"></div>  
                            </div>
                        </div>
                    </div>
                
                </div>
                <div class="row" class="row s-topbar-u-div" id="page-div-1">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Page Summary</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="summary_chart_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div id="page-summary-chart" style="width:100%;height:350px"></div>  
                            </div>
                        </div>
                    </div>
                
                </div>
                 <div class="row" class="row s-topbar-u-div" id="page-div-1">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Post Sentiment</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="post_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div id="post-sentiment-chart" style="width:100%;height:350px"></div>  
                            </div>
                        </div>
                    </div>
                
                </div>
                <div class="row" class="row s-topbar-u-div" id="page-div-1">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Comment Sentiment</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="comment_spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div id="comment-sentiment-chart" style="width:100%;height:350px"></div>  
                            </div>
                        </div>
                    </div>
                
                </div>
            <!--        <div class="row" id="page-div-2">
                  
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Total Reach</h4>
                                  <div style="display:none"  align="center" style="vertical-align: top;" id="reach-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                              <div id="reach-chart" style="width:100%; height:400px;"></div>   
                            </div>
                        </div>
                    </div>
                
                </div> -->
              
              <div class="row" id='comparison-div-4'>
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                          <div style="display:none"  align="center" style="vertical-align: top;" id="reaction-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             <h4 class="card-title">Social Reaction</h4>
                              <div id="div_reaction" class="table-responsive">
                                   <!--  <table  class="table table-bordered">
                                        <thead>
                                            <tr id="tr_header">
                                                <th>Reaction</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="tr_like">
                                               <td><a href="javascript:void(0)">Like</td>
                                              
                                                
                                            </tr>
                                            <tr id="tr_love">
                                                <td><a href="javascript:void(0)">Love</a></td>
                                           
                                              
                                            </tr>
                                            <tr id="tr_wow">
                                                <td><a href="javascript:void(0)">/a></td>
                                           
                                            </tr>
                                            <tr id="tr_haha">
                                                <td><a href="javascript:void(0)">HA HA</a></td>
                                                
                                            </tr>
                                            <tr id="tr_sad">
                                                <td><a href="javascript:void(0)">Sad</a></td>
                                               
                                            </tr>
                                            <tr id="tr_angry">
                                                <td><a href="javascript:void(0)">Angry</a></td>
                                               
                                            </tr>
                                             <tr id="tr_share">
                                                <td><a href="javascript:void(0)">Share</a></td>
                                              
                                            </tr>
                                        </tbody>
                                    </table> -->
                                </div>
                           
                            </div>
                        </div>
                    </div>
               
                </div>
              <!--    <div class="row" id='comparison-div-4'>
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                          <div style="display:none"  align="center" style="vertical-align: top;" id="summary-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             <h4 class="card-title">Page Summary</h4>
                              <div id="div_page_summary" class="table-responsive">
                                 
                                </div>
                           
                            </div>
                        </div>
                    </div>
               
                </div> -->
         <!--          <div class="row" id='comparison-div-4'>
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                          <div style="display:none"  align="center" style="vertical-align: top;" id="ranking-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             <h4 class="card-title">Page Ranking</h4>
                              <div id="div_page_ranking" class="table-responsive">
                                 
                              </div>
                           
                            </div>
                        </div>
                    </div>
               
                </div> -->
                
       
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" defer></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}" defer></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}" defer></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}" defer></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}" defer></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}" defer></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}" defer></script>
   <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script> -->
   <!--  <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script> -->
 
    
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}" defer></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}" defer></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" defer></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" defer></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}" defer></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}" defer></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}" defer></script>
   
   <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript')}}" defer ></script>
    <script src="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript')}}" defer></script>
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}" defer></script>
     <script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var myHeaderContent = document.getElementById("myHeaderContent");
var sticky = header.offsetTop;

function myFunction() {//alert("ho");
  if (window.pageYOffset > sticky) {
    header.classList.add("s-topbar");
    header.classList.add("s-topbar-fix");
    myHeaderContent.classList.add("myHeaderContent");
  } else {
    header.classList.remove("s-topbar");
    header.classList.remove("s-topbar-fix");
    myHeaderContent.classList.remove("myHeaderContent");
  }
}
</script>
    <script type="text/javascript">
var startDate;
var endDate;
var labelDate;
var colors=[ "#4267b2","#382448","#01ad9d","#cb73a9","#a1c652","#7b858e","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];
var colors_senti=["#28a745","#fb3a3a","#ffb22b"];


/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/
$(document).ready(function() {
  //generate legend data

  Clear_reaction_table();
  //Clear_page_summary_table();
 // Clear_page_ranking_table();
var compare_count=0;
var Legend_Data=[];var brand_id_arr=[];var reachseriesList=[];var reachLabel = [];var arr_reach_total=[];var arr_reach_label=[];
var fanseriesList=[];var fanLabel = [];var arr_fan_total=[];var arr_fan_label=[];var fan_abs_diff=[];// var min_fan_array=0;
var  post=[];var share=[];var comment=[];var summaryLabel=[];
var  arr_ov_positive=[];var arr_ov_negative=[];var arr_ov_neutral=[];var OverallLabel=[];
var  arr_cmt_positive=[];var arr_cmt_negative=[];var arr_cmt_neutral=[];var CommentLabel=[];
var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_social_total=[];
var Senti_xAxisData=[];var Senti_data1=[];var Senti_data2=[]; var Senti_seriesList=[];var Senti_pos_total=0;
var Senti_neg_total=0;
var arr_post=[];var arr_reaction=[];var arr_comment=[]; var arr_shared=[];var arr_ov_pos=[];
var arr_ov_neg=[];



    startDate = moment().subtract(1, 'month');
    endDate = moment();
    var periodType= '';
       
            if(periodType == '')
            {
              periodType='month';
            }
                  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};



function Bind_Pages()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#brand_id").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getallpages')}}", // This is the URL to the API
      data: { brand_id:brand_id}
    })
    .done(function( data ) {//   <option value="">Choose</option>
           $('#brand_id').append($('<option>', {
            value: '',
            text: 'Choose'
        }));
     for(var i in data) {

      $('#brand_id').append($('<option>', {
            value: data[i],
            text: data[i]
        }));
     }

     brand_id_arr.push(data[0]);
     Legend_Data.push(data[0]);
     ChooseDate(startDate,endDate,data[0],0,labelDate);
    
     
    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
Bind_Pages();
 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Comparison'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
hidden_div();
function ChooseDate(start,end,brand_id,sr_of_brand,label)
{
   $('.dateranges').val(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
          startDate=start;
          endDate=end;
         var brand_id = brand_id;
         var split_name = brand_id.split("-");
          if(split_name.length > 0)
          {
            if(isNaN(split_name[split_name.length-1]) == false )
              brand_id = split_name[split_name.length-1];
          }
         // alert(brand_id);
         var date_preset='this_month'; 
         var start = new Date(startDate);
         var end = new Date(endDate);
         var current_Date = new Date();
         var current_year =new Date().getFullYear();
         var timeDiff = Math.abs(current_Date.getTime() - start.getTime());
         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        if(label === 'Today' )  date_preset = 'today';
        else if (label === 'Yesterday')  date_preset = 'yesterday';
        else if (label === 'Last 7 Days' || diffDays <=7)  date_preset = 'last_7d';
        else if (label === 'Last 30 Days' ||  diffDays <=30 )  date_preset = 'last_30d';
        else if (label === 'This Quarter')  date_preset = 'this_quarter';
        else if (label === 'Last 90 Days' || diffDays <=90)  date_preset = 'last_90d';
        else if (label === 'This Year')  date_preset = 'this_year';
        else if (label === 'Last Year')  date_preset = 'last_year';
        else if (diffDays >90 && current_year === end.getFullYear() ) date_preset = 'this_year';

        socialdetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
       // requestsentimentbycategory(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand)
       // TotalReach(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand,date_preset);
     //   appendtoSummarytable(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
        FanGrowth(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand,date_preset);
       // appendtoRankingtable(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
       PageSummary(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
       PostSentiment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
       CommentSentiment(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
}
function PostSentiment(fday,sday,brand_id,post_sr_of_brand)
    {
      var PostSentiChart = echarts.init(document.getElementById('post-sentiment-chart'));
      $('#post_spin').show();
      $("#fan-growth-spin").show();
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),filter_page_name:brand_id}
    })
    .done(function(data) {
       
                var ov_total=parseInt(data[2][0]['ov_positive'])+parseInt(data[2][0]['ov_negative'])+parseInt(data[2][0]['ov_neutral'])
                var pcent_positive=parseFloat((parseInt(data[2][0]['ov_positive'])/ov_total)*100).toFixed(2);
                pcent_positive = isNaN(pcent_positive)?0:pcent_positive;
                arr_ov_positive.push(pcent_positive);
                var pcent_negative=parseFloat((parseInt(data[2][0]['ov_negative'])/ov_total)*100).toFixed(2);
                pcent_negative = isNaN(pcent_negative)?0:pcent_negative;
                arr_ov_negative.push(pcent_negative);
                var pcent_neutrual=parseFloat((parseInt(data[2][0]['ov_neutral'])/ov_total)*100).toFixed(2);
                pcent_neutrual = isNaN(pcent_neutrual)?0:pcent_neutrual;
                arr_ov_neutral.push(pcent_neutrual);
                 
                if(jQuery.inArray(Legend_Data[post_sr_of_brand], OverallLabel)=='-1')
                OverallLabel.push(Legend_Data[post_sr_of_brand]);

        option= null;
option = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    grid: {
      left: '3%',
      right: '10%',
      bottom: '3%',
      containLabel: true
    },
    xAxis:  {
      type: 'value',
      name:'Senti Percent',
       axisLabel: {
            formatter: '{value}%'
        },
        max:100
    },
    yAxis: {
      type: 'category', 
      name:'Page Name',
      data:OverallLabel,

},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_ov_neutral
}
]
};
 
    $( "#post_spin" ).hide();
    
    PostSentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            PostSentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
     var arrayLength=brand_id_arr.length;
     post_sr_of_brand = post_sr_of_brand +1;
     if(post_sr_of_brand<arrayLength )
     {
      PostSentiment(fday,sday,brand_id_arr[post_sr_of_brand],post_sr_of_brand);
      $("#fan-growth-spin").show();

     }
     else
     {
       $("#fan-growth-spin").hide();
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function CommentSentiment(fday,sday,brand_id,comment_sr_of_brand)
    {
      var CommentSentiChart = echarts.init(document.getElementById('comment-sentiment-chart'));
      $('#comment_spin').show();
      $("#fan-growth-spin").show();
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),filter_page_name:brand_id}
    })
    .done(function(data) {
       
                var comment_total=parseInt(data[1][0]['cmt_positive'])+parseInt(data[1][0]['cmt_negative'])+parseInt(data[1][0]['cmt_neutral'])+parseInt(data[1][0]['cmt_NA']);
                 // alert(comment_total);
                var neutralTotal=parseInt(data[1][0]['cmt_neutral'])+parseInt(data[1][0]['cmt_NA']);
                var pcent_positive=parseFloat((parseInt(data[1][0]['cmt_positive'])/comment_total)*100).toFixed(2);
                pcent_positive = isNaN(pcent_positive)?0:pcent_positive;
                arr_cmt_positive.push(pcent_positive);
                var pcent_negative=parseFloat((parseInt(data[1][0]['cmt_negative'])/comment_total)*100).toFixed(2);
                pcent_negative = isNaN(pcent_negative)?0:pcent_negative;
                arr_cmt_negative.push(pcent_negative);
                var pcent_neutrual=parseFloat((neutralTotal/comment_total)*100).toFixed(2);
                pcent_neutrual = isNaN(pcent_neutrual)?0:pcent_neutrual;
                arr_cmt_neutral.push(pcent_neutrual);
                 
                if(jQuery.inArray(Legend_Data[comment_sr_of_brand], CommentLabel)=='-1')
                CommentLabel.push(Legend_Data[comment_sr_of_brand]);

        option= null;
option = {
        color:colors_senti,

        tooltip : {
          trigger: 'axis',
           

    },
    legend: {
      data: ['positive', 'negative', 'neutral'],
     
    },
    grid: {
      left: '3%',
      right: '10%',
      bottom: '3%',
      containLabel: true
    },
    xAxis:  {
      type: 'value',
      name:'Senti Percent',
       axisLabel: {
            formatter: '{value}%'
        },
        max:100
    },
    yAxis: {
      type: 'category', 
      name:'Page Name',
      data:CommentLabel
},
series: [
{
  name: 'positive',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[0],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_positive
},
{
  name: 'negative',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[1],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_negative
},
{
  name: 'neutral',
  type: 'bar',
  barMaxWidth:30,
  barMinHeight:4,
  color:colors_senti[2],
  stack: 'Total amount',
  label: {
    normal: {
      show: true,
      position: 'insideLeft'
    }
  },
  data: arr_cmt_neutral
}
]
};
 
  $( "#comment_spin" ).hide();

    CommentSentiChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            CommentSentiChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

      var arrayLength=brand_id_arr.length;
     comment_sr_of_brand = comment_sr_of_brand +1;
     if(comment_sr_of_brand<arrayLength )
     {
      CommentSentiment(fday,sday,brand_id_arr[comment_sr_of_brand],comment_sr_of_brand);
      $("#fan-growth-spin").show();
     }
     else
     {
      $("#fan-growth-spin").hide();
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function PageSummary(fday,sday,brand_id,summary_sr_of_brand)
    {
      var SummaryChart = echarts.init(document.getElementById('page-summary-chart'));
      $('#summary_chart_spin').show();
      $("#fan-growth-spin").show();
     
      
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getPageSummary')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),filter_page_name:brand_id}
    })
    .done(function(data) {
       
        
      
                post.push(data[0][0]['total_post']);
                share.push(data[0][0]['shared']);
                comment.push(data[1][0]['total_comment']);
          
                if(jQuery.inArray(Legend_Data[summary_sr_of_brand], summaryLabel)=='-1')
                  summaryLabel.push(Legend_Data[summary_sr_of_brand]);

        option= null;
option = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross'
        },
      
    },
          grid: {
          top:    60,
    
    left:   '5%',
    right:  '10%',
    bottom:  '5%',
            containLabel: true
        },
         legend: {
        data:['post','comment','share'],
      
   
         padding :0,
       
        
        },

     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

    xAxis: [
        {
        
            type: 'category',
            name: 'Page Name',
             boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },
        
            data: summaryLabel
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Count',
            scale:true,
            position: 'left',
         
                    axisLabel: {
        formatter: function (e) {
            return kFormatter(e,1);
        }
    }
    
     }
    ],
    series: [{
            name:'post',
            type:'bar',
            smooth: 0.2,
            color:colors[0],
            barMaxWidth:30,
            barMinHeight:2,
            data:post
          
        },{
            name:'comment',
            type:'bar',
            smooth: 0.2,
            color:colors[1],
            barMaxWidth:30,
            barMinHeight:2,
            data:comment
          
        },{
            name:'share',
            type:'bar',
            smooth: 0.2,
            color:colors[2],
            barMaxWidth:30,
            barMinHeight:2,
            data:share
          
        }]
};
 
    $( "#summary_chart_spin" ).hide();
   
    SummaryChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            SummaryChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
      var arrayLength=brand_id_arr.length;
     summary_sr_of_brand = summary_sr_of_brand +1;
     if(summary_sr_of_brand<arrayLength )
     {
      PageSummary(fday,sday,brand_id_arr[summary_sr_of_brand],summary_sr_of_brand);
     }

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}
function FanGrowth(fday,sday,page_name,fan_sr_of_brand,date_preset)
{
    var FanGrowthChart = echarts.init(document.getElementById('fan-growth-chart'));

      $('#fan-growth-spin').show();
    var brand_id = GetURLParameter('pid');
    
$.ajax({
            type: "GET",
            dataType:'json',
            contentType: "application/json",
      url: "{{route('getPageFanDif')}}", // This is the URL to the API
      data: { date_preset: date_preset,page_name:page_name,fday:fday,sday:sday,brand_id:brand_id}
    })
    .done(function( data ) {//alert(data.length);

     var fan=[];
     var fan_abs=[];
       var fan_diff=[];
       var fan_temp_diff=[];
       var fan_total=0;
       var new_date_arr=[];
       // var fanLabel=[];
  
 // console.log('pagediff');
 //          console.log(data);
         for(var i in data) {//alert(data[i].mentions);

          if(parseInt(i) !== 0)
        {
                fan.push(data[i].fan);
                fan_abs.push(data[i].fan_abs_diff);
                fan_temp_diff.push(data[i].fan_diff);
                if(jQuery.inArray(data[i].end_time, fanLabel)=='-1')
                fanLabel.push(data[i].end_time);
                 
                new_date_arr.push(data[i].end_time);
                
       }
    
        
      }
      fanLabel.sort(function(a, b){
    var dateA=new Date(a), dateB=new Date(b)
    return dateA-dateB //sort by date ascending
})
       for(var j in fanLabel) {//alert(fanLabel[j]);
        var found =jQuery.inArray(fanLabel[j], new_date_arr);
      //  alert(found + fanLabel[j]);
        if(found=='-1')
          fan_diff.push('');
        else
          fan_diff.push(fan_temp_diff[found]);
       }
      // console.log(new_date_arr);
      // console.log(fan_temp_diff);
      console.log(fan_diff);
      console.log(fanLabel);
     //  if(min_fan_array == 0)
     //  {
     //   min_fan_array = Math.min.apply(Math, fan);
     //   min_fan_array = min_fan_array-10000;
     //  }
     //  else  if(Math.min.apply(Math, fan)<min_fan_array)
     // {
     //   min_fan_array = Math.min.apply(Math, fan);
     //   min_fan_array = min_fan_array-10000;
     //  }
     
      // console.log(fanLabel);
      $.each(fan,function(){fan_total+= parseInt(this) || 0;});
      arr_fan_label.push(Legend_Data[fan_sr_of_brand]+' : '+ formatNumber(fan_total));
      arr_fan_total.push(fan_total);
      fan_abs_diff.push(fan_abs);
      fanseriesList.push({
            name:Legend_Data[fan_sr_of_brand],
            type:'line',
            smooth: 0.2,
            color:colors[fan_sr_of_brand],
            barMinHeight:2,
            data:fan_diff
          
        },

                           );

      // console.log("fanseriesList");
      // console.log(fanseriesList);
       console.log(fan_abs_diff);

        option_growth= null;

option_growth = {
    color: colors,

    tooltip: {
        trigger: 'axis',
        axisPointer: {
           // type: 'cross'
        },
        formatter: function (params) {

        var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
        // let rez = '<p>' + params[0].name + '</p>';
        let rez = '';
        //console.log("test");
        //quite useful for debug
        params.forEach(item => {
            
             console.log(item);
             console.log(item.seriesIndex);
             console.log(item.dataIndex);
          var diff_amount = 0;
           var item_data=item.data==='-'?'-':formatNumber(item.data);
           // var xx = '<p>'   + colorSpan(item.color) + ' ' + item.seriesName +'<br>' + 'Fun Growth: ' + fan_abs_diff[item.dataIndex]  + '</p>'
           if(typeof fan_abs_diff[item.seriesIndex] !== 'undefined')
           {
            diff_amount = fan_abs_diff[item.seriesIndex][item.dataIndex];
           }
        var xx = '<p>'+params[0].name+'<br><span >' +item.seriesName +': '+item_data+'</span><br></p>'
              // var xx = '<p>'+params[0].name+'<br><span >' +item.seriesName +': '+item_data+'</span><br>' + 'Fun Growth: ' + diff_amount  + '</p>'

              // var xx = '<p>'+params[0].name+'<br><span >' +item.seriesName +': '+item_data+'</span></p>'
              rez += xx;
       
         }
           
        );

        return rez;
    }
    },
          grid: {
          top:    60,
    
    left:   '5%',
    right:  '5%',
    bottom:  '5%',
            containLabel: true
        },
         legend: {
        data:Legend_Data,
        // formatter: '{name}: '+ formatNumber(arr_reach_total[sr_of_brand]),
   
         padding :0,
       
        
        },

     toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: false, type: ['line','bar']},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },

    xAxis: [
        {
        
            type: 'category',
            name : 'Day',
             boundaryGap: true,
            axisTick: {
                alignWithLabel: true
            },
                  axisLabel: {
      formatter: function (value, index) {
       const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

     var date = new Date(value);
       var xx= date.getDate() + '\n' + monthNames[date.getMonth()];
       return xx;

},

    },
            data: fanLabel
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Fan Count',
            scale:true,
            // max: 250,
            position: 'left',
            // axisLine: {
            //     lineStyle: {
            //         color: colors[0]
            //     }
            // }
                    axisLabel: {
        formatter: function (e) {
            return formatNumber(e);
        }
    }
    
     }
    ],
    series: fanseriesList
};
//$("#fan-growth-spin").hide();

 FanGrowthChart.setOption(option_growth, true), $(function() {
    function resize() {
        setTimeout(function() {
            FanGrowthChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

     var arrayLength=brand_id_arr.length;
     fan_sr_of_brand = fan_sr_of_brand +1;
     if(fan_sr_of_brand<arrayLength )
     {
      FanGrowth(fday,sday,brand_id_arr[fan_sr_of_brand],fan_sr_of_brand,date_preset);
     }
    })
    .fail(function() {
              // If there is no communication between the server, show an error
              console.log( "error occured in FAN API" );
            });
}

        function socialdetail(fday,sday,brand_id,social_sr_of_brand)
    {//alert(keyword);
     /* var socialchart = document.getElementById('social-chart');
      var socialChart = echarts.init(socialchart);*/

      // $("#social-spin").show();
      
      $("#fan-growth-spin").show();
      $("#reaction-spin").show();
      
     
        //var brand_id = 22;
       /* alert (brand_id);
       alert(periodType);*/
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getinboundReaction')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),filter_page_name:brand_id,periodType:'month'}
    })
    .done(function(data) {//alert(data);alert("hhi");
//var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_mention_total=[];

       var socials = [];
       var social_total=0; var like_total=0; var love_total=0; var wow_total=0; var haha_total=0; var sad_total=0; var angry_total=0; 
       var share_total=0;var post_total=0;



       for(var i in data) 
       {
   
        var mediaReach=parseInt(data[i].Like)+parseInt(data[i].Love)+parseInt(data[i].Haha)+parseInt(data[i].Wow)+parseInt(data[i].Angry)+parseInt(data[i].Sad)+parseInt(data[i].shared)+parseInt(data[i].post_count)//add share
         socials.push(Math.round(mediaReach));
         if(jQuery.inArray(data[i].periodLabel, socialLabel)=='-1')
         socialLabel.push(data[i].periodLabel);
         like_total+=parseInt(data[i].Like);love_total+=parseInt(data[i].Love);wow_total+=parseInt(data[i].Wow);
         haha_total+=parseInt(data[i].Haha);sad_total+=parseInt(data[i].Sad);angry_total+=parseInt(data[i].Angry);
         share_total+=parseInt(data[i].shared);
         //console.log(data[i].post_count);
         post_total+=parseInt(data[i].post_count);

       }

      
       appendtoReactiontable(Legend_Data[social_sr_of_brand],like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,social_sr_of_brand,post_total);
     var arrayLength=brand_id_arr.length;
     social_sr_of_brand = social_sr_of_brand +1;
     if(social_sr_of_brand<arrayLength )
     {
      socialdetail(fday,sday,brand_id_arr[social_sr_of_brand],social_sr_of_brand);
     }
       $("#reaction-spin").hide();
       

//        social_total = kFormatter(social_total);

// arr_social_total.push(Legend_Data[sr_of_brand]+' : '+social_total);

// Social_seriesList.push({
//   name:arr_social_total[sr_of_brand],
//   type:'line',
//   data:socials,
//   barMaxWidth:30,
//   markPoint : {
//     large:true,
//     label: {
//       normal: {
//         formatter: function (param) {
//           return kFormatter(param.value);
//         },
//         textStyle: {
//           color:"#f5f2f2"
//         },
//         position: 'inside'
//       }
//     },
//     data : [
//     {type : 'max', name: 'maximum'},
//     {type : 'min', name: 'minimum'},

//     ]
//   }
// },

//                            );//end-push

     
// social_option = {
//   color: colors,

//       tooltip: {
//           trigger: 'axis',
//           axisPointer: {
//             type: 'cross'
//           },
//           formatter: function (params) {
//             var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
//             let rez = '<p>' + params[0].name + '</p>';
//        /* console.log(rez);*/ //quite useful for debug
//        params.forEach(item => {
//             /*console.log(item);
//             console.log(item.data);*/
//             var seriesname =item.seriesName.split(":");
//             var xx = '<p>'   + colorSpan(item.color) + ' ' + seriesname[0] + ': ' + kFormatter(item.data)  + '</p>'
//             rez += xx;
//           });

//        return rez;
//      }
//    },

//   legend: {
//     data:arr_social_total,
//     formatter: '{name}: '+arr_social_total[sr_of_brand],
//     padding :0,
//   },
//   toolbox: {
//     show : true,
//     feature : {
//       mark : {show: false},
//       dataView : {show: false, readOnly: false},
//       magicType : {show: true, type: ['line','bar']},
//       restore : {show: true},
//       saveAsImage : {show: true}
//     }
//   },
//   calculable : true,
//   xAxis : [
//   {
//    type: 'category', 
//    axisLabel: {
//     formatter: function (value, index) {
//     // Formatted to be month/day; display year only in the first label
//     const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
//     "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

//     var arr = value.split(' - ');
//     var date = new Date(arr[0]);
//        //console.log(date);
//        if(periodType === 'day')
//        {
//         var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];

//       }
//       else 
//       {

//         var texts = [date.getFullYear(), monthNames[date.getMonth()]];
//       }



//       return texts.join('-');

//     }
//   },
//   data : socialLabel
// }
// ],
// yAxis : [
// {
//   type : 'value',
//   axisLabel: {
//     formatter: function (e) {
//       return kFormatter(e);
//     }
//   }
// }
// ],

// series : Social_seriesList,
// };
// $("#social-spin").hide();
// $("#reaction-spin").hide();

//    // socialChart.setOption(option);
   
//    socialChart.setOption(social_option, true), $(function() {
//     function resize() {//alert("hi");
//     setTimeout(function() {
//       socialChart.resize()
//     }, 100)
//   }
//   $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
// });
  

//    socialChart.resize();

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}


//     function TotalReach(fday,sday,brand_id,sr_of_brand,date_preset)
// { 
      
//       var reachchart = document.getElementById('reach-chart');
//       var reachChart = echarts.init(reachchart);

//   $( "#reach-spin" ).show();


//     $.ajax({
//       type: "GET",
//       dataType:'json',
//       contentType: "application/json",
//       url: "{{route('getFbReach')}}", // This is the URL to the API
//      data: { date_preset: date_preset,period:'day',page_name:brand_id,fday:fday,sday:sday}
//     })
//     .done(function( data ) {
    
//         // alert(Object.keys(data).length);
        
//        var total_reach=[];
//        var reach_total=0;
 
//         for(var i in data) 
//         {
//         //alert(data[i].mentions); 
//           var total=parseInt(data[i].organic)+parseInt(data[i].paid);
//           if(total === 0)
//           {
//              total_reach.push('-');
//           }
//           else
//           {
//             total_reach.push(total);
//           }
          
//           if(jQuery.inArray(data[i].end_time, reachLabel)=='-1')
//           reachLabel.push(data[i].end_time);
          
//         }
// // console.log("reachLabel"); console.log(total_reach);
// $.each(total_reach,function(){reach_total+= parseInt(this) || 0;});

// /*var valueToPush = { };
// valueToPush[Legend_Data[sr_of_brand]]=mention_total;*/
// //alert(Legend_Data);
// //alert(Legend_Data[sr_of_brand]);
// arr_reach_label.push(Legend_Data[sr_of_brand]+' : '+ formatNumber(reach_total));
// //alert(arr_reach_label);
// // var result = [];
// // Legend_Data.forEach(function(key) {
// //     var found = false;
// //     arr_reach_label = arr_reach_label.filter(function(item) {
// //         var res=item.split(" :");
// //         // alert(res[0]);
// //         // alert(key);
// //         // alert(found);
// //         if(!found && String(res[0]) == String(key)) {//alert("hi");
// //             result.push(item);
// //             found = true;
// //             return false;
// //         } else 
// //             return true;
// //     })
// // })
// // arr_reach_label=[];
// // arr_reach_label=result;
// //  alert("arr "+arr_reach_label);

// arr_reach_total.push(reach_total);

 

// reachseriesList.push({
//               xAxes: [{ 
//                   ticks: {
//                   fontColor: "#f5f2f2", // this here
//                 },
//             }],

//             name:Legend_Data[sr_of_brand],
//             type:'line',
//             barMaxWidth:30,
//             smooth: 0.2,
//              barMinHeight:2,
//             data:total_reach,
//             color:colors[sr_of_brand],

//             // markPoint : {
//             //     data : [
//             //     {type : 'max', name: 'maximum'},
//             //     {type : 'min', name: 'minimum'},

//             //     ]
//             // }
//         },

//                            );//end-push

// /*console.log(arr_mention_total);
// console.log(Legend_Data);*/

// option = {
//         color:colors,
//          tooltip: {
//           trigger: 'axis',
//           axisPointer: {
//             type: 'cross'
//           },
//           formatter: function (params) {
//             var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
//             let rez = '<p>' + params[0].name + '</p>';
//        /* console.log(rez);*/ //quite useful for debug
//        params.forEach(item => {
//             //console.log(item);
//             //console.log(item.data);
//             var seriesname =item.seriesName.split(":");
//             var xx = '<p>'   + colorSpan(item.color) + ' ' + seriesname[0] + ': ' + kFormatter(item.data)  + '</p>'
//             rez += xx;
//           });

//        return rez;
//      }
//    },
      
//         legend: {
//         data:Legend_Data,
//         // formatter: '{name}: '+ formatNumber(arr_reach_total[sr_of_brand]),
   
//          padding :0,
       
        
//         },
//         toolbox: {
//             show : true,
//             feature : {
//                 mark : {show: false},
//                 dataView : {show: false, readOnly: false},
//                 magicType : {show: true, type: ['line','bar']},
//                 restore : {show: true},
//                 saveAsImage : {show: true}
//             }
//         },
//         calculable : true,
      
//         xAxis: [{
//     type: 'category', 
//             axisLabel: {
//       formatter: function (value, index) {
//      var date = new Date(value);
//        //console.log(date);
//        return date.getDate();

// },

//     },
// //     axisLabel: {
// //       formatter: function (value, index) {
// //     // Formatted to be month/day; display year only in the first label
// //     const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
// //   "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
  
// //      var arr = value.split(' - ');
// //     var date = new Date(arr[0]);

// // if(periodType === 'day')
// // {
// //     var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];
   
// // }
// // else 
// // {

// //       var texts = [date.getFullYear(), monthNames[date.getMonth()]];
// // }



// //     return texts.join('-');

// // }
// //     },
//          data : reachLabel,
// }],
        
//         yAxis : [
//         {
//             type : 'value',
//                        axisLabel: {
//         formatter: function (e) {
//             return kFormatter(e);
//         }
//     }
//         }
//         ],
//         series : reachseriesList
        
//     ,

// };
 
//     $( "#reach-spin" ).hide();
//     reachChart.setOption(option, true), $(function() {
//     function resize() {
//         setTimeout(function() {
//             reachChart.resize()
//         }, 100)
//     }
//     $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
// });
        
   
//          })
//             .fail(function() {
//               // If there is no communication between the server, show an error
//              console.log( "error occured in mentions" );
//             });

//         }

$('.dateranges').daterangepicker({
    locale: {
            format: 'MMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,

        },function(start, end,label) {//alert(label);
        var startDate;
        var endDate;
        var labelDate;
        startDate = start;
        endDate = end;
        labelDate = label;
        reachseriesList=[];reachLabel = [];arr_reach_total=[];arr_reach_label=[];
        fanseriesList=[]; fanLabel = []; arr_fan_total=[]; arr_fan_label=[]; fan_abs_diff=[];
        
        post=[]; share=[]; comment=[]; summaryLabel=[];
        arr_ov_positive=[]; arr_ov_negative=[]; arr_ov_neutral=[]; OverallLabel=[];
        arr_cmt_positive=[]; arr_cmt_negative=[]; arr_cmt_neutral=[]; CommentLabel=[];
        Social_seriesList=[]; socialLabel = [];arr_social_total=[];
        Senti_data1=[];Senti_data2=[];Senti_xAxisData=[];
  
        Clear_reaction_table();
        //Clear_page_summary_table();
        //Clear_page_ranking_table();
        compare_count=0;

        refreshGraph(startDate,endDate,labelDate);
      });


function refreshGraph(startDate,endDate,labelDate)
{//alert(brand_id_arr);
  // var arrayLength=brand_id_arr.length;
  // if(arrayLength >1)
  // {
  // for (var i = 0; i < arrayLength; i++) {
  //   compare_count=i;
  //   // alert(Legend_Data[compare_count]);
  //   ChooseDate(startDate,endDate,brand_id_arr[i],compare_count,labelDate);
  // }
    
  // }
  // else
  // {
    ChooseDate(startDate,endDate,brand_id_arr[0],0,labelDate);
  //}
   

}




   

$("#btn_add_project").click(function(){
var arrayLength=brand_id_arr.length;
  if(arrayLength >0)
  {
    for (var i = 0; i < arrayLength; i++) {
           var brand_id= brand_id_arr[i];
    //Do something
    // $('#'+brand_id).prop('disabled', true);
     $('#brand_id').children('option[value="'+brand_id+'"]').css('display','none');
}   
   /* $("select#brand_id").val(''); */
   $("select#brand_id").prop('selectedIndex', 0);
   $('#add-project').modal('show'); 
  }
  
});

$("#btn_add_brand").click(function(){
   $('#add-project').modal('hide'); 
  var selected_text=$( "#brand_id option:selected" ).text();
  var selected_value=$( "#brand_id option:selected" ).val();
 // alert(selected_value);
  if(selected_value !== '')
  {
    compare_count=compare_count+1;
    Legend_Data.push(selected_value);
    brand_id_arr.push(selected_value);
    ChooseDate(startDate,endDate,selected_value,compare_count,labelDate);

  }
 
  
  //add to legend array

});



function appendtoReactiontable(brand_name,like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,sr_no,total_mention)
{
    // total_mention=0;
  $("#tr_header").append("<th style='color:"+colors[sr_no]+"'><div>"+brand_name+"</div><div>Post: "+total_mention+"</div></th>");
  $("#tr_like").append("<td>"+kFormatter(like_total)+"</td>");
  $("#tr_love").append("<td>"+kFormatter(love_total)+"</td>");
  $("#tr_wow").append("<td>"+kFormatter(wow_total)+"</td>");
  $("#tr_haha").append("<td>"+kFormatter(haha_total)+"</td>");
  $("#tr_sad").append("<td>"+kFormatter(sad_total)+"</td>");
  $("#tr_angry").append("<td>"+kFormatter(angry_total)+"</td>");
 // $("#tr_share").append("<td>"+kFormatter(share_total)+"</td>");
}
//  function appendtoSummarytable(fday,sday,brand_id,sr_of_brand)
//     {
//       $("#summary-spin").show();
      
//        $.ajax({
//         type: "GET",
//         dataType:'json',
//         contentType: "application/json",
//       url: "{{route('getPageSummary')}}", // This is the URL to the API
//       data: { fday: fday,sday:sday,brand_id:GetURLParameter('pid'),filter_page_name:brand_id}
//     })
//     .done(function(data) {//alert(data);alert("hhi");


//   $("#tr_summary_header").append("<th style='color:"+colors[sr_of_brand]+"'><div>"+brand_id+"</div></th>");
//   $("#tr_post").append("<td>"+kFormatter(data[0][0]['total_post'])+"</td>");
//   $("#tr_overall_pos").append("<td>"+kFormatter(data[2][0]['ov_positive'])+"</td>");
//   $("#tr_overall_neg").append("<td>"+kFormatter(data[2][0]['ov_negative'])+"</td>");
//   $("#tr_reaction").append("<td>"+kFormatter(data[0][0]['total_reaction'])+"</td>");
//   $("#tr_summary_share").append("<td>"+kFormatter(data[0][0]['shared'])+"</td>");
//   $("#tr_comment").append("<td>"+kFormatter(data[1][0]['total_comment'])+"</td>");
//   $("#tr_cmt_pos").append("<td>"+kFormatter(data[1][0]['cmt_positive'])+"</td>");
//   $("#tr_cmt_neg").append("<td>"+kFormatter(data[1][0]['cmt_negative'])+"</td>");
 
// var table = document.getElementById('summary_table');
//          var cells = table.rows[0].cells.length-1;
//         // alert("cell" +  table.rows[0].cells.length);
//        //  alert("cell" +cells);
//          // Clear_page_summary_table();
//          // var sr_count=sr_of_brand+1;
//          // alert("sr" + sr_count);
         
//          var post_arr=[];var react_arr=[];var comment_arr=[];var cmt_pos_arr=[];var cmt_neg_arr=[];
//          var shared_arr=[];var ov_post_arr=[];var ov_comment_arr=[];
//          for(var i=0;i<cells;i++)
//          {
    
//             var title = (table.rows[0].cells[i].textContent.trim());
//             var value = (table.rows[1].cells[i].textContent.trim());
//             postArray= {};
//            // alert("title" +table.rows[0].cells[i+1]);
//             postArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
//             postArray['value'] =convertKtoThousand(table.rows[1].cells[i+1].textContent.trim());
          
//             post_arr.push(postArray);

//             OV_postArray= {};
//             OV_postArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
//             OV_postArray['value'] =convertKtoThousand(table.rows[2].cells[i+1].textContent.trim());

//             ov_post_arr.push(OV_postArray);

//             OV_commentArray = {};
//             OV_commentArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
//             OV_commentArray['value'] =convertKtoThousand(table.rows[3].cells[i+1].textContent.trim());

//             reactArray= {};
//             reactArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
//             reactArray['value'] =convertKtoThousand(table.rows[4].cells[i+1].textContent.trim());

//             react_arr.push(reactArray);

//             commentArray= {};
//             commentArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
//             commentArray['value'] =convertKtoThousand(table.rows[5].cells[i+1].textContent.trim());

//             comment_arr.push(commentArray);

//             commentPOSArray= {};
//             commentPOSArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
//             commentPOSArray['value'] =convertKtoThousand(table.rows[6].cells[i+1].textContent.trim());

//             cmt_pos_arr.push(commentPOSArray);

//             commentNegArray= {};
//             commentNegArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
//             commentNegArray['value'] =convertKtoThousand(table.rows[7].cells[i+1].textContent.trim());

//             cmt_neg_arr.push(commentNegArray);

//             sharedArray= {};
//             sharedArray['title'] = (table.rows[0].cells[i+1].textContent.trim());
//             sharedArray['value'] =convertKtoThousand(table.rows[8].cells[i+1].textContent.trim());

//             shared_arr.push(sharedArray);

          

//             ov_comment_arr.push(OV_commentArray);
           
           
//          }
   
//         post_arr.sort( function( a, b ) {
//     return b.value - a.value;
// }); 
//           react_arr.sort( function( a, b ) {
//     return b.value - a.value;
// }); 
//             comment_arr.sort( function( a, b ) {
//     return b.value - a.value;
// }); 
//     cmt_pos_arr.sort( function( a, b ) {
//     return b.value - a.value;
// }); 
//     cmt_neg_arr.sort( function( a, b ) {
//     return b.value - a.value;
// }); 
//               shared_arr.sort( function( a, b ) {
//     return b.value - a.value;
// }); 
//                 ov_post_arr.sort( function( a, b ) {
//     return b.value - a.value;
// }); 
//                   ov_comment_arr.sort( function( a, b ) {
//     return b.value - a.value;
// }); 
//   //console.log(post_arr);
//         appendtoRankingtable(post_arr,react_arr,comment_arr,cmt_pos_arr,cmt_neg_arr,shared_arr,ov_post_arr,ov_comment_arr,cells);
//         $("#summary-spin").hide();




//  })
// .fail(function() {
//       // If there is no communication between the server, show an error
//    //  alert( "error occured" );
//  });
// }
// function appendtoRankingtable(post_arr,react_arr,comment_arr,cmt_pos_arr,cmt_neg_arr,shared_arr,ov_post_arr,ov_comment_arr,sr_count)
//     {//alert(sr_count);
//       //alert("hoho");
//         Clear_page_ranking_table();
//       $("#ranking-spin").show();
//    var test='';

//    for(var i=0;i<sr_count;i++)
//    {
//     var no=i+1;
//   var a = Legend_Data.indexOf(post_arr[i]['title']);
//   // alert(colors[a]);
//   $("#tr_ranking_header").append("<th><div>"+LevelCheck(no)+"</div></th>");
//   $("#tr_rank_post").append("<td><font color='"+colors[a]+"'>"+post_arr[i]['title']+"</font></td>");
//   $("#tr_rank_pos").append("<td><font color='"+colors[a]+"'>"+ov_post_arr[i]['title']+"</font></td>");
//   $("#tr_rank_neg").append("<td><font color='"+colors[a]+"'>"+ov_comment_arr[i]['title']+"</font></td>");
//   $("#tr_rank_reaction").append("<td><font color='"+colors[a]+"'>"+react_arr[i]['title']+"</font></td>");
//   $("#tr_rank_share").append("<td><font color='"+colors[a]+"'>"+shared_arr[i]['title']+"</font></td>");
//   $("#tr_rank_comment").append("<td><font color='"+colors[a]+"'>"+comment_arr[i]['title']+"</font></td>");
//   $("#tr_rank_cmt_pos").append("<td><font color='"+colors[a]+"'>"+cmt_pos_arr[i]['title']+"</font></td>");
//   $("#tr_rank_cmt_neg").append("<td><font color='"+colors[a]+"'>"+cmt_neg_arr[i]['title']+"</font></td>");
 
//    }
  

       

      
     
  
//        $("#ranking-spin").hide();



// }



function Clear_reaction_table()
{
 $("#div_reaction").empty();

 $("#div_reaction").append(" <table  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_header'>"+
                           "<th>Reaction</th>"+
                          "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                           "<tr id='tr_like'>"+
                           "<td><a href='javascript:void(0)'>👍 Like</a></td>"+
                            "</tr>"+
                             "<tr id='tr_love'>"+
                             "<td><a href='javascript:void(0)'>😍 Love</a></td>"+
                             "</tr>"+
                             "<tr id='tr_wow'>"+
                             "<td><a href='javascript:void(0)'>😯 WOW</a></td>"+
                             " </tr>"+
                             "<tr id='tr_haha'>"+
                             "<td><a href='javascript:void(0)'>😆 HA HA</a></td>"+
                             "</tr>"+
                             "<tr id='tr_sad'>"+
                             "<td><a href='javascript:void(0)'>😥 Sad</a></td>"+
                             "</tr>"+
                             "<tr id='tr_angry'>"+
                             "<td><a href='javascript:void(0)'>😡 Angry</a></td>"+
                              "</tr>"+
                              // "<tr id='tr_share'>"+
                              // "<td><a href='javascript:void(0)'>⤴ Share</a></td>"+
                              // "</tr>"+
                              "</tbody>"+
                              "</table>");
}
// function Clear_page_summary_table()
// {
//  $("#div_page_summary").empty();

//  $("#div_page_summary").append(" <table id='summary_table' name='summary_table'  class='table table-bordered'>"+
//                            "<thead>"+
//                            "<tr id='tr_summary_header'>"+
//                            "<th>Benchmarks</th>"+
//                           "</tr>"+
//                            "</thead>"+
//                             "<tbody>"+
//                            "<tr id='tr_post'>"+
//                            "<td><a href='javascript:void(0)'>Posts</a></td>"+
//                             "</tr>"+
//                             "<tr id='tr_overall_pos'>"+
//                              "<td><a href='javascript:void(0)'>Overall Positive</a></td>"+
//                              "</tr>"+
//                              "<tr id='tr_overall_neg'>"+
//                              "<td><a href='javascript:void(0)'>Overall Negative</a></td>"+
//                               "</tr>"+
//                              "<tr id='tr_reaction'>"+
//                              "<td><a href='javascript:void(0)'>Reactions</a></td>"+
//                              "</tr>"+
//                              "<tr id='tr_comment'>"+
//                              "<td><a href='javascript:void(0)'>Comments</a></td>"+
//                              " </tr>"+
//                               "<tr id='tr_cmt_pos'>"+
//                              "<td><a href='javascript:void(0)'>Positive Comments</a></td>"+
//                              " </tr>"+
//                               "<tr id='tr_cmt_neg'>"+
//                              "<td><a href='javascript:void(0)'>Negative Comments</a></td>"+
//                              " </tr>"+
//                              "<tr id='tr_summary_share'>"+
//                              "<td><a href='javascript:void(0)'>Shares</a></td>"+
//                              "</tr>"+
//                              "</tbody>"+
//                              "</table>");
// }

// function Clear_page_ranking_table()
// {
//  $("#div_page_ranking").empty();

//  $("#div_page_ranking").append(" <table id='ranking_table' name='ranking_table'  class='table table-bordered'>"+
//                            "<thead>"+
//                            "<tr id='tr_ranking_header'>"+
//                            "<th>Benchmarks</th>"+
//                           "</tr>"+
//                            "</thead>"+
//                             "<tbody>"+
//                            "<tr id='tr_rank_post'>"+
//                            "<td><a href='javascript:void(0)'>Plenty Posts</a></td>"+
//                             "</tr>"+
//                             "<tr id='tr_rank_pos'>"+
//                              "<td><a href='javascript:void(0)'>Most Positive</a></td>"+
//                              "</tr>"+
//                              "<tr id='tr_rank_neg'>"+
//                              "<td><a href='javascript:void(0)'>Most Negative</a></td>"+
//                               "</tr>"+
//                              "<tr id='tr_rank_reaction'>"+
//                              "<td><a href='javascript:void(0)'>Highest Reaction</a></td>"+
//                              "</tr>"+
//                              "<tr id='tr_rank_comment'>"+
//                              "<td><a href='javascript:void(0)'>Most Comments</a></td>"+
//                              " </tr>"+
//                              "<tr id='tr_rank_cmt_pos'>"+
//                              "<td><a href='javascript:void(0)'>Most Positive Cmt:</a></td>"+
//                              " </tr>"+
//                              "<tr id='tr_rank_cmt_neg'>"+
//                              "<td><a href='javascript:void(0)'>Most Negative Cmt:</a></td>"+
//                              " </tr>"+
//                              "<tr id='tr_rank_share'>"+
//                              "<td><a href='javascript:void(0)'>Most Shared</a></td>"+
//                              "</tr>"+
                             
                              
//                               "</tbody>"+
//                               "</table>");
// }



//local customize function



     function kFormatter(num,get_zero=0) {
      if(num<=0 && get_zero==0)
      {
        return '-';
      }
      else
      {
         //return num > 999 ? (num/1000).toFixed(1) + 'k' : Math.round(num)
         return num > 999 ? Math.round((num/1000).toFixed(1)) + 'k' : Math.round(num)
      }
   
}

 function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

 function convertKtoThousand(s)
{

    var str=s;
    str=str.toUpperCase();
    if(str === '-')
    {
      return 0;
    }
    if (str.indexOf("K") !== -1) {
    str = str.replace("K",'');
    return parseFloat(str) * 1000;
  } else if (str.indexOf("M") !== -1) {
    str = str.replace("M",'');
     return parseFloat(str) * 1000000;
  } else {
    return parseFloat(str);
  }
  
}

 function LevelCheck(s)
{

    if(parseInt(s) === 1) return "1st";else if(parseInt(s) === 2) return "2nd";else if(parseInt(s) === 3) return "3rd";
    else if(parseInt(s) > 3) return s+"th";
  
}
    function judge_emotion_icon(emotion)
        {
            var emojis = ['0x1F620', '0x1F604', '0x1F616', '0x1F628', '0x1F604', '0x1F44D',
            '0x1F60D', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

        if (emotion ==="anger") return emojis[0]; 
        else if(emotion ==="interest") return emojis[1] ;
         if (emotion ==="disgust") return emojis[2] ; 
        else if(emotion ==="fear") return emojis[3] ;
         if (emotion ==="joy") return emojis[4]; 
        else if(emotion ==="like") return emojis[5] ;
         if (emotion ==="love") return emojis[6] ; 
        else if(emotion ==="neutral") return emojis[7] ;
         if (emotion ==="sadness")  return emojis[8]; 
        else if(emotion ==="surprise") return emojis[9]  ;
         else if(emotion === "trust") return emojis[10];

        }
 

       
  });
    </script>
    
<style>
.modal-dialog {
    position: absolute;
    top: 200px;
    right: 100px;
    bottom: 0;
    left: 100px;
    z-index: 10040;
    /* overflow: auto; */
}
.table td, .table th {
    padding: .75rem .5rem .75rem .5rem;
    }
    
.myHeaderContent
{
   margin-right:300px;
}
</style>
<link href="{{asset('css/own.css')}}" rel="stylesheet">
@endpush

