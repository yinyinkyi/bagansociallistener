@extends('layouts',['project_data' => $project_data,'count'=>$count,'title'=>$title])
@section('comparison')

<li class="nav-item dropdown">
                            <a  class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            
                          <!--     <button onclick="window.location='{{route('comparison', ['pid' => $pid])}}'" type="button" class="btn waves-effect waves-light btn-outline-warning">Comparison</button>  --> 
                                <button  onclick="window.location='{{route('comparison', ['pid' => $pid])}}'" class="btn btn-secondary waves-effect waves-light" style="background:#e9ebee;color:#263238" type="button"><span class="btn-label"><i class="ti-settings" ></i></span>Comparison</button>
                            </a>
                           
 </li>
 <li class="nav-item dropdown">
                            <a  class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            <button  onclick="window.location='{{route('insight', ['pid' => $pid])}}'" class="btn btn-secondary waves-effect waves-light" style="background:#e9ebee;color:#263238" type="button"><span class="btn-label"><i class="ti-settings"></i></span>Insight</button>  
                            </a>
                           
 </li>
@endsection
@section('content')
  <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-12">
                     <span class="text-danger">You can compare up to 5 brands.</span>
                    </div>
                    <div class="col-md-5 col-8 align-self-center">
                   <!--      <h3 class="text-themecolor">Comparison</h3> -->
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Comparison</a></li>
                            <li class="breadcrumb-item active"> @if (isset($project_name)) {{$project_name}} @endif</li>

                      <input type="hidden" id="brand_name" name="brand_name" value="{{$project_name}}">
                        </ol>

                    </div>

                     <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                         <div class="col-lg-4">
                         <button type="button" id="btn_add_project" class="btn waves-effect waves-light btn-block btn-warning">More Brands</button>
                         </div>
                         <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                         <div class='input-group mb-3'>
                         <input type='text' class="form-control dateranges" style="datepicker" />
                         <div class="input-group-append">
                         <span class="input-group-text">
                         <span class="ti-calendar"></span>
                         </span>
                         </div>
                         </div>
                         </div>
                          
                          </div>
                    </div>
                </div>

                <div id="add-project" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                                                    
                                                        <!-- /.modal-content -->
                       <div class="modal-content">
                                                           
                           <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Add Brand</h4>
                                                                <button type="button"  class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                         
                            <div class="message-box">
                            <div class="form-group has-success">
                                                    <label class="control-label">Brand</label>
                                                    <select id="brand_id" class="form-control custom-select">
                                                        <option value="">Choose</option>
                                                        @if (isset($project_data_exclude))
                                                        @foreach($project_data_exclude as $project_exclude)
                                                        <option value="{{$project_exclude->id}}" id="{{$project_exclude->id}}" >{{$project_exclude->name}}</option>
                                                        @endforeach
                                                        @endif
                                                   
                                          </select>
                                                    <small class="form-control-feedback"> Choose a brand to compare </small> </div>
                                                     <hr>
                                         
                                        <button type="button" id="btn_add_brand" class="btn btn-success"><i class="fa fa-check"></i> Add </button>
                            </div>
                  
                                                            </div>                              
                       </div>
                    </div>
                                                    <!-- /.modal-dialog -->
                       
               </div>

                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row" id='comparison-div-1'>

              <div class="col-lg-12">

                        <div class="card">
                         <div class="card-body">
                           <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Mentions</h3>
                                          
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                               <div style="display:none"  align="center" style="vertical-align: top;" id="mention-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             
                              <div id="mention-chart" style="width:100%; height:400px;"></div>   
                           
                            </div>
                        </div>
                    </div>
               
                </div>

              <div class="row" id='comparison-div-2'>
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                           <div class="d-flex flex-wrap">
                                    <div>
                                        <h3 class="card-title">Social Media Reach</h3>
                                          
                                        <!-- <h6 class="card-subtitle">Overview of Newsletter Campaign</h6> -->
                            
                                    </div>
                         
                                </div>
                               <div style="display:none"  align="center" style="vertical-align: top;" id="social-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             
                              <div id="social-chart" style="width:100%; height:400px;"></div>   
                           
                            </div>
                        </div>
                    </div>
               
                </div>
                <div class="row" id='comparison-div-3'>
                    <div  class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                     
                                       
                                    </div>
                                 
                                </div>
                                   <div style="display:none"  align="center" id="pie1-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           
                           <div id="pie1-chart" style="width:100%; height:200px;"></div>
                            </div>
                        </div>
                    </div>
                      <div  class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                     
                                       
                                    </div>
                                 
                                </div>
                                   <div style="display:none"  align="center" id="pie2-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           
                           <div id="pie2-chart" style="width:100%; height:200px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                       <div style="display: none" class="col-lg-4 col-md-4" id="pie_div3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                       
                                       
                                    </div>
                                 
                                </div>
                                   <div style="display:none"  align="center" id="pie3-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           
                           <div id="pie3-chart" style="width:100%; height:200px;"></div>
                            </div>
                        </div>
                    </div>
                                  <!-- Column -->
                    <!-- Column -->
                   <div style="display: none" class="col-lg-4 col-md-4" id="pie_div4">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                     
                                       
                                    </div>
                                 
                                </div>
                                   <div style="display:none"  align="center" id="pie4-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           
                           <div id="pie4-chart" style="width:100%; height:200px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div style="display: none" class="col-lg-4 col-md-4" id="pie_div5">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                     
                                       
                                    </div>
                                 
                                </div>
                                   <div style="display:none"  align="center" id="pie5-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                           
                           <div id="pie5-chart" style="width:100%; height:200px;"></div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Column -->
                </div>
              <div class="row" id='comparison-div-4'>
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                          <div style="display:none"  align="center" style="vertical-align: top;" id="reaction-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             <h4 class="card-title">Social Statistic</h4>
                              <div id="div_reaction" class="table-responsive">
                                    <table  class="table table-bordered">
                                        <thead>
                                            <tr id="tr_header">
                                                <th>Reaction</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="tr_like">
                                               <td><a href="javascript:void(0)">Like</a></td>
                                              
                                                
                                            </tr>
                                            <tr id="tr_love">
                                                <td><a href="javascript:void(0)">Love</a></td>
                                           
                                              
                                            </tr>
                                            <tr id="tr_wow">
                                                <td><a href="javascript:void(0)">WOW</a></td>
                                           
                                            </tr>
                                            <tr id="tr_haha">
                                                <td><a href="javascript:void(0)">HA HA</a></td>
                                                
                                            </tr>
                                            <tr id="tr_sad">
                                                <td><a href="javascript:void(0)">Sad</a></td>
                                               
                                            </tr>
                                            <tr id="tr_angry">
                                                <td><a href="javascript:void(0)">Angry</a></td>
                                               
                                            </tr>
                                             <tr id="tr_share">
                                                <td><a href="javascript:void(0)">Share</a></td>
                                              
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                           
                            </div>
                        </div>
                    </div>
               
                </div>
                <div class="row" id='comparison-div-5'>
              <div class="col-lg-12">
                        <div class="card">
                         <div class="card-body">
                          <div style="display:none"  align="center" style="vertical-align: top;" id="emotion-spin"> <img src="{{asset('assets\images\ajax-loader.gif')}}" id="loader"></div>
                             <h4 class="card-title">Emotion Analysis</h4>
                              <div id="div_emotion" class="table-responsive">
                                    <!-- <table  class="table table-bordered">
                                        <thead>
                                            <tr id="tr_emotion_header">
                                                <th>Emotion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="tr_anger">
                                               <td><a href="javascript:void(0)">Anger</a></td>
                                              
                                                
                                            </tr>
                                            <tr id="tr_anticipation">
                                                <td><a href="javascript:void(0)">Interest</a></td>
                                           
                                              
                                            </tr>
                                            <tr id="tr_disgust">
                                                <td><a href="javascript:void(0)">Disgust</a></td>
                                           
                                            </tr>
                                            <tr id="tr_fear">
                                                <td><a href="javascript:void(0)">Fear</a></td>
                                                
                                            </tr>
                                            <tr id="tr_joy">
                                                <td><a href="javascript:void(0)">Joy</a></td>
                                               
                                            </tr>
                                            <tr id="tr_e_like">
                                                <td><a href="javascript:void(0)">Like</a></td>
                                               
                                            </tr>
                                             <tr id="tr_e_love">
                                                <td><a href="javascript:void(0)">Love</a></td>
                                              
                                            </tr>
                                             <tr id="tr_neutral">
                                                <td><a href="javascript:void(0)">Neutral</a></td>
                                              
                                            </tr>
                                            <tr id="tr_sadness">
                                                <td><a href="javascript:void(0)">Sadness</a></td>
                                            </tr>
                                             <tr id="tr_surprise">
                                                <td><a href="javascript:void(0)">Surprise</a></td>
                                            </tr>
                                             <tr id="tr_trust">
                                                <td><a href="javascript:void(0)">Trust</a></td>
                                            </tr>
                                        </tbody>
                                    </table> -->
                                </div>
                           
                            </div>
                        </div>
                    </div>
               
                </div>
       
@endsection
@push('scripts')
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('assets/plugins/popper/popper.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/jquery.slimscroll.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/sidebarmenu.js')}}"></script>
    <!--stickey kit -->
    <script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/custom.min.js')}}"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   
    
    
    <!-- Chart JS -->
    <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script>
   <!--  <script src="{{asset('assets/plugins/echarts/echarts-init.js')}}"></script> -->
   <!--  <script src="{{asset('assets/plugins/echarts/echarts.min.js')}}"></script> -->
 
    
    <!-- Flot Charts JavaScript -->
    <script src="{{asset('assets/plugins/flot/excanvas.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
    
 <script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

    <script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
   
   <script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript')}}"></script>
    <script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
    <script type="text/javascript">
var startDate;
var endDate;
var colors=[ "#98c0d8","#7b858e","#01ad9d","#cb73a9","#a1c652","#dc3545","#e83e8c","#D2691E","#ADD8E6","#DDA0DD"];



/*var effectIndex = 2;
var effect = ['spin' , 'bar' , 'ring' , 'whirling' , 'dynamicLine' , 'bubble'];*/

  $.noConflict();

  jQuery( document ).ready(function ($) {
  //generate legend data
  Clear_emotion_table();
var compare_count=0;
var Legend_Data=[];var brand_id_arr=[];var seriesList=[];var mentionLabel = [];var arr_mention_total=[];arr_mention_label=[]
var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_social_total=[];
var brand_name=$("#brand_name").val();
Legend_Data.push(brand_name);


    startDate = moment().subtract(3, 'month');
    endDate = moment();
    var periodType= '';
       
            if(periodType == '')
            {
              periodType='month';
            }
                  var GetURLParameter = function GetURLParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

brand_id_arr.push(GetURLParameter('pid'));
 function hidden_div()
{//alert("popular");
    var brand_id = GetURLParameter('pid');
  // var brand_id = 22;
    $( "#popular-spin" ).show();
    $("#popular").empty();
  $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('gethiddendiv')}}", // This is the URL to the API
      data: { view_name:'Comparison'}
    })
    .done(function( data ) {//$("#popular").html('');
     for(var i in data) {
      $("#"+data[i].div_name).hide();
     }

    })
    .fail(function(xhr, textStatus, error) {
       console.log(xhr.statusText);
      console.log(textStatus);
      console.log(error);
      // If there is no communication between the server, show an error
     // alert( "error occured" );
    });

}
hidden_div();
function ChooseDate(start,end,brand_id,sr_of_brand)
{
   $('.dateranges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          startDate=start;
          endDate=end;
      var brand_id = brand_id;
         
        mentiondetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
        socialdetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
       
        SentimentDetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand)
      
}
 function mentiondetail(fday,sday,brand_id,sr_of_brand)
{//alert(brand_id);
      var mentionchart = document.getElementById('mention-chart');
      var mentionChart = echarts.init(mentionchart);

  $( "#mention-spin" ).show();


    $.ajax({
      type: "GET",
      dataType:'json',
      contentType: "application/json",
      url: "{{route('getmentiondetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:periodType}
    })
    .done(function( data ) {//alert("hihi");
    
         
        
        var mentions = [];
       var mention_total=0;
 
        for(var i in data) 
        {
        //alert(data[i].mentions); 
          mentions.push( Math.round(data[i].mention));
          if(jQuery.inArray(data[i].periodLabel, mentionLabel)=='-1')
          mentionLabel.push(data[i].periodLabel);
          
        }

$.each(mentions,function(){mention_total+= parseInt(this) || 0;});

/*var valueToPush = { };
valueToPush[Legend_Data[sr_of_brand]]=mention_total;*/
arr_mention_label.push(Legend_Data[sr_of_brand]+' : '+mention_total);
arr_mention_total.push(mention_total);

 

seriesList.push({
              xAxes: [{ 
                  ticks: {
                  fontColor: "#f5f2f2", // this here
                },
            }],

            name:arr_mention_label[sr_of_brand],
            type:'line',
            barMaxWidth:30,
            data:mentions,
            color:colors[sr_of_brand],

            markPoint : {
                data : [
                {type : 'max', name: 'maximum'},
                {type : 'min', name: 'minimum'},

                ]
            }
        },

                           );//end-push

/*console.log(arr_mention_total);
console.log(Legend_Data);*/

option = {
        color:colors,
         tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross'
          },
          formatter: function (params) {
            var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
            let rez = '<p>' + params[0].name + '</p>';
       /* console.log(rez);*/ //quite useful for debug
       params.forEach(item => {
            //console.log(item);
            //console.log(item.data);
            var seriesname =item.seriesName.split(":");
            var xx = '<p>'   + colorSpan(item.color) + ' ' + seriesname[0] + ': ' + kFormatter(item.data)  + '</p>'
            rez += xx;
          });

       return rez;
     }
   },
      
        legend: {
        data:arr_mention_label,
       /* formatter: '{name}: '+ kFormatter(arr_mention_total[sr_of_brand]),*/
   
         padding :0,
       
        
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: false, readOnly: false},
                magicType : {show: true, type: ['line','bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
      
        xAxis: [{
    type: 'category', 
    axisLabel: {
      formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
  
     var arr = value.split(' - ');
    var date = new Date(arr[0]);

if(periodType === 'day')
{
    var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];
   
}
else 
{

      var texts = [date.getFullYear(), monthNames[date.getMonth()]];
}



    return texts.join('-');

}
    },
         data : mentionLabel,
}],
        
        yAxis : [
        {
            type : 'value'
        }
        ],
        series : seriesList
        
    ,

};
 
    $( "#mention-spin" ).hide();
    mentionChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            mentionChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
        
    Emotiondetail(startDate.format('YYYY MM DD'),endDate.format('YYYY MM DD'),brand_id,sr_of_brand);
         })
            .fail(function() {
              // If there is no communication between the server, show an error
             console.log( "error occured in mentions" );
            });

        }

        function socialdetail(fday,sday,brand_id,sr_of_brand)
    {//alert(keyword);
      var socialchart = document.getElementById('social-chart');
      var socialChart = echarts.init(socialchart);

      $("#social-spin").show();
      $("#reaction-spin").show();
      
     
        //var brand_id = 22;
       /* alert (brand_id);
       alert(periodType);*/
       $.ajax({
        type: "GET",
        dataType:'json',
        contentType: "application/json",
      url: "{{route('getinteractiondetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'month'}
    })
    .done(function(data) {//alert(data);alert("hhi");
//var Social_Legend_Data=[];var Social_seriesList=[];var socialLabel = [];var arr_mention_total=[];

       var socials = [];
       var social_total=0; var like_total=0; var love_total=0; var wow_total=0; var haha_total=0; var sad_total=0; var angry_total=0; 
       var share_total=0;var post_total=0;
console.log(data);


       for(var i in data) 
       {
   
        var mediaReach=parseInt(data[i].Like)+parseInt(data[i].Love)+parseInt(data[i].Haha)+parseInt(data[i].Wow)+parseInt(data[i].Angry)+parseInt(data[i].Sad)+parseInt(data[i].shared)+parseInt(data[i].post_count)//add share
         socials.push(Math.round(mediaReach));
         if(jQuery.inArray(data[i].periodLabel, socialLabel)=='-1')
         socialLabel.push(data[i].periodLabel);
         like_total+=parseInt(data[i].Like);love_total+=parseInt(data[i].Love);wow_total+=parseInt(data[i].Wow);
         haha_total+=parseInt(data[i].Haha);sad_total+=parseInt(data[i].Sad);angry_total+=parseInt(data[i].Angry);
         share_total+=parseInt(data[i].shared);
         console.log(data[i].post_count);
         post_total+=parseInt(data[i].post_count);

       }

       $.each(socials,function(){social_total+= Math.round(parseInt(this)) || 0;});



       appendtoReactiontable(Legend_Data[sr_of_brand],like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,sr_of_brand,post_total);

       social_total = kFormatter(social_total);

arr_social_total.push(Legend_Data[sr_of_brand]+' : '+social_total);

Social_seriesList.push({
  name:arr_social_total[sr_of_brand],
  type:'line',
  data:socials,
  barMaxWidth:30,
  markPoint : {
    large:true,
    label: {
      normal: {
        formatter: function (param) {
          return kFormatter(param.value);
        },
        textStyle: {
          color:"#f5f2f2"
        },
        position: 'inside'
      }
    },
    data : [
    {type : 'max', name: 'maximum'},
    {type : 'min', name: 'minimum'},

    ]
  }
},

                           );//end-push

     
social_option = {
  color: colors,

      tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross'
          },
          formatter: function (params) {
            var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:'+color+';"></span>';
            let rez = '<p>' + params[0].name + '</p>';
       /* console.log(rez);*/ //quite useful for debug
       params.forEach(item => {
            /*console.log(item);
            console.log(item.data);*/
            var seriesname =item.seriesName.split(":");
            var xx = '<p>'   + colorSpan(item.color) + ' ' + seriesname[0] + ': ' + kFormatter(item.data)  + '</p>'
            rez += xx;
          });

       return rez;
     }
   },

  legend: {
    data:arr_social_total,
    /*formatter: '{name}: '+arr_social_total[sr_of_brand],*/
    padding :0,
  },
  toolbox: {
    show : true,
    feature : {
      mark : {show: false},
      dataView : {show: false, readOnly: false},
      magicType : {show: true, type: ['line','bar']},
      restore : {show: true},
      saveAsImage : {show: true}
    }
  },
  calculable : true,
  xAxis : [
  {
   type: 'category', 
   axisLabel: {
    formatter: function (value, index) {
    // Formatted to be month/day; display year only in the first label
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var arr = value.split(' - ');
    var date = new Date(arr[0]);
       //console.log(date);
       if(periodType === 'day')
       {
        var texts = [ date.getFullYear(),monthNames[date.getMonth()],date.getDate()];

      }
      else 
      {

        var texts = [date.getFullYear(), monthNames[date.getMonth()]];
      }



      return texts.join('-');

    }
  },
  data : socialLabel
}
],
yAxis : [
{
  type : 'value',
  axisLabel: {
    formatter: function (e) {
      return kFormatter(e);
    }
  }
}
],

series : Social_seriesList,
};
$("#social-spin").hide();
$("#reaction-spin").hide();

   // socialChart.setOption(option);
   
   socialChart.setOption(social_option, true), $(function() {
    function resize() {//alert("hi");
    setTimeout(function() {
      socialChart.resize()
    }, 100)
  }
  $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
  

   socialChart.resize();

 })
.fail(function() {
      // If there is no communication between the server, show an error
   //  alert( "error occured" );
 });
}

//emotion
function Emotiondetail(fday,sday,brand_id,sr_of_brand)
          {//alert(periodType);
  
            $("#emotion-spin").show();
            $.ajax({
             type: "GET",
             dataType:'json',
             contentType: "application/json",
      url: "{{route('getemotiondetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'month'}
    })
    .done(function( data ) {//alert("hihi");

      $( "#emotion-spin" ).hide();

      //console.log(data);//emotion

      var anger = [];
      var anticipation = [];
      var disgust = [];
      var fear = [];
      var joy = [];
      var like = [];
      var love = [];
      var neutral = [];
      var sadness = [];
      var surprise = [];
      var trust = [];
      
      var EmotionLabel = [];


      var anger_total =0;
      var anticipation_total =0;
      var disgust_total =0;
      var fear_total =0;
      var joy_total =0;
      var like_total =0;
      var love_total =0;
      var neutral_total =0;
      var sadness_total =0;
      var surprise_total =0;
      var trust_total =0;



      for(var i in data) 
      {
        //alert(data[i].mentions); 
        anger.push(data[i].anger);
        anticipation.push(data[i].anticipation);
        disgust.push(data[i].disgust);
        fear.push(data[i].fear);
        joy.push(data[i].joy);
        like.push(data[i].like);
        love.push(data[i].love);
        neutral.push(data[i].neutral);
        sadness.push(data[i].sadness);
        surprise.push(data[i].surprise);
        trust.push(data[i].trust);

        EmotionLabel.push(data[i].periodLabel);

      }

      $.each(anger,function(){anger_total += Math.round(parseInt(this)) || 0;});
      $.each(anticipation,function(){anticipation_total += Math.round(parseInt(this)) || 0;});
      $.each(disgust,function(){disgust_total += Math.round(parseInt(this)) || 0;});
      $.each(fear,function(){fear_total += Math.round(parseInt(this)) || 0;});
      $.each(joy,function(){joy_total += Math.round(parseInt(this)) || 0;});
      $.each(like,function(){like_total += Math.round(parseInt(this)) || 0;});
      $.each(love,function(){love_total += Math.round(parseInt(this)) || 0;});
      $.each(neutral,function(){neutral_total += Math.round(parseInt(this)) || 0;});
      $.each(sadness,function(){sadness_total += Math.round(parseInt(this)) || 0;});
      $.each(surprise,function(){surprise_total += Math.round(parseInt(this)) || 0;});
      $.each(trust,function(){trust_total += Math.round(parseInt(this)) || 0;});

  $("#emotion-spin").hide();
  $("#tr_emotion_header").append("<th  style='color:"+colors[sr_of_brand]+"'><div>"+Legend_Data[sr_of_brand]+"</div><div>Total:"+arr_mention_total[sr_of_brand]+"</div></th>");
  $("#tr_anger").append("<td>"+kFormatter(anger_total)+"</td>");
  $("#tr_anticipation").append("<td>"+kFormatter(anticipation_total)+"</td>");
  $("#tr_disgust").append("<td>"+kFormatter(disgust_total)+"</td>");
  $("#tr_fear").append("<td>"+kFormatter(fear_total)+"</td>");
  $("#tr_joy").append("<td>"+kFormatter(joy_total)+"</td>");
  $("#tr_e_like").append("<td>"+kFormatter(like_total)+"</td>");
  $("#tr_e_love").append("<td>"+kFormatter(love_total)+"</td>");
  $("#tr_neutral").append("<td>"+kFormatter(neutral_total)+"</td>");
  $("#tr_sadness").append("<td>"+kFormatter(sadness_total)+"</td>");
  $("#tr_surprise").append("<td>"+kFormatter(surprise_total)+"</td>");
  $("#tr_trust").append("<td>"+kFormatter(trust_total)+"</td>");
  })


.fail(function() {
              // If there is no communication between the server, show an error
              console.log(error);

            });

}
//PIE CHART
function SentimentDetail(fday,sday,brand_id,sr_of_brand){//alert(fday),alert(sday);

     var pie_no=sr_of_brand+1;
     /* console.log("hihi");
      console.log(pie_no);*/
     $("#pie"+pie_no+"-spin").show();

      //$("#reaction-count").empty();
     $("#pie_div"+pie_no).show();

     
 var colors = ["#98c0d8","#7b858e"];

  

         var pieChart= echarts.init(document.getElementById('pie'+pie_no+'-chart'));
        

  $.ajax({
             type: "GET",
             dataType:'json',
             contentType: "application/json",
      url: "{{route('getsentimentdetail')}}", // This is the URL to the API
      data: { fday: fday,sday:sday,brand_id:brand_id,periodType:'month'}
    })
    .done(function( data ) {console.log(data);

      var positive = [];
      var negative = [];
      var neutral = [];

      var positive_total =0;
      var negative_total =0;
      var neutral_total =0;
      var sentiment_Legend=["positive","negative"]

      for(var i in data) 
      {
        //alert(data[i].mentions); 
        positive.push(data[i].positive);
        negative.push(data[i].negative);
        neutral.push(data[i].neutral);
      

      }

      $.each(positive,function(){positive_total += Math.round(parseInt(this)) || 0;});
      $.each(negative,function(){negative_total += Math.round(parseInt(this)) || 0;});
      $.each(neutral,function(){neutral_total += Math.round(parseInt(this)) || 0;});

      var  all_total=positive_total+negative_total;
      var positive_percentage=parseFloat((positive_total/all_total)*100).toFixed(1);
      var negative_percentage= parseFloat((negative_total/all_total)*100).toFixed(1);

          positive_percentage = isNaN(positive_percentage)?0:positive_percentage;
          negative_percentage = isNaN(negative_percentage)?0:negative_percentage;

option = {
 title:{
  text:Legend_Data[sr_of_brand],
 },
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        x: 'center',
        y: 'bottom',
        data: sentiment_Legend,
              formatter: function (name) {
            if(name === 'positive')
            {
                 return name + ': ' + positive_percentage +'%';
            }
            return name  + ': ' + negative_percentage  +'%';

   
}
    },
    toolbox: {
        show: true,
        feature: {

            dataView: { show: false, readOnly: false },
            magicType: {
                show: true,
                type: ['pie', 'funnel']
            },
            restore: { show: true },
            saveAsImage: { show: true }
        }
    },
    color: colors,
    calculable: true,
    series: [
        {
            name: 'Area mode',
            type: 'pie',
            radius : '30%',
            center: ['50%', '40%'],
            x: '50%', // for funnel
            max: 40, // for funnel
            sort: 'ascending', // for funnel
            data: [
                { value:positive_total, name: sentiment_Legend[0] },
                { value:negative_total, name:  sentiment_Legend[1]  },
              
            ],

            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
 $("#pie"+pie_no+"-spin").hide();

pieChart.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            pieChart.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

       })


.fail(function() {
              // If there is no communication between the server, show an error
              console.log(error);

            });



}      
    

$('.dateranges').daterangepicker({
    locale: {
            format: 'MMMM D, YYYY'
        },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
              startDate: startDate,
              endDate: endDate,
        },function(start, end) {//alert("hihi")
        var startDate;
        var endDate;
        startDate = start;
        endDate = end;
        seriesList=[];mentionLabel = [];arr_mention_total=[];arr_mention_label=[];
        Social_seriesList=[]; socialLabel = [];arr_social_total=[];
        Clear_emotion_table();
        Clear_reaction_table();
        compare_count=0;
        refreshGraph(startDate,endDate);
      });


function refreshGraph(startDate,endDate)
{alert(brand_id_arr);
  var arrayLength=brand_id_arr.length;
  if(arrayLength >1)
  {
  for (var i = 0; i < arrayLength; i++) {
    compare_count=i;
    alert(Legend_Data[compare_count]);
    ChooseDate(startDate,endDate,brand_id_arr[i],compare_count);
  }
  }
  else
  {
    ChooseDate(startDate,endDate,GetURLParameter('pid'),0);
  }

}


    ChooseDate(startDate,endDate,GetURLParameter('pid'),0);

$("#btn_add_project").click(function(){
var arrayLength=brand_id_arr.length;
  if(arrayLength >0)
  {
    for (var i = 0; i < arrayLength; i++) {
           var brand_id= brand_id_arr[i];
    //Do something
    // $('#'+brand_id).prop('disabled', true);
     $('#brand_id').children('option[value="'+brand_id+'"]').css('display','none');
}   
   /* $("select#brand_id").val(''); */
   $("select#brand_id").prop('selectedIndex', 0);
   $('#add-project').modal('show'); 
  }
  
});

$("#btn_add_brand").click(function(){
   $('#add-project').modal('hide'); 
  var selected_text=$( "#brand_id option:selected" ).text();
  var selected_value=$("#brand_id").val();
  if(selected_value !== '')
  {
    compare_count=compare_count+1;
    Legend_Data.push(selected_text);
    brand_id_arr.push(selected_value);
    ChooseDate(startDate,endDate,selected_value,compare_count);
  }
 
  
  //add to legend array

});



function appendtoReactiontable(brand_name,like_total,love_total,wow_total,haha_total,sad_total,angry_total,share_total,sr_no,total_mention)
{
  $("#tr_header").append("<th style='color:"+colors[sr_no]+"'><div>"+brand_name+"</div><div>Total:"+total_mention+"</div></th>");
  $("#tr_like").append("<td>"+kFormatter(like_total)+"</td>");
  $("#tr_love").append("<td>"+kFormatter(love_total)+"</td>");
  $("#tr_wow").append("<td>"+kFormatter(wow_total)+"</td>");
  $("#tr_haha").append("<td>"+kFormatter(haha_total)+"</td>");
  $("#tr_sad").append("<td>"+kFormatter(sad_total)+"</td>");
  $("#tr_angry").append("<td>"+kFormatter(angry_total)+"</td>");
  $("#tr_share").append("<td>"+kFormatter(share_total)+"</td>");
}
function Clear_emotion_table()
{
  $("#div_emotion").empty();
                   

 $("#div_emotion").append("<table  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_emotion_header'>"+
                           "<th>Emotion</th>"+
                          "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                            "<tr id='tr_anger'>"+
                            "<td>"+String.fromCodePoint(judge_emotion_icon('anger'))+"<a href='javascript:void(0)'> Anger</a></td>"+
                            "</tr>"+
                            "<tr id='tr_anticipation'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('interest'))+ "<a href='javascript:void(0)'> Interest</a></td>"+
                            "</tr>"+
                            "<tr id='tr_disgust'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('disgust'))+ " <a href='javascript:void(0)'> Disgust</a></td>"+
                            "</tr>"+
                            "<tr id='tr_fear'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('fear'))+ " <a href='javascript:void(0)'> Fear</a></td>"+
                            "</tr>"+
                            "<tr id='tr_joy'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('joy'))+ "<a href='javascript:void(0)'> Joy</a></td>"+
                            "</tr>"+
                            "<tr id='tr_e_like'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('like'))+ "<a href='javascript:void(0)'> Like</a></td>"+
                            "</tr>"+
                            "<tr id='tr_e_love'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('love'))+ "<a href='javascript:void(0)'> Love</a></td>"+
                            "</tr>"+
                            "<tr id='tr_neutral'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('neutral'))+ "<a href='javascript:void(0)'> Neutral</a></td>"+
                            "</tr>"+
                            "<tr id='tr_sadness'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('sadness'))+ " <a href='javascript:void(0)'> Sadness</a></td>"+
                            "</tr>"+
                            "<tr id='tr_surprise'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('surprise'))+ " <a href='javascript:void(0)'> Surprise</a></td>"+
                            "</tr>"+
                            "<tr id='tr_trust'>"+
                            "<td>"+ String.fromCodePoint(judge_emotion_icon('trust'))+ " <a href='javascript:void(0)'>  Trust</a></td>"+
                            "</tr>"+
                            "</tbody>"+
                              "</table>");
}
function Clear_reaction_table()
{
 $("#div_reaction").empty();

 $("#div_reaction").append(" <table  class='table table-bordered'>"+
                           "<thead>"+
                           "<tr id='tr_header'>"+
                           "<th>Reaction</th>"+
                          "</tr>"+
                           "</thead>"+
                            "<tbody>"+
                           "<tr id='tr_like'>"+
                           "<td><a href='javascript:void(0)'>Like</a></td>"+
                            "</tr>"+
                             "<tr id='tr_love'>"+
                             "<td><a href='javascript:void(0)'>Love</a></td>"+
                             "</tr>"+
                             "<tr id='tr_wow'>"+
                             "<td><a href='javascript:void(0)'>WOW</a></td>"+
                             " </tr>"+
                             "<tr id='tr_haha'>"+
                             "<td><a href='javascript:void(0)'>HA HA</a></td>"+
                             "</tr>"+
                             "<tr id='tr_sad'>"+
                             "<td><a href='javascript:void(0)'>Sad</a></td>"+
                             "</tr>"+
                             "<tr id='tr_angry'>"+
                             "<td><a href='javascript:void(0)'>Angry</a></td>"+
                              "</tr>"+
                              "<tr id='tr_share'>"+
                              "<td><a href='javascript:void(0)'>Share</a></td>"+
                              "</tr>"+
                              "</tbody>"+
                              "</table>");
}



//local customize function



     function kFormatter(num) {
    return num > 999 ? (num/1000).toFixed(1) + 'k' : Math.round(num)
}

    function judge_emotion_icon(emotion)
        {
            var emojis = ['0x1F620', '0x1F604', '0x1F616', '0x1F628', '0x1F604', '0x1F44D',
            '0x1F60D', '0x1F610','0x1F614', '0x1F62E', '0x1F44C'];//

        if (emotion ==="anger") return emojis[0]; 
        else if(emotion ==="interest") return emojis[1] ;
         if (emotion ==="disgust") return emojis[2] ; 
        else if(emotion ==="fear") return emojis[3] ;
         if (emotion ==="joy") return emojis[4]; 
        else if(emotion ==="like") return emojis[5] ;
         if (emotion ==="love") return emojis[6] ; 
        else if(emotion ==="neutral") return emojis[7] ;
         if (emotion ==="sadness")  return emojis[8]; 
        else if(emotion ==="surprise") return emojis[9]  ;
         else if(emotion === "trust") return emojis[10];

        }
 

       
  });
    </script>
<style>
.modal-dialog {
    position: absolute;
    top: 200px;
    right: 100px;
    bottom: 0;
    left: 100px;
    z-index: 10040;
    /* overflow: auto; */
}
</style>
@endpush

