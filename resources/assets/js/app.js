
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// window._ = require('lodash');
// window._$=window.jQuery = require('jquery');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('tag', require('./components/Notification.vue'));

const app = new Vue({
   el: '#app',
    data:{
    	tags:'',
    },
    created(){
    	if(window.Laravel.userId){
    		axios.post('https://cms.baganintel.com/BaganSocialListener/public/notification/tag/notification'
               
                ).then(response=>{
    			this.tags = response.data;
    			console.log(response.data)
    		});

    		Echo.private('App.User.'+ window.Laravel.userId).notification((response)=>{
    			data = {"data":response,'created_at':response.tag.created_at};
    			this.tags.push(data);
    			console.log(response);
    		});
    	}


    }
});
