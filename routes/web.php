<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
  if($user = Auth::user())
{
  $title="Dashboard";
   return view('index',compact('title'));

}
else
{
    return view('auth.login');
}
});*/

Route::get('/', ['as' => 'brand','uses' => 'HomeController@index']);
/*Auth::routes();*/
Route::get('login', ['as' => 'login','uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login',['as' => '','uses' => 'Auth\LoginController@login']);

Route::get('comment', ['as' => 'updateComment','uses' => 'MongoCRUDController@commentUpdate']);

Route::get('tag',['as' => 'MakeTag','uses' => 'MongoCRUDController@doTag']);

// Password Reset Routes...
Route::post('password/email', [
  'as' => 'password.email',
  'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);
Route::get('password/reset', [
  'as' => 'password.request',
  'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('password/reset', [
  'as' => '',
  'uses' => 'Auth\ResetPasswordController@reset'
]);
Route::get('password/reset/{token}', [
  'as' => 'password.reset',
  'uses' => 'Auth\ResetPasswordController@showResetForm'
]);
Route::get('register', [
  'as' => 'register',
  'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('register', [
  'as' => '',
  'uses' => 'Auth\RegisterController@register'
]);


Route::group(['middleware' => 'auth'], function () {
  Route::post('/notification/tag/notification','tagsController@notification');
  

Route::get('gethiddendiv', [
  'as'   => 'gethiddendiv',
  'uses' => 'HomeController@gethiddendiv'
]);


Route::get('pointoutmention', [
  'as'   => 'pointoutmention',
  'uses' => 'HomeController@pointoutmention'
]);

Route::get('pointoutcomment', [
  'as'   => 'pointoutcomment',
  'uses' => 'HomeController@pointoutcomment'
]);
Route::get('pointoutpost', [
  'as'   => 'pointoutpost',
  'uses' => 'HomeController@pointoutpost'
]);

Route::post('brand_store', [
  'as'   => 'brand_store',
  'uses' => 'ProjectController@brandStore'
]);

Route::post('brand_update/{id}', [
  'as'   => 'brand_update',
  'uses' => 'ProjectController@brandUpdate'
]);

Route::get('brandList/edit/{id}', [
  'as'   => 'brandList/edit',
  'uses' => 'ProjectController@brandEdit'
]);

Route::get('brandList/delete/{id}', [
  'as'   => 'brandList/delete',
  'uses' => 'ProjectController@brand_delete'
]);

Route::get('brandList/create', [
  'as'   => 'brandList/create',
  'uses' => 'ProjectController@brandCreate'
]);

Route::get('mention', [
  'as'   => 'mention',
  'uses' => 'HomeController@mention'
]);


Route::get('analysis', [
  'as'   => 'analysis',
  'uses' => 'HomeController@analysis'
]);

Route::get('comparison', [
  'as'   => 'comparison',
  'uses' => 'HomeController@comparison'
]);


Route::get('competitor', [
  'as'   => 'competitor',
  'uses' => 'HomeController@competitor'
]);

Route::get('insight', [
  'as'   => 'insight',
  'uses' => 'HomeController@insight'
]);
Route::get('page_manage', [
  'as'   => 'page_manage',
  'uses' => 'HomeController@page_manage'
]);
Route::get('insight_facebook', [
  'as'   => 'insight_facebook',
  'uses' => 'HomeController@insight_facebook'
]);

Route::get('monitor', [
  'as'   => 'monitor',
  'uses' => 'HomeController@monitor'
]);

Route::get('brandList', [
  'as'   => 'brandList',
  'uses' => 'HomeController@brandList'
]);


Route::get('getallpages', [
  'as'   => 'getallpages',
  'uses' => 'HomeController@getallpages'
]);


Route::get('/dashboard', ['as' => 'dashboard','uses' => 'HomeController@dashboard']);
Route::get('post', ['as' => 'post','uses' => 'HomeController@post']);
Route::get('comment', ['as' => 'comment','uses' => 'HomeController@comment']);

Route::get('/charts', 
  ['as' => 'charts', function () {
    return view('charts.chartjs');
}]);

Route::resource('users','UserController');
Route::get('deleteuser/{id}', [ 'as' => 'deleteuser', 'uses' => 'UserController@destroy']);

Route::get('getuserlist', [ 'as' => 'getuserlist', 'uses' => 'UserController@getUserData']);

Route::get('getbrandlist', [ 'as' => 'getbrandlist', 'uses' => 'ProjectController@getProjectData']);
/*Auth::routes();*/

Route::get('register', [
  'as' => 'register',
  'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('register', [
  'as' => '',
  'uses' => 'Auth\RegisterController@register'
]);
Route::get('getmentionchartdatabymongo', [ 'as' => 'getmentionchartdatabymongo', 'uses' => 'MongodbDataController@getmentionchartdatabymongo']);
Route::get('getinteractiondatabymongo', [ 'as' => 'getinteractiondatabymongo', 'uses' => 'MongodbDataController@getinteractiondatabymongo']);
Route::get('getpopularmention', [ 'as' => 'getpopularmention', 'uses' => 'MongodbDataController@getpopularmention']);
Route::get('getlatestmention', [ 'as' => 'getlatestmention', 'uses' => 'MongodbDataController@getlatestmention']);
Route::get('getmentionchartdata',['as'=>'getmentionchartdata','uses'=>'RedshiftDataController@getmentionchartdata']);
Route::get('getmentiondetail',['as'=>'getmentiondetail','uses'=>'mentionDataController@getmentiondetail']);
Route::get('getsentimentdetail',['as'=>'getsentimentdetail','uses'=>'mentionDataController@getsentimentdetail']);

Route::get('getemotiondetail',['as'=>'getemotiondetail','uses'=>'mentionDataController@getemotiondetail']);
Route::get('getpopularnegative',['as'=>'getpopularnegative','uses'=>'mentionDataController@getpopularnegative']);
Route::get('getinteractiondetail',['as'=>'getinteractiondetail','uses'=>'mentionDataController@getinteractiondetail']);
Route::get('getinterestdata',['as'=>'getinterestdata','uses'=>'mentionDataController@getinterestdata']);
Route::get('getallmention',['as'=>'getallmention','uses'=>'mentionDataController@getallmention']);
Route::get('getallmentioncomment',['as'=>'getallmentioncomment','uses'=>'mentionDataController@getallmentioncomment']);
Route::get('getsentimentbycategory',['as'=>'getsentimentbycategory','uses'=>'mentionDataController@getsentimentbycategory']);
Route::get('getnetpromotorscore',['as'=>'getnetpromotorscore','uses'=>'mentionDataController@getnetpromotorscore']);
Route::get('getInfluencerProfile',['as'=>'getInfluencerProfile','uses'=>'mentionDataController@getInfluencerProfile']);
Route::get('getInfluencerPage',['as'=>'getInfluencerPage','uses'=>'mentionDataController@getInfluencerPage']);
Route::get('getallmentioninsert',['as'=>'getallmentioninsert','uses'=>'ProjectController@insert_Posts_Data']);

Route::get('getDashboardSentimentGraph',['as'=>'getDashboardSentimentGraph','uses'=>'MongodbDataController@getDashboardSentimentGraph']);
Route::get('getDashboardMentionGraph',['as'=>'getDashboardMentionGraph','uses'=>'MongodbDataController@getDashboardMentionGraph']);
Route::get('gethighlighted',['as'=>'gethighlighted','uses'=>'MongodbDataController@gethighlighted']);


Route::get('getcomments', [
  'as'=>'getcomments',
  'uses' => 'mentionDataController@getcomments'
]);

Route::post('/ajax/edit/postbookmark', [
  'as'=>'postbookmark',
  'uses' => 'MongoCRUDController@UpdateBookMarkPost'
]);
Route::post('/ajax/edit/in_postbookmark', [
  'as'=>'in_postbookmark',
  'uses' => 'MongoCRUDController@UpdateBookMarkInboundPost'
]);

Route::post('/ajax/edit/post_comment_bookmark', [
  'as'=>'post_comment_bookmark',
  'uses' => 'MongoCRUDController@UpdateBookMarkComment'
]);
Route::post('/ajax/edit/inbound_comment_bookmark', [
  'as'=>'inbound_comment_bookmark',
  'uses' => 'MongoCRUDController@UpdateBookMarkInboundComment'
]);


Route::post('logout',['as' => 'logout','uses' => 'Auth\LoginController@logout']);

//Route::get('/home', 'HomeController@index')->name('home');
/*point out controller*/
Route::get('getSentimentMention',['as'=>'getSentimentMention','uses'=>'pointOutController@getSentimentMention']);
Route::get('getsentimentcomment',['as'=>'getsentimentcomment','uses'=>'pointOutController@getsentimentcomment']);
Route::get('getCategoryMention',['as'=>'getCategoryMention','uses'=>'pointOutController@getCategoryMention']);
Route::get('remove_keyword', [
  'as'   => 'remove_keyword',
  'uses' => 'pointOutController@remove_keyword'
]);

//insight
Route::get('getemotionRate',['as'=>'getemotionRate','uses'=>'InsightController@getemotionRate']);

Route::get('getcommentEmotionbyPostType',['as'=>'getcommentEmotionbyPostType','uses'=>'InsightController@getcommentEmotionbyPostType']);

Route::get('getInteractionByPostType',['as'=>'getInteractionByPostType','uses'=>'InsightController@getInteractionByPostType']);

 //page management
Route::get('getRelatedcomment',['as'=>'getRelatedcomment','uses'=>'PageManageController@getRelatedcomment']);
Route::get('getInboundPost',['as'=>'getInboundPost','uses'=>'PageManageController@getInboundPost']);
Route::get('getTopAndLatestPost',['as'=>'getTopAndLatestPost','uses'=>'PageManageController@getTopAndLatestPost']);
Route::get('getRelatedPosts',['as'=>'getRelatedPosts','uses'=>'PageManageController@getRelatedPosts']);
Route::get('getsentimentbypage',['as'=>'getsentimentbypage','uses'=>'PageManageController@getsentimentbypage']);
Route::get('relatedcomment', [
  'as'   => 'relatedcomment',
  'uses' => 'HomeController@relatedcomment'
]);
Route::get('relatedcompetitorcomment', [
  'as'   => 'relatedcompetitorcomment',
  'uses' => 'HomeController@relatedcompetitorcomment'
]);

Route::get('relatedpost', [
  'as'   => 'relatedpost',
  'uses' => 'HomeController@relatedpost'
]);

Route::post('setUpdatedPredict', [
  'as'=>'setUpdatedPredict',
  'uses' => 'PageManageController@Updatepredict'
]);


Route::post('setUpdatedPostPredict', [
  'as'=>'setUpdatedPostPredict',
  'uses' => 'PageManageController@UpdatedPostPredict'
]);

Route::post('setActionUpdate', [
  'as'=>'setActionUpdate',
  'uses' => 'PageManageController@ActionUpdate'
]);
//tags
Route::resource('tags', 'tagsController');
Route::resource('companyInfo', 'CompanyInfoController');
Route::get('getCompanyInfo', [ 'as' => 'getCompanyInfo', 'uses' => 'CompanyInfoController@getCompanyInfo']);
Route::get('gettaglist', [ 'as' => 'gettaglist', 'uses' => 'tagsController@gettaglist']);
Route::get('deletetag/{id}', [ 'as' => 'deletetag', 'uses' => 'tagsController@destroy']);

Route::get('/notify', 'PusherController@sendNotification');
Route::post('quick_tag', [
  'as'=>'quick_tag',
  'uses' => 'tagsController@quick_tag'
]);
//Inbound
Route::get('getInHighlighted',['as'=>'getInHighlighted','uses'=>'Inbound\inboundController@getHighlighted']);
Route::get('getinboundReaction',['as'=>'getinboundReaction','uses'=>'Inbound\inboundController@getinboundReaction']);
Route::get('getinboundsentiment',['as'=>'getinboundsentiment','uses'=>'Inbound\inboundController@getInboundsentiment']);
Route::get('getInboundPointOut',['as'=>'getInboundPointOut','uses'=>'pointOutController@getInboundPointOut']);
Route::get('getInboundinterest',['as'=>'getInboundinterest','uses'=>'Inbound\inboundController@getInboundinterest']);
Route::get('getInboundlatestPOST', [ 'as' => 'getInboundlatestPOST', 'uses' => 'Inbound\inboundController@getInboundlatestPOST']);
Route::get('getInboundPostPoint',['as'=>'getInboundPostPoint','uses'=>'pointOutController@getInboundPostPoint']);
Route::get('getInboundcomments', [
  'as'=>'getInboundcomments',
  'uses' => 'PageManageController@getInboundcomments'
]);
Route::get('getTagSentiment',['as'=>'getTagSentiment','uses'=>'Inbound\inboundController@getTagSentiment']);
Route::get('getTagCount',['as'=>'getTagCount','uses'=>'Inbound\inboundController@getTagCount']);
//insight
Route::get('getInteractionByInboundPostType',['as'=>'getInteractionByInboundPostType','uses'=>'Inbound\insightInboundController@getInteractionByInboundPostType']);
Route::get('getEmotionByInboundPostType',['as'=>'getEmotionByInboundPostType','uses'=>'Inbound\insightInboundController@getEmotionByInboundPostType']);
Route::get('getserviceAnalysis',['as'=>'getserviceAnalysis','uses'=>'Inbound\insightInboundController@getserviceAnalysis']);
Route::get('getAccessToken',['as'=>'getAccessToken','uses'=>'Inbound\inboundController@getAccssToken']);
Route::get('getPostingStatus',['as'=>'getPostingStatus','uses'=>'Inbound\inboundController@getPostingStatus']);
Route::get('getEngagementStatus',['as'=>'getEngagementStatus','uses'=>'Inbound\inboundController@getEngagementStatus']);
Route::get('getPageSummary',['as'=>'getPageSummary','uses'=>'Inbound\inboundController@getPageSummary']);

//testing
Route::get('sync_mongodata',['as'=>'sync_mongodata','uses'=>'TestController@Sync_MongoData']);

//facebook Insight
Route::get('getFbReach',['as'=>'getFbReach','uses'=>'Inbound\FBInsightController@getFbReach']);
Route::get('getFbReachCompare',['as'=>'getFbReachCompare','uses'=>'Inbound\FBInsightController@getFbReachCompare']);
Route::get('getcityReach',['as'=>'getcityReach','uses'=>'Inbound\FBInsightController@getCityReach']);
Route::get('get-agegender-Reach',['as'=>'get-agegender-Reach','uses'=>'Inbound\FBInsightController@GetageGenderReach']);
Route::get('getPageFan',['as'=>'getPageFan','uses'=>'Inbound\FBInsightController@GetPageFan']);
Route::get('getPageFanDif',['as'=>'getPageFanDif','uses'=>'Inbound\FBInsightController@GetPageFanDif']);
Route::get('getcityFan',['as'=>'getcityFan','uses'=>'Inbound\FBInsightController@getCityFan']);
Route::get('get-agegender-Fan',['as'=>'get-agegender-Fan','uses'=>'Inbound\FBInsightController@GetageGenderFan']);

//Report
Route::get('rpt-sentipredict',['as'=>'rpt-sentipredict','uses'=>'HomeController@RptSentiPredict']);
Route::get('rpt-humanpredict',['as'=>'rpt-humanpredict','uses'=>'HomeController@RptHumanPredict']);
Route::get('rpt-postDetail-export',['as'=>'rpt-postDetail-export','uses'=>'HomeController@RptPostExport']);
Route::get('comparisonpdf', ['as'   => 'comparisonpdf','uses' => 'HomeController@comparisonpdf']);
Route::get('rpt-analysis',['as'=>'rpt-analysis','uses'=>'HomeController@RptAnalysis']);
Route::get('getPostExcelData',['as'=>'getPostExcelData','uses'=>'ReportController@getPostExcelData']);
Route::get('senti-predict-condition',['as'=>'senti-predict-condition','uses'=>'ReportController@SentiPredictCondition']);
Route::get('human-predict-list',['as'=>'human-predict-list','uses'=>'ReportController@HumanPredictRecord']);
Route::get('comparison_pdf',['as'=>'comparison_pdf','uses'=>'ReportController@comparison_pdf']);
});



//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});
 
Route::get('page_delete',['as'=>'pagedelete','uses'=>'ProjectController@page_delete']);