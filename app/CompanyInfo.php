<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{
     //
      protected $table = 'company_info';
  protected $dates = ['created_at','updated_at'];
    protected $fillable = [
        'id','name','phone','address','logo_path','created_at','updated_at'
    ];
}
