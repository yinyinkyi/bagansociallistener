<?php


namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class MongoFollowers extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'followers';


}