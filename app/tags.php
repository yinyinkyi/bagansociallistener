<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tags extends Model
{
    //
      protected $table = 'tags';
  protected $dates = ['created_at','updated_at'];
    protected $fillable = [
        'id','name','keywords','created_at','updated_at'
    ];
}
