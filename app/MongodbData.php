<?php


namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class MongodbData extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'posts';
    protected $dates = ['created_time'];

  public function getDates() {
        return array();
  }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_time','message', 'reaction','sentiment','emotion'
    ];
 
}