<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_permission extends Model
{
    //
    protected $table='user_permission';
     protected $fillable = [
        'id','user_id','edit','removing_keyword', 'setting'
    ];
}
