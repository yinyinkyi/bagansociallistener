<?php

namespace App\Http\Controllers;

use App\MongodbData;
use App\MongoPages;
use App\MongoFollowers;
use App\Comment;
use App\keywords_removed;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\GlobalController;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;

class pointOutController extends Controller
{
   use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getSentimentMention()
    {

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

}
     
       $brand_id=Input::get('brand_id');
       $from_graph=Input::get('from_graph');

       $filter_keyword ='';
       $filter_sentiment='';

       if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword');

       if(null !== Input::get('sentiment'))
       $filter_sentiment=Input::get('sentiment'); 

       if(null !== Input::get('type'))
       {
       $type=Input::get('type');
       $type = (strtolower($type) == 'positive')?'pos':'neg';
       } 
       $period_type = '';
       if(null !== Input::get('category'))
       $category=Input::get('category');//page category for example :beauty,news

       if(null !== Input::get('emotion_type'))
       $emotion_type=Input::get('emotion_type');//example: like,anger,interest

       if(null !== Input::get('highlight_text'))
       $highlight_text= str_replace(' ', '', Input::get('highlight_text'));
  $additional_where="";
      
        if(null !==Input::get('period'))
        {
       $period=Input::get('period');
        $char_count = substr_count($period,"-");
        $format_type = '';
        if($char_count == 1)
        {
            $format_type ='%Y-%m';
        }
        else if($char_count == 2)
        {
             $format_type ='%Y-%m-%d';
        }   
        else if($char_count > 2)
        {
            /*this is week*/
            $pieces = explode(" - ", $period);
            $format_type ='%Y-%m-%d';
            $period_type ='week';
        }

      
if($period_type == 'week')
{
 $additional_where = " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

}
else
{
    $additional_where = " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
}

       }
 $add_con="";
 $add_comment_con="";
if($filter_keyword !== '')
{
  $add_con = " and posts.message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}

if($filter_sentiment !== '')
{
  $add_con .= " and posts.sentiment = '".$filter_sentiment."'";
  $add_comment_con .= " and cmts.sentiment  = '".$filter_sentiment."'";
}
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
  $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}


        
       /*$brand_id = 47;
       $period = '2018-07';
       $type = 'pos';
       $category ='education';
       $highlight_text ='ဈခတျေမီသညျ့ကိုအသုံးပွုထားခွငျး';
       $from_graph='highlighted';*/
       

/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2013 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/


   if( $from_graph == 'sentiment')
       {
$query = "SELECT  Liked,Love,Wow,Haha,Sad,Angry,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_".$brand_id."_posts posts LEFT JOIN temp_".$brand_id."_comments cmts on posts.id=cmts.post_id ".
" WHERE posts.sentiment='".$type."'   AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $additional_where . $add_con .   $add_inbound_con.
" ORDER by DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') DESC";
}
else  if( $from_graph == 'category')
{
$query = "SELECT  Liked,Love,Wow,Haha,Sad,Angry,category,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_".$brand_id."_posts posts LEFT JOIN temp_".$brand_id."_comments cmts on posts.id=cmts.post_id ".
" inner JOIN temp_".$brand_id."_pages pages on posts.page_name = pages.page_name  ".
" WHERE posts.sentiment='".$type."' AND category='".$category."'   AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".  $add_con .   $add_inbound_con. 
" ORDER by DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') DESC";
}
else  if( $from_graph == 'emotion')
{
$query = "SELECT  Liked,Love,Wow,Haha,Sad,Angry,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_".$brand_id."_posts posts LEFT JOIN temp_".$brand_id."_comments cmts on posts.id=cmts.post_id ".
" WHERE posts.emotion='".$emotion_type."'   AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $additional_where .  $add_con .   $add_inbound_con. 
" ORDER by DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') DESC";
}

else  if( $from_graph == 'highlighted')
{

/*  $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2013 05 01')));
  $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/

$query = "SELECT  Liked,Love,Wow,Haha,Sad,Angry,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_".$brand_id."_posts posts LEFT JOIN temp_".$brand_id."_comments cmts on posts.id=cmts.post_id ".
" WHERE ( REPLACE(REPLACE(REPLACE(posts.message, '\r', ''), '\n', ''),' ','')) Like '%".$highlight_text."%'   AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".  $add_con .   $add_inbound_con.
" ORDER by DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') DESC";
}

else  if( $from_graph == 'reaction')
{
  $react_con='';
 $react_con =" posts.".$emotion_type.">0 ";

$query = "SELECT Liked,Love,Wow,Haha,Sad,Angry, posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id ".
" WHERE ".$react_con."   AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $additional_where .  $add_con  .
" ORDER by DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') DESC";

}

// echo $query;
// return;
$query_result = DB::select($query);
     
 /*print_r($query_result);
 return;*/
 $data = [];
 $data_mention=[];
 foreach ($query_result as  $key => $row) {
   $pos =($row->cmt_sentiment=='pos')?1:0 ;$neg=($row->cmt_sentiment=='neg')?1:0;
   $anger =($row->cmt_emotion=='anger')?1:0 ; $interest=($row->cmt_emotion=='interest')?1:0;
   $disgust=($row->cmt_emotion=='disgust')?1:0;$fear=($row->cmt_emotion=='fear')?1:0;
   $joy=($row->cmt_emotion=='joy')?1:0;$like=($row->cmt_emotion=='like')?1:0;
   $love=($row->cmt_emotion=='love')?1:0;$neutral=($row->cmt_emotion=='neutral')?1:0;
   $sadness=($row->cmt_emotion=='sadness')?1:0;$surprise=($row->cmt_emotion=='surprise')?1:0;
   $trust=($row->cmt_emotion=='trust')?1:0;
   $message=$this->readmore($row->message);
            if (isset($row ->id))
   {
                 $id= $row ->id;
   
    if (!array_key_exists($id , $data)) {
    $data[$id] = array(
                'id'     =>$row->id,
                'message' => $message,
                'page_name' =>$row->page_name,
                'created_time' =>$row->created_time,
                'link' =>$row->link,
                'full_picture' =>$row->full_picture,
                'sentiment' =>$row->sentiment,
                'emotion' =>$row->emotion,
                'positive'=> $pos,'negative'=>$neg,
                'liked'=>$row->Liked ,'loved'=>$row->Love,'haha'=>$row->Haha,'wow'=>$row->Wow,'sad'=>$row->Sad,'angry'=>$row->Angry,
                'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                'isBookMark'=>$row->isBookMark
            );
}
else
{
 $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
 $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
 $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
 $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
 $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
 $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
 $data[$id]['like'] = (int) $data[$id]['like'] + $like;
 $data[$id]['love'] = (int) $data[$id]['love'] + $love;
 $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
 $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
 $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
 $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;



}
//echo $data[$id]['negative'];echo $neg;return;

}
 
            }
            $data_mention=$data;

/*$id='799069593563429_1007801336023586';
echo $data[$id]['negative'];
 print_r($data_mention);
 return;
 $data_mention = $this->unique_multidim_array($data_mention,'id'); */
$permission_data = $this->getPermission(); 
/*print_r($data_mention);
return;*/
 return Datatables::of($data_mention)
       ->addColumn('action', function ($data_mention) {
                return '<a href="javascript:void(0)" id="'.$data_mention['id'].'" name="'.$data_mention['isBookMark'].'" class="mdi '.$data_mention['isBookMark'].' text-yellow btn-bookmark" style="float:right;color:#f39c12"></a>';
            }) 

       ->addColumn('post_div', function ($data_mention)  use($permission_data){
               $html= '<div class="profiletimeline"><div class="sl-item">
                                    <div class="sl-left"> <img src="'.$data_mention['full_picture'].'" alt="user" class="img-circle  img-bordered-sm"> </div> 
                                    <div class="sl-right"> <div><a href="'.$data_mention['link'].'"  class="link"  target="_blank">'.$data_mention['page_name'].'</a>
              <span class="sl-date">'.$data_mention['created_time'].'</span>';

                $html.= '<p>Sentiment : <select class="form-control custom-select sentiment-color customize-select" id="sentiment_'.$data_mention['id'].'"><option value="pos"';
                if($data_mention['sentiment'] == "pos")
                $html.= 'selected="selected"';
                $html.= '>pos</option><option value="neg"';
                if($data_mention['sentiment'] == "neg")
                $html.= 'selected="selected"';  
                $html.= '>neg</option><option value="neutral"';
                if($data_mention['sentiment'] == "neutral")
                $html.= 'selected="selected"';    
                $html.= '>neutral</option><option value="NA"';
                if($data_mention['sentiment'] == "NA")
                $html.= 'selected="selected"';  
                $html.= '>NA</option></select>';

                $html.= ' | Emotion : <select class="form-control custom-select emotion-color customize-select" id="emotion_'.$data_mention['id'].'"><option value="anger"';
                if($data_mention['emotion'] == "anger")
                $html.= 'selected="selected"';
                $html.= '>anger</option><option value="interest"';
                if($data_mention['emotion'] == "interest")
                $html.= 'selected="selected"';     
                $html.= '>interest</option><option value="disgust"';
                if($data_mention['emotion'] == "disgust")
                $html.= 'selected="selected"';  
                $html.= '>disgust</option><option value="fear"';
                if($data_mention['emotion'] == "fear")
                $html.= 'selected="selected"'; 
                $html.= '>fear</option><option value="joy"';
                if($data_mention['emotion'] == "joy")
                $html.= 'selected="selected"'; 
                $html.= '>joy</option><option value="like"';
                if($data_mention['emotion'] == "like")
                $html.= 'selected="selected"'; 
                $html.= '>like</option><option value="love"';
                if($data_mention['emotion'] == "love")
                $html.= 'selected="selected"'; 
                $html.= '>love</option><option value="neutral"';
                if($data_mention['emotion'] == "neutral")
                $html.= 'selected="selected"';
                $html.= '>neutral</option><option value="sadness"';
                if($data_mention['emotion'] == "sadness")
                $html.= 'selected="selected"';
                $html.= '>sadness</option><option value="surprise"';
                if($data_mention['emotion'] == "surprise")
                $html.= 'selected="selected"';
                $html.= '>surprise</option><option value="trust"';
                if($data_mention['emotion'] == "trust")
                $html.= 'selected="selected"';
                $html.= '>trust</option><option value="NA"';
                if($data_mention['emotion'] == "NA")
                $html.= 'selected="selected"';
                $html.= '>NA</option></select>';
               if($permission_data['edit'] === 1)          
                $html.= ' <a class="edit_post_predict" id="'.$data_mention['id'].'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a></span></p>';
                                                    
                                          
                 $html.= '
                                    <p class="m-t-10">'.$data_mention['message'].'...<a href="'.$data_mention['link'].'" target="_blank"> Read More
                                      </a></p></div></div>
                                       <p>👍 '.$this->thousandsCurrencyFormat($data_mention['liked']).' ❤️ '.$this->thousandsCurrencyFormat($data_mention['loved']).' 😆 '.$this->thousandsCurrencyFormat($data_mention['haha']).' 😮 '.$this->thousandsCurrencyFormat($data_mention['wow']).' 😪 '.$this->thousandsCurrencyFormat($data_mention['sad']).' 😡 '.$this->thousandsCurrencyFormat($data_mention['angry']).'</p>
                                      </div></div>';
            if($data_mention['positive']>0 || $data_mention['negative']>0 || $data_mention['anger']>0  || $data_mention['interest']>0 || $data_mention['disgust']>0 || $data_mention['fear']>0 || $data_mention['joy']>0 || $data_mention['like']>0 || $data_mention['love']>0 || $data_mention['neutral']>0 || $data_mention['sadness']>0 || $data_mention['surprise']>0|| $data_mention['trust']>0){                                   
                 $html.= '<div><div style="margin:0 0 0 70px;"> <form>
                  <fieldset>
                      <legend>Customer Feedback</legend>';
                    }
             if($data_mention['positive']>0){ //your condition
           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#dd4b39" class="popup" id="'.$data_mention['id'].'" name="Positive">positive : </a> 
                  <span class="label label-light-danger">'.$data_mention['positive'].'</span> ';
                  
         } 
             if($data_mention['negative']>0){ //your condition
           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#dd4b39" class="popup" id="'.$data_mention['id'].'" name="Negative">negative : </a>
                <span class="label label-light-danger" style="font-size:15px">'.$data_mention['negative'].'</span> ';
                  
         } 
        
                if($data_mention['anger']>0){  
          $html.='<a href="javascript:void(0)" style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="anger">anger : </a> 
                  <span class="label label-light-danger">'.$data_mention['anger'].'</span> ';                 
               }
                if($data_mention['interest']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="interest">interest : </a> 
                  <span class="label label-light-danger">'.$data_mention['interest'].'</span> ';                 
               }
                if($data_mention['disgust']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="disgust">disgust : </a> 
                  <span class="label label-light-danger">'.$data_mention['disgust'].'</span> ';                 
               }
                if($data_mention['fear']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="fear">fear : </a> 
                  <span class="label label-light-danger">'.$data_mention['fear'].'</span> ';                 
               }
                if($data_mention['joy']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="joy">joy : </a> 
                  <span class="label label-light-danger">'.$data_mention['joy'].'</span> ';                 
               }
                if($data_mention['like']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="like">like : </a> 
                  <span class="label label-light-danger">'.$data_mention['like'].'</span> ';                 
               }
                if($data_mention['love']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="love">love : </a> 
                  <span class="label label-light-danger">'.$data_mention['love'].'</span> ';                 
               }
                if($data_mention['neutral']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="neutral">neutral : </a> 
                  <span class="label label-light-danger">'.$data_mention['neutral'].'</span> ';                 
               }
                if($data_mention['sadness']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="sadness">sadness : </a> 
                  <span class="label label-light-danger">'.$data_mention['sadness'].'</span> ';                 
               }
                if($data_mention['surprise']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="surprise">surprise : </a> 
                  <span class="label label-light-danger">'.$data_mention['surprise'].'</span> ';                 
               }
                  if($data_mention['trust']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="trust">trust : </a> 
                  <span class="label label-light-danger">'.$data_mention['trust'].'</span> </fieldset>
</form></div></div>';                 
               }
                return $html;
               

            })
        
      ->rawColumns(['post_div','action'])
   
      ->make(true);

          //  echo json_encode($data);
    }

     public function getsentimentcomment()
    {
       $query="";
       $brand_id=Input::get('brand_id');
       $period=Input::get('period');
       $period_type='';

       if(null !== Input::get('type'))
       {
        $type=Input::get('type');
        $type = (strtolower($type) == 'positive')?'pos':'neg'; 

       }
       
       $filter_keyword ='';
       $filter_sentiment ='';

       if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword');

       if(null !== Input::get('sentiment'))
       $filter_sentiment=Input::get('sentiment'); 

       $from_graph=Input::get('from_graph');

       if(null !== Input::get('emotion_type'))
       $emotion_type=Input::get('emotion_type');

       if(null !== Input::get('highlight_text'))
       $highlight_text= str_replace(' ', '', Input::get('highlight_text'));

       $format_type = '';//for date format

       /*$brand_id = 44;
       $period = '2018-08';
       $emotion_type = 'anger';
       $from_graph='emotion';
       $format_type ='%Y-%m';*/
     
       
        if(null !== Input::get('period'))
        {
        $char_count = substr_count($period,"-");
        
        if($char_count == 1)
        {
            $format_type ='%Y-%m';
        }
        else if($char_count == 2)
        {
             $format_type ='%Y-%m-%d';
        }   
        else if($char_count > 2)
        {
            /*this is week*/
            $pieces = explode(" - ", $period);
            $format_type ='%Y-%m-%d';
            $period_type ='week';
        }  
        }
        

     

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 05')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 05')));*/
$additional_where="";
if($period_type == 'week')
{
 $additional_where = " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

}
else
{
    $additional_where = " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
}
 $add_con="";
 $add_comment_con="";
if($filter_keyword !== '')
{
  $add_con = " and posts.message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}

if($filter_sentiment !== '')
{
  $add_con .= " and posts.sentiment = '".$filter_sentiment."'";
  $add_comment_con .= " and cmts.sentiment  = '".$filter_sentiment."'";
}

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
  $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}

if($from_graph == 'sentiment')
{
$query="SELECT cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%Y-%m-%d %h:%i:%s %p') created_time, ".
" cmts.sentiment,cmts.emotion,(CASE WHEN cmts.isBookMark = 0 THEN 'mdi-star-outline' ELSE 'mdi-star' END) isBookMark ".
" FROM temp_".$brand_id."_comments cmts LEFT JOIN  temp_".$brand_id."_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NULL".
" AND cmts.sentiment ='".$type."' AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_inbound_con .
$additional_where .  $add_comment_con ." ORDER by DATE_FORMAT(cmts.created_time, '%Y-%m-%d %h:%i:%s %p')";
}
else if($from_graph == 'emotion')
{
  $query="SELECT cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%Y-%m-%d %h:%i:%s %p') created_time, ".
" cmts.sentiment,cmts.emotion,(CASE WHEN cmts.isBookMark = 0 THEN 'mdi-star-outline' ELSE 'mdi-star' END) isBookMark ".
" FROM temp_".$brand_id."_comments cmts LEFT JOIN  temp_".$brand_id."_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NULL".
" AND cmts.emotion ='".$emotion_type."' AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_inbound_con .
$additional_where .  $add_comment_con. " ORDER by DATE_FORMAT(cmts.created_time, '%Y-%m-%d %h:%i:%s %p')";
}

else if($from_graph == 'highlighted')
{
  $query="SELECT cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%Y-%m-%d %h:%i:%s %p') created_time, ".
" cmts.sentiment,cmts.emotion,(CASE WHEN cmts.isBookMark = 0 THEN 'mdi-star-outline' ELSE 'mdi-star' END) isBookMark ".
" FROM temp_".$brand_id."_comments cmts LEFT JOIN  temp_".$brand_id."_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NULL".
" AND ( REPLACE(REPLACE(REPLACE(cmts.message, '\r', ''), '\n', ''),' ','')) Like '%".$highlight_text."%' AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_inbound_con.
$additional_where .  $add_comment_con . " ORDER by DATE_FORMAT(cmts.created_time, '%Y-%m-%d %h:%i:%s %p')";
}

/*echo $query;
return;*/
$data=[];
if($query !== "")
{
  $data = DB::select($query);
}

/*     
 print_r($data);
 return;*/
$permission_data = $this->getPermission(); 
 return Datatables::of($data)
       ->addColumn('action', function ($data) {
                return '<a href="javascript:void(0)" id="'.$data->id.'" name="'.$data->isBookMark.'" class="mdi '.$data->isBookMark.' text-yellow btn-bookmark-comment" style="float:right;color:#f39c12"></a>';
            }) 

       ->addColumn('post_div', function ($data) use($permission_data) {
                 $html= '<div class="profiletimeline"><div class="sl-item">
                                    <div class="sl-left user-img"> <span class="round">A</span> </div> 
                                    <div class="sl-right"> <div><a href="javascript:void(0)" class="link">Comment</a>
              <span class="sl-date">'.$data->created_time.'</span>

              <p>Sentiment : <select class="form-control custom-select sentiment-color customize-select" id="sentiment_'.$data->id.'"  >';
                 if($data->sentiment == "")
                $html.= '<option value="" selected="selected"></option>';
                $html.='<option value="pos"';
                if($data->sentiment == "pos")
                $html.= 'selected="selected"';
                $html.= '>pos</option><option value="neg"';
                if($data->sentiment == "neg")
                $html.= 'selected="selected"';
                $html.= '>neg</option><option value="neutral"';
                if($data->sentiment == "neutral")
                $html.= 'selected="selected"';     
                $html.= '>neutral</option><option value="NA"';
                if($data->sentiment == "NA")
                $html.= 'selected="selected"';  
                $html.= '>NA</option></select>';

                $html.= ' | Emotion : <select class="form-control custom-select emotion-color customize-select" id="emotion_'.$data->id.'" >';
                if($data->emotion == "")
                $html.= '<option value="" selected="selected"></option>';
                $html.='<option value="anger"';
                if($data->emotion == "anger")
                $html.= 'selected="selected"';
                $html.= '>anger</option><option value="interest"';
                if($data->emotion == "interest")
                $html.= 'selected="selected"';     
                $html.= '>interest</option><option value="disgust"';
                if($data->emotion == "disgust")
                $html.= 'selected="selected"';  
                $html.= '>disgust</option><option value="fear"';
                if($data->emotion == "fear")
                $html.= 'selected="selected"'; 
                $html.= '>fear</option><option value="joy"';
                if($data->emotion == "joy")
                $html.= 'selected="selected"'; 
                $html.= '>joy</option><option value="like"';
                if($data->emotion == "like")
                $html.= 'selected="selected"'; 
                $html.= '>like</option><option value="love"';
                if($data->emotion == "love")
                $html.= 'selected="selected"'; 
                $html.= '>love</option><option value="neutral"';
                if($data->emotion == "neutral")
                $html.= 'selected="selected"';
                $html.= '>neutral</option><option value="sadness"';
                if($data->emotion == "sadness")
                $html.= 'selected="selected"';
                $html.= '>sadness</option><option value="surprise"';
                if($data->emotion == "surprise")
                $html.= 'selected="selected"';
                $html.= '>surprise</option><option value="trust"';
                if($data->emotion == "trust")
                $html.= 'selected="selected"';
                $html.= '>trust</option><option value="NA"';
                if($data->emotion == "NA")
                $html.= 'selected="selected"';
                $html.= '>NA</option></select>';
                if($permission_data['edit'] === 1)
                $html.= ' <a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>';
              $html.='</span></p>

                                    <p class="m-t-10">'.$data->message.'</p></div></div></div></div>';
                                    return $html;

            })
        
      ->rawColumns(['post_div','action'])
   
      ->make(true);

          //  echo json_encode($data);
    }

       function readmore($string){
                         $string = strip_tags($string);
                        if (strlen($string) > 500) {

                            // truncate string
                            $stringCut = substr($string, 0, 500);
                            $endPoint = strrpos($stringCut, ' ');

                            //if the string doesn't contain any space then it will cut without word basis.
                            $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                    
                        }
                        return $string;


        }

       
public function remove_keyword()
{
          $pid= Input::get('id');
          $keyword_remove= Input::get('keyword_remove');
      
              // dd($pages);
            $result =   keywords_removed::create([
                'brand_id'=>$pid,
                'keyword' => $keyword_remove,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString()

              ]);
              return $result ;
 
        /* $title="Dashboard";
         
          $project_data = $this->getProject();
          $project_data_id = $this->getProjectByid($pid);
          if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $count = $this->getProjectCount($project_data);
          return
          }*/
}

//For Inboound Comment
public function getInboundPostPoint()
    {

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

}
     
       $brand_id=Input::get('brand_id');
       $from_graph=Input::get('from_graph');

       $filter_keyword ='';
       $filter_sentiment='';

       if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword');

       if(null !== Input::get('sentiment'))
       $filter_sentiment=Input::get('sentiment'); 

       if(null !== Input::get('type'))
       {
       $type=Input::get('type');
       $type = (strtolower($type) == 'positive')?'pos':'neg';
       } 
       $period_type = '';
       if(null !== Input::get('category'))
       $category=Input::get('category');//page category for example :beauty,news

       if(null !== Input::get('emotion_type'))
       $emotion_type=Input::get('emotion_type');//example: like,anger,interest

       if(null !== Input::get('reaction_type'))
       $reaction_type=Input::get('reaction_type');


       if(null !== Input::get('highlight_text'))
       $highlight_text= str_replace(' ', '', Input::get('highlight_text'));
  $additional_where="";
      
        if(null !==Input::get('period'))
        {
       $period=Input::get('period');
        $char_count = substr_count($period,"-");
        $format_type = '';
        if($char_count == 1)
        {
            $format_type ='%Y-%m';
        }
        else if($char_count == 2)
        {
             $format_type ='%Y-%m-%d';
        }   
        else if($char_count > 2)
        {
            /*this is week*/
            $pieces = explode(" - ", $period);
            $format_type ='%Y-%m-%d';
            $period_type ='week';
        }

      
if($period_type == 'week')
{
 $additional_where = " AND (DATE(posts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

}
else
{
    $additional_where = " AND DATE_FORMAT(posts.created_time, '".$format_type."') = '".$period."'";
}

       }
 $add_con="";
 $add_comment_con="";
if($filter_keyword !== '')
{
  $add_con = " and posts.message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}

if($filter_sentiment !== '')
{
  $add_con .= " and posts.sentiment = '".$filter_sentiment."'";
  $add_comment_con .= " and cmts.checked_sentiment  = '".$filter_sentiment."'";
}

             if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

              $today = date('d-m-Y');//23-02-2015
               $today_plus=date('Y-m-d', strtotime('+1 day', strtotime($today)));
               $date_Week_Begin=date('Y-m-d', strtotime('-1 week', strtotime($today_plus)));
               $date_End=date('Y-m-d',strtotime($today));

               $date_Month_Begin=date('Y-m-d', strtotime('-1 month', strtotime($today_plus)));
        
       /*$brand_id = 47;
       $period = '2018-07';
       $type = 'pos';
       $category ='education';
       $highlight_text ='ဈခတျေမီသညျ့ကိုအသုံးပွုထားခွငျး';
       $from_graph='highlighted';*/
       

/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2013 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/

$react_con='';
$add_order='';
if( $from_graph == 'reaction')
{
  
  if($reaction_type <> '')
 {

  if(strtolower($reaction_type)==='like' )
  {
   $add_order = " Liked DESC,";
   $react_con = " AND posts.Liked > 0 ";
  } 

  else if(strtolower($reaction_type)==='ha ha' )
  {
   
   $add_order = " haha DESC,";
   $react_con =" AND posts.haha>0 ";
  }
  else
  {
    $add_order = $reaction_type . " DESC,";
    $react_con =" AND posts.".$reaction_type.">0 ";
  }
 }

$query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.checked_sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.checked_predict, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id ".
" WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ".$filter_pages . $add_con . $react_con . $additional_where . 
" ORDER by ". $add_order . " timestamp(posts.created_time) DESC " ;

// echo $query ;
// return;

}

//overall post
else if( $from_graph == 'today')
{
  
$query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.checked_sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.checked_predict, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id ".
" WHERE (DATE(posts.created_time) BETWEEN '".$date_End."' AND '".$date_End."') AND ". $filter_pages . 
"  ORDER by ". $add_order . " timestamp(posts.created_time) DESC " ;

}


//overall post
else if( $from_graph == 'weekly')
{
  
$query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.checked_sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.checked_predict, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id ".
" WHERE (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') AND ". $filter_pages . 
" ORDER by ". $add_order . " timestamp(posts.created_time) DESC " ;

}




$query_result = DB::select($query);
     
 /*print_r($query_result);
 return;*/
 $data = [];
 $data_mention=[];
 foreach ($query_result as  $key => $row) {
   // $pos =($row->cmt_sentiment=='pos' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0 ;
   // $neg=($row->cmt_sentiment=='neg' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0;
   // $anger =($row->cmt_emotion=='anger' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0 ;
   // $interest=($row->cmt_emotion=='interest'  && $row->parent === '' && (int)$row->checked_predict === 1)?1:0;
   // $disgust=($row->cmt_emotion=='disgust' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0;
   // $fear=($row->cmt_emotion=='fear' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0;
   // $joy=($row->cmt_emotion=='joy' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0;
   // $like=($row->cmt_emotion=='like' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0;
   // $love=($row->cmt_emotion=='love' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0;
   // $neutral=($row->cmt_emotion=='neutral' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0;
   // $sadness=($row->cmt_emotion=='sadness' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0;
   // $surprise=($row->cmt_emotion=='surprise' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0;
   // $trust=($row->cmt_emotion=='trust' && $row->parent === '' && (int)$row->checked_predict === 1)?1:0;
     $pos =($row->cmt_sentiment=='pos' && $row->parent === '' )?1:0 ;
   $neg=($row->cmt_sentiment=='neg' && $row->parent === '' )?1:0;
   $anger =($row->cmt_emotion=='anger' && $row->parent === '')?1:0 ;
   $interest=($row->cmt_emotion=='interest'  && $row->parent === '' )?1:0;
   $disgust=($row->cmt_emotion=='disgust' && $row->parent === '')?1:0;
   $fear=($row->cmt_emotion=='fear' && $row->parent === '' )?1:0;
   $joy=($row->cmt_emotion=='joy' && $row->parent === '' )?1:0;
   $like=($row->cmt_emotion=='like' && $row->parent === '' )?1:0;
   $love=($row->cmt_emotion=='love' && $row->parent === '' )?1:0;
   $neutral=($row->cmt_emotion=='neutral' && $row->parent === '' )?1:0;
   $sadness=($row->cmt_emotion=='sadness' && $row->parent === '' )?1:0;
   $surprise=($row->cmt_emotion=='surprise' && $row->parent === '' )?1:0;
   $trust=($row->cmt_emotion=='trust' && $row->parent === '' )?1:0;
   $message=$this->readmore($row->message);
            if (isset($row ->id))
   {
                 $id= $row ->id;
   
    if (!array_key_exists($id , $data)) {
    $data[$id] = array(
                'id'     =>$row->id,
                'message' => $message,
                'page_name' =>$row->page_name,
                'created_time' =>$row->created_time,
                'link' =>$row->link,
                'full_picture' =>$row->full_picture,
                'sentiment' =>$row->sentiment,
                'emotion' =>$row->emotion,
                'positive'=> $pos,'negative'=>$neg,
                'liked'=>$row->Liked ,'loved'=>$row->Love,'haha'=>$row->Haha,'wow'=>$row->Wow,'sad'=>$row->Sad,'angry'=>$row->Angry,
                'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                'isBookMark'=>$row->isBookMark
            );
}
else
{
 $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
 $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
 $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
 $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
 $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
 $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
 $data[$id]['like'] = (int) $data[$id]['like'] + $like;
 $data[$id]['love'] = (int) $data[$id]['love'] + $love;
 $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
 $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
 $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
 $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;



}
//echo $data[$id]['negative'];echo $neg;return;

}
 
            }

         
         // for dashboard overallpositive and negative
            if($from_graph === 'today' || $from_graph === 'weekly')
            {
              if($type === 'pos')
              {     foreach ($data as $key => $value) {

                       if( ! ((int) $value['positive'] > (int) $value['negative']))
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
                }
              else
              {
                          foreach ($data as $key => $value) {

                       if( ! ((int) $value['negative'] > (int) $value['positive']))
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
              }

            }
 
           
            $data_mention=$data;


/*$id='799069593563429_1007801336023586';
echo $data[$id]['negative'];
 print_r($data_mention);
 return;
 $data_mention = $this->unique_multidim_array($data_mention,'id'); */
$permission_data = $this->getPermission(); 
/*print_r($data_mention);
return;*/
 return Datatables::of($data_mention)
       ->addColumn('action', function ($data_mention) {
                return '<a href="javascript:void(0)" id="'.$data_mention['id'].'" name="'.$data_mention['isBookMark'].'" class="mdi '.$data_mention['isBookMark'].' text-yellow btn-bookmark" style="float:right;color:#f39c12"></a>';
            }) 

       ->addColumn('post_div', function ($data_mention)  use($permission_data){
               $html= '<div class="profiletimeline"><div class="sl-item">
                                    <div class="sl-left"> <img src="'.$data_mention['full_picture'].'" alt="user" class="img-circle  img-bordered-sm"> </div> 
                                    <div class="sl-right"> <div><a href="'.$data_mention['link'].'"  class="link"  target="_blank">'.$data_mention['page_name'].'</a>
              <span class="sl-date">'.$data_mention['created_time'].'</span>';

              if($data_mention['positive'] >  $data_mention['negative'])
              {
                $html.=' | Overall Sentiment <span class="label label-info">Positive</span>';
              }
              else
              {
                $html.=' | Overall Sentiment <span class="label label-danger">Negative</span>';
              }
                                                    
                                          
                 $html.= '
                                    <p class="m-t-10">'.$data_mention['message'].'...<a href="'.$data_mention['link'].'" target="_blank"> Read More
                                      </a></p></div></div>
                                       <p>👍 '.$this->thousandsCurrencyFormat($data_mention['liked']).' ❤️ '.$this->thousandsCurrencyFormat($data_mention['loved']).' 😆 '.$this->thousandsCurrencyFormat($data_mention['haha']).' 😮 '.$this->thousandsCurrencyFormat($data_mention['wow']).' 😪 '.$this->thousandsCurrencyFormat($data_mention['sad']).' 😡 '.$this->thousandsCurrencyFormat($data_mention['angry']).'</p>
                                      </div></div>';
            if($data_mention['positive']>0 || $data_mention['negative']>0 || $data_mention['anger']>0  || $data_mention['interest']>0 || $data_mention['disgust']>0 || $data_mention['fear']>0 || $data_mention['joy']>0 || $data_mention['like']>0 || $data_mention['love']>0 || $data_mention['neutral']>0 || $data_mention['sadness']>0 || $data_mention['surprise']>0|| $data_mention['trust']>0){                                   
                 $html.= '<div><div style="margin:0 0 0 70px;"> <form>
                  <fieldset>
                      <legend>Customer Feedback</legend>';
                    }
             if($data_mention['positive']>0){ //your condition
           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#dd4b39" class="popup" id="'.$data_mention['id'].'" name="Positive">positive : </a> 
                  <span class="label label-light-danger">'.$data_mention['positive'].'</span> ';
                  
         } 
             if($data_mention['negative']>0){ //your condition
           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#dd4b39" class="popup" id="'.$data_mention['id'].'" name="Negative">negative : </a>
                <span class="label label-light-danger" style="font-size:15px">'.$data_mention['negative'].'</span> ';
                  
         } 
        
//                 if($data_mention['anger']>0){  
//           $html.='<a href="javascript:void(0)" style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="anger">anger : </a> 
//                   <span class="label label-light-danger">'.$data_mention['anger'].'</span> ';                 
//                }
//                 if($data_mention['interest']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="interest">interest : </a> 
//                   <span class="label label-light-danger">'.$data_mention['interest'].'</span> ';                 
//                }
//                 if($data_mention['disgust']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="disgust">disgust : </a> 
//                   <span class="label label-light-danger">'.$data_mention['disgust'].'</span> ';                 
//                }
//                 if($data_mention['fear']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="fear">fear : </a> 
//                   <span class="label label-light-danger">'.$data_mention['fear'].'</span> ';                 
//                }
//                 if($data_mention['joy']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="joy">joy : </a> 
//                   <span class="label label-light-danger">'.$data_mention['joy'].'</span> ';                 
//                }
//                 if($data_mention['like']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="like">like : </a> 
//                   <span class="label label-light-danger">'.$data_mention['like'].'</span> ';                 
//                }
//                 if($data_mention['love']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="love">love : </a> 
//                   <span class="label label-light-danger">'.$data_mention['love'].'</span> ';                 
//                }
//                 if($data_mention['neutral']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="neutral">neutral : </a> 
//                   <span class="label label-light-danger">'.$data_mention['neutral'].'</span> ';                 
//                }
//                 if($data_mention['sadness']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="sadness">sadness : </a> 
//                   <span class="label label-light-danger">'.$data_mention['sadness'].'</span> ';                 
//                }
//                 if($data_mention['surprise']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="surprise">surprise : </a> 
//                   <span class="label label-light-danger">'.$data_mention['surprise'].'</span> ';                 
//                }
//                   if($data_mention['trust']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="trust">trust : </a> 
//                   <span class="label label-light-danger">'.$data_mention['trust'].'</span> </fieldset>
// </form></div></div>';                 
//                }
                return $html;
               

            })
        
      ->rawColumns(['post_div','action'])
   
      ->make(true);

          //  echo json_encode($data);
    }
public function getInboundPointOut()
    {

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

}
     
       $brand_id=Input::get('brand_id');
       $from_graph=Input::get('from_graph');

       $filter_keyword ='';
       $filter_sentiment='';

       if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword');

       if(null !== Input::get('sentiment'))
       $filter_sentiment=Input::get('sentiment'); 

       if(null !== Input::get('type'))
       {
         $type=Input::get('type');
       // $type = (strtolower($type) == 'positive')?'pos':'neg';
       if(strtolower($type) == 'positive')
        $type = 'pos';
       if(strtolower($type) == 'negative')
        $type = 'neg';
       if(strtolower($type) == 'neutral')
        $type = 'neutral';
       if(strtolower($type) == 'interest')
        $type = 1;
       } 
       $period_type = '';
    
       if(null !== Input::get('emotion_type'))
       $emotion_type=Input::get('emotion_type');//example: like,anger,interest

       if(null !== Input::get('highlight_text'))
       $highlight_text= str_replace(' ', '', Input::get('highlight_text'));
     if(null !== Input::get('category'))
       $category=Input::get('category');//example: like,anger,interest


         $additional_where="";
        if(null !==Input::get('period'))
        {
       $period=Input::get('period');
        $char_count = substr_count($period,"-");
        $format_type = '';
        if($char_count == 1)
        {
            $format_type ='%Y-%m';
        }
        else if($char_count == 2)
        {
             $format_type ='%Y-%m-%d';
        }   
        else if($char_count > 2)
        {
            /*this is week*/
            $pieces = explode(" - ", $period);
            $format_type ='%Y-%m-%d';
            $period_type ='week';
        }

     
if($period_type == 'week')
{
 $additional_where = " AND (DATE(cmts.created_time) BETWEEN '".$pieces[0]."' AND '".$pieces[1]."') ";    

}
else
{
    $additional_where = " AND DATE_FORMAT(cmts.created_time, '".$format_type."') = '".$period."'";
}

       }
 $add_con="";
 $add_comment_con="";
if($filter_keyword !== '')
{
  $add_con = " and cmts.message Like '%".$filter_keyword."%'";

}

if($filter_sentiment !== '')
{
  $add_con .= " and cmts.checked_sentiment = '".$filter_sentiment."'";

}
        if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }
       
              $today = date('d-m-Y');//23-02-2015
             $today_plus=date('Y-m-d', strtotime('+1 day', strtotime($today)));
             $date_Week_Begin=date('Y-m-d', strtotime('-1 week', strtotime($today_plus)));
             $date_End=date('Y-m-d',strtotime($today));

             $date_Month_Begin=date('Y-m-d', strtotime('-1 month', strtotime($today_plus)));

/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2013 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/
 $tag_con='';
 if(null !== Input::get('tag'))
  $tag_con=" AND tags Like '%".Input::get('tag')."%'";//example: like,anger,interest
//dd($tag_con);
   if( $from_graph == 'sentiment')
       {
$query = "SELECT  cmts.tags,cmts.post_id,cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
" cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp  ".
"FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN  temp_".$brand_id."_inbound_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  cmts.parent='' ".
" AND cmts.checked_sentiment='".$type."' ".$tag_con."   AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ".$filter_pages . $additional_where . $add_con . 
" ORDER by timestamp(cmts.created_time) DESC";
}

 else  if( $from_graph == 'category')
       {
$query = "SELECT  cmts.tags,cmts.post_id,cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
" cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp  ".
"FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN  temp_".$brand_id."_inbound_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NOT NULL  AND cmts.parent='' ".
" AND cmts.checked_sentiment='".$type."' and page_name='".$category."'   AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ".$filter_pages . $additional_where . $add_con . 
" ORDER by timestamp(cmts.created_time) DESC";
}


 else  if( $from_graph == 'enquiry')
       {
$query = "SELECT  cmts.tags,cmts.post_id,cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
" cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,cmts.interest,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp  ".
"FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN  temp_".$brand_id."_inbound_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND cmts.parent='' ".
" AND cmts.interest=".$type."   AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ".$filter_pages . $additional_where . $add_con . 
" ORDER by timestamp(cmts.created_time) DESC";

}

else  if( $from_graph == 'emotion')
{
$query = "SELECT  cmts.tags,cmts.post_id,cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
" cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp  ".
"FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN  temp_".$brand_id."_inbound_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  cmts.parent='' ".
" AND cmts.emotion='".$emotion_type."'   AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ".$filter_pages . $additional_where . $add_con . 
" ORDER by timestamp(cmts.created_time) DESC";

}


else  if( $from_graph == 'highlighted')
{

/*  $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2013 05 01')));
  $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/
$query = "SELECT  cmts.tags,cmts.post_id,cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
" cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp  ".
"FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN  temp_".$brand_id."_inbound_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NOT NULL  AND cmts.parent='' ".
" AND ( REPLACE(REPLACE(REPLACE(cmts.message, '\r', ''), '\n', ''),' ','')) Like '%".$highlight_text."%'   AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ".$filter_pages .  $add_con .
" ORDER by timestamp(cmts.created_time) DESC";

}


else if( $from_graph == 'today')
       {
$query = "SELECT  cmts.tags,cmts.post_id,cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
" cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp  ".
"FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN  temp_".$brand_id."_inbound_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NOT NULL  AND cmts.parent='' ".
" AND  (DATE(cmts.created_time) BETWEEN '".$date_End."' AND '".$date_End."') AND ".$filter_pages .
" AND cmts.checked_sentiment='".$type."' ORDER by timestamp(cmts.created_time) DESC";
}

else if( $from_graph == 'weekly')
       {
$query = "SELECT  cmts.tags,cmts.post_id,cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
" cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp  ".
"FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN  temp_".$brand_id."_inbound_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  cmts.parent='' ".
" AND (DATE(cmts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') AND ".$filter_pages. 
" AND cmts.checked_sentiment='".$type."' ORDER by timestamp(cmts.created_time) DESC";
}


/*echo $query;
return;*/
$data = DB::select($query);
 $tags =$this->gettags();   
 $permission_data = $this->getPermission(); 
 return Datatables::of($data)
       ->addColumn('action', function ($data)  {
      return '<button type="button" class="btn waves-effect waves-light btn-primary popup_post" id="'.$data->post_id.'" name="'.$data->page_name.'" >See Post</button>';
            })

     
       ->addColumn('post_div', function ($data)   use($tags,$permission_data) {
        $classforbtn='';$stylefora='';$bgcolor='';
              if($data->sentiment=='neg')
                {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
              else if($data->sentiment=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}
                $html = '<div class="profiletimeline"><div class="sl-item">
                                    <div class="sl-left user-img"> <span class="round">A</span> </div> 
                                    <div class="sl-right"> <div><a href="javascript:void(0)" class="link">'.$data->page_name.'</a>
              <span class="sl-date">'.$data->created_time.'</span>
<p>Sentiment : <select class="form-control custom-select sentiment-color in-comment-select edit_senti" id="sentiment_'.$data->id.'"  >';
                if($data->sentiment == "")
                $html.= '<option value="" selected="selected"></option>';
                $html.='<option value="pos"';
                if($data->sentiment == "pos")
                $html.= 'selected="selected"';
                $html.= '>pos</option><option value="neg"';
                if($data->sentiment == "neg")
                $html.= 'selected="selected"';
                $html.= '>neg</option><option value="neutral"';
                if($data->sentiment == "neutral")
                $html.= 'selected="selected"';      
                $html.= '>neutral</option><option value="NA"';
                if($data->sentiment == "NA")
                $html.= 'selected="selected"';  
                $html.= '>NA</option></select>';

                // $html.= ' | Emotion : <select class="form-control custom-select emotion-color in-comment-select" id="emotion_'.$data->id.'" >';
                // if($data->emotion == "")
                // $html.= '<option value="" selected="selected"></option>';
                // $html.='<option value="anger"';
                // if($data->emotion == "anger")
                // $html.= 'selected="selected"';
                // $html.= '>anger</option><option value="interest"';
                // if($data->emotion == "interest")
                // $html.= 'selected="selected"';     
                // $html.= '>interest</option><option value="disgust"';
                // if($data->emotion == "disgust")
                // $html.= 'selected="selected"';  
                // $html.= '>disgust</option><option value="fear"';
                // if($data->emotion == "fear")
                // $html.= 'selected="selected"'; 
                // $html.= '>fear</option><option value="joy"';
                // if($data->emotion == "joy")
                // $html.= 'selected="selected"'; 
                // $html.= '>joy</option><option value="like"';
                // if($data->emotion == "like")
                // $html.= 'selected="selected"'; 
                // $html.= '>like</option><option value="love"';
                // if($data->emotion == "love")
                // $html.= 'selected="selected"'; 
                // $html.= '>love</option><option value="neutral"';
                // if($data->emotion == "neutral")
                // $html.= 'selected="selected"';
                // $html.= '>neutral</option><option value="sadness"';
                // if($data->emotion == "sadness")
                // $html.= 'selected="selected"';
                // $html.= '>sadness</option><option value="surprise"';
                // if($data->emotion == "surprise")
                // $html.= 'selected="selected"';
                // $html.= '>surprise</option><option value="trust"';
                // if($data->emotion == "trust")
                // $html.= 'selected="selected"';
                // $html.= '>trust</option><option value="NA"';
                // if($data->emotion == "NA")
                // $html.= 'selected="selected"';
                // $html.= '>NA</option></select>';
                $html.=' | Tag :<select id="tags_'.$data->id.'" class="form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                        style="width:300px;height:36px">';
                 $tag_array=explode(',', $data->tags);
               foreach ($tags as $key => $value) {
               # code...
                 //if (strpos($data->tags, $value->name) !== false)
                if ( in_array( $value->name,$tag_array))
                $html.= '<option value="'.$value->name.'" selected="selected">'.$value->name.'</option>';
                 else
                $html.= '<option value="'.$value->name.'">'.$value->name.'</option>';
               }
                if($permission_data['edit'] === 1)
                $html.= ' </select> <a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"><i class="ti-pencil-alt" style="display:none"></i></a>';
               if($data->hp <> '')

                $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" style="font-size:25px" aria-expanded="true"><i class="mdi mdi-account-edit" title="Human Predict" data-toggle="tooltip"></i></a>';
                $html.='</p><p class="m-t-10">'.$data->message.'</p></div></div></div></div>';

                return $html;

            })
        
      ->rawColumns(['post_div','action'])
   
      ->make(true);


          //  echo json_encode($data);
    }
public function thousandsCurrencyFormat($num) {

  if($num>1000) {

        $x = round($num);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('k', 'm', 'b', 't');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];

        return $x_display;

  }
 return $num;
}
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
       
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
