<?php

namespace App\Http\Controllers;

use App\ProjectKeyword;
use Illuminate\Http\Request;

class ProjectKeywordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectKeyword  $projectKeyword
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectKeyword $projectKeyword)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectKeyword  $projectKeyword
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectKeyword $projectKeyword)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectKeyword  $projectKeyword
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectKeyword $projectKeyword)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectKeyword  $projectKeyword
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectKeyword $projectKeyword)
    {
        //
    }
}
