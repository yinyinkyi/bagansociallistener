<?php

namespace App\Http\Controllers;

use App\MongodbData;
use App\MongoPages;
use App\MongoFollowers;
use App\Comment;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;
class mentionDataController extends Controller
{
   use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
public function getmentiondetail()
    {
    
    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
     /* $brand_id=81;*/
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 
      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 
/*
      $brand_id=30;
     $groupType='month';*/
     

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/

 // $groupType="month";
 $dateformat ="";
 $add_con="";
 $add_comment_con="";

if($groupType=="month")
{
$dateformat ="%Y-%m"; 
 }

   else 
 {
$dateformat ="%Y-%m-%d";                
              

 }
//$format =" DATE_FORMAT(created_time, '%Y-%m-%d')";                
              

if($filter_keyword !== '')
{
  $add_con = " and message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}

if($filter_sentiment !== '')
{
  $add_con .= " and sentiment = '".$filter_sentiment."'";
  $add_comment_con .= " and cmts.sentiment  = '".$filter_sentiment."'";
}

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
  $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}

$query = "select count(*) count,created_time  from (SELECT DATE_FORMAT(created_time,'".$dateformat."') created_time,page_name FROM temp_".$brand_id."_posts  WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_con .$add_inbound_con." 
UNION ALL
SELECT DATE_FORMAT(cmts.created_time,'".$dateformat."') created_time,page_name FROM temp_".$brand_id."_comments cmts ".
"  LEFT JOIN  temp_".$brand_id."_posts  posts on posts.id=cmts.post_id ".
"WHERE posts.id IS NULL and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_comment_con .$add_inbound_con."  ) t1 Group By created_time ORDER BY created_time ";

/*echo $query;
return;*/
$query_result = DB::select($query);

 
   $data_mention = [];



    foreach ($query_result as  $key => $row) {
          
                $request['periods'] =$row ->created_time;
                $request['mention'] =$row ->count;
                $request['periodLabel'] ="";
              
                
               
 if($groupType == "week")
{
    $weekperiod=$this ->rangeWeek ($request['periods']);
    $request['periodLabel'] =$weekperiod['start'] . " - " . $weekperiod['end'] ;
    $periodLabel = $request['periodLabel'];
    if (!array_key_exists($periodLabel , $data_mention)) {
    $data_mention[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'mention' =>   $request['mention'],
                'periods' =>  $request['periods'],
            );
}
else
{
 $data_mention[$periodLabel]['mention'] = (int) $data_mention[$periodLabel]['mention'] +  $request["mention"];
 $data_mention[$periodLabel]['periods'] = $request['periods'];
}

}
 else
{
 $request['periodLabel'] =$request['periods'] ;
    $periodLabel = $request['periodLabel'];
    if (!array_key_exists($periodLabel , $data_mention)) {
    $data_mention[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'mention' =>   $request['mention'],
                'periods' =>  $request['periods'],
            );
}
else
{
 $data_mention[$periodLabel]['mention'] = (int) $data_mention[$periodLabel]['mention'] +  $request["mention"];
 $data_mention[$periodLabel]['periods'] = $request['periods'];
}
     
}
   }
$data_mention = $this->unique_multidim_array($data_mention,'periodLabel'); 
   usort($data_mention, function($a1, $a2) {
   $value1 = strtotime($a1['periods']);
   $value2 = strtotime($a2['periods']);
   return $value1 - $value2;
});

echo json_encode($data_mention);


}

public function getsentimentdetail()
    {
     //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      /*$brand_id=59;*/
      /*$groupType='month';*/
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 30')));*/

 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con="";
if($groupType=="month")
{
$group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
 }

   else 
 {
$group_period =" DATE_FORMAT(created_time, '%Y-%m-%d')";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and message Like '%".$filter_keyword."%'";
}
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
   $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}
$query = "SELECT sum(positive) positive,sum(negative) negative,sum(neutral) neutral,".$group_period." created_time from " .
" (SELECT IF(sentiment = 'pos', 1, 0)  positive, IF(sentiment = 'neg', 1, 0) negative,".
" IF(sentiment = 'neutral', 1, 0) neutral,page_name,message, DATE_FORMAT(created_time, '%Y-%m-%d') created_time ".
" FROM temp_".$brand_id."_posts  ".
" UNION ALL ".
" SELECT IF(cmts.sentiment = 'pos', 1, 0)  positive, IF(cmts.sentiment = 'neg', 1, 0) negative,".
" IF(cmts.sentiment = 'neutral', 1, 0) neutral,posts.page_name,cmts.message,DATE_FORMAT(cmts.created_time, '%Y-%m-%d') created_time".
" FROM temp_".$brand_id."_comments cmts  LEFT JOIN temp_".$brand_id."_posts posts on posts.id=cmts.post_id where posts.id IS NULL) T1 ".
" WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_con.$add_inbound_con.
"GROUP BY ".$group_period .
" ORDER BY ".$group_period;

/*echo $query;
return;*/

$query_result = DB::select($query);

   $data = [];
   $total=0;

   $data_sentiment = [];
   $data_mention = [];
   $total=0;
   $data_message = [];

    foreach ($query_result as  $key => $row) {
          
              $utcdatetime = $row->created_time;
         
               
               $pos =$row->positive;
               $neg =$row->negative;
               $nan =$row->neutral;
          
                $request['periodLabel'] ="";
      
                if($groupType == "week")
{
    $weekperiod=$this ->rangeWeek ($utcdatetime);
    $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
    
 if (!array_key_exists($periodLabel , $data_sentiment)) {
    $data_sentiment[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'positive' => $pos,
                'negative' => $neg,
                'neutral' => $nan,
                'periods' =>$utcdatetime,
            );
}
else
{
 $data_sentiment[$periodLabel]['positive'] = $data_sentiment[$periodLabel]['positive'] + $pos;
 $data_sentiment[$periodLabel]['negative'] = $data_sentiment[$periodLabel]['negative'] + $neg;
 $data_sentiment[$periodLabel]['neutral'] = $data_sentiment[$periodLabel]['neutral'] + $nan;
 $data_sentiment[$periodLabel]['periods'] =$utcdatetime;
}
 
                  


}
 else
{
 $data_sentiment[$utcdatetime]['periodLabel'] = $utcdatetime;
 $data_sentiment[$utcdatetime]['positive'] = $pos;
 $data_sentiment[$utcdatetime]['negative'] = $neg;
 $data_sentiment[$utcdatetime]['neutral'] =  $nan;
 $data_sentiment[$utcdatetime]['periods'] = $utcdatetime;

}

                
   }

 $data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 
    echo json_encode($data_sentiment);


}

public function getemotiondetail()
    {
    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

       if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

   /*   $brand_id=46;
     $groupType='month';*/
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }

/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 05')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 05')));*/

 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con ="";

if($groupType=="month")
{
$group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
 }

   else 
 {
$group_period =" DATE_FORMAT(created_time, '%Y-%m-%d')";                
              

 }
if($filter_keyword !== '')
{
  $add_con = "  and message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}

if($filter_sentiment !== '')
{
  $add_con .= " and sentiment = '".$filter_sentiment."'";
  $add_comment_con .= " and cmts.sentiment  = '".$filter_sentiment."'";
}

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}

$query = "SELECT sum(anger) anger,sum(interest) interest, ".
" sum(disgust) disgust,sum(fear) fear,sum(joy) joy,sum(liked) liked,sum(love) love,sum(neutral) neutral,".
" sum(sadness) sadness,sum(surprise) surprise,sum(trust) trust, "
 .$group_period .  " created_time from (SELECT IF(emotion = 'anger', 1, 0) anger, ".
" IF(emotion = 'interest', 1, 0) interest, IF(emotion = 'disgust', 1, 0) disgust, ".
" IF(emotion = 'fear', 1, 0) fear, IF(emotion = 'joy', 1, 0) joy, IF(emotion = 'like', 1, 0) liked, ".
" IF(emotion = 'love', 1, 0) love, IF(emotion = 'neutral', 1, 0) neutral, IF(emotion = 'sadness', 1, 0) sadness, ".
" IF(emotion = 'surprise', 1, 0) surprise,IF(emotion = 'trust', 1, 0) trust, ".
" DATE_FORMAT(created_time, '%Y-%m-%d') created_time,page_name, message,sentiment FROM temp_".$brand_id."_posts  ". 
" UNION ALL ".
" SELECT IF(cmts.emotion = 'anger', 1, 0) anger, IF(cmts.emotion = 'interest', 1, 0) interest, ".
" IF(cmts.emotion = 'disgust', 1, 0) disgust, IF(cmts.emotion = 'fear', 1, 0) fear, IF(cmts.emotion = 'joy', 1, 0) ".
" joy, IF(cmts.emotion = 'like', 1, 0) liked, IF(cmts.emotion = 'love', 1, 0) love, ".
" IF(cmts.emotion = 'neutral', 1, 0)".
" neutral, IF(cmts.emotion = 'sadness', 1, 0) sadness, IF(cmts.emotion = 'surprise', 1, 0) surprise, ".
" IF(cmts.emotion = 'trust', 1, 0) trust, DATE_FORMAT(cmts.created_time, '%Y-%m-%d') created_time ,page_name,cmts.message message,cmts.sentiment sentiment FROM ".
" temp_".$brand_id."_comments  cmts LEFT JOIN  temp_".$brand_id."_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NULL) T1 ".
" WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_con . $add_inbound_con.
" GROUP BY ".$group_period .
" ORDER by ".$group_period;



/*echo $query ;
return;*/

$query_result = DB::select($query);

   $data_emotion = [];
   $total=0;

    foreach ($query_result as  $key => $row) {
          
                $utcdatetime = $row ->created_time;
                $request["periods"]=$utcdatetime;
                    
                $request['anger']=$row ->anger;
                $request['interest']=$row ->interest;
                $request['disgust']=$row ->disgust;
                $request['fear']=$row ->fear;
                $request['joy']=$row ->joy;
                $request['like']=$row ->liked;
                $request['love']=$row ->love;
                $request['neutral']=$row ->neutral;
                $request['sadness']=$row ->sadness;
                $request['surprise']=$row ->surprise;
                $request['trust']=$row ->trust;
                $periodLabel ='';

                if($groupType == "week")
{
    $weekperiod=$this ->rangeWeek ( $utcdatetime);
   $periodLabel=$weekperiod['start'] . " - " . $weekperiod['end'] ;
  
}
else
{
 
  $periodLabel =$utcdatetime ;
}

if (!array_key_exists($periodLabel , $data_emotion)) {
    $data_emotion[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'anger' => $row ->anger,
                'interest' =>$row ->interest,
                'disgust' =>$row ->disgust,
                'fear' => $row ->fear,
                'joy' =>  $row ->joy,
                'like' => $row ->liked,
                'love' =>  $row ->love,
                'neutral' =>$row ->neutral,
                'sadness' => $row ->sadness,
                'surprise' => $row ->surprise,
                'trust' =>  $row ->trust,
                'periods' =>  $utcdatetime,
            );
}
else
{
 $data_emotion[$periodLabel]['anger'] = $data_emotion[$periodLabel]['anger'] +  $row ->anger;
 $data_emotion[$periodLabel]['interest'] = $data_emotion[$periodLabel]['interest'] + $row ->interest;
 $data_emotion[$periodLabel]['disgust'] = $data_emotion[$periodLabel]['disgust'] + $row ->disgust;
 $data_emotion[$periodLabel]['fear'] = $data_emotion[$periodLabel]['fear'] +$row ->fear;
 $data_emotion[$periodLabel]['joy'] = $data_emotion[$periodLabel]['joy'] + $row ->joy;
 $data_emotion[$periodLabel]['like'] = $data_emotion[$periodLabel]['like'] + $row ->liked;
 $data_emotion[$periodLabel]['love'] = $data_emotion[$periodLabel]['love'] +  $row ->love;
 $data_emotion[$periodLabel]['neutral'] = $data_emotion[$periodLabel]['neutral'] + $row ->neutral;
 $data_emotion[$periodLabel]['sadness'] = $data_emotion[$periodLabel]['sadness'] + $row ->sadness;
 $data_emotion[$periodLabel]['surprise'] = $data_emotion[$periodLabel]['surprise'] + $row ->surprise;
 $data_emotion[$periodLabel]['trust'] = $data_emotion[$periodLabel]['trust'] +  $row ->trust;
 $data_emotion[$periodLabel]['periods'] = $utcdatetime;
}
          
              }
               
$data_emotion = $this->unique_multidim_array($data_emotion,'periodLabel'); 



    echo json_encode($data_emotion);


}
public function getinterestdata()
{
      $brand_id=Input::get('brand_id');
 
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      // if(null !== Input::get('sentiment'))
      // $filter_sentiment=Input::get('sentiment'); 

     //  $brand_id=59;
     // $groupType='month';
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/

 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con ="";
if($groupType=="month")
{
//$group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
  $group_period ="%Y-%m"; 
 }

   else 
 {
$group_period ="%Y-%m-%d";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}
// if($filter_sentiment !== '')
// {
//   $add_con .= " and sentiment = '".$filter_sentiment."'";
//   $add_comment_con .= " and cmts.sentiment  = '".$filter_sentiment."'";
// }
  // $table = "temp_".$brand_id."_comments";
  // dd($table);

  $query = "SELECT sum(interest) interest_sum,count(*) comment_count,  DATE_FORMAT(created_time, '".$group_period."') periodLabel  FROM temp_".$brand_id."_comments WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_con .
" GROUP BY DATE_FORMAT(created_time, '".$group_period."')" ;
// echo $query;
// return ;
$query_result = DB::select($query);
// dd($query_result);
   echo json_encode($query_result);

}


public function getinteractiondetail()
    {
    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

      /*$brand_id=49;*/
    /* $groupType='month';*/
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 30')));
*/
 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con ="";

if($groupType=="month")
{
//$group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
  $group_period ="%Y-%m"; 
 }

   else 
 {
$group_period ="%Y-%m-%d";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}
if($filter_sentiment !== '')
{
  $add_con .= " and sentiment = '".$filter_sentiment."'";
  $add_comment_con .= " and cmts.sentiment  = '".$filter_sentiment."'";
}
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}


$query = "SELECT sum(Liked) Liked,sum(Love) Love,sum(Haha) Haha,sum(Angry) Angry,sum(Sad) Sad,sum(Wow) Wow,sum( post_count) post_count,  created_time, SUM(replace(shared, ',', '')) shared ".
" FROM (SELECT sum(Liked) Liked,sum(Love) Love,sum(Haha) Haha,sum(Angry) Angry,sum(Sad) Sad,sum(Wow) Wow,count(*)  post_count, DATE_FORMAT(created_time, '".$group_period."') created_time, ".
" SUM(replace(shared, ',', '')) shared FROM temp_".$brand_id."_posts posts WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_con . $add_inbound_con .
" GROUP BY DATE_FORMAT(created_time, '".$group_period."')" .
" UNION ALL ".
"SELECT  '0' Liked,'0' Love,'0' Haha,'0' Angry,'0' Sad,'0' Wow,count(*)  post_count, DATE_FORMAT(cmts.created_time, '".$group_period."') created_time,'0' shared FROM temp_".$brand_id."_comments cmts LEFT JOIN".
" temp_".$brand_id."_posts posts on cmts.post_id=posts.id   WHERE posts.id is null and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_comment_con . $add_inbound_con .
" GROUP BY DATE_FORMAT(cmts.created_time, '".$group_period."'))T1 " .
" GROUP BY created_time ORDER BY created_time";

/*echo $query ;
return;*/

$query_result = DB::select($query);
// dd($query_result);
   $data = [];
   $total=0;

    foreach ($query_result as  $key => $row) {
          
                           
                /*$datetime = $row ->created_time;

                $request['post_count'] =$row ->post_count;
             
                $request['Wow'] = $row ->Wow;
                $request['Love'] = $row ->Love;
                $request['Angry'] = $row ->Angry;
                $request['Sad'] = $row ->Sad;
                $request['Haha'] = $row ->Haha;
                $request['Like'] = $row ->Liked;
*/
                $datetime = $row ->created_time;
                $periodLabel ="";
                $weekcount ="";
                if($groupType == "week")
                {
                  $weekperiod=$this ->rangeWeek ($datetime);
                  $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
                 if (!array_key_exists($periodLabel , $data)) {
                  $data[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'Wow' =>  $row ->Wow,
                'Love' => $row ->Love,
                'Angry' => $row ->Angry,
                'Sad' => $row ->Sad,
                'Haha' => $row ->Haha,
                'Like' =>$row ->Liked,
                'periods' => $periodLabel,
                'shared' =>$row ->shared,
                'post_count' =>$row ->post_count,
            );
                                                                }
                else
                {
$data[$periodLabel]['Wow'] = (int) $data[$periodLabel]['Wow'] + (int) $row ->Wow;
$data[$periodLabel]['Love'] = (int) $data[$periodLabel]['Love'] + (int)$row ->Love;
$data[$periodLabel]['Angry'] = (int) $data[$periodLabel]['Angry'] +(int) $row ->Angry;
$data[$periodLabel]['Sad'] = (int) $data[$periodLabel]['Sad'] + (int) $row ->Sad;
$data[$periodLabel]['Haha'] = (int) $data[$periodLabel]['Haha'] + (int) $row ->Haha;
$data[$periodLabel]['Like'] = (int) $data[$periodLabel]['Like'] + (int) $row ->Liked;
$data[$periodLabel]['periods'] =  $datetime;
$data[$periodLabel]['shared'] =(int) $data[$periodLabel]['shared'] +(int)$row ->shared;
$data[$periodLabel]['post_count'] =(int) $data[$periodLabel]['post_count'] +(int)$row ->post_count;

                }
   
 
                }
    else 
    {
                 $data[$datetime]['Wow'] = $row ->Wow;
                 $data[$datetime]['Love'] =$row ->Love;
                 $data[$datetime]['Angry'] =$row ->Angry;
                 $data[$datetime]['Sad'] = $row ->Sad;
                 $data[$datetime]['Haha'] =$row ->Haha;
                 $data[$datetime]['Like'] =$row ->Liked;
                 $data[$datetime]['periods'] =  $datetime;
                 $data[$datetime]['periodLabel'] =  $datetime;
                 $data[$datetime]['shared'] =$row ->shared;
                 $data[$datetime]['post_count'] =$row ->post_count;
    }
                
              
                 
               
            }


$data = $this->unique_multidim_array($data,'periodLabel'); 
 

    echo json_encode($data);


}



public function getpopularnegative()

{
  //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
         
    /*  $brand_id=6;*/
 
     

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
}
 $query = "SELECT  (sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry)) ".
 " total,posts.id,message,posts.page_name,link,full_picture,sentiment,emotion,DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p')".
 " created_time,CAST(followers.fan_count AS UNSIGNED) +SUM(replace(shared, ',', '')) each_influencer  FROM  ".
 " temp_".$brand_id."_posts posts inner join temp_".$brand_id."_followers followers on posts.page_name=followers.page_name ".
 " WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_inbound_con .
 " and sentiment='neg'  GROUP BY DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p'),posts.id,message,page_name,link,full_picture,sentiment,emotion,fan_count ".
 " ORDER by total,DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p') DESC LIMIT 10 ";

$query_result = DB::select($query);
$data = [];
 $permission_data = $this->getPermission();
 $edit_permission=$permission_data['edit'];

 foreach ($query_result as  $key => $row) {

              $request["message"] = $row ->message;
              $request["page_name"] = $row ->page_name;
              $request["created_time"] =date('d-m-Y H:m:s', strtotime($row ->created_time));
              if(isset($row ->link))
              $request["link"] =$row ->link;
            else
              $request["link"] ='#';

             if(isset($row ->full_picture))
              $request["full_picture"] =$row ->full_picture;
            else
              $request["full_picture"] ='';

              $request["sentiment"] =$row ->sentiment;
              $request["emotion"] =$row ->emotion;
              $request["id"] =$row ->id;
              $request["edit_permission"]=$edit_permission;
              $data[] = $request;

            }
  echo json_encode($data);

}

public function getallmention()
    {

    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      /*$brand_id=59;*/
    /*  $groupType=Input::get('periodType');*/
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

      /*$brand_id=30;*/
 
     

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2017 06 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2017 06 30')));*/

$add_con='';
if($filter_keyword !== '')
  $add_con = " and posts.message Like '%".$filter_keyword."%'";

if($filter_sentiment !== '')
{
  $add_con .= " and posts.sentiment = '".$filter_sentiment."'";
}

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}

$query = "SELECT  posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_".$brand_id."_posts posts LEFT JOIN temp_".$brand_id."_comments cmts on posts.id=cmts.post_id ".
" WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_con . $add_inbound_con .
" ORDER by DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') DESC";

/*echo $query;
return;*/
$query_result = DB::select($query);
     
 /*print_r($query_result);
 return;*/
 $data = [];
 $data_mention=[];
 foreach ($query_result as  $key => $row) {
   $pos =($row->cmt_sentiment=='pos')?1:0 ;$neg=($row->cmt_sentiment=='neg')?1:0;
   $anger =($row->cmt_emotion=='anger')?1:0 ; $interest=($row->cmt_emotion=='interest')?1:0;
   $disgust=($row->cmt_emotion=='disgust')?1:0;$fear=($row->cmt_emotion=='fear')?1:0;
   $joy=($row->cmt_emotion=='joy')?1:0;$like=($row->cmt_emotion=='like')?1:0;
   $love=($row->cmt_emotion=='love')?1:0;$neutral=($row->cmt_emotion=='neutral')?1:0;
   $sadness=($row->cmt_emotion=='sadness')?1:0;$surprise=($row->cmt_emotion=='surprise')?1:0;
   $trust=($row->cmt_emotion=='trust')?1:0;
   $message=$this->readmore($row->message);
   //$message=$row->message;
            if (isset($row ->id))
   {
                 $id= $row ->id;
   
    if (!array_key_exists($id , $data)) {
    $data[$id] = array(
                'id'     =>$row->id,
                'message' => $message,
                'page_name' =>$row->page_name,
                'created_time' =>$row->created_time,
                'link' =>$row->link,
                'full_picture' =>$row->full_picture,
                'sentiment' =>$row->sentiment,
                'emotion' =>$row->emotion,
                'positive'=> $pos,'negative'=>$neg,
                'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                'isBookMark'=>$row->isBookMark
            );
}
else
{
 $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
 $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
 $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
 $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
 $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
 $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
 $data[$id]['like'] = (int) $data[$id]['like'] + $like;
 $data[$id]['love'] = (int) $data[$id]['love'] + $love;
 $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
 $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
 $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
 $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;



}
//echo $data[$id]['negative'];echo $neg;return;

}
 
            }
            $data_mention=$data;

/*$id='799069593563429_1007801336023586';
echo $data[$id]['negative'];
 print_r($data_mention);
 return;
 $data_mention = $this->unique_multidim_array($data_mention,'id'); */

$permission_data = $this->getPermission();
 return Datatables::of($data_mention)
       ->addColumn('action', function ($data_mention) {
                return '<a href="javascript:void(0)" id="'.$data_mention['id'].'" name="'.$data_mention['isBookMark'].'" class="mdi '.$data_mention['isBookMark'].' text-yellow btn-bookmark" style="float:right;color:#f39c12"></a>';
            }) 

       ->addColumn('post_div', function ($data_mention) use($permission_data) {
               $html= '<div class="profiletimeline"><div class="sl-item">
                                    <div class="sl-left"> <img src="'.$data_mention['full_picture'].'" alt="user" class="img-circle  img-bordered-sm"> </div> 
                                    <div class="sl-right"> <div><a href="'.$data_mention['link'].'" class="link"  target="_blank">'.$data_mention['page_name'].'</a>
              <span class="sl-date">'.$data_mention['created_time'].'  </span>

               <p>Sentiment : <select class="form-control custom-select sentiment-color customize-select" id="sentiment_'.$data_mention['id'].'"  style="max-width:13%;color:#dd4b39;height: 20px "><option value="pos"';
                if($data_mention['sentiment'] == "pos")
                $html.= 'selected="selected"';
                $html.= '>pos</option><option value="neg"';
                if($data_mention['sentiment'] == "neg")
                $html.= 'selected="selected"';
                $html.= '>neg</option><option value="neutral"';
                if($data_mention['sentiment'] == "neutral")
                $html.= 'selected="selected"';     
                $html.= '>neutral</option><option value="NA"';
                if($data_mention['sentiment'] == "NA")
                $html.= 'selected="selected"';  
                $html.= '>NA</option></select>';

                $html.= ' | Emotion : <select class="form-control custom-select emotion-color customize-select" id="emotion_'.$data_mention['id'].'" style="max-width:13%;color:#55acee;height: 20px"><option value="anger"';
                if($data_mention['emotion'] == "anger")
                $html.= 'selected="selected"';
                $html.= '>anger</option><option value="interest"';
                if($data_mention['emotion'] == "interest")
                $html.= 'selected="selected"';     
                $html.= '>interest</option><option value="disgust"';
                if($data_mention['emotion'] == "disgust")
                $html.= 'selected="selected"';  
                $html.= '>disgust</option><option value="fear"';
                if($data_mention['emotion'] == "fear")
                $html.= 'selected="selected"'; 
                $html.= '>fear</option><option value="joy"';
                if($data_mention['emotion'] == "joy")
                $html.= 'selected="selected"'; 
                $html.= '>joy</option><option value="like"';
                if($data_mention['emotion'] == "like")
                $html.= 'selected="selected"'; 
                $html.= '>like</option><option value="love"';
                if($data_mention['emotion'] == "love")
                $html.= 'selected="selected"'; 
                $html.= '>love</option><option value="neutral"';
                if($data_mention['emotion'] == "neutral")
                $html.= 'selected="selected"';
                $html.= '>neutral</option><option value="sadness"';
                if($data_mention['emotion'] == "sadness")
                $html.= 'selected="selected"';
                $html.= '>sadness</option><option value="surprise"';
                if($data_mention['emotion'] == "surprise")
                $html.= 'selected="selected"';
                $html.= '>sadness</option><option value="trust"';
                if($data_mention['emotion'] == "trust")
                $html.= 'selected="selected"';
                $html.= '>trust</option><option value="NA"';
                if($data_mention['emotion'] == "NA")
                $html.= 'selected="selected"';
                $html.= '>NA</option></select>';
                
                if($permission_data['edit'] === 1)
                $html.= ' <a class="edit_post_predict" id="'.$data_mention['id'].'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>';
                                                    
                                                     
                $html.=' </span></p>

                                    <p class="m-t-10">'.$data_mention['message'].'...<a href="'.$data_mention['link'].'" target="_blank"> Read More
                                      </a></p></div></div>
                                      </div></div>';
    if($data_mention['positive']>0 || $data_mention['negative']>0 || $data_mention['anger']>0  || $data_mention['interest']>0 || $data_mention['disgust']>0 || $data_mention['fear']>0 || $data_mention['joy']>0 || $data_mention['like']>0 || $data_mention['love']>0 || $data_mention['neutral']>0 || $data_mention['sadness']>0 || $data_mention['surprise']>0|| $data_mention['trust']>0){                                   
   $html.= '<div><div style="margin:0 0 0 70px;"> <form>
    <fieldset>
        <legend>Customer Feedback</legend>';
      }
             if($data_mention['positive']>0){ //your condition
           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#dd4b39:padding:5px" class="popup" id="'.$data_mention['id'].'" name="Positive">positive : </a> 
                  <span class="label label-light-danger">'.$data_mention['positive'].'</span> ';
                  
         } 
             if($data_mention['negative']>0){ //your condition
           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#dd4b39" class="popup" id="'.$data_mention['id'].'" name="Negative">negative : </a>
                <span class="label label-light-danger" style="font-size:15px">'.$data_mention['negative'].'</span> ';
                  
         } 
        
                if($data_mention['anger']>0){  
          $html.='<a href="javascript:void(0)" style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="anger">anger : </a> 
                  <span class="label label-light-danger">'.$data_mention['anger'].'</span> ';                 
               }
                if($data_mention['interest']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="interest">interest : </a> 
                  <span class="label label-light-danger">'.$data_mention['interest'].'</span> ';                 
               }
                if($data_mention['disgust']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="disgust">disgust : </a> 
                  <span class="label label-light-danger">'.$data_mention['disgust'].'</span> ';                 
               }
                if($data_mention['fear']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="fear">fear : </a> 
                  <span class="label label-light-danger">'.$data_mention['fear'].'</span> ';                 
               }
                if($data_mention['joy']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="joy">joy : </a> 
                  <span class="label label-light-danger">'.$data_mention['joy'].'</span> ';                 
               }
                if($data_mention['like']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="like">like : </a> 
                  <span class="label label-light-danger">'.$data_mention['like'].'</span> ';                 
               }
                if($data_mention['love']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="love">love : </a> 
                  <span class="label label-light-danger">'.$data_mention['love'].'</span> ';                 
               }
                if($data_mention['neutral']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="neutral">neutral : </a> 
                  <span class="label label-light-danger">'.$data_mention['neutral'].'</span> ';                 
               }
                if($data_mention['sadness']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="sadness">sadness : </a> 
                  <span class="label label-light-danger">'.$data_mention['sadness'].'</span> ';                 
               }
                if($data_mention['surprise']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="surprise">surprise : </a> 
                  <span class="label label-light-danger">'.$data_mention['surprise'].'</span> ';                 
               }
                  if($data_mention['trust']>0){  
          $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="trust">trust : </a> 
                  <span class="label label-light-danger">'.$data_mention['trust'].'</span></fieldset>
</form></div></div>';                 
               }
                return $html;
               

            })
        
      ->rawColumns(['post_div','action'])
   
      ->make(true);

          //  echo json_encode($data);
    }
    public function getcomments()
    {
       //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $post_id=Input::get('id');
      $brand_id=Input::get('brand_id');
      $cmt_type=Input::get('cmt_type');


/*      $post_id='308814943257921_308831799922902';
      $brand_id=59;
      $cmt_type='pos';*/

      if($cmt_type == 'Positive')
      {
$cmt_type ='pos';
      }
      if($cmt_type =="Negative")
      {
      	$cmt_type='neg';
      }
      $permission_data = $this->getPermission();
 $edit_permission=$permission_data['edit'];


        $query="SELECT id,".$edit_permission." as edit_permission,message,sentiment,emotion,DATE_FORMAT(created_time, '%d-%m-%Y %h:%i:%s %p') created_time ".
        " from temp_".$brand_id."_comments where post_id='".$post_id."' and (sentiment='".$cmt_type."' or emotion='".$cmt_type."') ORDER BY DATE_FORMAT(created_time, '%d-%m-%Y %h:%i:%s %p') DESC";

       /* echo $query;
        return;*/
        $data = DB::select($query);
       /* echo count($data);*/
        echo json_encode($data);
    }

    public function getallmentioncomment()
    {

    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
   
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment ='';

      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

     /* $brand_id=30;*/
 
     

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/

$add_con='';
if($filter_keyword !== '')
  $add_con = " and cmts.message Like '%".$filter_keyword."%'";

if($filter_sentiment !== '')

  $add_con .= " and cmts.sentiment = '".$filter_sentiment."'";
 
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
   $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}

$query="SELECT cmts.id,cmts.message,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
" cmts.sentiment,cmts.emotion,(CASE WHEN cmts.isBookMark = 0 THEN 'mdi-star-outline' ELSE 'mdi-star' END) isBookMark ".
" FROM temp_".$brand_id."_comments cmts LEFT JOIN  temp_".$brand_id."_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NULL".
" and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_con .  $add_inbound_con . 

" ORDER by DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p')";
/*
echo $query;
return;*/
$data = DB::select($query);
/*     
 print_r($data);
 return;*/
$permission_data = $this->getPermission(); 
 return Datatables::of($data)
       ->addColumn('action', function ($data) {
                return '<a href="javascript:void(0)" id="'.$data->id.'" name="'.$data->isBookMark.'" class="mdi '.$data->isBookMark.' text-yellow btn-bookmark-comment" style="float:right;color:#f39c12"></a>';
            }) 

       ->addColumn('post_div', function ($data) use($permission_data){
                $html= '<div class="profiletimeline"><div class="sl-item">
                                    <div class="sl-left user-img"> <span class="round">A</span> </div> 
                                    <div class="sl-right"> <div><a href="javascript:void(0)" class="link">Comment</a>
              <span class="sl-date">'.$data->created_time.'</span>
<p>Sentiment : <select class="form-control custom-select sentiment-color customize-select" id="sentiment_'.$data->id.'"  >';
                 if($data->sentiment == "")
                $html.= '<option value="" selected="selected"></option>';
                $html.='<option value="pos"';
                if($data->sentiment == "pos")
                $html.= 'selected="selected"';
                $html.= '>pos</option><option value="neg"';
                if($data->sentiment == "neg")
                $html.= 'selected="selected"';     
                $html.= '>neg</option><option value="neutral"';
                if($data->sentiment == "neutral")
                $html.= 'selected="selected"';  
                $html.= '>neutral</option></select>';

                $html.= ' | Emotion : <select class="form-control custom-select emotion-color customize-select" id="emotion_'.$data->id.'" >';
                if($data->emotion == "")
                $html.= '<option value="" selected="selected"></option>';
                $html.='<option value="anger"';
                if($data->emotion == "anger")
                $html.= 'selected="selected"';
                $html.= '>anger</option><option value="interest"';
                if($data->emotion == "interest")
                $html.= 'selected="selected"';     
                $html.= '>interest</option><option value="disgust"';
                if($data->emotion == "disgust")
                $html.= 'selected="selected"';  
                $html.= '>disgust</option><option value="fear"';
                if($data->emotion == "fear")
                $html.= 'selected="selected"'; 
                $html.= '>fear</option><option value="joy"';
                if($data->emotion == "joy")
                $html.= 'selected="selected"'; 
                $html.= '>joy</option><option value="like"';
                if($data->emotion == "like")
                $html.= 'selected="selected"'; 
                $html.= '>like</option><option value="love"';
                if($data->emotion == "love")
                $html.= 'selected="selected"'; 
                $html.= '>love</option><option value="neutral"';
                if($data->emotion == "neutral")
                $html.= 'selected="selected"';
                $html.= '>neutral</option><option value="sadness"';
                if($data->emotion == "sadness")
                $html.= 'selected="selected"';
                $html.= '>sadness</option><option value="surprise"';
                if($data->emotion == "surprise")
                $html.= 'selected="selected"';
               $html.= '>surprise</option><option value="trust"';
                if($data->emotion == "trust")
                $html.= 'selected="selected"';
                $html.= '>trust</option><option value="NA"';
                if($data->emotion == "NA")
                $html.= 'selected="selected"';
                $html.= '>NA</option></select>';
                if($permission_data['edit'] === 1)
                $html.= ' <a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>';
                                                    
                                                     
                $html.='</span></p>

                                    <p class="m-t-10">'.$data->message.'</p></div></div></div></div>';
                  return $html;
           
            })
        
      ->rawColumns(['post_div','action'])
   
      ->make(true);

          //  echo json_encode($data);
    }


    public function getallmentionwithcomment()
    {

         //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
         $brand_id=Input::get('brand_id');
         $filter_keyword='';
      if(null !== Input::get('keyword'))
        $filter_keyword=Input::get('keyword');
         $brand_id=16;
       $keyword_data = $this->getprojectkeywork($brand_id);
      
      $filter['$or'] = $this->getkeywordfilter($keyword_data);

        if(null !== Input::get('fday'))
            {
                   $dateBegin =new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('fday')))* 1000);

            }
      
           else
           {
                $dateBegin =new MongoDB\BSON\UTCDateTime(\Carbon\Carbon::now()->timestamp * 1000);
           }
    

    if(null !==Input::get('sday'))
    {

        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('sday')))* 1000);
    }
    
    else
    {
               $dateEnd =new MongoDB\BSON\UTCDateTime( \Carbon\Carbon::now()->timestamp * 1000);

           }

        
        $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2017 01 08'))* 1000);
        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2018 08 08'))* 1000);

     

  


 $query_result=MongodbData::raw(function ($collection) use($dateBegin,$dateEnd,$filter,$filter_keyword) {
    return $collection->aggregate([

           
             [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             $filter,
             ['message' => new MongoDB\BSON\Regex(".*" . $filter_keyword,'i' )],


          
]

        ]  
                   
                         ]

                         ,
               [
        '$lookup'=>[
            'from'=>'comments',
            'localField'=>'id',
            'foreignField'=>'post_id',
            'as'=>'comments'
        ]
   ],
   
  /* [
      '$match' => ['pages' => ['$ne'=> [] ]]
   ],*/
    [ '$unwind' => '$comments' ] ,/*,
                          [
            '$sort' =>['created_time'=>-1]


            ]*/
      
        
      
        

             


    ]);
})->toArray();
print_r($query_result);
return ;
$data = [];
$data_message = [];


foreach ($query_result as $row) {// print_r($row);
    //echo($row["name"]);return;
             /*   $test = $row["name"];
                echo  $test;
                return ;*/
              
                $request["message"] = $row["message"];
                if(isset($row["page_name"]))
                $request["page_name"] = $row["page_name"];
            else
                 $request["page_name"] ="Unknown";

                /* $timestamp = $row["created_time"] * 1000;*/
               //  $timestamp = 1453939200 * 1000;
                 $utcdatetime = $row["created_time"];

                  $datetime = $utcdatetime->toDateTime();

                 $request["created_time"] =$datetime->format('d-M-Y h:i:s A');
                 if(isset($row["link"]))
                $request["link"] =$row["link"];
              else
                $request["link"] ="";
               if(isset($row["full_picture"]))
                $request["full_picture"] =$row["full_picture"];
              else
                $request["full_picture"]="";

                $request["sentiment"] ="";
                $request["emotion"] ="";

                if(isset($row["isBookMark"]) && $row["isBookMark"] === true)
                $request["isBookMark"] ='mdi-star';
                else
                $request["isBookMark"]='mdi-star-outline';

                $request["id"]=$row["id"];
                $data[] = $request;

                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$row["message"]);;
            }
/*dd(count($data_message));*/


    }

                    function readmore($string){
                         $string = strip_tags($string);
                        if (strlen($string) > 500) {

                            // truncate string
                            $stringCut = substr($string, 0, 500);
                            $endPoint = strrpos($stringCut, ' ');

                            //if the string doesn't contain any space then it will cut without word basis.
                            $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                    
                        }
                        return $string;


        }

public function getsentimentbycategory()
    {
   //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');

      /*$brand_id=59;
*/
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 30')));*/

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
}

$query = "select category, sum(IF(sentiment = 'pos', 1, 0)) positive,sum(IF(sentiment = 'neg', 1, 0)) negative , ".
" sum(IF(sentiment = 'pos', 1, 0)) + sum(IF(sentiment = 'neg', 1, 0)) total ".
" from temp_".$brand_id."_posts posts inner join temp_".$brand_id."_pages pages on posts.page_name = pages.page_name  ".
" WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')  ". $add_inbound_con.
" GROUP BY category ".
" ORDER BY  total DESC ";

/*echo $query;
return;*/


$query_result = DB::select($query);


/*
    print_r($data);
    return;*/
      echo json_encode($query_result);

}
public function getnetpromotorscore()
    {

    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');

      /*$brand_id=2;*/

 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
}

$query = "select posts.page_name, sum(IF(sentiment = 'pos', 1, 0)) positive,sum(IF(sentiment = 'neg', 1, 0)) negative , ".
" sum(IF(sentiment = 'pos', 1, 0)) - sum(IF(sentiment = 'neg', 1, 0)) netscore ".
" from temp_".$brand_id."_posts posts inner join temp_".$brand_id."_pages pages on posts.page_name = pages.page_name  ".
" WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_inbound_con .
" GROUP BY posts.page_name ".
" ORDER BY  netscore DESC Limit 20";

/*echo $query;
return;*/

$query_result = DB::select($query);

      echo json_encode($query_result);

}

public function getInfluencerProfile()
    {
    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
        $brand_id=Input::get('brand_id');
   
       /*$brand_id=6;*/
        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
  $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
}

$query="SELECT posts.page_name page_name,ANY_VALUE(full_picture) full_picture,fan_count,SUM(replace(shared, ',', '')) shared,".
" CAST(fan_count AS UNSIGNED)+SUM(replace(shared, ',', '')) total ".
" FROM temp_".$brand_id."_posts posts inner join temp_".$brand_id."_followers followers on posts.page_name=followers.page_name ".
" inner join temp_".$brand_id."_pages pages on posts.page_name = pages.page_name".
" where profile='1'" .$add_inbound_con . " group by page_name,fan_count ".
" order by total DESC ".
" Limit 10 ";
/*echo $query;*/
$query_result = DB::select($query);
  
echo json_encode($query_result);

}

public function getInfluencerPage()
    {
    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
        $brand_id=Input::get('brand_id');
   
       /*$brand_id=6;*/
        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (posts.page_name  not in (".$inboundpages.") or posts.page_name is NULL)";
}

$query="SELECT posts.page_name page_name,ANY_VALUE(full_picture) full_picture,fan_count,SUM(replace(shared, ',', '')) shared,".
" CAST(fan_count AS UNSIGNED)+SUM(replace(shared, ',', '')) total ".
" FROM temp_".$brand_id."_posts posts inner join temp_".$brand_id."_followers followers on posts.page_name=followers.page_name ".
" inner join temp_".$brand_id."_pages pages on posts.page_name = pages.page_name".
" where profile='0' ". $add_inbound_con . " group by page_name,fan_count ". $add_inbound_con .
" order by total DESC ".
" Limit 10 ";
/*echo $query;*/
$query_result = DB::select($query);
  
echo json_encode($query_result);

}


 public function insert_Posts_Data()
    {
     
     $keyword_data = $this->getprojectkeywork(81);
       /*  $key_data_count = count($keyword_data);*/
     $filter['$or'] = $this->getkeywordfilter($keyword_data);
    $query_result=MongodbData::raw(function ($collection) use($filter) {//print_r($filter);

    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             $filter
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();
/*dd($query_result);*/
$data=[];
 $id_array=[];

 foreach ($query_result as  $key => $row) {
                $id ='';
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';
                $page_name ='';
                $share =0;
                $reaction ='';
             

                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                if(isset($row['message'])) $message = preg_replace('/(\r\n|\r|\n)+/', " ",$row['message']);
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$id_array[]=$page_name;}
                if(isset($row['share'])) $share = $row['share'];
              //  if(isset($row['reaction'])) $reaction =$row['reaction'];
          

                  $data[] =[
                    'id' => $id,
                    'full_picture' =>$full_picture,
                    'link' =>$link,
                    'name' =>$name,
                    'message' => $message,
                    'page_name' =>$page_name,
                    'share' =>$share,
                    'reaction' =>$reaction,
                    'created_time' =>$datetime,
                    'isBookMark' =>0,
                   ];                 

  }
  
$result = DB::table('temp_81_posts')->insert($data);

$follower_result=MongoFollowers::raw(function ($collection) use($filter,$id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
          ['page_name' => ['$in' => $id_array ] ] 
            
      ]

        ]  
           ]
    
    ]);
})->toArray();

 $data_followers=[];
 foreach ($follower_result as  $key => $row) {
                $id ='';
                $page_name ='';
                $fan_count ='';
                $date ='';
             
   
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['page_name'])) $page_name =$row['page_name'];
                if(isset($row['fan_count'])) $fan_count = $row['fan_count'];
                if(isset($row['date'])) $date = $row['date'];
                if(isset($row['reaction'])) $reaction =$row['reaction'];
          
                 $data_followers[] =[
                    'id' => $id,
                    'page_name' =>$page_name,
                    'fan_count' => $fan_count,
                    'date' =>$date
                    ];                 

  }
$result = DB::table('temp_81_followers')->insert($data_followers);

 $pages_result=MongoPages::raw(function ($collection) use($filter,$id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
          ['page_name' => ['$in' => $id_array ] ] 
            
      ]

        ]  
           ]
    
    ]);
})->toArray();
 $data_pages=[];
 foreach ($pages_result as  $key => $row) {
                $page_name ='';
                $about ='';
                $name ='';
                $category ='';
                $sub_category ='';
 
                if(isset($row['page_name'])) $page_name =$row['page_name'];
                if(isset($row['name'])) $name =$row['name'];
                if(isset($row['about'])) $about = $row['about'];
                if(isset($row['category'])) $category = $row['category'];
                if(isset($row['sub_category'])) $sub_category = $row['sub_category'];
              //  if(isset($row['reaction'])) $reaction =$row['reaction'];
          

                  $data_pages[] =[
                    'page_name' => $page_name,
                    'name' =>$name,
                    'about' => $about,
                    'category' =>$category,
                    'sub_category' =>$sub_category,
                    ];                 

  }
$result = DB::table('temp_81_pages')->insert($data_pages);
//dd($pages_result);
 $comment_result=Comment::raw(function ($collection) use($filter) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             $filter
            
      ]

        ]  
           ]
    
    ]);
})->toArray();
 
  $data_comment=[];
 foreach ($comment_result as  $key => $row) {
                $id ='';
                $message ='';
                $post_id ='';
                $comment_count =0;
   
                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['message'])) $message = preg_replace('/(\r\n|\r|\n)+/', " ",$row['message']);
                if(isset($row['post_id'])) $post_id = $row['post_id'];
                if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
              //  if(isset($row['reaction'])) $reaction =$row['reaction'];
          

                  $data_comment[] =[
                    'id' => $id,
                    'created_time' =>$datetime,
                    'message' => $message,
                    'post_id' =>$post_id,
                    'comment_count' =>$comment_count,
                    ];                 

  }
  $result = DB::table('temp_81_comments')->insert($data_comment);
    }


//region for local function

function unique_multidim_array($array, $key) { 
    $temp_array = array(); 
    $i = 0; 
    $key_array = array(); 
    
    foreach($array as $val) { 
        if (!in_array($val[$key], $key_array)) { 
            $key_array[$i] = $val[$key]; 
            $temp_array[$i] = $val; 
        } 
        $i++; 
    } 
    return $temp_array; 
} 

function check_array_value($to_find, $array_values, $strict = false) {
    foreach ($array_values as $key =>$item) {
        if (($strict ? $item === $to_find : $item == $to_find) || (is_array($item) && $this->check_array_value($to_find, $item, $strict))) {
          $key = (int) $key+1;
            return $key;
        }
    }
    return 0;
}
function rangeMonth ($datestr) {
   date_default_timezone_set (date_default_timezone_get());
   $dt = strtotime ($datestr);
   return array (
     "start" => date ('Y-m-d', strtotime ('first day of this month', $dt)),
     "end" => date ('Y-m-d', strtotime ('last day of this month', $dt))
   );
 }

 function rangeWeek ($datestr) {
   date_default_timezone_set (date_default_timezone_get());
   $dt = strtotime ($datestr);
   return array (
     "start" => date ('N', $dt) == 1 ? date ('Y-m-d', $dt) : date ('Y-m-d', strtotime ('last monday', $dt)),
     "end" => date('N', $dt) == 7 ? date ('Y-m-d', $dt) : date ('Y-m-d', strtotime ('next sunday', $dt))
   );
 }

    function replace_null($value, $replace) {
    if (is_null($value)) return $replace;
    return $value;
}

function sumofarray($arr)
{
  $result=0;
   foreach($arr as $i => $match)
    {
    
    if(strpos($match,'K') !== false)
        {
        $new = str_replace("K", "", $match);
         $new = 1000*$new;
        }
        else
        {
         $new=$match;   
        }

       
       $result = $result + (float)$new;
 
    }

    return $result;
}

   
function convertKtoThousand($value)
{
  $result=0;
 
    if(!empty($value[0]))
    {

    if(strpos($value[0],'K') !== false)
        {
        $new = str_replace("K", "", $value[0]);
         $new = 1000*$new;
        }
        else
        {
         $new=$value[0];   
        }

       
       $result = $result + (float)$new;
    }
    

    return $result;
}
//end region for local function


}


