<?php

namespace App\Http\Controllers;

use App\CompanyInfo;
use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\Notifications\NewTagNotification;
use Illuminate\Support\Facades\Input;

class CompanyInfoController extends Controller
{
    use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data,$id="")
    {
        return Validator::make($data, [
            'name' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            
           
        ]);
    }
    public function index()
    {     
          $data = [];
          $title="CompanyInfo";
          $source='in';
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
          $companyInfo = CompanyInfo::all();

          if(count($companyInfo)>0)
          $data = $companyInfo[0];
         

            return view('company')->with('project_data',$project_data)
                                    ->with('permission_data',$permission_data )
                                    ->with('count',$count)
                                    ->with('source',$source)
                                    ->with('title',$title)
                                    ->with('data',$data);
    }

    public function getCompanyInfo()
    {
        $data = [];
        $companyInfo = CompanyInfo::all();

          if(count($companyInfo)>0)
          $data = $companyInfo[0];
      echo json_encode(array($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
      //dd($request->logo_path);
      // $file = $request->file('image');
      // print_r($file->getClientOriginalName());
      // return;
                   
        // if (file_exists($path)) {
        //     unlink($path) ;
        // }  
   $this->validate($request, [
            'name'=> 'required',
            'phone'=> 'required',
            'address'=> 'required',
        ]);
          $save_path='';
          $logo_path = $request->logo_path;

         if($logo_path){
            $extension = \File::extension($request->logo_path->getClientOriginalName());
            $save_path = "companyLogo.". $extension;

            $folder_path=public_path('Logo/');
            $file_path1=$folder_path . $save_path;
            if (file_exists($file_path1)) {
            unlink($file_path1) ;
        }  
             
            $request->logo_path->move(public_path('Logo'), $save_path);
        }
// dd($save_path);
       CompanyInfo::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'logo_path' => $save_path
          
        ]);
         
        /*$this->guard()->login($user);*/
        $project_data=$this->getProject();
        $count=$this->getProjectCount($project_data);
       return redirect()->route('companyInfo.index')
                        ->with('message','Record update successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyInfo  $companyInfo
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyInfo $companyInfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyInfo  $companyInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyInfo $companyInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyInfo  $companyInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'name'=> 'required',
            'phone'=> 'required',
            'address'=> 'required',
        ]);

//dd($request->logo_path->getClientOriginalName());
        $logo_path = $request->logo_path;

       if($logo_path){
       $item =companyInfo::find($id);
       $folder_path=public_path('Logo/');
       $file_path1=$folder_path . $item->logo_path;
       
   
        if(file_exists($file_path1) && !empty($item->logo_path))
                {
              unlink($file_path1);
                }
            $extension = \File::extension($request->logo_path->getClientOriginalName());
            $input_p['logo_path'] ="companyLogo.". $extension;
          
            $request->logo_path->move(public_path('Logo'), $input_p['logo_path']);
        }

        $input_p['name']=$request->name;
        $input_p['phone']=$request->phone;
        $input_p['address']=$request->address;
      //  $input_p['logo_path']=$request->logo_path;
     
    
        CompanyInfo::find($id)->update($input_p);
        return redirect()->route('companyInfo.index')
                        ->with('message','Record update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyInfo  $companyInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyInfo $companyInfo)
    {
        //
    }
}
