<?php

namespace App\Http\Controllers;

use App\tags;
use App\User;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Notifications\NewTagNotification;
use Illuminate\Support\Facades\Input;
class tagsController extends Controller
{
     use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected function validator(array $data,$id="")
    {
        return Validator::make($data, [
            'name' => 'required|string|max:15|unique:tags,name,'.$id,
            'keywords' => 'required|string'
           
        ]);
    }
    public function index()
    {
        
          $title="TAG";
          $source='in';
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
            return view('tag_entry')->with('project_data',$project_data)
                                    ->with('permission_data',$permission_data )
                                    ->with('count',$count)
                                    ->with('source',$source)
                                    ->with('title',$title);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validator($request->all())->validate();
       


       tags::create([
            'name' => $request->name,
            'keywords' =>$request->keywords
          
        ]);
                 /*$this->guard()->login($user);*/
        $project_data=$this->getProject();
        $count=$this->getProjectCount($project_data);
      
        $users = User::all();
         $login_user = auth()->user()->id;
      
      // if(\Notification::send($users,new NewTagNotification(tags::latest('id')->first())))
      //     if(\Notification::send($users,new NewTagNotification(tags::all())))
      // {
      //   return redirect('tags')->with('project_data',$project_data)
      //                               ->with('count',$count)
      //                               ->with('message', 'Successfully created a new account.');
      // }
         return redirect('tags')->with('project_data',$project_data)
                                    ->with('count',$count)
                                    ->with('message', 'Successfully created a new tag.');

        
    }
    public function quick_tag()
    {
       $name= Input::post("name");
       $keywords=Input::post("keywords");
if (tags::where('name', '=', $name)->exists()) {
   return "exist";
}
         tags::create([
            'name' => $name,
            'keywords' => $keywords
          
        ]);
        return;

    }

    public function notification()
    {
        return auth()->user()->unreadNotifications;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function show(tags $tags)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $title="Tag";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission();
          $data = tags::find($id);
          $source='in';
         return view('tag_entry',compact('data','title','project_data','count','source','permission_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
         $this->validator($request->all(),$id)->validate();

        $input_p['name']=$request->name;
        $input_p['keywords']=$request->keywords;
     
    
        tags::find($id)->update($input_p);
        return redirect()->route('tags.index')
                        ->with('message','Record update successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tags  $tags
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $brand_id=0;
        $project_data = $this->getProject();
        if(count($project_data)>0)
          {
          $brand_id = $project_data[0]['id'];
          }
          // dd($brand_id);
        $tags = tags::find($id);
        if(!empty($tags))
        {
        $tagName=$tags->name;
        $con1="SELECT  id,tags  FROM temp_".$brand_id."_inbound_comments  where tags <> '' and tags Like '%,".$tagName."%'";
        $con1_result = DB::select($con1);
        if(count($con1_result)>0)
        {
        $update="UPDATE temp_17_inbound_comments
                 SET tags  = REPLACE(tags,',".$tagName."','') 
                 WHERE tags like '%,".$tagName."%';";
        $update_result = DB::select($update);
             
        }
        else
        {
        $con2="SELECT  id,tags  FROM temp_".$brand_id."_inbound_comments  where tags <> '' and tags Like '%".$tagName.",%'";
        $con2_result = DB::select($con2);
             if(count($con2_result)>0)
            {
            $update="UPDATE temp_17_inbound_comments
                     SET tags  = REPLACE(tags,'".$tagName.",','') 
                     WHERE tags like '%".$tagName.",%';";
            $update_result = DB::select($update);
                 
            }       

            else
            {
            $con3="SELECT  id,tags  FROM temp_".$brand_id."_inbound_comments  where tags <> '' and tags Like '%".$tagName."%'";
            $con3_result = DB::select($con3);
                if(count($con3_result)>0)
                {
                    
                $update="UPDATE temp_17_inbound_comments
                         SET tags  = REPLACE(tags,'".$tagName."','') 
                         WHERE tags like '%".$tagName."%';";
                $update_result = DB::select($update);
                }
            }

        }
         tags::find($id)->delete();
            
         
           
        }
         
        return redirect()->route('tags.index')
                        ->with('success','Record deleted successfully');
    }
     public function gettaglist()
   {
     $source='in';
     $taglist = tags::select(['id', 'name', 'keywords'])->orderBy('id');

     return Datatables::of($taglist)
       ->addColumn('action', function ($taglist) use($source) {
                return '<a href="'. route('tags.edit', [$taglist->id]) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
              <a href="'. route('deletetag',$taglist->id) .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            })

    /* ->addColumn('action', function ($booking) {
        return '<a href="'. route('deleteBooking',$booking->id) .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

    })*/




     ->make(true);
           // ->rawColumns(['image', 'action'])

 }
}
