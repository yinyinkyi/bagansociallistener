<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\GlobalController;
use Illuminate\Support\Facades\Input;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use GlobalController;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('guest');
    }

 

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
       public function showRegistrationForm()
    {
          /*$this->guard()->login($user);*/

          $title="Register";
           $source=Input::get('source');
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
            return view('auth.register')->with('project_data',$project_data)
                                    ->with('count',$count)
                                    ->with('permission_data',$permission_data)
                                    ->with('source',$source)
                                    ->with('title',$title);
                                
    }

      public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $this->create($request->all());

        /*$this->guard()->login($user);*/
        $project_data=$this->getProject();
        $count=$this->getProjectCount($project_data);

        return redirect('register')->with('project_data',$project_data)
                                    ->with('count',$count)
                                    ->with('message', 'Successfully created a new account.');
    }
}
