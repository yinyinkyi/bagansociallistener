<?php

namespace App\Http\Controllers;

use App\MongodbData;
use App\Comment;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;
use App\Project;
class PageManageController extends Controller
{
    //
    use GlobalController;
    public function getTopAndLatestPost()
    {

    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      $format_type=Input::get('format_type');

      $filter_keyword ='';
      $filter_sentiment=''; $limit=10;
      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

      if(null !== Input::get('limit'))
      $limit=Input::get('limit');

      /*$brand_id=30;*/
 
     

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2017 06 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2017 06 30')));*/

          if(null !== Input::get('filter_page_name'))
           {
             $filter_pages= " page_name in ('".Input::get('filter_page_name')."') ";
           }
           else if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }
       

$add_con='';
if($filter_keyword !== '')
  $add_con = " and posts.message Like '%".$filter_keyword."%'";

if($filter_sentiment !== '')
{
  $add_con .= " and posts.sentiment = '".$filter_sentiment."'";
}

$query='';

// if($format_type == 'top_post')
// {

// $query = "SELECT  (sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry)) total,Liked,Love,Wow,Haha,Sad,Angry,".
// " posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
// " created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.sentiment ".
// " cmt_sentiment,cmts.emotion cmt_emotion, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
// " ELSE 'mdi-star' ".
// " END) isBookMark ".
// "FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id ".
// " WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_con .
// " GROUP BY posts.id,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p'),posts.message,posts.page_name,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.sentiment,cmts.emotion".
// " ORDER by total,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') DESC LIMIT 20";
// echo $query ;

// }
// else
// {
$query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,cmts.checked_sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.checked_predict, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id ".
" WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND". $filter_pages . $add_con .
" ORDER by timestamp(posts.created_time) DESC ";
// echo $query ;

// }

// return;

// dd($query);
$query_result = DB::select($query);

$data = [];
 $data_mention=[];
 foreach ($query_result as  $key => $row) {
 
// $pos=($row->cmt_sentiment=='pos' && $row->parent=== '' && (int)$row->checked_predict===1)?1:0 ;
// $neg=($row->cmt_sentiment=='neg' && $row->parent=== '' && (int)$row->checked_predict===1)?1:0;
// $anger =($row->cmt_emotion=='anger' && $row->parent==='' && (int)$row->checked_predict===1)?1:0 ;
// $interest=($row->cmt_emotion=='interest' && $row->parent==='' && (int)$row->checked_predict===1)?1:0;
// $disgust=($row->cmt_emotion=='disgust' && $row->parent ==='' && (int)$row->checked_predict===1)?1:0;
// $fear=($row->cmt_emotion=='fear' && $row->parent=== '' && (int)$row->checked_predict===1)?1:0;
// $joy=($row->cmt_emotion=='joy' && $row->parent==='' && (int)$row->checked_predict===1)?1:0;
// $like=($row->cmt_emotion=='like' && $row->parent==='' && (int)$row->checked_predict===1)?1:0;
// $love=($row->cmt_emotion=='love' && $row->parent==='' && (int)$row->checked_predict===1)?1:0;
// $neutral=($row->cmt_emotion=='neutral' && $row->parent==='' && (int)$row->checked_predict===1)?1:0;
// $sadness=($row->cmt_emotion=='sadness' && $row->parent==='' && (int)$row->checked_predict===1)?1:0;
// $surprise=($row->cmt_emotion=='surprise' && $row->parent==='' && (int)$row->checked_predict===1)?1:0;
// $trust=($row->cmt_emotion=='trust' && $row->parent==='' && (int)$row->checked_predict===1)?1:0;
  $pos=($row->cmt_sentiment=='pos' && $row->parent=== '' )?1:0 ;
$neg=($row->cmt_sentiment=='neg' && $row->parent=== '' )?1:0;
$anger =($row->cmt_emotion=='anger' && $row->parent==='' )?1:0 ;
$interest=($row->cmt_emotion=='interest' && $row->parent==='' )?1:0;
$disgust=($row->cmt_emotion=='disgust' && $row->parent ==='')?1:0;
$fear=($row->cmt_emotion=='fear' && $row->parent=== '' )?1:0;
$joy=($row->cmt_emotion=='joy' && $row->parent==='')?1:0;
$like=($row->cmt_emotion=='like' && $row->parent==='' )?1:0;
$love=($row->cmt_emotion=='love' && $row->parent==='' )?1:0;
$neutral=($row->cmt_emotion=='neutral' && $row->parent==='' )?1:0;
$sadness=($row->cmt_emotion=='sadness' && $row->parent==='' )?1:0;
$surprise=($row->cmt_emotion=='surprise' && $row->parent==='' )?1:0;
$trust=($row->cmt_emotion=='trust' && $row->parent==='')?1:0;
 
   
   $message=$this->readmore($row->message);
   $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');
   $total = (int)$row->Liked + (int)$row->Love + (int)$row->Wow + (int)$row->Haha + (int)$row->Sad+ (int)$row->Angry;
   //$message=$row->message;
            if (isset($row ->id))
   {
                 $id= $row ->id;
   
    if (!array_key_exists($id , $data)) {
    $data[$id] = array(
                'id'     =>$row->id,
                'message' => $message,
                'page_name' =>$row->page_name,
                'created_time' =>$row->created_time,
                'link' =>$row->link,
                'full_picture' =>$row->full_picture,
                'sentiment' =>$row->sentiment,
                'emotion' =>$row->emotion,
                'positive'=> $pos,'negative'=>$neg,
                'liked'=>$row->Liked ,'loved'=>$row->Love,'haha'=>$row->Haha,'wow'=>$row->Wow,'sad'=>$row->Sad,'angry'=>$row->Angry,
                'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                'format_type'=>$format_type,
                'total'=>$total,
                'isBookMark'=>$row->isBookMark
            );
}
else
{
 $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
 $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
 $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
 $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
 $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
 $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
 $data[$id]['like'] = (int) $data[$id]['like'] + $like;
 $data[$id]['love'] = (int) $data[$id]['love'] + $love;
 $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
 $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
 $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
 $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;



}
//echo $data[$id]['negative'];echo $neg;return;

}
 
            }

            $data_mention=$data;
// dd($data);

if($format_type === "top_post")
{
   usort($data_mention, function($a1, $a2) {
   $value1 = strtotime($a1['total']);
   $value2 = strtotime($a2['total']);
   return $value2 - $value1;
});

}
// else
// {
//     usort($data_mention, function($a1, $a2) {
//    $value1 = strtotime($a1['created_time']);
//    $value2 = strtotime($a2['created_time']);
//    return $value2 - $value1;
// });
// }
    // dd($data_mention);
// print_r($data_mention);
// return;
$data_mention = array_slice($data_mention,0,$limit);            


/*$id='799069593563429_1007801336023586';
echo $data[$id]['negative'];
 print_r($data_mention);
 return;
 $data_mention = $this->unique_multidim_array($data_mention,'id'); */

$permission_data = $this->getPermission();
 return Datatables::of($data_mention)
       ->addColumn('action', function ($data_mention) {
                return '<a href="javascript:void(0)" id="'.$data_mention['id'].'" name="'.$data_mention['isBookMark'].'" class="mdi '.$data_mention['isBookMark'].' text-yellow btn-bookmark" style="float:right;color:#f39c12"></a>';
            }) 

       ->addColumn('post_div', function ($data_mention) use($permission_data){
               $html= '<div class="profiletimeline"><div class="sl-item">
                                    <div class="sl-left"> <img src="'.$data_mention['full_picture'].'" alt="user" class="img-circle  img-bordered-sm"> </div> 
                                    <div class="sl-right"> <div><a href="'.$data_mention['link'].'" class="link"  target="_blank">'.$data_mention['page_name'].'</a>
              <span class="sl-date">'.$data_mention['created_time'].'  </span> ';
              if($data_mention['positive'] >  $data_mention['negative'])
              {
                $html.=' | Overall Customer Feedback <span class="label label-info">Positive</span>';
              }
              else
              {
                $html.=' | Overall Customer Feedback <span class="label label-danger">Negative</span>';
              }
              // $html= '<p>Sentiment : <select class="form-control custom-select sentiment-color customize-select" id="sentiment_'.$data_mention['format_type'].'_'.$data_mention['id'].'"  ><option value="pos"';
              //   if($data_mention['sentiment'] == "pos")
              //   $html.= 'selected="selected"';
              //   $html.= '>pos</option><option value="neg"';
              //   if($data_mention['sentiment'] == "neg")
              //   $html.= 'selected="selected"';     
              //   $html.= '>neg</option><option value="neutral"';
              //   if($data_mention['sentiment'] == "neutral")
              //   $html.= 'selected="selected"';  
              //   $html.= '>neutral</option><option value="NA"';
              //   if($data_mention['sentiment'] == "NA")
              //   $html.= 'selected="selected"';
              //   $html.= '>NA</option></select>';

              //   $html.= ' | Emotion : <select class="form-control custom-select emotion-color customize-select" id="emotion_'.$data_mention['format_type'].'_'.$data_mention['id'].'" ><option value="anger"';
              //   if($data_mention['emotion'] == "anger")
              //   $html.= 'selected="selected"';
              //   $html.= '>anger</option><option value="interest"';
              //   if($data_mention['emotion'] == "interest")
              //   $html.= 'selected="selected"';     
              //   $html.= '>interest</option><option value="disgust"';
              //   if($data_mention['emotion'] == "disgust")
              //   $html.= 'selected="selected"';  
              //   $html.= '>disgust</option><option value="fear"';
              //   if($data_mention['emotion'] == "fear")
              //   $html.= 'selected="selected"'; 
              //   $html.= '>fear</option><option value="joy"';
              //   if($data_mention['emotion'] == "joy")
              //   $html.= 'selected="selected"'; 
              //   $html.= '>joy</option><option value="like"';
              //   if($data_mention['emotion'] == "like")
              //   $html.= 'selected="selected"'; 
              //   $html.= '>like</option><option value="love"';
              //   if($data_mention['emotion'] == "love")
              //   $html.= 'selected="selected"'; 
              //   $html.= '>love</option><option value="neutral"';
              //   if($data_mention['emotion'] == "neutral")
              //   $html.= 'selected="selected"';
              //   $html.= '>neutral</option><option value="sadness"';
              //   if($data_mention['emotion'] == "sadness")
              //   $html.= 'selected="selected"';
              //   $html.= '>sadness</option><option value="surprise"';
              //   if($data_mention['emotion'] == "surprise")
              //   $html.= 'selected="selected"';
              //   $html.= '>surprise</option><option value="trust"';
              //   $html.= '>sadness</option><option value="trust"';
              //   if($data_mention['emotion'] == "trust")
              //   $html.= 'selected="selected"';
              //   $html.= '>trust</option><option value="NA"';
              //   if($data_mention['emotion'] == "NA")
              //   $html.= 'selected="selected"';
              //   $html.= '>NA</option></select>';
              //   if($permission_data['edit'] === true)
              //   $html.= ' <a class="edit_post_'.$data_mention['format_type'].'_predict" id="'.$data_mention['id'].'" href="javascript:void(0)"><i class="ti-pencil-alt"></i></a></span></p>';
                                                    
                                                     
                $html.='<p class="m-t-10">'.$data_mention['message'].'...<a href="'.$data_mention['link'].'" target="_blank"> Read More
                                      </a></p></div></div>
                                      <p>👍 '.$this->thousandsCurrencyFormat($data_mention['liked']).' ❤️ '.$this->thousandsCurrencyFormat($data_mention['loved']).' 😆 '.$this->thousandsCurrencyFormat($data_mention['haha']).' 😮 '.$this->thousandsCurrencyFormat($data_mention['wow']).' 😪 '.$this->thousandsCurrencyFormat($data_mention['sad']).' 😡 '.$this->thousandsCurrencyFormat($data_mention['angry']).'</p></div></div>';
if($data_mention['positive']>0 || $data_mention['negative']>0 || $data_mention['anger']>0  || $data_mention['interest']>0 || $data_mention['disgust']>0 || $data_mention['fear']>0 || $data_mention['joy']>0 || $data_mention['like']>0 || $data_mention['love']>0 || $data_mention['neutral']>0 || $data_mention['sadness']>0 || $data_mention['surprise']>0|| $data_mention['trust']>0){                                   
                 $html.= '<div><div style="margin:0 0 0 70px;"> <form>
                  <fieldset>
                      <legend>Customer Feedback</legend>';
                    }
             if($data_mention['positive']>0){ //your condition
           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#dd4b39" class="popup" id="'.$data_mention['id'].'" name="Positive">positive : </a> 
                  <span class="label label-light-danger">'.$data_mention['positive'].'</span> ';
                  
         } 
             if($data_mention['negative']>0){ //your condition
           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#dd4b39" class="popup" id="'.$data_mention['id'].'" name="Negative">negative : </a>
                <span class="label label-light-danger" style="font-size:15px">'.$data_mention['negative'].'</span> ';
                  
         } 
        
//                 if($data_mention['anger']>0){  
//           $html.='<a href="javascript:void(0)" style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="anger">anger : </a> 
//                   <span class="label label-light-danger">'.$data_mention['anger'].'</span> ';                 
//                }
//                 if($data_mention['interest']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="interest">interest : </a> 
//                   <span class="label label-light-danger">'.$data_mention['interest'].'</span> ';                 
//                }
//                 if($data_mention['disgust']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="disgust">disgust : </a> 
//                   <span class="label label-light-danger">'.$data_mention['disgust'].'</span> ';                 
//                }
//                 if($data_mention['fear']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="fear">fear : </a> 
//                   <span class="label label-light-danger">'.$data_mention['fear'].'</span> ';                 
//                }
//                 if($data_mention['joy']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="joy">joy : </a> 
//                   <span class="label label-light-danger">'.$data_mention['joy'].'</span> ';                 
//                }
//                 if($data_mention['like']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="like">like : </a> 
//                   <span class="label label-light-danger">'.$data_mention['like'].'</span> ';                 
//                }
//                 if($data_mention['love']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="love">love : </a> 
//                   <span class="label label-light-danger">'.$data_mention['love'].'</span> ';                 
//                }
//                 if($data_mention['neutral']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="neutral">neutral : </a> 
//                   <span class="label label-light-danger">'.$data_mention['neutral'].'</span> ';                 
//                }
//                 if($data_mention['sadness']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="sadness">sadness : </a> 
//                   <span class="label label-light-danger">'.$data_mention['sadness'].'</span> ';                 
//                }
//                 if($data_mention['surprise']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="surprise">surprise : </a> 
//                   <span class="label label-light-danger">'.$data_mention['surprise'].'</span> ';                 
//                }
//                   if($data_mention['trust']>0){  
//           $html.='<a href="javascript:void(0)"  style="font-size:15px;color:#55acee" class="popup" id="'.$data_mention['id'].'" name="trust">trust : </a> 
//                   <span class="label label-light-danger">'.$data_mention['trust'].'</span> </fieldset>
// </form></div></div>';                 
//                }
                return $html;
               

            })
        
      ->rawColumns(['post_div','action'])
   
      ->make(true);

          //  echo json_encode($data);
    }

    public function getRelatedcomment()
    {

    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
   
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment ='';
      $is_competitor = "false";
    
      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment');

      if(null !== Input::get('is_competitor'))
      $is_competitor=Input::get('is_competitor'); 
    // dd
    // dd($is_competitor);
      // global filter
       $add_global_filter='';
      if(null !== Input::get('tsearch_senti'))
      {
        $global_senti=Input::get('tsearch_senti'); 
        if($global_senti == 'neutral')
        $add_global_filter .=" and (cmts.checked_sentiment = '".$global_senti."' or cmts.checked_sentiment = 'NA')";
        else
        $add_global_filter .=" and cmts.checked_sentiment = '".$global_senti."'";
      }
       if(null !== Input::get('tsearch_tag'))
      {
        $global_tag=Input::get('tsearch_tag'); 
        $add_global_filter .=" and FIND_IN_SET('".$global_tag."', checked_tags) >0";
      }

      if(null !== Input::get('tsearch_emo'))
      {
        $global_emo=Input::get('tsearch_emo');
          $add_global_filter .=" and cmts.emotion = '".$global_emo."'";

         }
         if(null !== Input::get('tsearch_interest') && Input::get('tsearch_interest') === "true")
      {
        // echo "hihi";
          $add_global_filter .=" and cmts.interest = 1";

         }
     

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
// $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
// $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));

            if(null !== Input::get('filter_page_name'))
           {
             $filter_pages= " page_name in ('".Input::get('filter_page_name')."') ";
           }
           else if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

$add_con='';

if($filter_keyword !== '')
  $add_con = " and cmts.message Like '%".$filter_keyword."%'";

if($filter_sentiment !== '')

  $add_con .= " and cmts.checked_sentiment = '".$filter_sentiment."'";

if(null !== Input::get('post_id'))
 $add_con = "and cmts.post_id='".Input::get('post_id')."'"; 

$tags =$this->gettags();
/*print_r($tags);
return ;*/
$query="SELECT pros.id profileID,pros.type as profileType,pros.name as profileName,cmts.checked_tags tags,cmts.post_id,cmts.id,cmts.message,link,DATE_FORMAT(cmts.created_time, '%d-%m-%Y %h:%i:%s %p') created_time, ".
" cmts.checked_sentiment sentiment,cmts.emotion,posts.page_name,IF(cmts.sentiment = cmts.checked_sentiment,'','human predict!') hp,IF(cmts.tags = cmts.checked_tags,'','human predict!') hp_tags,action_status,action_taken ".
" FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN  temp_".$brand_id."_inbound_posts  posts on posts.id=cmts.post_id ".
" LEFT JOIN temp_profiles pros on cmts.profile=pros.temp_id ".
" WHERE posts.id IS NOT NULL".
" and  parent='' AND  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ". $filter_pages . $add_con . $add_global_filter.

" ORDER by timestamp(cmts.created_time) DESC LIMIT 500";
// dd($query);
 // print_r($query);
 // return;
$data = DB::select($query);
 $project = Project::find($brand_id);
 $monitor_pages = $project->monitor_pages;
 $monitor_pages = explode(',', $monitor_pages);

$permission_data = $this->getPermission(); 
if($is_competitor == "false")
{
  return Datatables::of($data)
        
       ->addColumn('post_div', function ($data)   use($tags,$permission_data,$monitor_pages) {
        $page_name=$data->page_name ;
          if(is_numeric($page_name))
          {
             $input = preg_quote($page_name, '~'); // don't forget to quote input string!
             $keys = preg_grep('~' . $input . '~', $monitor_pages);
             $vals = array();
                foreach ( $keys as $key )
                {
                   $page_name= $key;
                }
               
          }
        //generate comment link
        $profilePhoto='';$ProfileName='';
        $profileLink='javascript:void(0)';
        $fb_p_id=explode('_', $data->post_id);
        $fb_p_id=$fb_p_id[1];
        $fb_c_id=explode('_', $data->id);
        $fb_c_id=$fb_c_id[1];
          if($data->profileName <> '') $ProfileName=$data->profileName; 
          else if($data->profileID <> '')  $ProfileName=$data->profileID;
          else  $ProfileName=$page_name;

        if($data->profileType === 'number')
        {
          $profilePhoto='https://graph.facebook.com/'.$data->profileID.'/picture?type=large';
          $ProfileName=$page_name;
        }
        
        if(isset($data->profileID))
        $profileLink='https://www.facebook.com/'.$data->profileID;

      $comment_Link= 'https://www.facebook.com/' . $page_name .'/posts/' . $fb_p_id .'?'.'comment_id='.$fb_c_id;
        $classforbtn='';$stylefora='';$bgcolor='';
              if($data->sentiment=='neg')
                {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
              else if($data->sentiment=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}
                $html = '<div class="profiletimeline"><div class="sl-item">';
                if($profilePhoto<>'')
                $html .= '<div class="sl-left user-img"> <img src="'. $profilePhoto.'" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                else
                /*$html .= '<div class="sl-left user-img"> <span class="round"><i class="mdi mdi-comment-account"></i></span> </div>';*/
              $html .= '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
               
                 $html .='<div class="form-material sl-right"><div> 
              <a href="'.$profileLink.'" class="link" target="_blank">'.$ProfileName.'</a>';
         

                if($data->hp <> '')

                $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Human Predict" data-toggle="tooltip"></i></a>';
               $html.='<div class="sl-right"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
              
               $html.='<p>Sentiment : <select id="sentiment_'.$data->id.'"  class="form-material form-control in-comment-select edit_senti ';
               if($data->sentiment=="")$html.='"><option value="" selected="selected" ></option>';
               if($data->sentiment=="pos")$html.='text-success" >';
               if($data->sentiment=="neg")$html.='text-red" >';
               if($data->sentiment=="neutral" || $data->sentiment == "NA")$html.='text-warning" >';
            
                $html.='<option value="pos" class="text-success"';
                if($data->sentiment == "pos")
                $html.= 'selected="selected"';
                $html.= '>Positive</option><option value="neg" class="text-red"';
                if($data->sentiment == "neg")
                $html.= 'selected="selected"';
                $html.= '>Negative</option><option value="neutral" class="text-warning"';
                if($data->sentiment == "neutral" || $data->sentiment == "NA")
                $html.= 'selected="selected"';      
                $html.= '>Neutral</option></select>';

                // $html.= ' | Emotion : <select class="form-control custom-select emotion-color in-comment-select" id="emotion_'.$data->id.'" >';
                // if($data->emotion == "")
                // $html.= '<option value="" selected="selected"></option>';
                // $html.='<option value="anger"';
                // if($data->emotion == "anger")
                // $html.= 'selected="selected"';
                // $html.= '>anger</option><option value="interest"';
                // if($data->emotion == "interest")
                // $html.= 'selected="selected"';     
                // $html.= '>interest</option><option value="disgust"';
                // if($data->emotion == "disgust")
                // $html.= 'selected="selected"';  
                // $html.= '>disgust</option><option value="fear"';
                // if($data->emotion == "fear")
                // $html.= 'selected="selected"'; 
                // $html.= '>fear</option><option value="joy"';
                // if($data->emotion == "joy")
                // $html.= 'selected="selected"'; 
                // $html.= '>joy</option><option value="like"';
                // if($data->emotion == "like")
                // $html.= 'selected="selected"'; 
                // $html.= '>like</option><option value="love"';
                // if($data->emotion == "love")
                // $html.= 'selected="selected"'; 
                // $html.= '>love</option><option value="neutral"';
                // if($data->emotion == "neutral")
                // $html.= 'selected="selected"';
                // $html.= '>neutral</option><option value="sadness"';
                // if($data->emotion == "sadness")
                // $html.= 'selected="selected"';
                // $html.= '>sadness</option><option value="surprise"';
                // if($data->emotion == "surprise")
                // $html.= 'selected="selected"';
                // $html.= '>surprise</option><option value="trust"';
                // if($data->emotion == "trust")
                // $html.= 'selected="selected"';
                // $html.= '>trust</option><option value="NA"';
                // if($data->emotion == "NA")
                // $html.= 'selected="selected"';
                // $html.= '>NA</option></select>';
              
                $html.=' | Tag :<select id="tags_'.$data->id.'" class="form-material form-control  select2 edit_tag" multiple="multiple" data-placeholder="Select tag"
                        style="width:300px;height:36px">';
                 $tag_array=explode(',', $data->tags);
                foreach ($tags as $key => $value) {
                # code...
                  //if (strpos($data->tags, $value->name) !== false)
                 if ( in_array( $value->name,$tag_array))
                 $html.= '<option value="'.$value->name.'" selected="selected">'.$value->name.'</option>';
                  else
                 $html.= '<option value="'.$value->name.'">'.$value->name.'</option>';
                }
                $html.='</select>';
                if($permission_data['edit'] === 1)
                $html.= '<a class="edit_predict" id="'.$data->id.'" href="javascript:void(0)"  style="display:none"><i class="ti-pencil-alt"></i></a> ';
               $html.='<a class="get-code" data-toggle="collapse" href="#tt4" style="font-size:20px" aria-expanded="true" id="add_tag"><i class="mdi mdi-tag-plus" title="Add Tag" data-toggle="tooltip"></i></a>';
                
                $html.='</p><p class="m-t-10">'.$data->message.'</p></div><div class="sl-right" style="float: right;">
              <div style="display: inline-block"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-info popup_post" id="'.$data->post_id.'">See Post</a></div>
               <div style="display: inline-block"><a href="'.$comment_Link.'" class="waves-effect waves-light btn-sm btn-gray" target="_blank">Take Action</a></div>
              <div style="display: inline-block" class="btn-group">';
              if($data->action_status === "Action Taken")
              $html.='<button type="button" class="waves-effect waves-light btn-sm btn-gray dropdown-toggle" id="btnaction_'.$data->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               Action Taken
             </button>';
             else
             $html.='<button type="button" class="waves-effect waves-light btn-sm btn-gray dropdown-toggle" id="btnaction_'.$data->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Require Action
             </button>';
          $html.='<div class="dropdown-menu">
            <a class="dropdown-item" id="dlink_'.$data->id.'" href="javascript:void(0)">Require Action</a>
            <a class="dropdown-item" id="dlink_'.$data->id.'" href="javascript:void(0)">Action Taken</a>

          </div></div></div></div></div>';

                return $html;

            })
  
      ->rawColumns(['post_div'])
   
      ->make(true);
}
else
{
  return Datatables::of($data)
        
       ->addColumn('post_div', function ($data)   use($tags,$permission_data,$monitor_pages) {
         $page_name=$data->page_name ;
          if(is_numeric($page_name))
          {
             $input = preg_quote($page_name, '~'); // don't forget to quote input string!
             $keys = preg_grep('~' . $input . '~', $monitor_pages);
             $vals = array();
                foreach ( $keys as $key )
                {
                   $page_name= $key;
                }
               
          }
        //generate comment link
        $profilePhoto=''; $ProfileName ='';
        $profileLink='javascript:void(0)';
        $fb_p_id=explode('_', $data->post_id);
        $fb_p_id=$fb_p_id[1];
        $fb_c_id=explode('_', $data->id);
        $fb_c_id=$fb_c_id[1];
         if($data->profileName <> '') $ProfileName=$data->profileName; 
          else if($data->profileID <> '')  $ProfileName=$data->profileID;
          else  $ProfileName=$page_name;
          if($data->profileType === 'number')
        {
          $profilePhoto='https://graph.facebook.com/'.$data->profileID.'/picture?type=large';
          $ProfileName=$page_name; 
        }
        if(isset($data->profileID))
        $profileLink='https://www.facebook.com/'.$data->profileID;

      $comment_Link= 'https://www.facebook.com/' . $page_name .'/posts/' . $fb_p_id .'?'.'comment_id='.$fb_c_id;
        $classforbtn='';$stylefora='';$bgcolor='';
              if($data->sentiment=='neg')
                {$classforbtn='label label-danger';$stylefora="color:#ffffff"; } 
              else if($data->sentiment=='pos') {$classforbtn='label label-success';$stylefora="color:#ffffff"; } 
                else{$classforbtn='label';$stylefora="color:#343a40" ;$bgcolor='background:#e9ecef';}
              $html = '<div class="profiletimeline"><div class="sl-item">';
                if($profilePhoto<>'')
                $html .= '<div class="sl-left user-img"> <img src="'. $profilePhoto.'" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
                else
                $html .= '<div class="sl-left user-img"> <img src="assets/images/unknown_photo.jpg" alt="user" class="img-circle  img-bordered-sm"> </div>'; 
           
                 $html .='<div class="form-material sl-right"><div> 
              <a href="'.$profileLink.'" class="link" target="_blank">'.$ProfileName.'</a>';
             
                if($data->hp <> '')

                $html.='<a class="text-danger get-code" data-toggle="collapse" href="#tt4" aria-expanded="true"><i class="mdi mdi-account-edit" title="Human Predict" data-toggle="tooltip"></i></a>';
               $html.='<div class="sl-right"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data->created_time.'</span></div>';
              
               $html.='<p>Sentiment : <select  id="sentiment_'.$data->id.'"  class="form-material form-control in-comment-select edit_senti ';
                if($data->sentiment=="")$html.='"><option value="" selected="selected" ></option>';
               if($data->sentiment=="pos")$html.='text-success" >';
               if($data->sentiment=="neg")$html.='text-red" >';
               if($data->sentiment=="neutral" || $data->sentiment == "NA")$html.='text-warning" >';

                $html.='<option value="pos" class="text-success"';
                if($data->sentiment == "pos")
                $html.= 'selected="selected"';
                $html.= '>Positive</option><option value="neg" class="text-red"';
                if($data->sentiment == "neg")
                $html.= 'selected="selected"';
                $html.= '>Negative</option><option value="neutral" class="text-warning"';
                if($data->sentiment == "neutral" || $data->sentiment == "NA")
                $html.= 'selected="selected"';      
                $html.= '>Neutral</option></select>';

            
               
                $html.='</p><p class="m-t-10">'.$data->message.'</p></div><div class="sl-right" style="float: left;">
              <div style="display: inline-block"><a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm btn-info popup_post" id="'.$data->post_id.'">See Post</a></div>
              </div></div></div>';

                return $html;

            })
  
      ->rawColumns(['post_div'])
   
      ->make(true);
 

          //  echo json_encode($data);
}
    }
    public function getInboundPost()
    {

   //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
  
     

        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2017 06 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2017 06 30')));*/

          if(null !== Input::get('filter_page_name'))
           {
             $filter_pages= " page_name in ('".Input::get('filter_page_name')."') ";
           }
           else if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }
       


$query='';


$query = "SELECT Liked,Love,Wow,Haha,Sad,Angry,replace(shared, ',', '') shared,posts.id,posts.message,posts.page_name,DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p') ".
" created_time,posts.link,posts.full_picture,posts.sentiment,posts.emotion,posts.isDeleted,cmts.checked_sentiment ".
" cmt_sentiment,cmts.emotion cmt_emotion,cmts.parent,cmts.checked_predict, (CASE WHEN posts.isBookMark = 0 THEN 'mdi-star-outline' ".
" ELSE 'mdi-star' ".
" END) isBookMark ".
"FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id ".
" WHERE (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND". $filter_pages  .
" ORDER by timestamp(posts.created_time) DESC ";


$query_result = DB::select($query);

$data = [];
 $data_mention=[];

 $project = Project::find($brand_id);
 $monitor_pages = $project->monitor_pages;
 $monitor_pages = explode(',', $monitor_pages);


 foreach ($query_result as  $key => $row) {
  //change page name from id to orginal user register name.
  $page_name=$row->page_name;
          if(is_numeric($page_name))
          {
             $input = preg_quote($page_name, '~'); // don't forget to quote input string!
             $keys = preg_grep('~' . $input . '~', $monitor_pages);
             $vals = array();
                foreach ( $keys as $key )
                {
                   $page_name= $key;
                }
               
          }

$pos=($row->cmt_sentiment=='pos' && $row->parent=== '' )?1:0 ;
$neg=($row->cmt_sentiment=='neg' && $row->parent=== '' )?1:0;
$cmtneutral=($row->cmt_sentiment=='neutral' && $row->parent=== '' )?1:0;
$cmtNA=($row->cmt_sentiment=='NA' && $row->parent=== '' )?1:0;
$cmtneutral=(int)$cmtneutral+(int) $cmtNA;
$anger =($row->cmt_emotion=='anger' && $row->parent==='' )?1:0 ;
$interest=($row->cmt_emotion=='interest' && $row->parent==='' )?1:0;
$disgust=($row->cmt_emotion=='disgust' && $row->parent ==='')?1:0;
$fear=($row->cmt_emotion=='fear' && $row->parent=== '' )?1:0;
$joy=($row->cmt_emotion=='joy' && $row->parent==='')?1:0;
$like=($row->cmt_emotion=='like' && $row->parent==='' )?1:0;
$love=($row->cmt_emotion=='love' && $row->parent==='' )?1:0;
$neutral=($row->cmt_emotion=='neutral' && $row->parent==='' )?1:0;
$sadness=($row->cmt_emotion=='sadness' && $row->parent==='' )?1:0;
$surprise=($row->cmt_emotion=='surprise' && $row->parent==='' )?1:0;
$trust=($row->cmt_emotion=='trust' && $row->parent==='')?1:0;
 
   
   $message=$this->readmore($row->message);
  $message=mb_convert_encoding($message, 'UTF-8', 'UTF-8');

   $total = (int)$row->Liked + (int)$row->Love + (int)$row->Wow + (int)$row->Haha + (int)$row->Sad+ (int)$row->Angry;
   //$message=$row->message;
            if (isset($row ->id))
   {
      $comment_count=($row->parent==='')?1:0;

                 $id= $row ->id;
   
    if (!array_key_exists($id , $data)) {
    $data[$id] = array(
                'id'     =>$row->id,
                'message' => $message,
                'page_name' =>$page_name,
                'created_time' =>$row->created_time,
                'link' =>$row->link,
                'full_picture' =>$row->full_picture,
                'sentiment' =>$row->sentiment,
                'emotion' =>$row->emotion,
                'positive'=> $pos,'negative'=>$neg,'cmtneutral'=>$cmtneutral,
                'liked'=>$row->Liked ,'loved'=>$row->Love,'haha'=>$row->Haha,'wow'=>$row->Wow,'sad'=>$row->Sad,'angry'=>$row->Angry,'shared'=>$row->shared,
                'anger'=>$anger ,'interest'=>$interest,'disgust'=>$disgust,
                'fear'=>$fear,'joy'=>$joy,'like' =>$like,'love'=>$love,'neutral'=>$neutral,
                'sadness'=>$sadness,'surprise'=>$surprise,'trust'=>$trust,
                'total'=>$total,
                'comment_count'=>$comment_count,
                'isDeleted'=>$row->isDeleted ,
                'isBookMark'=>$row->isBookMark
            );
}
else
{
 $data[$id]['positive'] = (int) $data[$id]['positive'] +$pos;
 $data[$id]['negative'] = (int) $data[$id]['negative'] +$neg;
 $data[$id]['cmtneutral'] = (int) $data[$id]['cmtneutral'] +$cmtneutral;
 $data[$id]['anger'] = (int) $data[$id]['anger'] + $anger;
 $data[$id]['interest'] = (int) $data[$id]['interest'] +$interest;
 $data[$id]['disgust'] = (int) $data[$id]['disgust'] + $disgust;
 $data[$id]['joy'] = (int) $data[$id]['joy'] +$joy;
 $data[$id]['like'] = (int) $data[$id]['like'] + $like;
 $data[$id]['love'] = (int) $data[$id]['love'] + $love;
 $data[$id]['neutral'] = (int) $data[$id]['neutral'] +$neutral;
 $data[$id]['sadness'] = (int) $data[$id]['sadness'] +$sadness;
 $data[$id]['surprise'] = (int) $data[$id]['surprise'] + $surprise;
 $data[$id]['trust'] = (int) $data[$id]['trust'] + $trust;
 $data[$id]['comment_count'] = (int) $data[$id]['comment_count'] + $comment_count;



}
//echo $data[$id]['negative'];echo $neg;return;

}
 
            }

             if(null !== Input::get('filter_overall'))
            {
              if( Input::get('filter_overall') === 'pos')//
              {     foreach ($data as $key => $value) {

        if( (int)$value['positive'] <  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'])
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
                }
              else if( Input::get('filter_overall') === 'neg')
              {
           foreach ($data as $key => $value) {

            if((int)$value['positive'] >  (int)$value['negative'] || (int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 || (int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0 || (int)$value['positive'] ==  (int)$value['negative'] &&  (int)$value['cmtneutral'] <> 0)
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
              }
               else if( Input::get('filter_overall') === 'neutral')
              {
                          foreach ($data as $key => $value) {

                       if( ! ( ((int)$value['positive'] ==  0 && (int)$value['negative'] == 0 && (int)$value['cmtneutral']  == 0 ) || ((int)$value['positive'] ==0 &&  (int)$value['negative'] ==0 &&  (int)$value['cmtneutral'] <> 0)))
                       {
                          unset($data[$key]);
                       }
                        # code...
                      }
              }

            }

            $data_mention=$data;

         

            return Datatables::of($data_mention)
     
       ->addColumn('post', function ($data_mention){
        $fb_p_id=explode('_', $data_mention['id']);
        $fb_p_id=$fb_p_id[1];
      $post_Link= 'https://www.facebook.com/' . $data_mention['page_name'] .'/posts/' . $fb_p_id ;
      $page_Link= 'https://www.facebook.com/' . $data_mention['page_name'] ;
              $total = (int) $data_mention['positive'] +(int) $data_mention['negative']+(int) $data_mention['cmtneutral'];
              $pos_pcent='';$neg_pcent='';$neutral_pcent='';
             
               $html= '<div class="profiletimeline"><div class="sl-item">
                                    <div class="sl-left"> <img src="'.$data_mention['full_picture'].'" alt="user" class="img-circle  img-bordered-sm"> </div> 
                                    <div class="sl-right"> <div><a href="'.$page_Link.'" class="link"  target="_blank">'.$data_mention['page_name'].' </a>';
              if((int)$data_mention['isDeleted']==1)
              {
               $html.='  <span  class="text-red">   This post is no longer available on FB. </span>';
              }
              $html.='<div class="sl-right">
              <div style ="width:35%;display: inline-block"><span class="sl-date"><i class="mdi mdi-calendar-clock"></i> '.$data_mention['created_time'].'</span> </div>
              <div style="width:55%;display: inline-block">';
               if((int) $data_mention['negative'] > 0)
             { 
              $neg_pcent=round((int) $data_mention['negative']/$total*100,2);
              $html.='<span class="fa fa-circle text-red m-r-10" title="negative comments" data-toggle="tooltip"></span>'.$neg_pcent.'%';
            }
            if((int) $data_mention['positive'] > 0)
              {
                $pos_pcent=round((int) $data_mention['positive']/$total*100,2);
                $html.='<span class="fa fa-circle text-success m-r-10" title="positive comments" data-toggle="tooltip"></span>'.$pos_pcent.'%';
              }
            if((int) $data_mention['cmtneutral'] > 0)
             {
              $neutral_pcent=round((int) $data_mention['cmtneutral']/$total*100,2);
             $html.='<span class="fa fa-circle text-warning m-r-10" title="neutral comments" data-toggle="tooltip"></span>'.$neutral_pcent.'%';
             }
                       
                $html.='</div>
              </div><p style="text-align:justify" class="m-t-10">'.$data_mention['message'].'...<a href="javascript:void(0)" class="btn waves-effect waves-light btn-sm  popup_post"  id="'.$data_mention['id'].'">View More</a></p>';
                        
              $html.= '</div></div>
                           <div class="sl-right">
              <div style ="width:50%;display: inline-block">';
              $html.='<p>';
              if((int)$data_mention['liked'] > 0)
              $html.='👍 '.$this->thousandsCurrencyFormat($data_mention['liked']);
             if((int)$data_mention['loved'] > 0)
              $html.=' ❤️ '.$this->thousandsCurrencyFormat($data_mention['loved']);
             if((int)$data_mention['haha'] > 0)
              $html.=' 😆 '.$this->thousandsCurrencyFormat($data_mention['haha']);
             if((int)$data_mention['wow'] > 0)
              $html.=' 😮 '.$this->thousandsCurrencyFormat($data_mention['wow']);
             if((int)$data_mention['sad'] > 0)
              $html.=' 😪 '.$this->thousandsCurrencyFormat($data_mention['sad']);
             if((int)$data_mention['angry'] > 0)
              $html.=' 😡 '.$this->thousandsCurrencyFormat($data_mention['angry']);
              $html.='</p></div><div style="width:50%;display: inline-block" align="right"><a href="#" class="btn waves-effect waves-light btn-sm btn-info see_comment"  id="seeComment_'.$data_mention['id'].'" target="_blank">See Comment</a> <a href="'.$post_Link.'" class="btn waves-effect waves-light btn-sm btn-info" target="_blank">See Post on FB</a></div></div></div> </div>';

     
           
        
                return $html;
               

            })
  ->addColumn('reaction', function ($data_mention) {
                return "<span style='font-weight:bold'>" . number_format($data_mention['total'])."</span>";
            }) 

  ->addColumn('comment', function ($data_mention) {
                 return "<span style='font-weight:bold'>" . number_format($data_mention['comment_count'])."</span>";
            }) 
  ->addColumn('share', function ($data_mention) {
                return  "<span style='font-weight:bold'>" . number_format($data_mention['shared'])."</span>";
            }) 
  ->addColumn('overall', function ($data_mention) {
                 if((int)$data_mention['positive'] >  (int)$data_mention['negative'])
              {
                $html='<span class="text-success" style="font-weight:500">Positive</span>';
              }
              else   if((int)$data_mention['positive'] <  (int)$data_mention['negative'])
              {
                $html='<span class="text-red" style="font-weight:500">Negative</span>';
              }
                 else   if((int)$data_mention['positive'] ==  0 && (int)$data_mention['negative'] == 0 && (int)$data_mention['cmtneutral']  == 0 )
              {
                $html='<span class="text-warning" style="font-weight:500">Neutral</span>';
              }
              else   if((int)$data_mention['positive'] ==0 &&  (int)$data_mention['negative'] ==0 &&  (int)$data_mention['cmtneutral'] <> 0)
              {
                $html='<span class="text-warning" style="font-weight:500">Neutral</span>';
              }
              else   if((int)$data_mention['positive'] ==  (int)$data_mention['negative'] &&  (int)$data_mention['cmtneutral'] <> 0)
              {
                $html='<span class="text-success" style="font-weight:500">Positive</span>';
              }
              else   if((int)$data_mention['positive'] ==  (int)$data_mention['negative'])
              {
                $html='<span class="text-red" style="font-weight:500">Negative</span>';
              }
           
              return $html;
            }) 

        
      ->rawColumns(['post','overall','reaction','comment','share'])
   
      ->make(true);


    }
public function getInboundcomments()
    {
       //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $post_id=Input::get('id');
      $brand_id=Input::get('brand_id');
      $cmt_type=Input::get('cmt_type');
      $s_cmt_type='';

      // $post_id='228195334394029_341163736430521';
      // $brand_id=17;
      // $cmt_type='Positive';

      if($cmt_type == 'Positive')
      {
       $s_cmt_type ='pos';
      }
      if($cmt_type =="Negative")
      {
        $s_cmt_type='neg';
      }
      $permission_data = $this->getPermission();
 $edit_permission=$permission_data['edit'];


        $query="SELECT id,".$edit_permission." as edit_permission,message,checked_sentiment sentiment,emotion,DATE_FORMAT(created_time, '%d-%m-%Y %h:%i:%s %p') created_time,IF(sentiment = checked_sentiment,'','human predict!') hp ".
        " from temp_".$brand_id."_inbound_comments where post_id='".$post_id."' and parent='' and (checked_sentiment='".$s_cmt_type."' or emotion='".$cmt_type."') ORDER BY DATE(created_time) DESC";

        // echo $query;
        // return;
        $data = DB::select($query);
      
        echo json_encode($data);
    }
     public function getRelatedPosts()
    {
       //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $post_id=Input::get('id');
      $brand_id=Input::get('brand_id');
     /* $cmt_type=Input::get('cmt_type');*/


     /* $post_id='799069593563429_1007801336023586';
      $brand_id=30;
      $cmt_type='anger';
*/
 $permission_data = $this->getPermission();
 $edit_permission=$permission_data['edit'];

$query="SELECT  posts.id,1 as edit_permission,ANY_VALUE(posts.message) message,ANY_VALUE(posts.page_name) page_name,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ".
" created_time,ANY_VALUE(posts.link) link,ANY_VALUE(posts.full_picture) full_picture,ANY_VALUE(posts.sentiment) sentiment,ANY_VALUE(posts.emotion) emotion, sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,sum(IF(cmts.checked_sentiment = 'NA', 1, 0)) NA,".
" ANY_VALUE(posts.isDeleted) isDeleted from temp_".$brand_id."_inbound_posts posts  LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id".
"  where posts.id='".$post_id."'  and cmts.parent='' GROUP BY id";

        // echo $query;
        // return;
        $data = DB::select($query);
//if no data above query we need to just show post data without comment data , in order to get post data remove cmts.parent''
        if (count($data) <=0 )
        {
          $query="SELECT  posts.id,1 as edit_permission,ANY_VALUE(posts.message) message,ANY_VALUE(posts.page_name) page_name,ANY_VALUE(DATE_FORMAT(posts.created_time, '%d-%m-%Y %h:%i:%s %p')) ".
" created_time,ANY_VALUE(posts.link) link,ANY_VALUE(posts.full_picture) full_picture,ANY_VALUE(posts.sentiment) sentiment,ANY_VALUE(posts.emotion) emotion, sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,sum(IF(cmts.checked_sentiment = 'NA', 1, 0)) NA, ".
" ANY_VALUE(posts.isDeleted) isDeleted from temp_".$brand_id."_inbound_posts posts  LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id".
"  where posts.id='".$post_id."'  GROUP BY id";
$data = DB::select($query);
        }
         $project = Project::find($brand_id);
         $monitor_pages = $project->monitor_pages;
         $monitor_pages = explode(',', $monitor_pages);
       /* echo count($data);*/
        echo json_encode(array($data,$monitor_pages));
    }

    public function getsentimentbypage()
    {
   //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
           $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
      $filter_keyword=Input::get('keyword'); 
      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

      /*$brand_id=2;*/

 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));
*/
 $add_comment_con="";
if($filter_keyword !== '')
{
 $add_comment_con = " and (posts.message Like '%".$filter_keyword."%'";
 $add_comment_con .= " or cmt.message Like '%".$filter_keyword."%')";
}

if($filter_sentiment !== '')
{
  $add_comment_con .= " and cmt.checked_sentiment  = '".$filter_sentiment."'";
}
          if(null !== Input::get('filter_page_name'))
           {
             $filter_pages= " posts.page_name in ('".Input::get('filter_page_name')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

$query = "select posts.page_name page_name, sum(IF(cmt.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmt.checked_sentiment = 'neg', 1, 0)) negative , ".
" sum(IF(cmt.checked_sentiment = 'pos', 1, 0)) + sum(IF(cmt.checked_sentiment = 'neg', 1, 0)) total ".
" from temp_".$brand_id."_inbound_comments cmt left join  temp_".$brand_id."_inbound_posts posts on cmt.post_id=posts.id inner join temp_".$brand_id."_pages pages on posts.page_name = pages.page_name  ".
" WHERE posts.id IS NOT NULL AND cmt.parent='' and (DATE(cmt.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ".$filter_pages.$add_comment_con.
" GROUP BY page_name ".
" ORDER BY  total DESC ";

// echo $query;
// return;

$query_result = DB::select($query);


/*
    print_r($data);
    return;*/
      echo json_encode($query_result);

}
 public function Updatepredict()
    {
          
          $post_id = Input::post('id');
          $brand_id = Input::post('brand_id');
          $sentiment = Input::post('sentiment');
          $emotion = Input::post('emotion');
          $tags ='';
          if(null !== Input::post('tags'))
            $tags=implode(",",Input::post('tags'));
        

            // if($tags !=='')
            // {

             $result =DB::table('temp_'.$brand_id.'_inbound_comments')
           
            ->where('id', $post_id)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('checked_sentiment' => $sentiment,'checked_emotion' => $emotion,'change_predict'=>1,'checked_tags'=>$tags));  // update the record in the DB. 
            // }
            // else
            //    {

            //  $result =DB::table('temp_'.$brand_id.'_inbound_comments')
           
            // ->where('id', $post_id)  // find your user by their email
            // ->limit(1)  // optional - to ensure only one record is updated.
            // ->update(array('checked_sentiment' => $sentiment,'checked_emotion' => $emotion,'change_predict'=>1));  // update the record in the DB. 
            //    }

         
            return $result;

        }

        public function UpdatedPostPredict ()
{
          $post_id = Input::post('id');
          $brand_id = Input::post('brand_id');
          $sentiment = Input::post('sentiment');
          $emotion = Input::post('emotion');
  
             $result =DB::table('temp_'.$brand_id.'_inbound_posts')
            ->where('id', $post_id)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('sentiment' => $sentiment,'emotion' => $emotion,'change_predict'=>1));  // update the record in the DB. 
         
            return $result;
}

public function ActionUpdate()
{
          $comment_id = Input::post('id');
          $brand_id = Input::post('brand_id');
          $action_status = Input::post('action_status');

           $result =DB::table('temp_'.$brand_id.'_inbound_comments')
            ->where('id', $comment_id)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('action_status' => $action_status,'action_taken' =>Auth::user()->name)); 
            return $result; 
}

       function readmore($string){
                         $string = strip_tags($string);
                        if (strlen($string) > 500) {

                            // truncate string
                            $stringCut = substr($string, 0, 500);
                            $endPoint = strrpos($stringCut, ' ');

                            //if the string doesn't contain any space then it will cut without word basis.
                            $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                    
                        }
                        return $string;


        }

        function date_compare($a, $b)
{
    $t1 = strtotime($a['datetime']);
    $t2 = strtotime($b['datetime']);
    return $t1 - $t2;
} 


        function thousandsCurrencyFormat($num) {

  if($num>1000) {

        $x = round($num);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('k', 'm', 'b', 't');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];

        return $x_display;

  }

  return $num;
}



}
