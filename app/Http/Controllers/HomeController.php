<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\InboundPages;
use App\Project;
use App\ProjectKeyword;
use App\demo;
use App\Http\Controllers\GlobalController;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{

     use GlobalController;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $layout = 'layouts';
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      if($user=Auth::user())
        {
            $login_user = auth()->user()->id;
            $permission_data = $this->getPermission();
            $source ='in';
            $title="Brand";
            $project_data = $this->getProject();
                  

          if(count($project_data)>0)
          {
          $project_name = $project_data[0]['name'];
          $pid= $project_data[0]['id'];
          $ownpage=$project_data[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
          $count = $this->getProjectCount($project_data);
          return \Redirect::route('dashboard',['pid' => $pid,'source'=>$source]);
          }
          else
          {
             return \Redirect::route('brandList',['source'=>$source]);
          }
            // $count = $this->getProjectCount($project_data);
            // return view('brandlist',compact('title','project_data','permission_data','count','source'));
       }

        
        else
        {
           
             $title="Login";
            return view('auth.login',compact('title'));
        }
        

    }
    public function companyInfo()
    {
        $title="companyInfo";
          $source='in';
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
            return view('company')->with('project_data',$project_data)
                                    ->with('permission_data',$permission_data )
                                    ->with('count',$count)
                                    ->with('source',$source)
                                    ->with('title',$title);
    }
 

    
    public function dashboard()
    {
     
      
     
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $title="Dashboard";
          $source=Input::get('source');
          $project_data = $this->getProject();
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission(); 
          $DemoHideDiv =$this->getDemoHideDiv("Dashboard");

          // $last_crawl_date =$this->getlastcrawldate($pid);
          // $last_crawl_date = date('d-M-Y h:i:s a',strtotime($last_crawl_date[0]));

          if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $monitorpage=$project_data_id[0]['monitor_pages'];
          $monitorpage=explode(',', $monitorpage);
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['own_pages'];
          $ownpage=explode(',', $ownpage);

          $last_crawl_date = InboundPages::where('page_name',$ownpage[0])->get();
       
           foreach ($last_crawl_date as  $key => $row) {
                $last_crawl_date = $row["last_crawl_date"];
                if(isset( $last_crawl_date))
                {
                   $last_crawl_date = $last_crawl_date->toDateTime();
                $last_crawl_date=$last_crawl_date->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $last_crawl_date = $last_crawl_date->format('M d, Y h:i:s A');
                }
                else
                {
                  $last_crawl_date='';
                }
                
        }
       //  dd($last_crawl_date);
          if($source === 'out')
          return view('dashboard',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source'));
          else
          return view('dashboard_in',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','monitorpage','last_crawl_date'));
          }

          else
          {
            
               return abort(404);
          }

        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }


    }
     public function post()
    {
     
      
     
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $title="Posts";
          $source=Input::get('source');
          $project_data = $this->getProject();
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission(); 
          $DemoHideDiv =$this->getDemoHideDiv("Post");
        
          if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
          $count = $this->getProjectCount($project_data);
         
          return view('posts',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','ownpage'));
          }

          else
          {
            
               return abort(404);
          }

        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }


    }
    public function comment()
    {
     
      
     
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $title="Comments";
          $source=Input::get('source');
          $project_data = $this->getProject();
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission(); 
          $DemoHideDiv =$this->getDemoHideDiv("Comment");
        
          if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
          $count = $this->getProjectCount($project_data);
         
          return view('comments',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','ownpage'));
          }

          else
          {
            
               return abort(404);
          }

        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }


    }

    public function mention()
    {
      
      $pid=Input::get("pid");
     
        if($user=Auth::user())
        {
          $title="Mention";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission(); 
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
         
          return view('mention',compact('title','project_data','count','permission_data','project_name','pid','source'));
          }

                else
          {
            /*return 404*/
               return abort(404);
          }
        }
      
    
    }

      public function page_manage()
    {
      
      $pid=Input::get("pid");
     
        if($user=Auth::user())
        {
          $title="page management";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission(); 
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
         
          return view('page_management',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage'));
          }

                else
          {
            /*return 404*/
               return abort(404);
          }
        }
      
    
    }


        public function pointoutmention()
    {
     
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $title="Mention";
          $source='out';
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
     
          return view('pointOutMention',compact('title','project_data','count','permission_data','project_name','pid','source'));
           }
            else
            {
              return abort(404);
            }
        }
     
    }

    public function pointoutcomment()
    {
     
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $title="Mention";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
     
          return view('pointOutComment',compact('title','project_data','count','permission_data','project_name','pid','source'));
           }
            else
            {
              return abort(404);
            }
        }
     
    }
    public function relatedcomment()
    {
     
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $title="Related comments";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
     
          return view('showrelatedcomment',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage'));
           }
            else
            {
              return abort(404);
            }
        }
     
    }
    
    public function relatedcompetitorcomment()
    {
     
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $title="Related comments";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
     
          return view('showrelatedcompetitorcmt',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage'));
           }
            else
            {
              return abort(404);
            }
        }
     
    }
     public function relatedpost()
    {
     
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $title="Related Posts";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $ownpage=$project_data_id[0]['monitor_pages'];
          $ownpage=explode(',', $ownpage);
     
          return view('showrelatedpost',compact('title','project_data','count','permission_data','project_name','pid','source','ownpage'));
           }
            else
            {
              return abort(404);
            }
        }
     
    }
    public function pointoutpost()
    {
      $pid=Input::get("pid");
      if($user=Auth::user())
        {
          $title="Mention";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission();
          // print_r($permission_data) ; return;
           if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
     
          return view('pointOutPost',compact('title','project_data','count','permission_data','project_name','pid','source'));
           }
            else
            {
              return abort(404);
            }
        }
    }
    public function gethiddendiv()
    {
          $hidden_div=[];
         if($this->getIsDemouser() === true)
         {
          $view_name=Input::get("view_name");
          $hidden_div = demo::select('*')->where('is_hide',1)->where('view_name',$view_name);
          $hidden_div=$hidden_div->orderBy('id','DESC')->get();
         }
         
          echo json_encode($hidden_div);
    }
    public function analysis()
    {
      
       $pid=Input::get("pid");
       $project_name="";
        if($user=Auth::user())
        {
          $title="Analysis";
           $source=Input::get('source');
           $project_data_id = $this->getProjectByid($pid);
           $permission_data = $this->getPermission(); 
             if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
         if($source === 'out')
        return view('analysis',compact('title','project_data','count','permission_data','project_name','pid','source'));
          else
         return view('analysis_in',compact('title','project_data','count','permission_data','project_name','pid','source'));
          }
        else
      {
        /*return 404*/
           return abort(404);
      }
        }
      
    }

     public function comparison()
    {
     
       $pid=Input::get("pid");
       $project_name="";
        if($user=Auth::user())
        {
          $title="Comparison";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission(); 
              if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          //$project_data_exclude = $this->getProjectByExcludeid($pid);
          $compare_pages= $this->getComparisonPage($pid);
          $compare_pages=explode(',', $compare_pages);
          // dd($compare_pages);
           if($source === 'out')
     return view('comparison',compact('title','project_data','count','project_name','permission_data','compare_pages','pid','source'));
          else
        return view('comparison_in',compact('title','project_data','count','project_name','permission_data','compare_pages','pid','source'));

           }
            else
          {
            /*return 404*/
               return abort(404);
          }
        }
     
    }
    public function comparisonpdf()
    {
     
       $pid=Input::get("pid");
       $project_name="";
        if($user=Auth::user())
        {
          $title="Analysis Report";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission(); 
              if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          //$project_data_exclude = $this->getProjectByExcludeid($pid);
          $compare_pages= $this->getComparisonPage($pid);
          $compare_pages=explode(',', $compare_pages);
          // dd($compare_pages);
          $companyData=$this->getCompanyData();
        return view('reports.comparisonPDF',compact('title','project_data','count','project_name','permission_data','compare_pages','pid','companyData','source'));

           }
            else
          {
            /*return 404*/
               return abort(404);
          }
        }
     
    }

      public function competitor()
    {
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $title="Competitor";
          $source=Input::get('source');
          $project_data = $this->getProject();
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission(); 
          $DemoHideDiv =$this->getDemoHideDiv("Post");
        
          if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $monitor=$project_data_id[0]['monitor_pages'];
          $monitor=explode(',', $monitor);
          $ownpage=$project_data_id[0]['own_pages'];
          $ownpage=explode(',', $ownpage);
          $otherpage=array_diff($monitor,$ownpage);
          // dd($ownpage);
          $count = $this->getProjectCount($project_data);
         
          return view('competitor',compact('title','project_data','count','permission_data','project_name','DemoHideDiv','pid','source','otherpage'));
          }

          else
          {
            
               return abort(404);
          }

        }
        else
        {
            $title="Login";
            return view('auth.login',compact('title'));
        }


    }

    public function getallpages()
    {
      $brand_id=Input::get('brand_id');
      $compare_pages= $this->getComparisonPage($brand_id);
      $compare_pages=explode(',', $compare_pages);

      echo json_encode($compare_pages);
    }

      public function insight()
    {
     
       $pid=Input::get("pid");
       $project_name="";
        if($user=Auth::user())
        {
          $title="Insight";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission(); 
               if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $project_data_exclude = $this->getProjectByExcludeid($pid);
         
          if($source === 'out')
              return view('insight',compact('title','project_data','count','project_name','permission_data','project_data_exclude','pid','source'));
          else
            return view('insight_in',compact('title','project_data','count','project_name','permission_data','project_data_exclude','pid','source'));

          }
            else
          {
            /*return 404*/
               return abort(404);
          }
        }
      
    }
      public function insight_facebook()
    {
     
       $pid=Input::get("pid");
       $project_name="";
        if($user=Auth::user())
        {
          $title="Insight";
          $source=Input::get('source');
          $project_data_id = $this->getProjectByid($pid);
          $permission_data = $this->getPermission(); 
               if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $ownpage=$project_data_id[0]['own_pages'];
          $ownpage=explode(',', $ownpage);
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $project_data_exclude = $this->getProjectByExcludeid($pid);
         
        
            return view('insight_facebook',compact('title','project_data','count','project_name','permission_data','project_data_exclude','pid','source','ownpage'));

          }
            else
          {
            /*return 404*/
               return abort(404);
          }
        }
      
    }

     public function monitor()
    {
        if($user=Auth::user())
        {
          $title="Monitor-Alert";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
         
          return view('monitorAlert',compact('title','project_data','count'));
        }
    }
       public function brandList()
    {
      // return view('brandlist');
        if($user=Auth::user())
        {
          $source =Input::get('source');
          $title="Brand List";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          
          $projects = Project::all();
          /* print_r($projects);
           return;*/
          $permission_data = $this->getPermission(); 
          return view('brandlist',compact('title','project_data','permission_data','count','test','source'));
        }
    }

      public function RptSentiPredict()
    {
      // return view('brandlist');
        if($user=Auth::user())
        {
          $source =Input::get('source');
          $title="Predict Statistic";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          
          $projects = Project::all();
          /* print_r($projects);
           return;*/
          $permission_data = $this->getPermission(); 
          return view('reports.sentipredictcondition',compact('title','project_data','permission_data','count','source'));
        }
    }
    public function RptHumanPredict()
    {
      // return view('brandlist');
        if($user=Auth::user())
        {
          $source =Input::get('source');
          $title="Human Predict";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          
          $projects = Project::all();
          /* print_r($projects);
           return;*/
          $permission_data = $this->getPermission(); 
          return view('reports.humanpreditrecords',compact('title','project_data','permission_data','count','source'));
        }
    }
    public function RptPostExport()
    {
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $source =Input::get('source');
          $title="Post Data";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          $project_data_id = $this->getProjectByid($pid);
          if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $monitor=$project_data_id[0]['monitor_pages'];
          $monitor=explode(',', $monitor);
          
          }
         
          return view('reports.postDataExport',compact('title','project_data','permission_data','monitor','count','source'));
        }
    }
    public function RptAnalysis()
    {
        $pid=Input::get("pid");
        if($user=Auth::user())
        {
          $source =Input::get('source');
          $title="Post Data";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          $project_data_id = $this->getProjectByid($pid);
          if(count($project_data_id)>0)
          {
          $project_name = $project_data_id[0]['name'];
          $monitor=$project_data_id[0]['monitor_pages'];
          $monitor=explode(',', $monitor);
          
          }
         
          return view('reports.analysisReport',compact('title','project_data','permission_data','monitor','count','source'));
        }
    }


        
}
