<?php

namespace App\Http\Controllers;
use App\MongodbData;
use App\MongoPages;
use App\MongoFollowers;
use App\Comment;
use App\MongoInboundPost;
use App\MongoInboundComment;
use MongoDB;
use Illuminate\Http\Request;
use Auth;
use App\Project;
use App\ProjectKeyword;
use App\InboundPages;
use GuzzleHttp\Client;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use DB;

class TestController extends Controller
{
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
 public function getpagefilter($page_data)
{
  $conditional=[];
  
  $conditional_require_or=[];
  foreach($page_data as $i =>$element)
                            {

                            $require_keyword_filter[] = [ 'page_name' =>  $element];
                            $conditional_require_or['$or']=$require_keyword_filter;
                            }
 $conditional[] =$conditional_require_or;
 
 return  $conditional;
}

       public function getPageWhere($related_pages)
{
    $pages='';
     $filter_pages = '';
      foreach ($related_pages as $key => $value) {
        
        if((int)$key === 0)
        $pages ="'".$value."'";
      else
        $pages .=",'".$value."'";

        $filter_pages="  AND page_name in (".$pages.")";
      }
    return  $filter_pages;
}

   public function get_table_id($project_id,$dateBegin,$dateEnd,$table,$require_var,$related_pages=[])
    {
     
      $filter_pages=$this->getPageWhere($related_pages);
      
      $query = "SELECT ".$require_var." from temp_".$project_id."_".$table ."  Where 1=1 ".
      " AND (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')" . $filter_pages;
      //dd($query);
      $id_result = DB::select($query);
      $id_array = array_column($id_result, $require_var);
      return $id_array;
    }

     public function get_table_page($project_id,$table,$require_var)
    {
     
    
      
      $query = "SELECT ".$require_var." from temp_".$project_id."_".$table ;
      //dd($query);
      $id_result = DB::select($query);
      $id_array = array_column($id_result, $require_var);
      return $id_array;
    }

    public function getInboundPostId($brand_id)
    {
        $id_query="select id from temp_".$brand_id."_inbound_posts";
        $id_result = DB::select($id_query);
        $id_array = array_column($id_result, 'id');
        return  $id_array;
    }


    public function sync_mongodata()
    {
     $projects = Project::all();
      $data =  [];
      
      foreach($projects as $project)
      {
        if($project->id === 17)
        {
        $id = $project->id;
        $monitor_pages = $project->monitor_pages;
        $monitor_pages = explode(',', $monitor_pages);
        $admin_pages = $project->own_pages;
        $admin_pages = explode(',', $admin_pages);
        $other_pages=array_values(array_diff($monitor_pages,$admin_pages));
        $admin_pages_count = count($admin_pages);
        $other_pages_count = count($other_pages);
        if($admin_pages_count>0) 
        $this->insert_Inbound_Data($id,$admin_pages);
        if($other_pages_count>0) 
        $this->insert_Inbound_Other_Data($id,$other_pages);
        }
       
      }
    }
    public function insert_Inbound_Data($project_id,$my_pages)
    {

     $today = date('22-09-2018');//22-09-2018//  date('d-m-Y')
     $today_plus=date('Y-m-d', strtotime('+1 day', strtotime($today)));
     $date_Begin=date('Y-m-d', strtotime('-1 week', strtotime($today_plus)));
     // var_dump(extension_loaded('mongodb'));
     // return;
     $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_Begin))* 1000);
    
     $date_End=date('Y-m-d',strtotime($today));
     $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_End))* 1000);
    
      //get 7 day id from mysql

      //for edit
     $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];
     
     // $keyword_data = $this->getprojectkeywork($project_id);
     // $filter['$or'] = $this->getkeywordfilter($keyword_data);



  
       
     $page['$or']= $this->getpagefilter($my_pages);
 
 
$inbound_post_id_array = $this->get_table_id($project_id,$date_Begin,$date_End,"inbound_posts","id",$my_pages);

 $inbound_post_result=MongoInboundPost::raw(function ($collection) use($page,$dateBegin,$dateEnd,$inbound_post_id_array) {//print_r($filter);

    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             ['id'=> ['$exists'=> true]],
             ['id'=>[ '$nin'=> $inbound_post_id_array ] ],
             $page
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();
 
/*
dd($inbound_post_result);*/
$data=[];
 $page_array=[];
 $data_message=[];

 foreach ($inbound_post_result as  $key => $row) {
                $id ='';
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';$type ='';
                $page_name ='';
                $share =0;
                $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
                
                if(isset($row['message'])) $message = $row['message'];
                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime = $datetime->format('Y-m-d H:i:s');
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                
                if(isset($row['type'])) $type = $row['type'];
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
                if(isset($row['share'])) $share = $row['share'];
                if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand($row['reaction']['Angry']);
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];

                  $data[] =[
                    'id' => $id,
                    'full_picture' =>$full_picture,
                    'link' =>$link,
                    'name' =>$name,
                    'message' => $message,
                    'type' =>$type,
                    'wb_message' =>'',
                    'page_name' =>$page_name,
                    'shared' =>$share,
                    'Liked' =>$Like,
                    'Love' =>$Love,
                    'Wow' =>$Wow,
                    'Haha' =>$Haha,
                    'Sad' =>$Sad,
                    'Angry' =>$Angry,
                    'sentiment' =>$sentiment,
                    'emotion' =>$emotion,
                    'created_time' =>$datetime,
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'isBookMark' =>0,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                   ];                 

  }
  $path = storage_path('app/data_output/weekly_inbound_posts'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data);

 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_inbound_posts FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,full_picture,link,name,message,type,wb_message,page_name,shared,Liked,Love,Wow,Haha,Sad,Angry,sentiment,emotion,created_time,change_predict,checked_predict,isBookMark,created_at,updated_at)";

 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);
 if (file_exists($path)) {
    unlink($path) ;
}
/////////////////////////////////////////////////////////////////

 $post_id = $this->getInboundPostId($project_id);/*dd( $filter);*/
// dd($post_id);
 
$inbound_cmt_id_array = $this->get_table_id($project_id,$date_Begin,$date_End,"inbound_comments","id");

//$this->get_table_id($project_id,$date_Begin,$dateEnd,"inbound_posts","id",$my_pages);
  /*dd($inbound_cmt_id_array);*/
 $inbound_comment_result=MongoInboundComment::raw(function ($collection) use($page,$dateBegin,$dateEnd,$inbound_cmt_id_array,$post_id) {//print_r($filter);
 
    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             ['id'=> ['$exists'=> true]],
             ['post_id'=>['$in'=>$post_id]],
             ['id'=>[ '$nin'=> $inbound_cmt_id_array ] ],
           
            
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();
 /*dd($inbound_comment_result);*/

  $data_comment=[];
 foreach ($inbound_comment_result as  $key => $row) {
                $id ='';
                $message ='';
                $post_id ='';
                $comment_count =0;
                $sentiment='';$emotion='';$parent='';
                $tags='';
                $datetime = $row["created_time"];
                $datetime = $datetime->toDateTime();
                $datetime = $datetime->format('Y-m-d H:i:s');
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['message'])) $message = $row['message'];
                if(isset($row['post_id'])) $post_id = $row['post_id'];
                if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['parent'])) $parent =  $row['parent'];
           

                  $data_comment[] =[
                    'id' => $id,
                    'created_time' =>$datetime,
                    'message' => $message,
                    'wb_message' =>'',
                    'post_id' =>$post_id,
                    'comment_count' =>$comment_count,
                    'sentiment' =>$sentiment,
                    'emotion' =>$emotion,
                    'interest' =>'',
                    'tags'=>'',
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'parent'=>$parent,
                    'isBookMark' =>0,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }

$path = storage_path('app/data_output/weekly_inbound_comment'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_comment);
 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_inbound_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,post_id,comment_count,sentiment,emotion,interest,tags,change_predict,checked_predict,parent,isBookMark,created_at,updated_at)";
 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);

if (file_exists($path)) {
    unlink($path) ;
} 

// return;

$follow_id_array = $this->get_table_page($project_id,"followers","page_name");
$follower_result=MongoFollowers::raw(function ($collection) use($my_pages,$follow_id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
          ['page_name' => ['$in' => $my_pages ] ],
          ['page_name'=>[ '$nin'=> $follow_id_array ] ], 
            
      ]

        ]  
           ]
    
    ]);
})->toArray();
 
/* print_r($follower_result);
 return;*/
 $data_followers=[];
 foreach ($follower_result as  $key => $row) {
                $id ='';
                $page_name ='';
                $fan_count =0;
                $date ='';
             
   
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['page_name'])) $page_name =$row['page_name'];
                if(isset($row['fan_count'])) $fan_count = $row['fan_count'];
                if(isset($row['date'])) $date = $row['date'];
                if(isset($row['reaction'])) $reaction =$row['reaction'];
          
                 $data_followers[] =[
                    'id' => $id,
                    'page_name' =>$page_name,
                    'fan_count' => $fan_count,
                    'date' =>$date,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }
 $path = storage_path('app/data_output/weekly_followers'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_followers);

$query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_followers FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,page_name,fan_count,date,created_at,updated_at)";

 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);
 if (file_exists($path)) {
    unlink($path) ;
} 

 $page_id_array = $this->get_table_page($project_id,"pages","page_name");
 $pages_result=MongoPages::raw(function ($collection) use($my_pages,$page_id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
          ['page_name' => ['$in' => $my_pages ] ] ,
          [ 'page_name'=>[ '$nin'=> $page_id_array ] ],
            
      ]

        ]  
           ]
    
    ]);
})->toArray();

 $data_pages=[];
 foreach ($pages_result as  $key => $row) {
                $page_name ='';
                $about ='';
                $name ='';
                $category ='';
                $sub_category ='';
                $profile='';
 
                if(isset($row['page_name'])) $page_name =$row['page_name'];
                if(isset($row['name'])) $name =$row['name'];
                if(isset($row['about'])) $about = $row['about'];
                if(isset($row['category'])) $category = $row['category'];
                if(isset($row['sub_category'])) $sub_category = $row['sub_category'];
                if(isset($row['profile'])) $profile = $row['profile'];
              //  if(isset($row['reaction'])) $reaction =$row['reaction'];
          

                  $data_pages[] =[
                    'page_name' => $page_name,
                    'name' =>$name,
                    'about' => $about,
                    'category' =>$category,
                    'sub_category' =>$sub_category,
                    'profile'=>$profile,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }
 $path = storage_path('app/data_output/weekly_pages'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_pages);

$query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_pages FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (page_name,name,about,category,sub_category,profile,created_at,updated_at)";

 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);

 if (file_exists($path)) {
    unlink($path) ;
} 

 return true;

 

}
 public function insert_Inbound_Other_Data($project_id,$other_pages)
    {

     $today = date('22-09-2018');//22-09-2018//  date('d-m-Y')
     $today_plus=date('Y-m-d', strtotime('+1 day', strtotime($today)));
     $date_Begin=date('Y-m-d', strtotime('-1 week', strtotime($today_plus)));
     var_dump(extension_loaded('mongodb'));
     return;
     $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_Begin))* 1000);
    
     $date_End=date('Y-m-d',strtotime($today));
     $dateEnd =new MongoDB\BSON\UTCDateTime(strtotime(str_replace('-','/',$date_End))* 1000);
    
      //get 7 day id from mysql

      //for edit
     $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];
     
     // $keyword_data = $this->getprojectkeywork($project_id);
     // $filter['$or'] = $this->getkeywordfilter($keyword_data);



  
       
     $page['$or']= $this->getpagefilter($other_pages);
 
 
$inbound_post_id_array = $this->get_table_id($project_id,$date_Begin,$date_End,"inbound_posts","id",$other_pages);

 $inbound_post_result=MongodbData::raw(function ($collection) use($page,$dateBegin,$dateEnd,$inbound_post_id_array) {//print_r($filter);

    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             ['id'=> ['$exists'=> true]],
             ['id'=>[ '$nin'=> $inbound_post_id_array ] ],
             $page
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();
 
/*
dd($inbound_post_result);*/
$data=[];
 $page_array=[];
 $data_message=[];

 foreach ($inbound_post_result as  $key => $row) {
                $id ='';
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';$type ='';
                $page_name ='';
                $share =0;
                $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
                
                if(isset($row['message'])) $message = $row['message'];
                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime = $datetime->format('Y-m-d H:i:s');
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                
                if(isset($row['type'])) $type = $row['type'];
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
                if(isset($row['share'])) $share = $row['share'];
                if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand($row['reaction']['Angry']);
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];

                  $data[] =[
                    'id' => $id,
                    'full_picture' =>$full_picture,
                    'link' =>$link,
                    'name' =>$name,
                    'message' => $message,
                    'type' =>$type,
                    'wb_message' =>'',
                    'page_name' =>$page_name,
                    'shared' =>$share,
                    'Liked' =>$Like,
                    'Love' =>$Love,
                    'Wow' =>$Wow,
                    'Haha' =>$Haha,
                    'Sad' =>$Sad,
                    'Angry' =>$Angry,
                    'sentiment' =>$sentiment,
                    'emotion' =>$emotion,
                    'created_time' =>$datetime,
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'isBookMark' =>0,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                   ];                 

  }
  $path = storage_path('app/data_output/weekly_inbound_posts'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data);

 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_inbound_posts FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,full_picture,link,name,message,type,wb_message,page_name,shared,Liked,Love,Wow,Haha,Sad,Angry,sentiment,emotion,created_time,change_predict,checked_predict,isBookMark,created_at,updated_at)";

 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);
 if (file_exists($path)) {
    unlink($path) ;
}
/////////////////////////////////////////////////////////////////

 $post_id = $this->getInboundPostId($project_id);/*dd( $filter);*/
// dd($post_id);
 
$inbound_cmt_id_array = $this->get_table_id($project_id,$date_Begin,$date_End,"inbound_comments","id");

//$this->get_table_id($project_id,$date_Begin,$dateEnd,"inbound_posts","id",$my_pages);
  /*dd($inbound_cmt_id_array);*/
 $inbound_comment_result=Comment::raw(function ($collection) use($page,$dateBegin,$dateEnd,$inbound_cmt_id_array,$post_id) {//print_r($filter);
 
    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             ['id'=> ['$exists'=> true]],
             ['post_id'=>['$in'=>$post_id]],
             ['id'=>[ '$nin'=> $inbound_cmt_id_array ] ],
           
            
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();
 /*dd($inbound_comment_result);*/

  $data_comment=[];
 foreach ($inbound_comment_result as  $key => $row) {
                $id ='';
                $message ='';
                $post_id ='';
                $comment_count =0;
                $sentiment='';$emotion='';$parent='';
                $tags='';
                $datetime = $row["created_time"];
                $datetime = $datetime->toDateTime();
                $datetime = $datetime->format('Y-m-d H:i:s');
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['message'])) $message = $row['message'];
                if(isset($row['post_id'])) $post_id = $row['post_id'];
                if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['parent'])) $parent =  $row['parent'];
           

                  $data_comment[] =[
                    'id' => $id,
                    'created_time' =>$datetime,
                    'message' => $message,
                    'wb_message' =>'',
                    'post_id' =>$post_id,
                    'comment_count' =>$comment_count,
                    'sentiment' =>$sentiment,
                    'emotion' =>$emotion,
                    'interest' =>'',
                    'tags'=>'',
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'parent'=>$parent,
                    'isBookMark' =>0,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }

$path = storage_path('app/data_output/weekly_inbound_comment'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_comment);
 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_inbound_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,post_id,comment_count,sentiment,emotion,interest,tags,change_predict,checked_predict,parent,isBookMark,created_at,updated_at)";
 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);

if (file_exists($path)) {
    unlink($path) ;
} 

// return;

$follow_id_array = $this->get_table_page($project_id,"followers","page_name");
$follower_result=MongoFollowers::raw(function ($collection) use($other_pages,$follow_id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
          ['page_name' => ['$in' => $other_pages ] ],
          ['page_name'=>[ '$nin'=> $follow_id_array ] ], 
            
      ]

        ]  
           ]
    
    ]);
})->toArray();
 
/* print_r($follower_result);
 return;*/
 $data_followers=[];
 foreach ($follower_result as  $key => $row) {
                $id ='';
                $page_name ='';
                $fan_count =0;
                $date ='';
             
   
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['page_name'])) $page_name =$row['page_name'];
                if(isset($row['fan_count'])) $fan_count = $row['fan_count'];
                if(isset($row['date'])) $date = $row['date'];
                if(isset($row['reaction'])) $reaction =$row['reaction'];
          
                 $data_followers[] =[
                    'id' => $id,
                    'page_name' =>$page_name,
                    'fan_count' => $fan_count,
                    'date' =>$date,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }
 $path = storage_path('app/data_output/weekly_followers'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_followers);

$query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_followers FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,page_name,fan_count,date,created_at,updated_at)";

 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);
 if (file_exists($path)) {
    unlink($path) ;
} 

 $page_id_array = $this->get_table_page($project_id,"pages","page_name");
 $pages_result=MongoPages::raw(function ($collection) use($other_pages,$page_id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
          ['page_name' => ['$in' => $other_pages ] ] ,
          [ 'page_name'=>[ '$nin'=> $page_id_array ] ],
            
      ]

        ]  
           ]
    
    ]);
})->toArray();

 $data_pages=[];
 foreach ($pages_result as  $key => $row) {
                $page_name ='';
                $about ='';
                $name ='';
                $category ='';
                $sub_category ='';
                $profile='';
 
                if(isset($row['page_name'])) $page_name =$row['page_name'];
                if(isset($row['name'])) $name =$row['name'];
                if(isset($row['about'])) $about = $row['about'];
                if(isset($row['category'])) $category = $row['category'];
                if(isset($row['sub_category'])) $sub_category = $row['sub_category'];
                if(isset($row['profile'])) $profile = $row['profile'];
              //  if(isset($row['reaction'])) $reaction =$row['reaction'];
          

                  $data_pages[] =[
                    'page_name' => $page_name,
                    'name' =>$name,
                    'about' => $about,
                    'category' =>$category,
                    'sub_category' =>$sub_category,
                    'profile'=>$profile,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }
 $path = storage_path('app/data_output/weekly_pages'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_pages);

$query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_pages FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (page_name,name,about,category,sub_category,profile,created_at,updated_at)";

 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);

 if (file_exists($path)) {
    unlink($path) ;
} 

 return true;

 

}

function doJson($path,$json)
{
    $fp = fopen($path, 'w');
   fwrite($fp, $json);
   fclose($fp);
}
   function doCSV($path, $array)
{
   $fp = fopen($path, 'w');
$i = 0;
foreach ($array as $fields) {
    if($i == 0){
        fputcsv($fp, array_keys($fields));
    }
    fputcsv($fp, array_values($fields));
    $i++;
}

fclose($fp);
}

    function convertKtoThousand($s)
{
    if (strpos(strtoupper($s), "K") != false) {
    $s = rtrim($s, "kK");
    return floatval($s) * 1000;
  } else if (strpos(strtoupper($s), "M") != false) {
    $s = rtrim($s, "mM");
    return floatval($s) * 1000000;
  } else {
    return floatval($s);
  }
}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    public function TruncateTable($id)
  {
          if (Schema::hasTable('temp_'.$id.'_posts')) {
              DB::table('temp_'.$id.'_posts')->truncate();

             }
               if (Schema::hasTable('temp_'.$id.'_comments')) {
              DB::table('temp_'.$id.'_comments')->truncate();
             }
               if (Schema::hasTable('temp_'.$id.'_followers')) {
              DB::table('temp_'.$id.'_followers')->truncate();
             }
               if (Schema::hasTable('temp_'.$id.'_pages')) {
              DB::table('temp_'.$id.'_pages')->truncate();
             }
             if (Schema::hasTable('temp_'.$id.'_inbound_posts')) {
              DB::table('temp_'.$id.'_inbound_posts')->truncate();
             }
             if (Schema::hasTable('temp_'.$id.'_inbound_comments')) {
              DB::table('temp_'.$id.'_inbound_comments')->truncate();
             }
             return true;
  }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function brand_delete($id)
    {
    
        $prj = DB::table('projects')->where('id',$id)->delete();
        $keywords =  DB::table('project_keywords')->where('project_id', $id)->delete();
        // dd($id);
        $pages = InboundPages::where('brand_id',$id)->delete();
        $this->DropTable($id);
         return redirect('brandList')->with([
        'flash_message' => 'Deleted',
        'flash_message_important' => false
  ]);
    }

    public function DropTable($id)
  {
    Schema::dropIfExists('temp_'.$id.'_posts'); 
    Schema::dropIfExists('temp_'.$id.'_comments'); 
    Schema::dropIfExists('temp_'.$id.'_followers'); 
    Schema::dropIfExists('temp_'.$id.'_pages'); 
    Schema::dropIfExists('temp_'.$id.'_inbound_posts'); 
    Schema::dropIfExists('temp_'.$id.'_inbound_comments'); 

    return true;
  }

     public function getProjectData ()
   {
     $source='';
     if(!null==Input::get('source'))
     $source=Input::get('source');

     $login_user = auth()->user()->id;
     $permission_data = $this->getPermission();
     $projectlist = Project::select(['id', 'name', 'created_at'])->where('user_id',$login_user)->orderBy('id');

     // echo $source ;
     // return;

     return Datatables::of($projectlist)
       ->addColumn('action', function ($projectlist) use($permission_data,$source) {
        $html='';
                if($permission_data['setting'] === 1)
                {
                $html ='<a href="brandList/edit/'.$projectlist->id.'?source='.$source.'" class="btn btn-xs btn-primary"><i class="mdi mdi-table-edit"></i> Edit</a> 
            <button class="btn btn-xs btn-danger btn-delete" data-remote="' . route('brandList/delete', $projectlist->id) . '"><i class="mdi mdi-delete"></i>Delete</button> <a href="dashboard?pid='.$projectlist->id.'&source='.$source.'" class="btn btn-xs btn-secondary"><i class="fas fa-home"></i> Dashboard</a>';
                }
               return $html;

            })
       ->editColumn('created_at', function ($projectlist) {
                 if (isset($projectlist->created_at)) {
                return   $projectlist->created_at->format('d/m/Y');
            }
            })
       ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })

      ->make(true);
           // ->rawColumns(['image', 'action'])


 }
 function page_delete()
 {
   $pages = InboundPages::where('brand_id',77)->delete();
 }

}
