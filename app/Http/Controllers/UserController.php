<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
class UserController extends Controller
{
     use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      protected function validator(array $data,$id)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users,username,'.$id,
            'email' => 'required|string|max:255|unique:users,email,'.$id,
           'password' => 'nullable|string|min:6|confirmed',
           
        ]);
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $title="Register";
          $source=Input::get('source');
          $project_data = $this->getProject();
          $permission_data = $this->getPermission(); 
          $count = $this->getProjectCount($project_data);
          $userdata = User::find($id);
       
        return view('auth.register',compact('userdata','title','project_data','count','source','permission_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator($request->all(),$id)->validate();

        $input_p['name']=$request->name;
        $input_p['username']=$request->username;
        $input_p['email']=$request->email;
        if(isset($request->password))
        {
        $input_p['password']=Hash::make($request->password);
        }
    
        User::find($id)->update($input_p);
        return redirect()->route('register')
                        ->with('message','Record update successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('register')
                        ->with('success','Record deleted successfully');
    }

    //customize function
     public function getUserData()
   {

     $source='';
     if(!null==Input::get('source'))
     $source=Input::get('source');      

     $userlist = User::select(['id', 'name', 'username', 'email'])->orderBy('id');

     return Datatables::of($userlist)
       ->addColumn('action', function ($userlist) use($source) {
                return '<a href="'. route('users.edit', ['id'=>$userlist->id,'source'=>$source]) .'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
              <a href="'. route('deleteuser',$userlist->id) .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            })

    /* ->addColumn('action', function ($booking) {
        return '<a href="'. route('deleteBooking',$booking->id) .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

    })*/




     ->make(true);
           // ->rawColumns(['image', 'action'])

 }
}
