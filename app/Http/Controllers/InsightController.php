<?php

namespace App\Http\Controllers;

use App\MongodbData;
use App\MongoPages;
use App\MongoFollowers;
use App\Comment;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;

class InsightController extends Controller
{
    use GlobalController;
    public function getemotionRate()
    {
      $brand_id=Input::get('brand_id');
        
       /* $brand_id = 46;*/

   if(null !== Input::get('fday'))
        {
      $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        }
        else
        {
        $dateBegin=date('Y-m-d');
        }
      

      if(null !==Input::get('sday'))
      {

   $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }
      
      else
      {
  $dateEnd=date('Y-m-d');

             }

  /*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 05')));
  $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 05')));*/
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
  $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}

  $query = "select count(*) post_count,emotionCategory from ( SELECT page_name,DATE_FORMAT(created_time, '%Y-%m-%d') created_time, ".
  " CASE    WHEN emotion = 'surprise'  or emotion ='trust'    THEN '1' ".
" WHEN emotion = 'interest'  or emotion='fear'  THEN '2' ".
" WHEN emotion = 'joy'  or emotion='sadness'  THEN '3' ".
" WHEN emotion = 'like'  or emotion='disgust'  THEN '4' ".
" WHEN emotion = 'love'  or emotion='anger'  THEN '5' ".
" ELSE '0' END emotionCategory ".
" FROM temp_".$brand_id."_posts ".
" UNION ALL ".
" SELECT DATE_FORMAT(cmts.created_time, '%Y-%m-%d') created_time,page_name, ".
" CASE    WHEN cmts.emotion = 'surprise'  or cmts.emotion ='trust'    THEN '1' ".
" WHEN cmts.emotion = 'interest'  or cmts.emotion='fear'  THEN '2' ".
" WHEN cmts.emotion = 'joy'  or cmts.emotion='sadness'  THEN '3' ".
" WHEN cmts.emotion = 'like'  or cmts.emotion='disgust'  THEN '4' ".
" WHEN cmts.emotion = 'love'  or cmts.emotion='anger'  THEN '5' ". 
" ELSE '0' END emotionCategory ".
" FROM temp_".$brand_id."_comments  cmts LEFT JOIN temp_".$brand_id."_posts  posts on posts.id=cmts.post_id WHERE posts.id IS NULL) T1 ".
" WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_inbound_con .
" GROUP BY emotionCategory";

$query_result = DB::select($query);

echo json_encode($query_result);
      }
    

  

	    public function getcommentEmotionbyPostType()
    {
    	$brand_id=Input::get('brand_id');
        
        /*$brand_id = 51;*/

	 if(null !== Input::get('fday'))
	      {
	    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
	      }
	      else
	      {
	      $dateBegin=date('Y-m-d');
	      }
	    

	    if(null !==Input::get('sday'))
	    {

	 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
	    }
	    
	    else
	    {
	$dateEnd=date('Y-m-d');

	           }

	/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 05')));
	$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 05')));*/
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}

$query = "SELECT count(*) comment_count,cmts.emotion emotion_name, ".
" CASE WHEN cmts.emotion ='anger' THEN 0
WHEN cmts.emotion ='interest' THEN 1
WHEN cmts.emotion ='disgust' THEN 2
WHEN cmts.emotion ='fear' THEN 3
WHEN cmts.emotion ='joy' THEN 4
WHEN cmts.emotion ='like' THEN 5
WHEN cmts.emotion ='love' THEN 6
WHEN cmts.emotion ='neutral' THEN 7
WHEN cmts.emotion ='sadness' THEN 8
WHEN cmts.emotion ='surprise' THEN 9
ELSE 10 END AS emotion, " .
" CONCAT( IF(CHAR_LENGTH(posts.message) > 150 ,'long','short'),' ',type ) post_type,max(post_id) ".
" FROM temp_".$brand_id."_posts  posts Left JOIN temp_".$brand_id."_comments cmts on posts.id=cmts.post_id ".
" where cmts.post_id is not null".
" and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". $add_inbound_con.
" GROUP By cmts.emotion,post_type";
/*echo $query;
return;*/

$query_result = DB::select($query);

$j=["anger","interest","disgust","fear","joy","like","love","neutral","sadness","surprise","trust"];
$i = ["Short status","Long Posts","Short with image","Long with image","Short with video","Long with video"];
$data=[];
/*$data = [[0,0,0],[0,1,0],[0,2,0],[0,3,0],[0,4,0],[0,5,0],[0,6,0],[0,7,0],
[0,8,0],[0,9,0],[0,10,0],[1,0,0],[1,1,0],[1,2,0],[1,3,0],[1,4,0],[1,5,0],
[1,6,0],[1,7,0],[1,8,0],[1,9,0],[1,10,0],[2,0,0],[2,1,0],[2,2,0],[2,3,0],
[2,4,0],[2,5,0],[2,6,0],[2,7,0],[2,8,0],[2,9,0],[2,10,0],[3,0,0],[3,1,0],[3,2,0],
[3,3,0],[3,4,0],[3,5,0],[3,6,0],[3,7,0],[3,8,0],[3,9,0],[3,10,0],[4,0,0],[4,1,0],[4,2,0],
[4,3,0],[4,4,0],[4,5,0],[4,6,0],[4,7,0],[4,8,0],[4,9,0],[4,10,0],[5,0,0],[5,1,0],[5,2,0],
[5,3,0],[5,4,0],[5,5,0],[5,6,0],[5,7,0],[5,8,0],[5,9,0],[5,10,0]];*/
foreach ($query_result as  $key => $row) {

  	//[0,0],[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],[0,7],[0,8],[0,9],[0,10]
  	if($row->post_type == "short status"|| $row->post_type == "short link" || $row->post_type == "short note" )
  	{
  
  	$data[0][] =[0,$row->emotion,$row->comment_count];
  	
  	}
  	else if ($row->post_type == "long status"|| $row->post_type == "long link" || $row->post_type == "long note" )
  	{
  	
  	$data[0][]=[1,$row->emotion,$row->comment_count];
  	}
  	else if ($row->post_type == "short photo"|| $row->post_type == "short album"  )
  	{
  	
  	$data[0][] =[2,$row->emotion,$row->comment_count];
  	}
  	else if ($row->post_type == "long photo"|| $row->post_type == "long album"  )
  	{
  	
  	$data[0][] =[3,$row->emotion,$row->comment_count];
  	}
  	else if ($row->post_type == "short video"|| $row->post_type == "short music")
  	{
  	
  	$data[0][] =[4,$row->emotion,$row->comment_count];
  	}
  		else if ($row->post_type == "long video"|| $row->post_type == "long music")
  	{
  	
  	$data[0][] =[5,$row->emotion,$row->comment_count];
  	}
    $data[1][] =$row->comment_count;

}


echo json_encode($data);
	    }

	       public function getInteractionByPostType()
    {
    	$brand_id=Input::get('brand_id');
        
        /*$brand_id = 51;*/

	 if(null !== Input::get('fday'))
	      {
	    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
	      }
	      else
	      {
	      $dateBegin=date('Y-m-d');
	      }
	    

	    if(null !==Input::get('sday'))
	    {

	 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
	    }
	    
	    else
	    {
	$dateEnd=date('Y-m-d');

	           }

	/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 05')));
	$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 05')));*/

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}

$query = "SELECT (sum(Liked) +sum(Love) +sum(Haha) +sum(Angry) +sum(Sad) +sum(Wow) +SUM(replace(shared, ',', '')) ) interaction , 
  count(*) post_count, CONCAT( IF(CHAR_LENGTH(posts.message) > 150 ,'long','short'),' ',type ) post_type  ".
" FROM temp_".$brand_id."_posts  posts  ".
" where (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_inbound_con.
" GROUP By post_type";


$query_result = DB::select($query);

$data=[[0,0,0,0,0,0],[0,0,0,0,0,0]];

foreach ($query_result as  $key => $row) {
	if($row->post_type == "short status"|| $row->post_type == "short link" || $row->post_type == "short note" )
  	{
  
  	$data[0][0] =$row->interaction;
  	$post_data[1][0] =$row->post_count;
  	}
  	else if ($row->post_type == "long status"|| $row->post_type == "long link" || $row->post_type == "long note" )
  	{
  	
  		$data[0][1] =$row->interaction;
  		$data[1][1] =$row->post_count;
  	}
  	else if ($row->post_type == "short photo"|| $row->post_type == "short album"  )
  	{
  	
  		$data[0][2]=$row->interaction;
  		$data[1][2]=$row->post_count;
  	}
  	else if ($row->post_type == "long photo"|| $row->post_type == "long album"  )
  	{
  	
  		$data[0][3]=$row->interaction;
  		$data[1][3]=$row->post_count;
  	}
  	else if ($row->post_type == "short video"|| $row->post_type == "short music")
  	{
  	
  		$data[0][4]=$row->interaction;
  		$data[1][4]=$row->post_count;
  	}
  		else if ($row->post_type == "long video"|| $row->post_type == "long music")
  	{
  	
  		$data[0][5]=$row->interaction;
  		$data[1][5]=$row->post_count;
  	}
}

echo json_encode($data);
	    }

}
