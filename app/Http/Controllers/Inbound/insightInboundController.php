<?php

namespace App\Http\Controllers\Inbound;

use App\MongodbData;
use App\MongoPages;
use App\MongoFollowers;
use App\Comment;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;
class insightInboundController extends Controller
{
    use GlobalController;

    public function getEmotionByInboundPostType()
    {
    $brand_id=Input::get('brand_id');
        
        /*$brand_id = 51;*/

   if(null !== Input::get('fday'))
        {
      $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        }
        else
        {
        $dateBegin=date('Y-m-d');
        }
      

      if(null !==Input::get('sday'))
      {

   $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }
      
      else
      {
  $dateEnd=date('Y-m-d');

             }

  /*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 05')));
  $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 05')));*/


$query = "SELECT count(*) comment_count,cmts.emotion emotion_name, ".
" CASE WHEN cmts.emotion ='anger' THEN 0
WHEN cmts.emotion ='interest' THEN 1
WHEN cmts.emotion ='disgust' THEN 2
WHEN cmts.emotion ='fear' THEN 3
WHEN cmts.emotion ='joy' THEN 4
WHEN cmts.emotion ='like' THEN 5
WHEN cmts.emotion ='love' THEN 6
WHEN cmts.emotion ='neutral' THEN 7
WHEN cmts.emotion ='sadness' THEN 8
WHEN cmts.emotion ='surprise' THEN 9
ELSE 10 END AS emotion, " .
" CONCAT( IF(CHAR_LENGTH(posts.message) > 150 ,'long','short'),' ',type ) post_type,max(post_id) ".
" FROM temp_".$brand_id."_inbound_posts  posts Left JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id ".
" where cmts.post_id is not null and cmts.parent=''".
" and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ". 
" GROUP By cmts.emotion,post_type";

$query_result = DB::select($query);

$j=["anger","interest","disgust","fear","joy","like","love","neutral","sadness","surprise","trust"];
$i = ["Short status","Long Posts","Short with image","Long with image","Short with video","Long with video"];
$data=[];
/*$data = [[0,0,0],[0,1,0],[0,2,0],[0,3,0],[0,4,0],[0,5,0],[0,6,0],[0,7,0],
[0,8,0],[0,9,0],[0,10,0],[1,0,0],[1,1,0],[1,2,0],[1,3,0],[1,4,0],[1,5,0],
[1,6,0],[1,7,0],[1,8,0],[1,9,0],[1,10,0],[2,0,0],[2,1,0],[2,2,0],[2,3,0],
[2,4,0],[2,5,0],[2,6,0],[2,7,0],[2,8,0],[2,9,0],[2,10,0],[3,0,0],[3,1,0],[3,2,0],
[3,3,0],[3,4,0],[3,5,0],[3,6,0],[3,7,0],[3,8,0],[3,9,0],[3,10,0],[4,0,0],[4,1,0],[4,2,0],
[4,3,0],[4,4,0],[4,5,0],[4,6,0],[4,7,0],[4,8,0],[4,9,0],[4,10,0],[5,0,0],[5,1,0],[5,2,0],
[5,3,0],[5,4,0],[5,5,0],[5,6,0],[5,7,0],[5,8,0],[5,9,0],[5,10,0]];*/
foreach ($query_result as  $key => $row) {

    //[0,0],[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],[0,7],[0,8],[0,9],[0,10]
    if($row->post_type == "short status"|| $row->post_type == "short link" || $row->post_type == "short note" )
    {
  
    $data[0][] =[0,$row->emotion,$row->comment_count];
    
    }
    else if ($row->post_type == "long status"|| $row->post_type == "long link" || $row->post_type == "long note" )
    {
    
    $data[0][]=[1,$row->emotion,$row->comment_count];
    }
    else if ($row->post_type == "short photo"|| $row->post_type == "short album"  )
    {
    
    $data[0][] =[2,$row->emotion,$row->comment_count];
    }
    else if ($row->post_type == "long photo"|| $row->post_type == "long album"  )
    {
    
    $data[0][] =[3,$row->emotion,$row->comment_count];
    }
    else if ($row->post_type == "short video"|| $row->post_type == "short music")
    {
    
    $data[0][] =[4,$row->emotion,$row->comment_count];
    }
      else if ($row->post_type == "long video"|| $row->post_type == "long music")
    {
    
    $data[0][] =[5,$row->emotion,$row->comment_count];
    }
    $data[1][] =$row->comment_count;

}


echo json_encode($data);
      }

      public function getserviceAnalysis()
    {
      $brand_id=Input::get('brand_id');
        
        // $brand_id = 59;

   if(null !== Input::get('fday'))
        {
      $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        }
        else
        {
        $dateBegin=date('Y-m-d');
        }
      

      if(null !==Input::get('sday'))
      {

   $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }
      
      else
      {
  $dateEnd=date('Y-m-d');

             }

  // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 05')));
  // $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 05')));

 $data_analysis = [];
  $query = "SELECT tags ".
" FROM temp_".$brand_id."_inbound_comments ".
" WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ";
$tags =$this->gettags();
$query_result = DB::select($query);
foreach ($query_result as  $key => $row) {
  //Grouping Tag 
  foreach ($tags as  $tag_key => $tag_row) {
        if (strpos($row->tags, $tag_row->name) !== false)
        {
            $service_name=$tag_row->name ;
               if (!array_key_exists($service_name , $data_analysis)) {
    $data_analysis[$service_name] = array(
                'name' => $service_name,
                'value' =>1
            );
        }
        else
            {
            $data_analysis[$service_name]['value'] = (int) $data_analysis[$service_name]['value'] +  1;
            }
        }
  }

  }

echo json_encode($data_analysis);
      }


	    public function getInteractionByInboundPostType()
    {
    $brand_id=Input::get('brand_id');
        
        /*$brand_id = 51;*/

   if(null !== Input::get('fday'))
        {
      $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
        }
        else
        {
        $dateBegin=date('Y-m-d');
        }
      

      if(null !==Input::get('sday'))
      {

   $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
      }
      
      else
      {
  $dateEnd=date('Y-m-d');

             }

  /*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 05')));
  $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 05')));*/



$query = "SELECT (sum(Liked) +sum(Love) +sum(Haha) +sum(Angry) +sum(Sad) +sum(Wow) +SUM(replace(shared, ',', '')) ) interaction , 
  count(*) post_count, CONCAT( IF(CHAR_LENGTH(posts.message) > 150 ,'long','short'),' ',type ) post_type  ".
" FROM temp_".$brand_id."_inbound_posts  posts  ".
" where (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".
" GROUP By post_type";


$query_result = DB::select($query);

$data=[[0,0,0,0,0,0],[0,0,0,0,0,0]];

foreach ($query_result as  $key => $row) {
  if($row->post_type == "short status"|| $row->post_type == "short link" || $row->post_type == "short note" )
    {
  
    $data[0][0] =$row->interaction;
    $post_data[1][0] =$row->post_count;
    }
    else if ($row->post_type == "long status"|| $row->post_type == "long link" || $row->post_type == "long note" )
    {
    
      $data[0][1] =$row->interaction;
      $data[1][1] =$row->post_count;
    }
    else if ($row->post_type == "short photo"|| $row->post_type == "short album"  )
    {
    
      $data[0][2]=$row->interaction;
      $data[1][2]=$row->post_count;
    }
    else if ($row->post_type == "long photo"|| $row->post_type == "long album"  )
    {
    
      $data[0][3]=$row->interaction;
      $data[1][3]=$row->post_count;
    }
    else if ($row->post_type == "short video"|| $row->post_type == "short music")
    {
    
      $data[0][4]=$row->interaction;
      $data[1][4]=$row->post_count;
    }
      else if ($row->post_type == "long video"|| $row->post_type == "long music")
    {
    
      $data[0][5]=$row->interaction;
      $data[1][5]=$row->post_count;
    }
}

echo json_encode($data);	    }

	     

}
