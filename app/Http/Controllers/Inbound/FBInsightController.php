<?php

namespace App\Http\Controllers\Inbound;


use App\fb_token;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;
use App\MongoboundPost;
use Carbon\Carbon;
use App\MongoFan;
use Config;
class FBInsightController extends Controller
{
     //
	use GlobalController;

	public function getFbReach()
	{
		$data=[];
		$date_preset= Input::get('date_preset');
		$period= Input::get('period');

		if(null !== Input::get('page_name') && '' !== Input::get('page_name'))
		{
			$page_name= Input::get('page_name');
		}
		else
		{
			$my_pages= $this->getOwnPage(Input::get('brand_id'));
			$my_pages=explode(',', $my_pages);
			$page_name=$my_pages[0];
		}

        // $fb_token =Config::get('constants.fb_token'); 
		$fb_tokens=fb_token::where('page_name', $page_name)->first();
		if($fb_tokens !== null)
			$fb_token=$fb_tokens->token;
		else
			$fb_token='';
		$api_url=Config::get('constants.graph_api_url'); 
		$client = new Client(['base_uri' =>  $api_url,'headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
		$organic_uri = 'page_impressions_organic_unique';
		$paid_uri='page_impressions_paid_unique';
		$total_uri='page_impressions_unique';



            // $date_preset= 'last_30d';
            // $period= 'day';
            // $page_name= 'wunzinn2home';
		$formData = array(
			'access_token' =>  $fb_token,
			'date_preset' => $date_preset,
			'period' => $period,
			'page_name'=>$page_name

		);       
		if(null !== Input::get('fday'))
		{
			$dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
		}
		else
		{
			$dateBegin=date('Y-m-d');
		}


		if(null !==Input::get('sday'))
		{

			$dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
		}

		else
		{
			$dateEnd=date('Y-m-d');

		}

          // $formData = json_encode($formData);

		$api_response = $client->post($organic_uri, [
			'form_params' => $formData,
		]);


		$result = ($api_response->getBody()->getContents());

		$organic_result = json_decode($result, true);
		if(isset($organic_result['data'][0]['values']))
			$organic_result = $organic_result['data'][0]['values'];

		$api_response = $client->post($paid_uri, [
			'form_params' => $formData
		]);

		$result = ($api_response->getBody()->getContents());
		$paid_result = json_decode($result, true);
		if(isset($paid_result['data'][0]['values']))
			$paid_result = $paid_result['data'][0]['values'];


		$api_response = $client->post($total_uri, [
			'form_params' => $formData,
		]);

		$result = ($api_response->getBody()->getContents());
		$total_result = json_decode($result, true);
		if(isset($total_result['data'][0]['values']))
		{
			$total_result = $total_result['data'][0]['values'];

			foreach ($total_result as  $key => $row)
			{
				$organic_value=0;
				$paid_value =0;
				$total_value=0;
				$end_time='';

				$total_date = strtotime ( '-1 day' , strtotime ( $total_result[$key]['end_time'] ) ) ;
				$total_date = date ('Y-m-d' , $total_date );

				$organic_date = strtotime ('-1 day' , strtotime($organic_result[$key]['end_time']) ) ;
				$organic_date = date ('Y-m-d' , $organic_date );

				$paid_date = strtotime ('-1 day' , strtotime($paid_result[$key]['end_time']) ) ;
				$paid_date = date ('Y-m-d' , $paid_date );
              // echo($total_date);

				if ( $organic_date >= $dateBegin  &&  $organic_date <= $dateEnd)
					$organic_value=$organic_result[$key]['value'];
				if ($paid_date >= $dateBegin  && $paid_date  <= $dateEnd)
					$paid_value=$paid_result[$key]['value'];
              //   $newarray[] = $item;
				if ( $total_date >= $dateBegin  &&  $total_date  <= $dateEnd)
				{
					$total_value=$row['value'];
					$end_time =$total_date;
					$data[] =[
						'total' => $total_value,
						'organic' => $organic_value,
						'paid' => $paid_value,
						'end_time' =>  $end_time,

					];
				}


			}

		}






		echo json_encode($data);

	}
	public function getCityReach()
	{
		$date_preset= 'last_7d';
		$period='day';
		if(null !== Input::get('page_name') && '' !== Input::get('page_name'))
		{
			$page_name= Input::get('page_name');
		}
		else
		{
              // $my_pages= $this->getOwnPage(Input::get('brand_id'));
			$my_pages= $this->getOwnPage(Input::get('brand_id'));
			$my_pages=explode(',', $my_pages);
			$page_name=$my_pages[0];
		}
        //$fb_token =Config::get('constants.fb_token'); 
		$fb_tokens=fb_token::where('page_name', $page_name)->first();
		if($fb_tokens !== null)
			$fb_token=$fb_tokens->token;
		else
			$fb_token='';
		$api_url=Config::get('constants.graph_api_url'); 
		$client = new Client(['base_uri' =>  $api_url,'headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
		$city_uri ='page_impressions_by_city_unique';



            //$page_name= 'wunzinn2home';
		$formData = array(
			'access_token' =>$fb_token,
			'date_preset'  =>$date_preset,
			'period'       =>$period,
			'page_name'    =>$page_name
		);
		$api_response = $client->post($city_uri, [
			'form_params' => $formData,
		]);


		$result = ($api_response->getBody()->getContents());

		$city_result = json_decode($result, true);
		$city_result = $city_result['data'][0]['values'];
		$city_result = $city_result[count($city_result)-1]['value'];

		arsort($city_result);
		$city_result = array_slice($city_result, 0, 5, true);
            // dd($city_result);
		echo json_encode($city_result);
	}

	public function GetageGenderReach()
	{
		$date_preset= 'last_7d';
		$period='day';
		if(null !== Input::get('page_name') && '' !== Input::get('page_name'))
		{
			$page_name= Input::get('page_name');
		}
		else
		{
              // $my_pages= $this->getOwnPage(Input::get('brand_id'));
			$my_pages= $this->getOwnPage(Input::get('brand_id'));
			$my_pages=explode(',', $my_pages);
			$page_name=$my_pages[0];
		}
        //$fb_token =Config::get('constants.fb_token'); 
		$fb_tokens=fb_token::where('page_name', $page_name)->first();
		if($fb_tokens !== null)
			$fb_token=$fb_tokens->token;
		else
			$fb_token='';
		$api_url=Config::get('constants.graph_api_url'); 
		$client = new Client(['base_uri' =>  $api_url,'headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
		$city_uri ='page_impressions_by_age_gender_unique';



           // $page_name= 'wunzinn2home';
		$formData = array(
			'access_token' =>$fb_token,
			'date_preset'  =>$date_preset,
			'period'       =>$period,
			'page_name'    =>$page_name
		);
		$api_response = $client->post($city_uri, [
			'form_params' => $formData,
		]);


		$result = ($api_response->getBody()->getContents());

		$city_result = json_decode($result, true);
		$city_result = $city_result['data'][0]['values'];
		$city_result = $city_result[count($city_result)-1]['value'];
		$data=[];
		$Total=0;
		foreach ($city_result as  $key => $row)
		{

			$pieces = explode(".",$key);
			$G_Type =$pieces[0];
			$Label  =$pieces[1];
			$Fvalue = 0;$Mvalue = 0;
			if($G_Type === 'F') $Fvalue= $row ; else if ($G_Type === 'M') $Mvalue = $row;

			$Total = (int) $Total + (int) $Fvalue + (int) $Mvalue;

			if (!array_key_exists($Label , $data)) {
				$data[$Label] = array(
					'Label' => $Label,
					'F' =>   $Fvalue,
					'M' =>   $Mvalue,
					'T' => $Total,

				);
			}
			else
			{
				$data[$Label]['F'] = (int) $data[$Label]['F'] + $Fvalue;
				$data[$Label]['M'] = (int) $data[$Label]['M'] + $Mvalue;
				$data[$Label]['T'] = $Total;
			}
		}

		echo json_encode($data);
	}
           //Fans
	public function getcityFan()
	{
		$date_preset= 'last_7d';
		$period='lifetime';
		$page_name= Input::get('page_name');
        //$fb_token =Config::get('constants.fb_token'); 
		$fb_tokens=fb_token::where('page_name', $page_name)->first();
		if($fb_tokens !== null)
			$fb_token=$fb_tokens->token;
		else
			$fb_token='';
		$api_url=Config::get('constants.graph_api_url'); 
		$client = new Client(['base_uri' =>  $api_url,'headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
		$city_uri ='page_fans_city';



           // $page_name= 'wunzinn2home';
		$formData = array(
			'access_token' =>$fb_token,
			'date_preset'  =>$date_preset,
			'period'       =>$period,
			'page_name'    =>$page_name
		);
		$api_response = $client->post($city_uri, [
			'form_params' => $formData,
		]);


		$result = ($api_response->getBody()->getContents());

		$city_result = json_decode($result, true);
		$city_result = $city_result['data'][0]['values'];

		echo json_encode($city_result[count($city_result)-1]['value']);
	}

	public function GetageGenderFan()
	{
		$date_preset= 'last_7d';
		$period='lifetime';
		$page_name= Input::get('page_name');
        //$fb_token =Config::get('constants.fb_token'); 
		$fb_tokens=fb_token::where('page_name', $page_name)->first();
		if($fb_tokens !== null)
			$fb_token=$fb_tokens->token;
		else
			$fb_token='';
		$api_url=Config::get('constants.graph_api_url'); 
		$client = new Client(['base_uri' =>  $api_url,'headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
		$city_uri ='page_fans_gender_age';



           // $page_name= 'wunzinn2home';
		$formData = array(
			'access_token' =>$fb_token,
			'date_preset'  =>$date_preset,
			'period'       =>$period,
			'page_name'    =>$page_name
		);
		$api_response = $client->post($city_uri, [
			'form_params' => $formData,
		]);


		$result = ($api_response->getBody()->getContents());

		$city_result = json_decode($result, true);
		$city_result = $city_result['data'][0]['values'];
		$city_result = $city_result[count($city_result)-1]['value'];
		$data=[];
		$Total=0;
		foreach ($city_result as  $key => $row)
		{

			$pieces = explode(".",$key);
			$G_Type =$pieces[0];
			$Label  =$pieces[1];
			$Fvalue = 0;$Mvalue = 0;
			if($G_Type === 'F') $Fvalue= $row ; else if ($G_Type === 'M') $Mvalue = $row;

			$Total = (int) $Total + (int) $Fvalue + (int) $Mvalue;

			if (!array_key_exists($Label , $data)) {
				$data[$Label] = array(
					'Label' => $Label,
					'F' =>   $Fvalue,
					'M' =>   $Mvalue,
					'T' => $Total,

				);
			}
			else
			{
				$data[$Label]['F'] = (int) $data[$Label]['F'] + $Fvalue;
				$data[$Label]['M'] = (int) $data[$Label]['M'] + $Mvalue;
				$data[$Label]['T'] = $Total;
			}
		}

		echo json_encode($data);
	}
	public function GetPageFan()
	{
		$date_preset= Input::get('date_preset');
            //$period= Input::get('period');
		if(null !== Input::get('page_name') && '' !== Input::get('page_name'))
		{
			$page_name= Input::get('page_name');
		}
		else
		{
              // $my_pages= $this->getOwnPage(Input::get('brand_id'));
			$my_pages= $this->getOwnPage(Input::get('brand_id'));
			$my_pages=explode(',', $my_pages);
			$page_name=$my_pages[0];
		}
            //$fb_token =Config::get('constants.fb_token');
		$fb_tokens=fb_token::where('page_name', $page_name)->first();
		if($fb_tokens !== null)
			$fb_token=$fb_tokens->token;
		else
			$fb_token='';

            //$period= Input::get('period');

            //$date_preset= 'last_30d';
            // $period= 'day';
            // $page_name= 'wunzinn2home';

		if(null !== Input::get('fday'))
		{
			$dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));

		}
		else
		{
			$dateBegin=date('Y-m-d');

		}


		if(null !==Input::get('sday'))
		{

			$dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
		}

		else
		{
			$dateEnd=date('Y-m-d');

		}



		$api_url=Config::get('constants.graph_api_url'); 
		$client = new Client(['base_uri' =>  $api_url,'headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
		$fan_uri = 'page_fans';
		$fan_online_uri='page_fans_online_per_day';





          // $formData = json_encode($formData);

		$api_response = $client->post($fan_uri, [
			'form_params' =>[ 
				'access_token' =>  $fb_token,
				'date_preset' => $date_preset,
				'period' => 'lifetime',
				'page_name'=>$page_name]
			]);


		$result = ($api_response->getBody()->getContents());

		$fan_result = json_decode($result, true);

		$fan_result = $fan_result['data'][0]['values'];
         //   dd($fan_result);
		$api_response = $client->post($fan_online_uri, [
			'form_params' => [ 
				'access_token' =>  $fb_token,
				'date_preset' => $date_preset,
				'period' => 'day',
				'page_name'=>$page_name]
			]);

		$result = ($api_response->getBody()->getContents());
		$fan_online_result = json_decode($result, true);
		$fan_online_result = $fan_online_result['data'][0]['values'];

		$data=[];
		foreach ($fan_result as  $key => $row)
		{
			$fan_value=0;
			$fan_online_value =0;

			$end_time='';

			$fan_date = strtotime ( '-1 day' , strtotime ( $fan_result[$key]['end_time'] ) ) ;
			$fan_date = date ('Y-m-d' , $fan_date );

			if(isset($fan_online_result[$key]['end_time']))
			{
				$fan_online_date = strtotime ('-1 day' , strtotime($fan_online_result[$key]['end_time']) ) ;
				$fan_online_date = date ('Y-m-d' , $fan_online_date );

               // echo($total_date);

				if ( $fan_online_date >= $dateBegin  &&  $fan_online_date <= $dateEnd)
					$fan_online_value=$fan_online_result[$key]['value'];
			}
			else
			{
				$fan_online_value = 0;
			}


              //   $newarray[] = $item;
			if ( $fan_date >= $dateBegin  &&  $fan_date  <= $dateEnd)
			{
				$fan_value=$row['value'];
				$end_time =$fan_date;
				$data[] =[
					'fan' => $fan_value,
					'fan_online' => $fan_online_value,
					'end_time' =>  $end_time,

				];
			}


		}

		echo json_encode($data);
	}

	public function GetPageFanDif()
	{
		$data=[];


		$date_preset= Input::get('date_preset');
		if(null !== Input::get('page_name') && '' !== Input::get('page_name'))
		{
			$page_name= Input::get('page_name');
		}
		else
		{

			$my_pages= $this->getOwnPage(Input::get('brand_id'));
			$my_pages=explode(',', $my_pages);
			$page_name=$my_pages[0];
		}
       //    $page_name="wunzinn2home";

		$fb_tokens=fb_token::where('page_name', $page_name)->first();
		if($fb_tokens !== null)
			$fb_token=$fb_tokens->token;
		else
			$fb_token='';
		if(null !== Input::get('fday'))
		{
			$dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
		}
		else
		{
			$dateBegin=date('Y-m-d');
		}

		if(null !==Input::get('sday'))
		{

			$dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
		}

		else
		{
			$dateEnd=date('Y-m-d');

		}



         //if $fb_token is blank get data from mongodb
		if($fb_token == '')
		{
			// $dateBegin = strtotime ( '-1 day' , strtotime ($dateBegin) ) ;
			// $dateBegin = date ('Y-m-d' , $dateBegin );

			// $dateEnd = strtotime ( '+1 day' , strtotime ($dateEnd) ) ;
			// $dateEnd = date ('Y-m-d' , $dateEnd );
			// $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime($dateBegin)* 1000);
			// $dateEnd   = new MongoDB\BSON\UTCDateTime(strtotime($dateEnd)* 1000);
			// // dd($dateEnd);
			// $MongoFan=MongoFan::raw(function ($collection) use($page_name,$dateBegin,$dateEnd) {//print_r($filter);

			// 	    return $collection->aggregate([
			// 	        [
			// 	        '$match' =>[
			// 	             '$and'=> [ 
			// 	             // ['createdAt' => ['$gte' => $dateBegin, '$lt' => $dateEnd]],
			// 	             ['page_name'=> $page_name],
			// 	             // ['createdAt'=> ['$exists'=>true]],
				          
			// 	                      ]
			// 	        ]  
				                   
			// 	       ],
				        
			// 	    ]);
			// 	})->toArray();

			// dd($MongoFan);
			$MongoFan = DB::connection('mongodb')->collection('followers')->whereBetween('createdAt', array( Carbon::createFromDate(date('Y', strtotime($dateBegin)),date('m', strtotime($dateBegin)), date('d', strtotime($dateBegin))),
				Carbon::createFromDate(date('Y', strtotime($dateEnd)),date('m', strtotime($dateEnd)),date('d', strtotime($dateEnd)))))->orderBy('createdAt', 'asc')->where('page_name',$page_name)->get();
			// $MongoFan = DB::connection('mongodb')->collection('followers')->whereBetween('createdAt', array( Carbon::createFromDate(2018,12,1),
			// Carbon::createFromDate(2018,12,5)))->where('page_name',$page_name)->get();
			//dd($MongoFan);

			foreach ($MongoFan as  $key => $row) {
				$fan_value=0;
	
				$end_time='';

				$fan_date = strtotime ( '-1 day' , strtotime (  $MongoFan[$key]['date'] ) ) ;
				$fan_date = date ('Y-m-d' , $fan_date );
			

					if($key <> 0)
					{
						$fan_diff=$MongoFan[$key]['fan_count']-$MongoFan[$key-1]['fan_count'];
						$fan_abs_diff=abs($fan_diff);
						$fan_value=$row['fan_count'];
						$end_time =$fan_date;
					}
					else
					{
						$fan_diff=$row['fan_count'];
						$fan_abs_diff=abs($fan_diff);
						$fan_value=$row['fan_count'];
						$end_time =$fan_date;
					}

					$data[] =[
						'fan' => $fan_value,
						'fan_diff' => $fan_diff,
						'fan_abs_diff' => $fan_abs_diff,
						'end_time' =>  $end_time,

					];
				//	$i=$i+1;
				
			}
		}

		else
		{
			$api_url=Config::get('constants.graph_api_url'); 
		$client = new Client(['base_uri' =>  $api_url,'headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
		$fan_uri = 'page_fans';
		$fan_online_uri='page_fans_online_per_day';




		$dateBegin = strtotime ( '-1 day' , strtotime ( $dateBegin) ) ;
		$dateBegin = date ('Y-m-d' , $dateBegin );


		$api_response = $client->post($fan_uri, [
			'form_params' =>[ 
				'access_token' =>  $fb_token,
				'date_preset' => $date_preset,
				'period' => 'lifetime',
				'page_name'=>$page_name]
			]);


		$result = ($api_response->getBody()->getContents());

		$fan_result = json_decode($result, true);
		if(isset($fan_result['data'][0]['values']))
		{
			$fan_result = $fan_result['data'][0]['values'];
			$i=0;

			foreach ($fan_result as  $key => $row)
			{
				$fan_value=0;

				$end_time='';

				$fan_date = strtotime ( '-1 day' , strtotime ( $fan_result[$key]['end_time'] ) ) ;
				$fan_date = date ('Y-m-d' , $fan_date );

				if ( $fan_date >= $dateBegin  &&  $fan_date  <= $dateEnd)
				{

					if($i <> 0)
					{
						$fan_diff=$fan_result[$key]['value']-$fan_result[$key-1]['value'];
						$fan_abs_diff=abs($fan_diff);
						$fan_value=$row['value'];
						$end_time =$fan_date;
					}
					else
					{
						$fan_diff=$row['value'];
						$fan_abs_diff=abs($fan_diff);
						$fan_value=$row['value'];
						$end_time =$fan_date;
					}

					$data[] =[
						'fan' => $fan_value,
						'fan_diff' => $fan_diff,
						'fan_abs_diff' => $fan_abs_diff,
						'end_time' =>  $end_time,

					];
					$i=$i+1;
				}


			}
		}
		}
		


		echo json_encode($data);
	}

	public function getFbReachCompare()
	{
		$date_preset= Input::get('date_preset');
		$period= Input::get('period');
		$page_name= Input::get('page_name');
        // $fb_token =Config::get('constants.fb_token'); 
        // $date_preset= 'this_year';
        //     $period= 'day';
        //     $page_name= 'wunzinn2home';
		$fb_tokens=fb_token::where('page_name', $page_name)->first();
		if($fb_tokens !== null)
			$fb_token=$fb_tokens->token;
		else
			$fb_token='';


		$api_url=Config::get('constants.graph_api_url'); 
		$client = new Client(['base_uri' =>  $api_url,'headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
		$organic_uri = 'page_impressions_organic_unique';
		$paid_uri='page_impressions_paid_unique';
		$total_uri='page_impressions_unique';




		$formData = array(
			'access_token' =>  $fb_token,
			'date_preset' => $date_preset,
			'period' => $period,
			'page_name'=>$page_name

		);       
		if(null !== Input::get('fday'))
		{
			$dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
		}
		else
		{
			$dateBegin=date('Y-m-d');
		}


		if(null !==Input::get('sday'))
		{

			$dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
		}

		else
		{
			$dateEnd=date('Y-m-d');

		}
 // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 08 01')));
 //           $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 11 01')));
          // $formData = json_encode($formData);

		$api_response = $client->post($organic_uri, [
			'form_params' => $formData,
		]);


		$result = ($api_response->getBody()->getContents());

		$organic_result = json_decode($result, true);
		$organic_result = $organic_result['data'][0]['values'];

		$api_response = $client->post($paid_uri, [
			'form_params' => $formData
		]);

		$result = ($api_response->getBody()->getContents());
		$paid_result = json_decode($result, true);
		$paid_result = $paid_result['data'][0]['values'];


		$api_response = $client->post($total_uri, [
			'form_params' => $formData,
		]);

		$result = ($api_response->getBody()->getContents());
		$total_result = json_decode($result, true);
		$total_result = $total_result['data'][0]['values'];
          // dd($total_result);
		$data=[];

		if($total_result!== null)
		{
			foreach ($total_result as  $key => $row)
			{
				$organic_value=0;
				$paid_value =0;
				$total_value=0;
				$end_time='';

				$total_date = strtotime ( '-1 day' , strtotime ( $total_result[$key]['end_time'] ) ) ;
				$total_date = date ('Y-m-d' , $total_date );

				$organic_date = strtotime ('-1 day' , strtotime($organic_result[$key]['end_time']) ) ;
				$organic_date = date ('Y-m-d' , $organic_date );

				$paid_date = strtotime ('-1 day' , strtotime($paid_result[$key]['end_time']) ) ;
				$paid_date = date ('Y-m-d' , $paid_date );
              // echo($total_date);

				if ( $organic_date >= $dateBegin  &&  $organic_date <= $dateEnd)
					$organic_value=$organic_result[$key]['value'];
				if ($paid_date >= $dateBegin  && $paid_date  <= $dateEnd)
					$paid_value=$paid_result[$key]['value'];
              //   $newarray[] = $item;
				if ( $total_date >= $dateBegin  &&  $total_date  <= $dateEnd)
				{

					$total_value=$row['value'];

					$periodLabel = date ('m-y' , strtotime($total_date) );

					if (!array_key_exists($periodLabel , $data)) {
						$data[$periodLabel] = array(
							'total' => $total_value,
							'organic' => $organic_value,
							'paid' => $paid_value,
							'end_time' => $periodLabel,
						);
					}
					else
					{
						$data[$periodLabel]['total'] = (int) $data[$periodLabel]['total'] + (int) $total_value;
						$data[$periodLabel]['organic'] = (int) $data[$periodLabel]['organic'] + (int)$organic_value;
						$data[$periodLabel]['paid'] = (int) $data[$periodLabel]['paid'] +(int) $paid_value;
					}
				}


			}
		}


// dd($data);
		echo json_encode($data);

	}

}
