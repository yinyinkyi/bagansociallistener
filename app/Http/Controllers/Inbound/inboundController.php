<?php

namespace App\Http\Controllers\Inbound;

use App\MongodbData;
use App\MongoPages;
use App\MongoFollowers;
use App\Comment;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GlobalController;
class inboundController extends Controller
{
   use GlobalController;
  

   public function getHighlighted()
{
 

   $brand_id=Input::get('brand_id');
   $type=Input::get('type');
 /* if($brand_id != 46 || $brand_id != 47)*/
// $brand_id=17; 
// $type="all";

         if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
  // $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 09 15')));
  // $dateEnd  =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 22')));

   $keyword_param=[];
   $removed_keyword=[];

$removed_query = "SELECT keyword FROM  keywords_removed where brand_id=".$brand_id;
$removed_result = DB::select($removed_query);
foreach ($removed_result as  $key => $row) {
          $removed_keyword[$row->keyword] =$row->keyword ;
          }

$keyword_query = "SELECT main_keyword FROM  project_keywords where project_id=".$brand_id;
$keyword_result = DB::select($keyword_query);

       foreach ($keyword_result as  $key => $row) {
          $keyword_param[] =$row->main_keyword ;
          }

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
  $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}
if($type == 'all')
$query = " SELECT cmts.id,cmts.wb_message FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN temp_".$brand_id."_inbound_posts posts ".
" on cmts.post_id=posts.id ".
" WHERE posts.id IS NOT NULL and cmts.parent = '' and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')";
else
$query = " SELECT cmts.id,cmts.wb_message FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN temp_".$brand_id."_inbound_posts posts ".
" on cmts.post_id=posts.id ".
" WHERE posts.id IS NOT NULL and cmts.parent = '' and cmts.interest = 1 and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')";

/*echo $query;
return;*/

 $data_message=[];
$query_result=[];
$query_result = DB::select($query);


do {
   $query_result = DB::select($query);
 /*dd($query_result);
   return ;*/
 foreach ($query_result as  $key => $row) {
   if(isset($row->wb_message))
  $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$row->wb_message);

  }

  }while (count($data_message)<=0 && count($query_result)>0 );
  // dd($data_message);

$topics=[];

if(count($data_message)>0)
{


$client = new Client(['base_uri' => 'http://35.185.97.177:5001/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
$uri_keyword = 'keyword_get';

    $formData = array(
    'keywords' => $keyword_param,
    'messages' =>  $data_message,
    'ntopics' => '100'
   
);


$formData = json_encode($formData);
/*dd($formData);*/
// $path = storage_path('app/data_output/wunzinn_error_100218.json');
// $this->doJson($path,$formData);
// return;

$keyword_response = $client->post($uri_keyword, [
    'form_params' => [
        'raw' =>  $formData,
    ],
]);

$keyword_result = $keyword_response->getBody()->getContents();


$topics = json_decode($keyword_result, true);

foreach($topics as $key => $val)
{

$found=array_search($val['topic_name'], $removed_keyword) ;
   if($found === $val['topic_name'])
    {

       unset($topics[$key]);
    }
}
  

  }

  usort($topics, function($a, $b) {
    if($a['weight']==$b['weight']) return 0;
    return $a['weight'] < $b['weight']?1:-1;
});//Decending

  $topics = array_slice($topics, 0, 50, true);
   echo json_encode($topics);



}

function doJson($path,$json)
{
    $fp = fopen($path, 'w');
   fwrite($fp, $json);
   fclose($fp);
}

public function getinboundReaction()
    {
    //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      if(null !== Input::get('sentiment'))
      $filter_sentiment=Input::get('sentiment'); 

     // $brand_id=17;
     // $groupType='day';
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
// $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 09 15')));
// $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 21')));

 // $groupType="month";
           //check for comparison
           if(null !== Input::get('filter_page_name'))
           {
             $filter_pages= " page_name in ('".Input::get('filter_page_name')."') ";
           }
           else   if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);

           }
             

 $group_period ="";
 $add_con="";
 $add_comment_con ="";

if($groupType=="month")
{
//$group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
  $group_period ="%Y-%m"; 
 }

   else 
 {
$group_period ="%Y-%m-%d";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}
if($filter_sentiment !== '')
{
  $add_con .= " and sentiment = '".$filter_sentiment."'";
  $add_comment_con .= " and cmts.checked_sentiment  = '".$filter_sentiment."'";
}



$query = " SELECT sum(Liked) Liked,sum(Love) Love,sum(Haha) Haha,sum(Angry) Angry,sum(Sad) Sad,sum(Wow) Wow,count(*)  post_count, DATE_FORMAT(posts.created_time, '".$group_period."') created_time, ".
" SUM(replace(shared, ',', '')) shared FROM temp_".$brand_id."_inbound_posts posts WHERE  (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ". $filter_pages.  $add_con . 
" GROUP BY DATE_FORMAT(posts.created_time, '".$group_period."') ORDER BY DATE(created_time) DESC " ;

// echo $query;
$query_result = DB::select($query);
 // dd($query_result);
   $data = [];
   $total=0;

    foreach ($query_result as  $key => $row) {

                $datetime = $row ->created_time;
                $periodLabel ="";
                $weekcount ="";
                if($groupType == "week")
                {
                  $weekperiod=$this ->rangeWeek ($datetime);
                  $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
                 if (!array_key_exists($periodLabel , $data)) {
                  $data[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'Wow' =>  $row ->Wow,
                'Love' => $row ->Love,
                'Angry' => $row ->Angry,
                'Sad' => $row ->Sad,
                'Haha' => $row ->Haha,
                'Like' =>$row ->Liked,
                'periods' => $periodLabel,
                'shared' =>$row ->shared,
                'post_count' =>$row ->post_count,
            );
                                                                }
                else
                {
$data[$periodLabel]['Wow'] = (int) $data[$periodLabel]['Wow'] + (int) $row ->Wow;
$data[$periodLabel]['Love'] = (int) $data[$periodLabel]['Love'] + (int)$row ->Love;
$data[$periodLabel]['Angry'] = (int) $data[$periodLabel]['Angry'] +(int) $row ->Angry;
$data[$periodLabel]['Sad'] = (int) $data[$periodLabel]['Sad'] + (int) $row ->Sad;
$data[$periodLabel]['Haha'] = (int) $data[$periodLabel]['Haha'] + (int) $row ->Haha;
$data[$periodLabel]['Like'] = (int) $data[$periodLabel]['Like'] + (int) $row ->Liked;
$data[$periodLabel]['periods'] =  $datetime;
$data[$periodLabel]['shared'] =(int) $data[$periodLabel]['shared'] +(int)$row ->shared;
$data[$periodLabel]['post_count'] =(int) $data[$periodLabel]['post_count'] +(int)$row ->post_count;

                }
   
 
                }
    else 
    {
                 $data[$datetime]['Wow'] = $row ->Wow;
                 $data[$datetime]['Love'] =$row ->Love;
                 $data[$datetime]['Angry'] =$row ->Angry;
                 $data[$datetime]['Sad'] = $row ->Sad;
                 $data[$datetime]['Haha'] =$row ->Haha;
                 $data[$datetime]['Like'] =$row ->Liked;
                 $data[$datetime]['periods'] =  $datetime;
                 $data[$datetime]['periodLabel'] =  $datetime;
                 $data[$datetime]['shared'] =$row ->shared;
                 $data[$datetime]['post_count'] =$row ->post_count;
    }
                
              
                 
               
            }


//$data = $this->unique_multidim_array($data,'periodLabel'); 
 

    echo json_encode($data);


}

public function getInboundsentiment()
    {
     //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');

      $groupType=Input::get('periodType');
      $filter_keyword ='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      // $brand_id=17;
      // $groupType='day';
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

   }
            if(null !== Input::get('filter_page_name'))
           {
             $filter_pages= " page_name in ('".Input::get('filter_page_name')."') ";
           }
           else if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {

              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

 // dd($filter_pages);
// $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 09 17')));
// $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 17')));

 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con="";
if($groupType=="month")
{
$group_period =" DATE_FORMAT(cmts.created_time, '%Y-%m')"; 
 }

   else 
 {
$group_period =" DATE_FORMAT(cmts.created_time, '%Y-%m-%d')";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and cmts.message Like '%".$filter_keyword."%'";
}

$query = "SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,".$group_period." created_time  " .
 "FROM temp_".$brand_id."_inbound_comments cmts INNER JOIN temp_".$brand_id."_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND ".$filter_pages.
" AND  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_con.
"GROUP BY ".$group_period .
" ORDER BY DATE(cmts.created_time) ASC";
// echo $query;
// return;

$query_result = DB::select($query);

   $data = [];
   $total=0;

   $data_sentiment = [];
   $data_mention = [];
   $total=0;
   $data_message = [];

    foreach ($query_result as  $key => $row) {
          
              $utcdatetime = $row->created_time;
         
               
               $pos =$row->positive;
               $neg =$row->negative;
               $nan =$row->neutral;
          
                $request['periodLabel'] ="";
      
                if($groupType == "week")
{
    $weekperiod=$this ->rangeWeek ($utcdatetime);
    $periodLabel =$weekperiod['start'] . " - " . $weekperiod['end'] ;
    
 if (!array_key_exists($periodLabel , $data_sentiment)) {
    $data_sentiment[$periodLabel] = array(
                'periodLabel' => $periodLabel,
                'positive' => $pos,
                'negative' => $neg,
                'neutral' => $nan,
                'periods' =>$utcdatetime,
            );
}
else
{
 $data_sentiment[$periodLabel]['positive'] = $data_sentiment[$periodLabel]['positive'] + $pos;
 $data_sentiment[$periodLabel]['negative'] = $data_sentiment[$periodLabel]['negative'] + $neg;
 $data_sentiment[$periodLabel]['neutral'] = $data_sentiment[$periodLabel]['neutral'] + $nan;
 $data_sentiment[$periodLabel]['periods'] =$utcdatetime;
}
 
                  


}
 else
{
 $data_sentiment[$utcdatetime]['periodLabel'] = $utcdatetime;
 $data_sentiment[$utcdatetime]['positive'] = $pos;
 $data_sentiment[$utcdatetime]['negative'] = $neg;
 $data_sentiment[$utcdatetime]['neutral'] =  $nan;
 $data_sentiment[$utcdatetime]['periods'] = $utcdatetime;

}

                
   }

 //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 

    echo json_encode($data_sentiment);


}
public function getTagSentiment()
    {
     //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');

      $groupType=Input::get('periodType');
      $filter_keyword ='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

     //$brand_id=17;
      // $groupType='day';
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

   }
            if(null !== Input::get('filter_page_name'))
           {
             $filter_pages= " page_name in ('".Input::get('filter_page_name')."') ";
           }
           else if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {

              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

 // dd($filter_pages);
// $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 10 25')));
// $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 31')));

 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con="";
if($groupType=="month")
{
$group_period =" DATE_FORMAT(cmts.created_time, '%Y-%m')"; 
 }

   else 
 {
$group_period =" DATE_FORMAT(cmts.created_time, '%Y-%m-%d')";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and cmts.message Like '%".$filter_keyword."%'";
}

$query = "SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)) neutral,tags  " .
 "FROM temp_".$brand_id."_inbound_comments cmts INNER JOIN temp_".$brand_id."_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND tags<>'' AND ".$filter_pages.
" AND  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_con .
"Group By tags";

// echo $query;
//  return;

$query_result = DB::select($query);
// dd($query_result);
   $data = [];
   $total=0;

   $data_tag = [];
   $total=0;
  

    foreach ($query_result as  $key => $row) {
          
                          
               $pos =$row->positive;
               $neg =$row->negative;
               $neutral =$row->neutral;
               $tags =$row->tags;
        // if( (int) $pos !== 0 || (int) $neg !== 0)
        // {
            $arr_tag=explode(',', $tags);
          
               $request['tagLabel'] ="";
      foreach ($arr_tag as  $key => $tag) {
            if (!array_key_exists($tag , $data_tag)) {
              $data_tag[$tag] = array(
                          'tagLabel' => $tag,
                          'positive' => $pos,
                          'negative' => $neg,
                          'neutral' => $neutral,
                         
                      );
            }
            else
            {
             $data_tag[$tag]['positive'] = (int) $data_tag[$tag]['positive'] + $pos;
             $data_tag[$tag]['negative'] = (int) $data_tag[$tag]['negative'] + $neg;
             $data_tag[$tag]['neutral'] = (int) $data_tag[$tag]['neutral'] + $neutral;
         
            }
      }
        // }
             

    

 


                
   }

 //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 

    // dd($data_tag);
    echo json_encode($data_tag);


}

public function getTagCount()
    {
     //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
   
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

    

       
     // $brand_id=17;
      // $groupType='day';
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

   }
            if(null !== Input::get('filter_page_name'))
           {
             $filter_pages= " page_name in ('".Input::get('filter_page_name')."') ";
           }
           else if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {

              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

 // dd($filter_pages);
// $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 10 25')));
// $dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 10 31')));

 $add_con="";
 $add_comment_con="";

if($filter_keyword !== '')
{
  $add_con = " and cmts.message Like '%".$filter_keyword."%'";
}

$query = " SELECT tags  " .
 "FROM temp_".$brand_id."_inbound_comments cmts INNER JOIN temp_".$brand_id."_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND cmts.parent='' AND tags<>'' AND ".$filter_pages.
" AND  (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".$add_con ;
// echo $query;
//  return;

$query_result = DB::select($query);

   $data = [];
   $total=0;

   $data_tag = [];
   $total=0;
  

    foreach ($query_result as  $key => $row) {
          
   
               $tags =$row->tags;
        
            $arr_tag=explode(',', $tags);
          
               $request['tagLabel'] ="";
      foreach ($arr_tag as  $key => $tag) {
            if (!array_key_exists($tag , $data_tag)) {
              $data_tag[$tag] = array(
                          'tagLabel' => $tag,
                          'tagCount' => 1,
                       
                         
                      );
            }
            else
            {
             $data_tag[$tag]['tagCount'] = (int) $data_tag[$tag]['tagCount'] + 1;
                 
            }
      }
      
             

    

 


                
   }

 //$data_sentiment = $this->unique_multidim_array($data_sentiment,'periodLabel'); 

 
     usort($data_tag, function($a, $b) {
    if($a['tagCount']==$b['tagCount']) return 0;
    return $a['tagCount'] < $b['tagCount']?1:-1;
});
     if(null === Input::get('limit'))
     $data_tag = array_slice($data_tag, 0, 5, true);
      
     echo json_encode($data_tag);


}

     public function getInboundlatestPOST()
    {

      //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
        $brand_id=Input::get('brand_id');
       
       
        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }

              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);

 $query = "SELECT id,message,page_name,link,full_picture,sentiment,emotion,DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p')".
 " created_time FROM  ".
 " temp_".$brand_id."_inbound_posts WHERE  (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ". $filter_pages .
 " GROUP BY DATE_FORMAT(created_time, '%Y-%m-%d  %h:%i:%s %p'),message,page_name,link,full_picture,sentiment,emotion ".
 " ORDER by DATE(created_time) DESC LIMIT 5 ";


$query_result = DB::select($query);

$data = [];

 $permission_data = $this->getPermission();
 $edit_permission=$permission_data['edit'];

foreach ($query_result as  $key => $row) {

              $request["message"] = $row ->message;
              $request["page_name"] = $row ->page_name;
              $request["created_time"] =date('d-m-Y H:m:s', strtotime($row ->created_time));
              if(isset($row ->link))
              $request["link"] =$row ->link;
            else
              $request["link"] ='#';

             if(isset($row ->full_picture))
              $request["full_picture"] =$row ->full_picture;
            else
              $request["full_picture"] ='';

              $request["sentiment"] =$row ->sentiment;
              $request["emotion"] =$row ->emotion;
              $request["id"]=$row->id;
              $request["edit_permission"]=$edit_permission;
              $data[] = $request;

            }
         

            echo json_encode($data);
    }

    public function getInboundinterest()
{
      $brand_id=Input::get('brand_id');
 
      $groupType=Input::get('periodType');
      $filter_keyword ='';
      $filter_sentiment='';
      if(null !== Input::get('keyword'))
       $filter_keyword=Input::get('keyword'); 

      // if(null !== Input::get('sentiment'))
      // $filter_sentiment=Input::get('sentiment'); 

     //  $brand_id=59;
     // $groupType='month';
 if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }

 // $groupType="month";
 $group_period ="";
 $add_con="";
 $add_comment_con ="";
if($groupType=="month")
{
//$group_period =" DATE_FORMAT(created_time, '%Y-%m')"; 
  $group_period ="%Y-%m"; 
 }

   else 
 {
$group_period ="%Y-%m-%d";                
              

 }
if($filter_keyword !== '')
{
  $add_con = " and message Like '%".$filter_keyword."%'";
  $add_comment_con = " and cmts.message Like '%".$filter_keyword."%'";
}

  $query = "SELECT sum(interest) interest_sum,count(*) comment_count, ".
  " DATE_FORMAT(cmts.created_time, '".$group_period."') periodLabel  ".
  "FROM temp_".$brand_id."_inbound_comments cmts INNER JOIN temp_".$brand_id."_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE  posts.id IS NOT NULL AND   cmts.parent='' ".
  " AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') AND ". $filter_pages .  $add_con .
" GROUP BY DATE_FORMAT(cmts.created_time, '".$group_period."')" ;

$query_result = DB::select($query);
 echo json_encode($query_result);

}

public function getPostingStatus()
{
     $data = array();
      $brand_id=Input::get('brand_id');
    //$brand_id=17;
 
     // $today = date('d-m-Y');//23-02-2015
     // $today_plus=date('Y-m-d', strtotime('+1 day', strtotime($today)));
     // $date_Week_Begin=date('Y-m-d', strtotime('-1 week', strtotime($today_plus)));
     // $date_End=date('Y-m-d',strtotime($today));

     // $date_Month_Begin=date('Y-m-d', strtotime('-1 month', strtotime($today_plus)));

     if(null !== Input::get('fday'))
      {
    $date_Week_Begin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $date_Week_Begin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $date_End=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$date_End=date('Y-m-d');

   }
      
    // echo $date_Week_Begin;echo $date_Month_Begin;echo $date_End;
          
          if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }
          //comment
  $query_comment_today = "SELECT count(*) total,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral ".
  "FROM temp_".$brand_id."_inbound_comments cmts INNER JOIN temp_".$brand_id."_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ".
  " AND (DATE(cmts.created_time) BETWEEN '".$date_End."' AND '".$date_End."')";
  //  dd($query_week);
$query_today_comment_result = DB::select($query_comment_today);

$query_comment_week = "SELECT count(*) total,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) negative,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) neutral  ".
  "FROM temp_".$brand_id."_inbound_comments cmts INNER JOIN temp_".$brand_id."_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ".
  " AND (DATE(cmts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')";
 
$query_week_comment_result = DB::select($query_comment_week);

  //post         
  // $query_post_today = "SELECT COALESCE(sum(IF(cmts.sentiment = 'neutral', 1, 0)),0) neutral, COALESCE(sum(IF(cmts.sentiment = 'NA', 1, 0)),0) NA, COALESCE(sum(IF(cmts.sentiment = 'pos', 1, 0)),0) positive,COALESCE(sum(IF(cmts.sentiment = 'neg', 1, 0)),0) negative ".
  // "FROM temp_".$brand_id."_inbound_posts posts INNER JOIN temp_".$brand_id."_inbound_comments cmts ".
  // " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND ".$filter_pages." AND cmts.parent='' ".
  // " AND (DATE(posts.created_time) BETWEEN '".$date_End."' AND '".$date_End."')";
    //dd($query_post_today);
$query_post_today ="select  COALESCE(sum(IF(positive>negative, 1, 0)),0) ov_positive, COALESCE(sum(IF(negative>positive, 1, 0)),0) ov_negative from (SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative  FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id 
where cmts.post_id IS NOT NULL AND  ".$filter_pages." AND cmts.parent = '' ".
" AND (DATE(posts.created_time) BETWEEN '".$date_End."' AND '".$date_End."') " .
" GROUP BY posts.id)T1";
$query_today_post_result = DB::select($query_post_today);

// $query_post_week ="select  COALESCE(sum(IF(positive>negative, 1, 0)),0) ov_positive, COALESCE(sum(IF(negative>positive, 1, 0)),0) ov_negative from (SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative  FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id 
// where cmts.post_id IS NOT NULL AND  ".$filter_pages." AND cmts.parent = '' ".
// " AND (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') " .
// " GROUP BY posts.id)T1";



$query_post_week ="SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral' OR  cmts.checked_sentiment ='NA' , 1, 0)) neutral  FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id 
where cmts.post_id IS NOT NULL AND  ".$filter_pages." AND cmts.parent = '' ".
" AND (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."') " .
" GROUP BY posts.id";

// dd($query_post_week);

$query_week_post_result = DB::select($query_post_week);
$data_week_post_result=[];
$ov_positive=0;$ov_negative=0;$ov_neutral=0;
foreach ($query_week_post_result  as $key => $value) {
 // dd($value->positive);
  $ov_positive = $ov_positive+$value->positive;
  $ov_negative = $ov_negative+$value->negative;
  $ov_neutral = $ov_neutral+$value->neutral;
     # code...
}

$data_week_post_result[]=array(
                'ov_positive'     =>$ov_positive,
                'ov_negative' => $ov_negative,
                'ov_neutral' => $ov_neutral,
              );

//total post
 $query_post_tol_today = "SELECT count(Distinct posts.id) total ".
  "FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages.
  " AND (DATE(posts.created_time) BETWEEN '".$date_End."' AND '".$date_End."')";
   // dd($query_post_tol_today);
$query_post_tol_today_result = DB::select($query_post_tol_today);

$query_post_tol_week =  "SELECT count(Distinct posts.id) total ".
  "FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND ".$filter_pages.
  " AND (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')";
   //   dd($query_post_tol_week);
$query_post_tol_week_result = DB::select($query_post_tol_week);


 echo json_encode(array($query_today_comment_result,$query_week_comment_result,$query_today_post_result,$data_week_post_result,$query_post_tol_today_result,$query_post_tol_week_result));

}

public function getEngagementStatus()
{
     $data = array();
      $brand_id=Input::get('brand_id');
  //  $brand_id=17;
 
     // $today = date('d-m-Y');//23-02-2015
     // $today_plus=date('Y-m-d', strtotime('+1 day', strtotime($today)));
     // $date_Week_Begin=date('Y-m-d', strtotime('-1 week', strtotime($today_plus)));
     // $date_End=date('Y-m-d',strtotime($today));

     // $date_Month_Begin=date('Y-m-d', strtotime('-1 month', strtotime($today_plus)));
      
    // echo $date_Week_Begin;echo $date_Month_Begin;echo $date_End;
      if(null !== Input::get('fday'))
      {
    $date_Week_Begin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $date_Week_Begin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $date_End=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$date_End=date('Y-m-d');

   }
           if(null !== Input::get('admin_page') && '' !== Input::get('admin_page'))
           {
             $filter_pages= " page_name in ('".Input::get('admin_page')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }
//post
  $query_post_today = "SELECT count(posts.id) total_post,COALESCE(sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry),0)  total_reaction, ".
  " COALESCE(SUM(replace(shared, ',', '')),0) shared ".
  " FROM temp_".$brand_id."_inbound_posts posts ".
  " WHERE ".$filter_pages.
  " AND (DATE(posts.created_time) BETWEEN '".$date_End."' AND '".$date_End."')";
    
  $query_today_post_result = DB::select($query_post_today);

  $query_post_week ="SELECT count(posts.id) total_post,COALESCE(sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry),0)  total_reaction, ".
  " COALESCE(SUM(replace(shared, ',', '')),0) shared ".
  " FROM temp_".$brand_id."_inbound_posts posts ".
  " WHERE ".$filter_pages.
  " AND (DATE(posts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')";
   // dd($query_post_week);
  $query_week_post_result = DB::select($query_post_week);

  
//comment count
  $query_today_comment = "SELECT count(*) total_comment ".
  "FROM temp_".$brand_id."_inbound_comments cmts INNER JOIN temp_".$brand_id."_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ".
  " AND (DATE(cmts.created_time) BETWEEN '".$date_End."' AND '".$date_End."')";

$query_today_comment_result = DB::select($query_today_comment);

$query_week_comment = "SELECT count(*) total_comment ".
  "FROM temp_".$brand_id."_inbound_comments cmts INNER JOIN temp_".$brand_id."_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND ".$filter_pages." AND cmts.parent='' ".
  " AND (DATE(cmts.created_time) BETWEEN '".$date_Week_Begin."' AND '".$date_End."')";
   // dd($query_month);
$query_week_comment_result = DB::select($query_week_comment);

         
  
   // dd($query_month);


 echo json_encode(array($query_today_post_result,$query_week_post_result,$query_today_comment_result,$query_week_comment_result));

}


public function getPageSummary()
{
     $data = array();
      $brand_id=Input::get('brand_id');
  //  $brand_id=17;
 
      if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
   
            if(null !== Input::get('filter_page_name'))
           {
             $filter_pages= " posts.page_name in ('".Input::get('filter_page_name')."') ";
           }
           else
           {
              $my_pages= $this->getOwnPage($brand_id);
              $my_pages=explode(',', $my_pages);
              $filter_pages=$this->getPageWhereGen($my_pages);
           }
//post


  $query_post ="SELECT count(posts.id) total_post,COALESCE(sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry),0)  total_reaction, ".
  " COALESCE(SUM(replace(shared, ',', '')),0) shared ".
  " FROM temp_".$brand_id."_inbound_posts posts ".
  " WHERE ".$filter_pages.
  " AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')";
   // dd($query_post_week);
  $post_count = DB::select($query_post);

  $query_comment = "SELECT count(*) total_comment,COALESCE(sum(IF(cmts.checked_sentiment = 'neutral', 1, 0)),0) cmt_neutral, COALESCE(sum(IF(cmts.checked_sentiment = 'NA', 1, 0)),0) cmt_NA, COALESCE(sum(IF(cmts.checked_sentiment = 'pos', 1, 0)),0) cmt_positive,COALESCE(sum(IF(cmts.checked_sentiment = 'neg', 1, 0)),0) cmt_negative  ".
  "FROM temp_".$brand_id."_inbound_comments cmts INNER JOIN temp_".$brand_id."_inbound_posts posts ".
  " on posts.id=cmts.post_id WHERE posts.id IS NOT NULL AND  ".$filter_pages." AND cmts.parent='' ".
  " AND (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')";
 

   // dd($query_month);
$comment_count = DB::select($query_comment);


$query_overall_PN ="SELECT sum(IF(cmts.checked_sentiment = 'pos', 1, 0)) positive,sum(IF(cmts.checked_sentiment = 'neg', 1, 0)) negative,sum(IF(cmts.checked_sentiment = 'neutral' OR  cmts.checked_sentiment ='NA' , 1, 0)) neutral  FROM temp_".$brand_id."_inbound_posts posts LEFT JOIN temp_".$brand_id."_inbound_comments cmts on posts.id=cmts.post_id 
where cmts.post_id IS NOT NULL AND  ".$filter_pages." AND cmts.parent = '' ".
" AND (DATE(posts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') " .
" GROUP BY posts.id";

// dd($query_post_week);

$result_overall_PN = DB::select($query_overall_PN);
$overall_PN=[];
$ov_positive=0;$ov_negative=0;$ov_neutral=0;
foreach ($result_overall_PN  as $key => $value) {
 // dd($value->positive);
  $ov_positive = $ov_positive+$value->positive;
  $ov_negative = $ov_negative+$value->negative;
  $ov_neutral = $ov_neutral+$value->neutral;
     # code...
}
$overall_PN[]=array(
                'ov_positive'     =>$ov_positive,
                'ov_negative' => $ov_negative,
                'ov_neutral' => $ov_neutral,
              );

 echo json_encode(array($post_count,$comment_count,$overall_PN));

}



public function getAccssToken()
{
//   echo "hihi";
//   var FbToken= Ext.create('Ext.util.DelayedTask', function() {
//     var accessToken= '12345678998754321' //sample token
//      graph.facebook.com/me?access_token=' + accessToken;
// });


}

}


