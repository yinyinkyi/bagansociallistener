<?php

namespace App\Http\Controllers;

use App\MongodbData;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
class MongodbDataController extends Controller
{
   use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getinteractiondatabymongo()
    {
         //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
       $brand_id=Input::get('brand_id');
       

         $keyword_data = $this->getprojectkeywork($brand_id);
       /*  $key_data_count = count($keyword_data);*/
      
          $filter['$or'] = $this->getkeywordfilter($keyword_data);

        if(null !== Input::get('fday'))
            {
                   $dateBegin =new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('fday')))* 1000);

            }
      
           else
           {
                $dateBegin =new MongoDB\BSON\UTCDateTime(\Carbon\Carbon::now()->timestamp * 1000);
           }
    

    if(null !==Input::get('sday'))
    {

        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('sday')))* 1000);
    }
    
    else
    {
               $dateEnd =new MongoDB\BSON\UTCDateTime( \Carbon\Carbon::now()->timestamp * 1000);

           }

        /*
        $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2018 05 20'))* 1000);
        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2018 08 20'))* 1000);*/

  




 $query_result=MongodbData::raw(function ($collection) use($dateBegin,$dateEnd,$filter) {
    return $collection->aggregate([
             [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             $filter

          
]

        ]  
                   
                         ],
      
        
        [

            '$group' => [
                '_id'   => [
                  
                   /*   'id'   =>  ['$_id' ],*/
              ],
               'Wow'  => ['$push' =>['$ifNull' => ['$reaction.Wow', 0 ]]],
               'Love'  => ['$push' =>['$ifNull' => ['$reaction.Love', 0 ]]],
               'Angry'  =>['$push' =>['$ifNull' => ['$reaction.Angry', 0 ]]],
               'Sad'  => ['$push' =>['$ifNull' => ['$reaction.Sad', 0 ]]],
               'Haha'  => ['$push' =>['$ifNull' => ['$reaction.Haha', 0 ]]],
               'Liked'  => ['$push' =>['$ifNull' => ['$reaction.Like', 0 ]]]

                   
            ],
          
            
        ],
        

             


    ]);
})->toArray();


$data = [];


foreach ($query_result as  $key => $row) {

              
                $request['Wow'] = $this->sumofarray(iterator_to_array($row['Wow']));
                $request['Angry'] =$this->sumofarray(iterator_to_array($row['Angry']));
                $request['Love'] =$this->sumofarray(iterator_to_array($row['Love']));
                $request['Liked'] =$this->sumofarray(iterator_to_array($row['Liked']));
                $request['Haha'] =$this->sumofarray(iterator_to_array($row['Haha']));
                $request['Sad'] =$this->sumofarray(iterator_to_array($row['Sad']));
          
                $data[] = $request;
            }

            echo json_encode($data);
    }
public function getpopularmention()

{
//get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
        $brand_id=Input::get('brand_id');
   
       /*$brand_id=59;*/
        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 06 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 09 30')));*/

 
 $add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
  $add_inbound_con = " and posts.page_name  not in (".$inboundpages.")";
}

 $query = "SELECT  (sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry)) ".
 " total,posts.id id,message,posts.page_name,link,full_picture,sentiment,emotion,DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p')".
 " created_time,CAST(followers.fan_count AS UNSIGNED) +SUM(replace(shared, ',', '')) each_influencer  FROM  ".
 " temp_".$brand_id."_posts posts inner join temp_".$brand_id."_followers followers on posts.page_name=followers.page_name ".
 " WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')". $add_inbound_con .
 " GROUP BY posts.id,DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p'),message,page_name,link,full_picture,sentiment,emotion,fan_count ".
 " ORDER by total,DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p') DESC LIMIT 10 ";

/*echo  $query;
echo "_ ";*/
$query_result = DB::select($query);
$query_influencer_total="select (sum(fan_count)+sum(shared)) total_influencer ". 
" from (SELECT  (sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry)) total, ".
" ANY_VALUE(fan_count) fan_count ,SUM(replace(shared, ',', '')) shared ".
" FROM temp_".$brand_id."_posts posts  inner join temp_".$brand_id."_followers followers on posts.page_name=followers.page_name ".
" WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".
" ORDER by total DESC LIMIT 10) T1";
$query_total_result = DB::select($query_influencer_total);

 $permission_data = $this->getPermission();
 $edit_permission=$permission_data['edit'];

$data = [];
$data_message=[];

$total_influencer=0;
 foreach ($query_total_result as $query_total_result) {
  $total_influencer = $query_total_result->total_influencer;

  }
foreach ($query_result as  $key => $row) {

              $request["message"] = $row ->message;
              $request["page_name"] = $row ->page_name;
              $request["created_time"] =date('d-m-Y H:m:s', strtotime($row ->created_time));
              if(isset($row ->link))
              $request["link"] =$row ->link;
            else
              $request["link"] ='#';

             if(isset($row ->full_picture))
              $request["full_picture"] =$row ->full_picture;
            else
              $request["full_picture"] ='';

              $request["sentiment"] =$row ->sentiment;
              $request["emotion"] =$row ->emotion;
              $each_total=$row ->each_influencer;
              $request["influencer_score"] =ceil((int)$each_total/(int)$total_influencer*10);
              $request["id"]=$row->id;
              $request["edit_permission"]=$edit_permission;
              $data[] = $request;

            }
         
           
            echo json_encode($data);

}
     public function getlatestmention()
    {

      //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
        $brand_id=Input::get('brand_id');
       
       
        if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
/*$dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 05 01')));
$dateEnd =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));*/
$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
    $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}
 
 $query = "SELECT id,message,page_name,link,full_picture,sentiment,emotion,DATE_FORMAT(created_time, '%Y-%m-%d %h:%i:%s %p')".
 " created_time FROM  ".
 " temp_".$brand_id."_posts WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')".$add_inbound_con .
 " GROUP BY DATE_FORMAT(created_time, '%Y-%m-%d  %h:%i:%s %p'),message,page_name,link,full_picture,sentiment,emotion ".
 " ORDER by DATE_FORMAT(created_time, '%Y-%m-%d  %h:%i:%s %p') DESC LIMIT 10 ";

/* $query_influencer_total="select (sum(fan_count)+sum(shared)) total_influencer ". 
" from (SELECT  (sum(Liked)+sum(Love)+sum(Wow)+sum(Haha)+sum(Sad)+sum(Angry)) total, ".
" CAST(followers.fan_count AS UNSIGNED) fan_count ,SUM(replace(shared, ',', '')) shared ".
" FROM temp_".$brand_id."_posts posts  inner join temp_".$brand_id."_followers followers on posts.page_name=followers.page_name ".
" WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."') ".
" GROUP BY fan_count ".
" ORDER by total DESC LIMIT 10) T1";*/
/*echo $query;
return;
*/
$query_result = DB::select($query);

$data = [];

 $permission_data = $this->getPermission();
 $edit_permission=$permission_data['edit'];

foreach ($query_result as  $key => $row) {

              $request["message"] = $row ->message;
              $request["page_name"] = $row ->page_name;
              $request["created_time"] =date('d-m-Y H:m:s', strtotime($row ->created_time));
              if(isset($row ->link))
              $request["link"] =$row ->link;
            else
              $request["link"] ='#';

             if(isset($row ->full_picture))
              $request["full_picture"] =$row ->full_picture;
            else
              $request["full_picture"] ='';

              $request["sentiment"] =$row ->sentiment;
              $request["emotion"] =$row ->emotion;
              $request["id"]=$row->id;
              $request["edit_permission"]=$edit_permission;
              $data[] = $request;

            }
         

            echo json_encode($data);
    }

     public function getmentionchartdatabymongo()
    {
        

        //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
          $brand_id=Input::get('brand_id');
          $keyword_data = $this->getprojectkeywork($brand_id);
       /*  $key_data_count = count($keyword_data);*/
      
          $filter['$or'] = $this->getkeywordfilter($keyword_data);
        if(null !== Input::get('fday'))
            {
                   $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('fday')))* 1000);

            }
      
           else
           {
                $dateBegin =new MongoDB\BSON\UTCDateTime(\Carbon\Carbon::now()->timestamp * 1000);
           }
    

    if(null !==Input::get('sday'))
    {

        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('sday')))* 1000);
    }
    
    else
    {
               $dateEnd =new MongoDB\BSON\UTCDateTime( \Carbon\Carbon::now()->timestamp * 1000);

           }
       
        
        /*$dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2018 01 01'))* 1000);
        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2018 08 19'))* 1000);*/
    
    $query_result=MongodbData::raw(function ($collection) use($dateBegin,$dateEnd,$filter) {
    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
             $filter

          
]

        ]  
                   
                         ],
                         [

            '$group' => [
                '_id'   =>[
                    'year'   => ['$year' => '$created_time'],
                    'month'  => ['$month' => '$created_time'],
                   
                     
              ],
               'count'=> ['$sum'=> 1 ] 
              
                   
            ],
          
            
        ],
                         

           [
            '$sort' =>['_id.year'=>1,'_id.month'=>1]


            ]

          


    ]);
})->toArray();
    /*print_r($query_result);
    return;
*/
   $data = [];
   $total=0;

    foreach ($query_result as  $key => $row) {
          
                           
                $request['periods'] = $row['_id']['year'] . "-" . $row['_id']['month'] ;
                $request['count'] = $row['count'];
                $total= $total+ (float) $row['count'];
                $request['total'] =$total;
       
                $data[] = $request;
            }

    echo json_encode($data);


}

public function getDashboardMentionGraph()
    {
          //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
      // $brand_id=22;
       $keyword_data = $this->getprojectkeywork($brand_id);
       /*  $key_data_count = count($keyword_data);*/
      
          $filter['$or'] = $this->getkeywordfilter($keyword_data);

        if(null !== Input::get('fday'))
            {
                   $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('fday')))* 1000);

            }
      
           else
           {
                $dateBegin =new MongoDB\BSON\UTCDateTime(\Carbon\Carbon::now()->timestamp * 1000);
           }
    

    if(null !==Input::get('sday'))
    {

        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('sday')))* 1000);
    }
    
    else
    {
               $dateEnd =new MongoDB\BSON\UTCDateTime( \Carbon\Carbon::now()->timestamp * 1000);

           }
       
     /*   
        $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2018 01 01'))* 1000);
        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2018 08 19'))* 1000);*/
    
    
$query_result=MongodbData::raw(function ($collection) use($dateBegin,$dateEnd,$filter) {
    return $collection->aggregate([
             [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
              ['id'=> ['$exists'=> true]],
              $filter

          
]

        ]  
                   
                         ],

                          [
            '$sort' =>['created_time'=>-1]


            ],
      
        
        
        

             


    ]);
})->toArray();
    /*print_r($query_result);
    return;
*/

  
   $data_mention = [];
   
   $total=0;
   /* $key = 0;*/

    foreach ($query_result as  $key => $row) {
                
                 $utcdatetime = $row["created_time"];

                 $datetime = $utcdatetime->toDateTime();

                $created_time=$datetime->format('Y-M');
                /*           
                $request['message'] = $row['message'] ;*/
               
                $request['mention'] = 0;
                $request['created_time']=$created_time;
                 $data_message[]= preg_replace('/(\r\n|\r|\n)+/', " ",$row['message']);

 
if (!array_key_exists($created_time, $data_mention)) {
    $data_mention[$created_time] = array(
                'created_time' => $created_time,
                'mention' => 1,
            );
}
else
{
 $data_mention[$created_time]['mention'] = $data_mention[$created_time]['mention'] + 1;
}

            }





    usort($data_mention, function($a1, $a2) {
   $value1 = strtotime($a1['created_time']);
   $value2 = strtotime($a2['created_time']);
   return $value1 - $value2;
});

    echo json_encode($data_mention);


}

public function getDashboardSentimentGraph()
    {
          //get keywork
    //  $keyword_data = ProjectKeyword::find(1);//projectid
      $brand_id=Input::get('brand_id');
   //   $brand_id=22;
       $keyword_data = $this->getprojectkeywork($brand_id);
       /*  $key_data_count = count($keyword_data);*/
      
          $filter['$or'] = $this->getkeywordfilter($keyword_data);

        if(null !== Input::get('fday'))
            {
                   $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('fday')))* 1000);

            }
      
           else
           {
                $dateBegin =new MongoDB\BSON\UTCDateTime(\Carbon\Carbon::now()->timestamp * 1000);
           }
    

    if(null !==Input::get('sday'))
    {

        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/',Input::get('sday')))* 1000);
    }
    
    else
    {
               $dateEnd =new MongoDB\BSON\UTCDateTime( \Carbon\Carbon::now()->timestamp * 1000);

           }
       
        
      /*  $dateBegin = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2018 01 01'))* 1000);
        $dateEnd = new MongoDB\BSON\UTCDateTime(strtotime(str_replace(' ','/','2018 08 19'))* 1000);*/
  //  return $dateBegin;
    
$query_result=MongodbData::raw(function ($collection) use($dateBegin,$dateEnd,$filter) {
    return $collection->aggregate([
             [
        '$match' =>[
             '$and'=> [ 
             ['created_time' => ['$gte' => $dateBegin, '$lte' => $dateEnd]],
              ['id'=> ['$exists'=> true]],
              $filter

          
]

        ]  
                   
                         ],

                          [
            '$sort' =>['created_time'=>-1]


            ],
      
        
        
        

             


    ]);
})->toArray();
    /*print_r($query_result);
    return;
*/
   $data_sentiment = [];
   $data = [];
   $data_message = [];
   $total=0;
   /* $key = 0;*/

    foreach ($query_result as  $key => $row) {
                
                 $utcdatetime = $row["created_time"];

                 $datetime = $utcdatetime->toDateTime();

                $created_time=$datetime->format('Y-M');
                /*           
                $request['message'] = $row['message'] ;*/
                $request['positive'] = 0;
                $request['negative'] = 0;
                $request['na'] = 0;
                
                $request['created_time']=$created_time;
                 $data_message[]= preg_replace('/(\r\n|\r|\n)+/', " ",$row['message']);
                  $data[] = $request;
 

         
               
            }


if(count($data_message)>0)
{
$client = new Client(['base_uri' => 'http://35.185.97.177:5000/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
$uri_sentiment = 'sentiment_predict';
 //$message =preg_replace('/(\r\n|\r|\n)+/', " ",$row['message']);
    $formData = array(
    'raw' =>  $data_message,
   
);

$formData = json_encode($formData);
//dd($formData);

$sentiment_response = $client->post($uri_sentiment, [
    'form_params' => [
        'raw' =>  $formData,
    ],
]);

$sentiment_result = $sentiment_response->getBody()->getContents();
/*dd($sentiment_result);*/
$json_sentiment_array = json_decode($sentiment_result, true);

$sentiment_result = $json_sentiment_array['raw'];
/*dd(str_replace('"', "0",$sentiment_result[0]) );*/
/*
$sentiment_result=  str_replace(']', '',  str_replace('[', '', $sentiment_result));
$sentiment_result= explode(',', $sentiment_result);*/
/*dd($sentiment_result);*/
/*print_r($sentiment_result);
return;*/
for($i=0;$i<count($data);$i++)
{
  $pos=0;
  $neg=0;
  $sentiment_type=str_replace(" ", "",str_replace("'", "", $sentiment_result[$i])) ;

  $created_time=$data[$i]['created_time'];
  if($sentiment_type == "pos") $pos=  1 ; 
else if ($sentiment_type == "neg")  $neg = 1 ;


 if (!array_key_exists($created_time, $data_sentiment)) {
   $data_sentiment[$created_time] = array(
                'created_time' => $created_time,
                'positive' =>$pos,
                'negative' =>$neg,

               
            );

}
else
{
 $data_sentiment[$created_time]['positive'] = $data_sentiment[$created_time]['positive'] + $pos;
 $data_sentiment[$created_time]['negative'] = $data_sentiment[$created_time]['negative'] + $neg;

}
  
}
/*dd($data_sentiment);*/
  usort($data_sentiment, function($a1, $a2) {
   $value1 = strtotime($a1['created_time']);
   $value2 = strtotime($a2['created_time']);
   return $value1 - $value2;
});
}




    echo json_encode($data_sentiment);


}


//region for local function

function unique_multidim_array($array, $key) { 
    $temp_array = array(); 
    $i = 0; 
    $key_array = array(); 
    
    foreach($array as $val) { 
        if (!in_array($val[$key], $key_array)) { 
            $key_array[$i] = $val[$key]; 
            $temp_array[$i] = $val; 
        } 
        $i++; 
    } 
    return $temp_array; 
} 

function check_array_value($to_find, $array_values, $strict = false) {
    foreach ($array_values as $key =>$item) {
        if (($strict ? $item === $to_find : $item == $to_find) || (is_array($item) && $this->check_array_value($to_find, $item, $strict))) {
          $key = (int) $key+1;
            return $key;
        }
    }
    return 0;
}
function rangeMonth ($datestr) {
   date_default_timezone_set (date_default_timezone_get());
   $dt = strtotime ($datestr);
   return array (
     "start" => date ('Y-m-d', strtotime ('first day of this month', $dt)),
     "end" => date ('Y-m-d', strtotime ('last day of this month', $dt))
   );
 }

 function rangeWeek ($datestr) {
   date_default_timezone_set (date_default_timezone_get());
   $dt = strtotime ($datestr);
   return array (
     "start" => date ('N', $dt) == 1 ? date ('Y-m-d', $dt) : date ('Y-m-d', strtotime ('last monday', $dt)),
     "end" => date('N', $dt) == 7 ? date ('Y-m-d', $dt) : date ('Y-m-d', strtotime ('next sunday', $dt))
   );
 }

    function replace_null($value, $replace) {
    if (is_null($value)) return $replace;
    return $value;
}

function sumofarray($arr)
{
  $result=0;
   foreach($arr as $i => $match)
    {
    
    if(strpos($match,'K') !== false)
        {
        $new = str_replace("K", "", $match);
         $new = 1000*$new;
        }
        else
        {
         $new=$match;   
        }

       
       $result = $result + (float)$new;
 
    }

    return $result;
}

   
function convertKtoThousand($value)
{
  $result=0;
 
    if(!empty($value[0]))
    {

    if(strpos($value[0],'K') !== false)
        {
        $new = str_replace("K", "", $value[0]);
         $new = 1000*$new;
        }
        else
        {
         $new=$value[0];   
        }

       
       $result = $result + (float)$new;
    }
    

    return $result;
}

public function gethighlighted()
{
  
   $brand_id=Input::get('brand_id');
 /* if($brand_id != 46 || $brand_id != 47)*/
    $brand_id=17; 


         if(null !== Input::get('fday'))
      {
    $dateBegin=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('fday'))));
      }
      else
      {
      $dateBegin=date('Y-m-d');
      }
    

    if(null !==Input::get('sday'))
    {

 $dateEnd=date('Y-m-d', strtotime(str_replace(' ','/',Input::get('sday'))));
    }
    
    else
    {
$dateEnd=date('Y-m-d');

           }
  $dateBegin=date('Y-m-d', strtotime(str_replace(' ','-','2018 01 01')));
  $dateEnd  =date('Y-m-d', strtotime(str_replace(' ','-','2018 08 30')));

   $keyword_param=[];
   $removed_keyword=[];

$removed_query = "SELECT keyword FROM  keywords_removed where brand_id=".$brand_id;
$removed_result = DB::select($removed_query);
foreach ($removed_result as  $key => $row) {
          $removed_keyword[$row->keyword] =$row->keyword ;
          }

$keyword_query = "SELECT main_keyword FROM  project_keywords where project_id=".$brand_id;
$keyword_result = DB::select($keyword_query);

       foreach ($keyword_result as  $key => $row) {
          $keyword_param[] =$row->main_keyword ;
          }

$add_inbound_con ="";
$inboundpages=$this->getInboundPages($brand_id);
if ($inboundpages !== '')
{
  $add_inbound_con = " and (page_name  not in (".$inboundpages.") or page_name is NULL)";
}

/*$query = "SELECT id,wb_message FROM temp_".$brand_id."_posts WHERE (DATE(created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')".$add_inbound_con.
" UNION ALL".
" SELECT cmts.id,cmts.wb_message FROM temp_".$brand_id."_comments cmts LEFT JOIN temp_".$brand_id."_posts posts ".
" on cmts.post_id=posts.id ".
" WHERE posts.id IS NULL and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')".$add_inbound_con;*/

// $query = " SELECT cmts.id,cmts.wb_message FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN temp_".$brand_id."_inbound_posts posts ".
// " on cmts.post_id=posts.id ".
// " WHERE posts.id IS NOT NULL and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')";


$query = " SELECT cmts.id,cmts.message FROM temp_".$brand_id."_inbound_comments cmts LEFT JOIN temp_".$brand_id."_inbound_posts posts ".
" on cmts.post_id=posts.id ".
" WHERE posts.id IS NOT NULL and cmts.parent = '' and (DATE(cmts.created_time) BETWEEN '".$dateBegin."' AND '".$dateEnd."')";

/*echo $query;
return;*/

 $data_message=[];
$query_result=[];
$query_result = DB::select($query);


do {
   $query_result = DB::select($query);
 /*dd($query_result);
   return ;*/
 foreach ($query_result as  $key => $row) {
   if(isset($row->message))
  $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$row->message);

  }

  }while (count($data_message)<=0 && count($query_result)>0 );
  /*dd($data_message);*/

$topics=[];

if(count($data_message)>0)
{


$client = new Client(['base_uri' => 'http://35.185.97.177:5001/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
$uri_keyword = 'keyword_get';

    $formData = array(
    'keywords' => $keyword_param,
    'messages' =>  $data_message,
    'ntopics' => '100'
   
);


$formData = json_encode($formData);
/*dd($formData);*/
$path = storage_path('app/data_output/wunzinn_msg_parent_null_29918.json');
$this->doJson($path,$formData);
return;

$keyword_response = $client->post($uri_keyword, [
    'form_params' => [
        'raw' =>  $formData,
    ],
]);

$keyword_result = $keyword_response->getBody()->getContents();


$topics = json_decode($keyword_result, true);


/*print_r($topics);
return;*/
/*print_r($removed_keyword);*/
//$removed_keyword = array('ဇွန် ၄ ရက်နေ့မနက် ၉ နာရီ မတိုင် မီပုံ'=>'ဇွန် ၄ ရက်နေ့မနက် ၉ နာရီ မတိုင် မီပုံ');
//echo array_search('ဇွန် ၄ ရက်နေ့မနက် ၉ နာရီ မတိုင် မီပုံ', $removed_keyword);
foreach($topics as $key => $val)
{

$found=array_search($val['topic_name'], $removed_keyword) ;
   if($found === $val['topic_name'])
    {
 /*     print_r($topics[$key]);
  return;*/
       unset($topics[$key]);
    }
}
  

  }
/*  usort($topics, function ($a, $b) {
    return $a['weight'] <=> $b['weight'];
});//Ascending */
  usort($topics, function($a, $b) {
    if($a['weight']==$b['weight']) return 0;
    return $a['weight'] < $b['weight']?1:-1;
});//Decending
/*
  print_r($topics);
  return;*/
  $topics = array_slice($topics, 0, 50, true);
   echo json_encode($topics);

     /* $brand_id=20;*/
  /*    $path = storage_path('app/data_output/highlighted'.$brand_id.'.json');
      $str = file_get_contents($path);
     $topics = json_decode($str, true);
      echo json_encode($topics);*/


}
function doJson($path,$json)
{
    $fp = fopen($path, 'w');
   fwrite($fp, $json);
   fclose($fp);
}
//end region for local function


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MongodbData  $mongodbData
     * @return \Illuminate\Http\Response
     */
    public function show(MongodbData $mongodbData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MongodbData  $mongodbData
     * @return \Illuminate\Http\Response
     */
    public function edit(MongodbData $mongodbData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MongodbData  $mongodbData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MongodbData $mongodbData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MongodbData  $mongodbData
     * @return \Illuminate\Http\Response
     */
    public function destroy(MongodbData $mongodbData)
    {
        //
    }

   
}
