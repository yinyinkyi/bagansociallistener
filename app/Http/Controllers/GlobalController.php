<?php

namespace App\Http\Controllers;

use App\MongodbData;
use App\Mongodbpage;
use App\Comment;
use App\ProjectKeyword;
use App\Project;
use App\user_permission;
use App\demo;
use App\CompanyInfo;
use Illuminate\Http\Request;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
trait GlobalController
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function getCompanyData()
    {
        $data = [];
        $companyInfo = CompanyInfo::all();

          if(count($companyInfo)>0)
          $data = $companyInfo[0];
     return $data;
    }
    public function getPermission()
    {
          $login_user = auth()->user()->id;
          $permission_data = user_permission::select('*')->where('user_id', $login_user);
          $permission_data=$permission_data->orderBy('id','DESC')->first();

          return  $permission_data;
    }
     public function getProject()
    {
          $login_user = auth()->user()->id;
          $project_data = Project::select('*')->where('user_id', $login_user);
          $project_data=$project_data->orderBy('id','DESC')->get();

          return  $project_data;
    }
     public function getIsDemouser()
    {
          $login_user = auth()->user()->id;
          $demo_user= user_permission::select('*')->where('user_id', $login_user)->where('demo', 1);
          $demo_user=$demo_user->orderBy('id','DESC')->get();
          if(count($demo_user)>0)
          {
             return  true;
          }
          else
          {
              return  false;
          }
          
    }
    public function getInboundPostId($brand_id)
    {
        $id_query="select id from temp_".$brand_id."_inbound_posts";
        $id_result = DB::select($id_query);
         $id_array = array_column($id_result, 'id');
          return  $id_array;
    }
     public function getlastcrawldate($brand_id)
    {
      $query="select CONVERT_TZ(created_at,'+00:00','+06:30') last_crawl_date from temp_".$brand_id."_inbound_comments ORDER BY timestamp(created_at) DESC";
       $result = DB::select($query);
       $last_crawl_date = array_column($result, 'last_crawl_date');
          return  $last_crawl_date;
    }

    public function getDemoHideDiv($view_name)
    {
          $hidden_div = demo::select('*')->where('is_hide',1)->where('view_name',$view_name);
          $hidden_div=$hidden_div->orderBy('id','DESC')->get();
          $hidden_div_name='';
          foreach ($hidden_div as $key => $value) {
            if($key === 0 )
            $hidden_div_name .=$value['div_name'];
          else
            $hidden_div_name .="," . $value['div_name'];
              
          }
          return  $hidden_div_name;
    }
    public function getInboundPages($brand_id)
    {
      $page_query="select DISTINCT page_name from temp_".$brand_id."_inbound_posts";
      $page_result = DB::select($page_query);
      $pages='';
       foreach ($page_result as  $key => $row) {
                      if ($key == 0) {
                         $pages  =  "'" . $row->page_name . "'";
                       }
                       else
                       {
                          $pages  = $pages .",'". $row->page_name. "'";
                       }
       }
       return $pages;
    }

    public function gettags()
    {
      $query="select name,keywords,id from tags";
      $result = DB::select($query);
      return $result;
    }
    public function getProjectByid($id)
    {
      
          $project_data = Project::select('*')->where('id', $id)->get();
          return  $project_data;
    }
    public function getProjectByExcludeid($id)
    {
          $login_user = auth()->user()->id;
          $project_data = Project::select('*')->where('user_id', $login_user);
          $project_data= $project_data->where('id','<>',$id);
          $project_data=$project_data->orderBy('id','DESC')->get();

          return  $project_data;
    }

    public function getProjectCount($project_data)
    {
     
         $count = $project_data->count();
         return $count;
    }

    
public function getprojectkeywork($brand_id)
    {
   $keyword_data = ProjectKeyword::select('main_keyword','require_keyword','exclude_keyword')->where('project_id','=',$brand_id)->get()->toArray();
   return $keyword_data;
    }

    public function getpagename($brand_id)
    {
     $page_list = Project::select('monitor_pages')->where('id','=',$brand_id)->get()->toArray();
      return $page_list;
    }
public function getPageWhereGen($related_pages)
{
    $pages='';
     $filter_pages = '';
      foreach ($related_pages as $key => $value) {
        
        if((int)$key === 0)
        $pages ="'".$value."'";
      else
        $pages .=",'".$value."'";

        $filter_pages="  posts.page_name in (".$pages.")";
      }
    return  $filter_pages;
}
public function getOwnPage($brand_id)
 {
   $keyword_data = Project::select('own_pages')->where('id','=',$brand_id)->get()->toArray();
   return $keyword_data[0]['own_pages'];
 }
 public function getComparisonPage($brand_id)
 {  //echo $brand_id;
   $keyword_data = Project::select('monitor_pages')->where('id','=',$brand_id)->get()->toArray();
   //dd($keyword_data);
   return $keyword_data[0]['monitor_pages'];
 }
  

public function getpagefilter($page_data)
{
  $conditional=[];
  
  $conditional_require_or=[];
  foreach($page_data as $i =>$element)
                            {

                            $require_keyword_filter[] = [ 'page_name' =>  $element];
                            $conditional_require_or['$or']=$require_keyword_filter;
                            }
 $conditional[] =$conditional_require_or;
 
 return  $conditional;
}
    public function getkeywordfilter($keyword_data)
{
     $conditional=[];
         foreach ($keyword_data as  $key => $row) {
         
            
                       //  $request['main_keyword']       =    '.*' . $row['main_keyword'];
                         //$request['require_keyword']       =    $row['require_keyword'];
     $main_keyword_filter =[ 'message' => new MongoDB\BSON\Regex(".*" . $row['main_keyword'],'i' )];
      
       
                         $require_keyword = explode(",", $row['require_keyword']);
                          $conditional_require_or=[];
                         $require_keyword_filter=[];
                            foreach($require_keyword as $i =>$element)
                            {

                            $require_keyword_filter[] = [ 'message' => new MongoDB\BSON\Regex(".*". $element,'i'  )];
                            $conditional_require_or['$or']=$require_keyword_filter;
                            }
                         
                            
                          $exclude_keyword = explode(",", $row['exclude_keyword']);
                          $conditional_exclude_or=[];
                          $exclude_keyword_filter=[];
                        
                            foreach($exclude_keyword as $i =>$element)
                            {
                                if(!empty($element))
                                {
                                               

                            $exclude_keyword_filter[] = [ 'message' => ['$not'=>new MongoDB\BSON\Regex(".*". $element,'i'  )]];
                            $conditional_exclude_or['$or']=$exclude_keyword_filter;
                                }
                            }
                      

                             $conditional_and['$and'] =[$main_keyword_filter];

                            if(!empty($conditional_require_or))
                            {
                                $conditional_and['$and']=[$conditional_and,$conditional_require_or];
                            }
                           
                                               
                            if(!empty($conditional_exclude_or))
                            {
                               
                               $conditional_and['$and']=[$conditional_and,$conditional_exclude_or];
                            }
                           
                          //   $conditional_and['$and'] =[$main_keyword_filter,$conditional_require_or,$conditional_exclude_or];

                        
                        $conditional[] =$conditional_and;
                       
         }

return  $conditional;

}

public function createTable($table_name, $fields = [],$autoincrease=1)
    {
        // check if table is not already exists
        if (!Schema::hasTable($table_name)) {
            Schema::create($table_name, function (Blueprint $table) use ($fields, $table_name,$autoincrease) {
              if($autoincrease <> 0)
                $table->increments('temp_id');

                if (count($fields) > 0) {
                    foreach ($fields as $field) {
                      if(isset($field['index']))
                      {
                        if(isset($field['length']))
                        $table->{$field['type']}($field['name'],$field['length'])->nullable()->index();
                       else
                        $table->{$field['type']}($field['name'])->nullable()->index();
                      }
                      else
                      {
                        if(isset($field['length']))
                        $table->{$field['type']}($field['name'],$field['length'])->nullable();
                        else
                        $table->{$field['type']}($field['name'])->nullable();
                      }
                    }
                }
                $table->timestamps();
            });
 
            return response()->json(['message' => 'Given table has been successfully created!'], 200);
        }
 
        return response()->json(['message' => 'Given table is already existis.'], 400);
    }
    public function new_table_add($brand_id)
    {
       $table_inbound_profiles = 'temp_' . $brand_id . '_inbound_profiles';
       $fields_inbound_profiles = [
            ['name' => 'id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'name', 'type' => 'text'],
            ['name' => 'type', 'type' => 'text'],
            
        ];

       // set dynamic table name according to your requirements
 
       //  $table_inbound_posts = 'temp_' . $brand_id . '_inbound_posts';
       //  $table_inbound_comments = 'temp_' . $brand_id . '_inbound_comments';
     
       //  $fields_inbound_posts = [
       //      ['name' => 'id', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'full_picture', 'type' => 'text'],
       //      ['name' => 'link', 'type' => 'text'],
       //      ['name' => 'name', 'type' => 'text'],
       //      ['name' => 'message', 'type' => 'text'],
       //      ['name' => 'original_message', 'type' => 'text'],
       //      ['name' => 'unicode',  'type' => 'string','length'=>'50'],
       //      ['name' => 'type', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'wb_message', 'type' => 'text'],
       //      ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
       //      ['name' => 'shared', 'type' => 'text'],
       //      ['name' => 'Liked', 'type' => 'text'],
       //      ['name' => 'Love', 'type' => 'text'],
       //      ['name' => 'Haha', 'type' => 'text'],
       //      ['name' => 'Wow', 'type' => 'text'],
       //      ['name' => 'Sad', 'type' => 'text'],
       //      ['name' => 'Angry', 'type' => 'text'],
       //      ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'decided_keyword', 'type' => 'text'],
       //      ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'checked_emotion', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
       //      ['name' => 'change_predict','type'=>'boolean'],
       //      ['name' => 'checked_predict','type'=>'boolean'],
       //      ['name' => 'isBookMark', 'type' => 'boolean']

       //  ];


       // $fields_inbound_comments = [
       //      ['name' => 'id', 'type' => 'text'],
       //      ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
       //      ['name' => 'message', 'type' => 'text'],
       //      ['name' => 'wb_message', 'type' => 'text'],
       //      ['name' => 'original_message', 'type' => 'text'],
       //      ['name' => 'unicode',  'type' => 'string','length'=>'50'],
       //      ['name' => 'post_id', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'comment_count', 'type' => 'text'],
       //      ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'decided_keyword', 'type' => 'text'],
       //      ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'checked_emotion', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'interest', 'type' => 'boolean'],
       //      ['name' => 'tags', 'type' => 'text'],
       //      ['name' => 'change_predict', 'type' => 'boolean'],
       //      ['name' => 'checked_predict','type'=>'boolean'],
       //      ['name' => 'parent', 'type' => 'string','length'=>'50','index'=>1],
       //      ['name' => 'action_status', 'type' => 'string','length'=>'50'],
       //      ['name' => 'action_taken', 'type' => 'string','length'=>'100'],
       //      ['name' => 'isBookMark', 'type' => 'boolean'],
       //      ['name' => 'profile', 'type' => 'string','length'=>'50','index'=>1]
       // ];
         //  return; //need to remove if you want to add this new table to brand
 
       //$this->createTable($table_inbound_posts, $fields_inbound_posts);
       //return $this->createTable($table_inbound_comments, $fields_inbound_comments);
       return $this->createTable($table_inbound_profiles, $fields_inbound_profiles);

    }

      public function operate($brand_id)
    {
        // set dynamic table name according to your requirements
 
        $table_posts = 'temp_' . $brand_id . '_posts';
        $table_comments = 'temp_' . $brand_id . '_comments';
        $table_pages = 'temp_' . $brand_id . '_pages';
        $table_followers = 'temp_' . $brand_id . '_followers';
        $table_inbound_posts = 'temp_' . $brand_id . '_inbound_posts';
        $table_inbound_comments = 'temp_' . $brand_id . '_inbound_comments';
        $table_inbound_profiles = 'temp_profiles';
      
 
        // set your dynamic fields (you can fetch this data from database this is just an example)
        $fields_posts = [
            ['name' => 'id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'full_picture', 'type' => 'text'],
            ['name' => 'link', 'type' => 'text'],
            ['name' => 'name', 'type' => 'text'],
            ['name' => 'message', 'type' => 'text'],
            ['name' => 'original_message', 'type' => 'text'],
            ['name' => 'unicode',  'type' => 'string','length'=>'50'],
            ['name' => 'type', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'wb_message', 'type' => 'text'],
            ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
            ['name' => 'shared', 'type' => 'text'],
            ['name' => 'Liked', 'type' => 'text'],
            ['name' => 'Love', 'type' => 'text'],
            ['name' => 'Haha', 'type' => 'text'],
            ['name' => 'Wow', 'type' => 'text'],
            ['name' => 'Sad', 'type' => 'text'],
            ['name' => 'Angry', 'type' => 'text'],
            ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'decided_keyword', 'type' => 'text'],
            ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'checked_emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
            ['name' => 'change_predict','type'=>'boolean'],
            ['name' => 'checked_predict','type'=>'boolean'],
            ['name' => 'isBookMark', 'type' => 'boolean']

        ];

        $fields_comments = [
            ['name' => 'id', 'type' => 'text'],
            ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
            ['name' => 'message', 'type' => 'text'],
            ['name' => 'wb_message', 'type' => 'text'],
            ['name' => 'original_message', 'type' => 'text'],
            ['name' => 'unicode',  'type' => 'string','length'=>'50'],
            ['name' => 'post_id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'comment_count', 'type' => 'text'],
            ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'decided_keyword', 'type' => 'text'],
            ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'checked_emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'interest', 'type' => 'boolean'],
            ['name' => 'profile', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'change_predict', 'type' => 'boolean'],
            ['name' => 'checked_predict','type'=>'boolean'],
            ['name' => 'parent', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'isBookMark', 'type' => 'boolean'],
            
        ];
         $fields_pages = [
            ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
            ['name' => 'about', 'type' => 'text'],
            ['name' => 'name', 'type' => 'text'],
            ['name' => 'category', 'type' => 'text'],
            ['name' => 'sub_category', 'type' => 'text'],
            ['name'=>'profile','type' => 'text','length'=>'5']
        ];
         $fields_followers = [
            ['name' => 'id', 'type' => 'text'],
            ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
            ['name' => 'fan_count', 'type' => 'text'],
            ['name' => 'date', 'type' => 'text']
        ];
    $fields_inbound_posts = [
            ['name' => 'id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'full_picture', 'type' => 'text'],
            ['name' => 'link', 'type' => 'text'],
            ['name' => 'name', 'type' => 'text'],
            ['name' => 'message', 'type' => 'text'],
            ['name' => 'original_message', 'type' => 'text'],
            ['name' => 'unicode',  'type' => 'string','length'=>'50'],
            ['name' => 'type', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'wb_message', 'type' => 'text'],
            ['name' => 'page_name', 'type' => 'string','length'=>'1000','index'=>1],
            ['name' => 'shared', 'type' => 'text'],
            ['name' => 'Liked', 'type' => 'text'],
            ['name' => 'Love', 'type' => 'text'],
            ['name' => 'Haha', 'type' => 'text'],
            ['name' => 'Wow', 'type' => 'text'],
            ['name' => 'Sad', 'type' => 'text'],
            ['name' => 'Angry', 'type' => 'text'],
            ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'decided_keyword', 'type' => 'text'],
            ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'checked_emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
            ['name' => 'change_predict','type'=>'boolean'],
            ['name' => 'checked_predict','type'=>'boolean'],
            ['name' => 'isBookMark', 'type' => 'boolean'],
            ['name' => 'reactUpdatedAt', 'type' => 'timestamp'],
            ['name' => 'isDeleted', 'type' => 'boolean'],

        ];

        $fields_inbound_comments = [
            ['name' => 'id', 'type' => 'text'],
            ['name' => 'created_time', 'type' => 'timestamp','index'=>1],
            ['name' => 'message', 'type' => 'text'],
            ['name' => 'wb_message', 'type' => 'text'],
            ['name' => 'original_message', 'type' => 'text'],
            ['name' => 'unicode',  'type' => 'string','length'=>'50'],
            ['name' => 'post_id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'comment_count', 'type' => 'text'],
            ['name' => 'sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'checked_sentiment', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'decided_keyword', 'type' => 'text'],
            ['name' => 'emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'checked_emotion', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'interest', 'type' => 'boolean'],
            ['name' => 'profile', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'tags', 'type' => 'text'],
            ['name' => 'checked_tags', 'type' => 'text'],
            ['name' => 'change_predict', 'type' => 'boolean'],
            ['name' => 'checked_predict','type'=>'boolean'],
            ['name' => 'parent', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'action_status', 'type' => 'string','length'=>'50'],
            ['name' => 'action_taken', 'type' => 'string','length'=>'100'],
            ['name' => 'isBookMark', 'type' => 'boolean'],
           
            

        ];
          $fields_inbound_profiles = [
            ['name' => 'temp_id', 'type' => 'string','length'=>'50','index'=>1],
            ['name' => 'id', 'type' => 'text'],
            ['name' => 'name', 'type' => 'text'],
            ['name' => 'type', 'type' => 'text'],
            
        ];

 
       $this->createTable($table_posts, $fields_posts);
       $this->createTable($table_pages, $fields_pages);
       $this->createTable($table_followers, $fields_followers);
        $this->createTable($table_comments, $fields_comments);
       $this->createTable($table_inbound_posts, $fields_inbound_posts);
       $this->createTable($table_inbound_profiles, $fields_inbound_profiles,0);
       return $this->createTable($table_inbound_comments, $fields_inbound_comments);
       
    }
    function convertKtoThousand($value)
{
  $result=0;
 
    if(!empty($value[0]))
    {

    if(strpos($value[0],'K') !== false)
        {
        $new = str_replace("K", "", $value[0]);
         $new = 1000*$new;
        }
        else
        {
         $new=$value[0];   
        }

       
       $result = $result + (float)$new;
    }
    

    return $result;
}

   
    /**
     * To delete the tabel from the database 
     * 
     * @param $table_name
     *
     * @return bool
     */
    public function removeTable($table_name)
    {
        Schema::dropIfExists($table_name); 
        
        return true;
    }

}
