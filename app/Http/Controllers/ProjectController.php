<?php

namespace App\Http\Controllers;
use App\MongodbData;
use App\MongoPages;
use App\MongoFollowers;
use App\Comment;
use App\MongoInboundPost;
use App\MongoInboundComment;
use App\MongoboundPost;
use App\MongoboundComment;
use App\MongoProfile;
use MongoDB;
use Illuminate\Http\Request;
use Auth;
use App\Project;
use App\ProjectKeyword;
use App\InboundPages;
use GuzzleHttp\Client;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use DB;

class ProjectController extends Controller
{
       use GlobalController;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

     public function brandCreate()
    {
        if($user=Auth::user())
        {
          $title="Setting";
          $source = Input::get('source');
          //dd($source);
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $permission_data = $this->getPermission(); 
          return view('brand_create',compact('title','project_data','permission_data','count','source'));
        }
    }

     public function brandEdit($id)
    {
        if($user=Auth::user())
        {
          $source=Input::get('source');
         // dd($source);
          $title="Setting";
          $project_data = $this->getProject();
          $count = $this->getProjectCount($project_data);
          $project = Project::find($id);
          $permission_data = $this->getPermission(); 
          $pp= $project['monitor_pages'];
          $op= $project['own_pages'];
         
            $monitor_pages = explode(',', $pp);
            $pages = (object) $monitor_pages;

            $own_pages = explode(',', $op);
            $own_pages = (object) $own_pages;
            //dd($own_pages);
          $keywords = ProjectKeyword::where('project_id',$id)->get();
     
          return view('brand_edit',compact('title','project_data','count','project','permission_data','keywords','pages','own_pages','source'));
        }
    }
public function Format_Page_Name($array)
    {
      $new_array=[];
       foreach ($array as  $key => $row) {
        $arr_page_name=explode("-",$row);
        if(count($arr_page_name)>0)
        {
          $last_index=$arr_page_name[count($arr_page_name)-1];
          if(is_numeric($last_index))
          $new_array[] = $last_index;
          else
          $new_array[] = $row;
        }
        else
        {
          $new_array[]=$row;
        }
       
          
      
       }

      return  $new_array;
    }

     public function brandStore(Request $request)
    {
        if($user=Auth::user())
        {
           // print_r($request->unchk_val);
           // dd($request->chk_val);
          $pages='';
          $arr_pages=[];
          $my_pages='';
          $other_pages='';
          $main_key= $request->main_key;
          $include_key = $request->include_key;
          $exclude_key = $request->exclude_key;
           $source = $request->source;
          if(isset($request->monitor_pages))
          {
          $monitor_pages = $request->monitor_pages;
          $pages = implode($monitor_pages,',');
             
          $my_pages=$request->chk_val;
          $other_pages=$request->unchk_val;
          

          }
       
          //Insert data to Project table
           $project_id = DB::table('projects')->insertGetId([
                'name' => $request->brand_name,
                'new_mentions' => "fake 100",
                'monitor_pages' => $pages,
                'own_pages' => $my_pages,
                'user_id'=>auth()->user()->id,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString()

             ]);

           //Insert My Pages to Mongodb for inbound Data Craw
           $my_pages=explode(',', $my_pages);
           // dd($my_pages);
           $my_pages = $this->Format_Page_Name($my_pages);
           // dd($my_pages);
           $other_pages=explode(',', $other_pages);
           $other_pages = $this->Format_Page_Name($other_pages);
           // dd($other_pages); 

            $my_pg_count = count($my_pages);
            $other_pg_count = count($other_pages);
            // dd($pg_count);
             for($i=0;$i<$my_pg_count;$i++){
             $this->check_save_mongo($my_pages[$i],1);
                  
            }

             for($i=0;$i<$other_pg_count;$i++){
              echo $request->other_pages[$i];
             $this->check_save_mongo($other_pages[$i],0);
                  
            }

          
            //Insert data to Project Keyword
             $count=count($main_key);
             for($i=0;$i<$count;$i++)
             {
              // dd($pages);
              ProjectKeyword::create([
                'project_id' => $project_id,
                'main_keyword'=>$main_key[$i],
                'require_keyword' => $include_key[$i],
                'exclude_keyword' => $exclude_key[$i],
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString()

              ]);

              }
            // Create Table for Brand
            $response=$this->operate($project_id);
            //Down Brand Data From Mongodb and insert to Mysql
            if(http_response_code() == "200")
            if($my_pg_count>0) 
            $this->insert_Inbound_Data($project_id,$my_pages,'inbound_page');
            if($other_pg_count>0) 
             $this->insert_Inbound_Data($project_id,$other_pages,'outbound_page'); 
            return redirect()->route('brandList',['source'=>$source]);
        }
    }
     public function brandUpdate(Request $request, $id)
    {
    
          $arr_pages=[];
          $my_pages='';
          $other_pages='';
           // print_r($request->unchk_val);
           // dd($request->chk_val);

          $name = $request->brand_name;
          $main_key= $request->main_key;
          $include_key = $request->include_key;
          $exclude_key = $request->exclude_key;
          $source = $request->source;
          $monitor_pages = $request->monitor_pages;
            if(isset($request->monitor_pages))
          {
          $monitor_pages = $request->monitor_pages;
          $pages = implode($monitor_pages,',');
             
          $my_pages=$request->chk_val;
          $other_pages=$request->unchk_val;
          

          }
            else
            {
              $pages=NULL;

            }
         
          

          $pj = Project::find($id);
          $pj->name = $name;
          $pj->monitor_pages = $pages;
          $pj->own_pages= $my_pages;
          $pj->save();



          $keyword = DB::table('project_keywords')->where('project_id',$id)->delete();

          // where('brand_id',$id)->delete();
            $my_pages=explode(',', $my_pages);
            $my_pages = $this->Format_Page_Name($my_pages);

           $other_pages=explode(',', $other_pages);
           $other_pages = $this->Format_Page_Name($other_pages);

            $my_pg_count = count($my_pages);
            $other_pg_count = count($other_pages);
          
            for($i=0;$i<$my_pg_count;$i++){
             $this->check_save_mongo($my_pages[$i],1);
                  
            }

             for($i=0;$i<$other_pg_count;$i++){
              $this->check_save_mongo($other_pages[$i],0);
                  
            }
          
          $count=count($main_key);
          for($i=0;$i<$count;$i++)
          {

             ProjectKeyword::create([
            'project_id' => $id,
            'main_keyword'=>$main_key[$i],
            'require_keyword' => $include_key[$i],
            'exclude_keyword' => $exclude_key[$i],
            'created_at' => now()->toDateTimeString(),
            'updated_at' => now()->toDateTimeString()

            ]);    

          }
           //  dd($source);
           //$this->TruncateTable($id);
           //$this->new_table_add($id);
          // $this->new_table_add($id);
           if($my_pg_count>0) 
            $this->insert_Inbound_Data($id,$my_pages,'inbound_page');
            if($other_pg_count>0) 
             $this->insert_Inbound_Data($id,$other_pages,'outbound_page'); 
          

          return redirect()->route('brandList',['source'=>$source]);
    }

    public function check_save_mongo($monitor_page,$isadmin)
    {
       $result = InboundPages::where('page_name',$monitor_page)->get();
            // dd($result);
           if($result->isEmpty())
           {
            if($monitor_page <> "")
            
          InboundPages::create(['page_name' => $monitor_page,'admin' => $isadmin]);
           }
           else
           {
             $update = InboundPages::where('page_name' , '=' , $monitor_page)->first();
             $update->admin = $isadmin;
             $update->save();
           }
        return true;
    }

    public function getPageWhere($related_pages)
{
    $pages='';
     $filter_pages = '';
      foreach ($related_pages as $key => $value) {
        
        if((int)$key === 0)
        $pages ="'".$value."'";
      else
        $pages .=",'".$value."'";

        $filter_pages="  AND page_name in (".$pages.")";
      }
    return  $filter_pages;
}
  

    public function get_table_id($project_id,$table,$require_var,$related_pages=[])
    {
     
      $filter_pages=$this->getPageWhere($related_pages);
      
      $query = "SELECT ".$require_var." from temp_".$project_id."_".$table ."  Where 1=1 " . $filter_pages;
      //dd($query);
      $id_result = DB::select($query);
      $id_array = array_column($id_result, $require_var);
      return $id_array;
    }
     public function get_common_table_id($table,$require_var)
    {
     
      $id_result=[];
      $query = "SELECT Distinct ".$require_var." from ".$table ."  Where 1=1 ";
      //dd($query);
      $id_result = DB::select($query);
      $id_array = array_column($id_result, $require_var);
      foreach ($id_array as $key => $value) {
        if(null !== $value)
        $id_result[]=new MongoDB\BSON\ObjectId((string)$value);
        # code...
      }
      return $id_result;
    }

    public function insert_Inbound_Data($project_id,$my_pages,$crawl_type)
    {

     
      //for edit

     
     // $keyword_data = $this->getprojectkeywork($project_id);
     // $filter['$or'] = $this->getkeywordfilter($keyword_data);

      
     $page['$or']= $this->getpagefilter($my_pages);
 
 
$inbound_post_id_array = $this->get_table_id($project_id,"inbound_posts","id",$my_pages);

 $inbound_post_result=MongoboundPost::raw(function ($collection) use($page,$inbound_post_id_array,$crawl_type) {//print_r($filter);

    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             // ['crawl_type'=>$crawl_type],
             ['id'=>[ '$nin'=> $inbound_post_id_array ] ],
             $page
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();
 


$data=[];
 $page_array=[];
 $data_message=[];

    
 foreach ($inbound_post_result as  $key => $row) {
                $id ='';$datetime=NULL;
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';$type ='';$original_message ='';$unicode ='';
                $page_name ='';
                $share =0;
                $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
                $reactUpdatedAt=NULL;$isDeleted=0;
                
                if(isset($row['message'])) $message = $row['message'];
                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

                if(isset($row['original_message'])) $original_message = $row['original_message'];
            
                if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['created_time']))
                {
                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                }
                
                
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                
                if(isset($row['type'])) $type = $row['type'];
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
                if(isset($row['share'])) $share = $row['share'];
                if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand($row['reaction']['Angry']);
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];

                 if(isset($row['UpdatedAt']))
                {
                $utcdatetime = $row["UpdatedAt"];
                $reactUpdatedAt = $utcdatetime->toDateTime();
                $reactUpdatedAt=$reactUpdatedAt->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $reactUpdatedAt = $reactUpdatedAt->format('Y-m-d H:i:s');
                }
                  if(isset($row['isDeleted'])) $isDeleted = $row['isDeleted'];
                  $data[] =[
                    'id' => $id,
                    'full_picture' =>$full_picture,
                    'link' =>$link,
                    'name' =>$name,
                    'message' => $message,
                    'original_message'=>$original_message,
                    'unicode'=>$unicode,
                    'type' =>$type,
                    'wb_message' =>'',
                    'page_name' =>$page_name,
                    'shared' =>$share,
                    'Liked' =>$Like,
                    'Love' =>$Love,
                    'Wow' =>$Wow,
                    'Haha' =>$Haha,
                    'Sad' =>$Sad,
                    'Angry' =>$Angry,
                    'sentiment' =>$sentiment,
                    'checked_sentiment' =>$sentiment,
                    'decided_keyword' =>'',
                    'emotion' =>$emotion,
                    'checked_emotion' =>$emotion,
                    'created_time' =>$datetime,
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'isBookMark' =>0,
                    'reactUpdatedAt' =>$reactUpdatedAt,
                    'isDeleted' =>$isDeleted,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                   ];                 

  }
  $path = storage_path('app/data_output/posts'.$crawl_type.'_'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data);

 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_inbound_posts FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,full_picture,link,name,message,original_message,unicode,type,wb_message,page_name,shared,Liked,Love,Wow,Haha,Sad,Angry,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,created_time,change_predict,checked_predict,isBookMark,reactUpdatedAt, isDeleted,created_at,updated_at)";

$pdo = DB::connection('mysql')->getPdo();
 $pdo->exec($query_load);
 if (file_exists($path)) {
    unlink($path) ;
}
/////////////////////////////////////////////////////////////////

 $post_id = $this->getInboundPostId($project_id,$my_pages);/*dd( $filter);*/
// dd($post_id);
 
$inbound_cmt_id_array = $this->get_table_id($project_id,"inbound_comments","id");

//$this->get_table_id($project_id,$date_Begin,$dateEnd,"inbound_posts","id",$my_pages);
  /*dd($inbound_cmt_id_array);*/
 $inbound_comment_result=MongoboundComment::raw(function ($collection) use($page,$inbound_cmt_id_array,$post_id) {//print_r($filter);
 
    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             ['post_id'=>['$in'=>$post_id]],
             ['id'=>[ '$nin'=> $inbound_cmt_id_array ] ],
           
            
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();

$profile_id=[];
  $data_comment=[];
 foreach ($inbound_comment_result as  $key => $row) {
                $id ='';
                $message ='';
                $post_id ='';
                $comment_count =0;$original_message ='';$unicode ='';$profile='';
                $sentiment='';$emotion='';$parent='';
                $tags='';
                if(isset($row['profile']))
                {
                $profile = $row['profile'];
                $profile_id[]=new MongoDB\BSON\ObjectId((string)$profile);
                } 

                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['message'])) $message = $row['message'];
                if(isset($row['original_message'])) $original_message = $row['original_message'];
                if(isset($row['unicode'])) $unicode = $row['unicode'];
                if(isset($row['post_id'])) $post_id = $row['post_id'];
                if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['parent'])) $parent =  $row['parent'];
           

                  $data_comment[] =[
                    'id' => $id,
                    'created_time' =>$datetime,
                    'message' => $message,
                    'wb_message' =>'',
                    'original_message'=>$original_message,
                    'unicode'=>$unicode,
                    'post_id' =>$post_id,
                    'comment_count' =>$comment_count,
                    'sentiment' =>$sentiment,
                    'checked_sentiment' =>$sentiment,
                    'decided_keyword' =>'',
                    'emotion' =>$emotion,
                    'checked_emotion' =>$emotion,
                    'interest' =>'',
                    'profile'=>$profile,
                    'tags'=>'',
                    'checked_tags'=>'',
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'parent'=>$parent,
                    'action_status'=>'',
                    'action_taken'=>'',
                    'isBookMark' =>0,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }

$path = storage_path('app/data_output/comment'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_comment);
 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_inbound_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,original_message,unicode,post_id,comment_count,sentiment,checked_sentiment,decided_keyword,emotion,checked_emotion,interest,profile,tags,checked_tags,change_predict,checked_predict,parent,action_status,action_taken,isBookMark,created_at,updated_at)";
 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);

if (file_exists($path)) {
    unlink($path) ;
}

 $profile_id_array = $this->get_common_table_id("temp_profiles","temp_id");

 $profiles_result=MongoProfile::raw(function ($collection) use($profile_id,$profile_id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
        '$and'=> [ 
          ['_id' => ['$in' => $profile_id ] ] ,
          ['_id'=>[ '$nin'=> $profile_id_array ] ],
            
      ]

        ]  
           ]
    
    ]);
})->toArray();

 $data_profiles=[];
 foreach ($profiles_result as  $key => $row) {
                $_id ='';
                $id ='';
                $name ='';
                $type ='';
              
                if(isset($row['_id'])) $_id =$row['_id'];
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['name'])) $name =$row['name'];
                if(isset($row['type'])) $type = $row['type'];
              
          

                  $data_profiles[] =[
                    'temp_id' => $_id,
                    'id' => $id,
                    'name' =>$name,
                    'type' => $type,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }
 $path = storage_path('app/data_output/profiles'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_profiles);

$query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_profiles FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (temp_id,id,name,type,created_at,updated_at)";

$pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);

 if (file_exists($path)) {
    unlink($path) ;
}  

// return;

// $follow_id_array = $this->get_table_id($project_id,"followers","page_name");
// $follower_result=MongoFollowers::raw(function ($collection) use($my_pages,$follow_id_array) {//print_r($filter);
//  return $collection->aggregate([
//         [
//         '$match' =>[
//              '$and'=> [ 
//           ['page_name' => ['$in' => $my_pages ] ],
//           ['page_name'=>[ '$nin'=> $follow_id_array ] ], 
            
//       ]

//         ]  
//            ]
    
//     ]);
// })->toArray();

// /* print_r($follower_result);
//  return;*/
//  $data_followers=[];
//  foreach ($follower_result as  $key => $row) {
//                 $id ='';
//                 $page_name ='';
//                 $fan_count =0;
//                 $date ='';
             
   
//                 if(isset($row['id'])) $id =$row['id'];
//                 if(isset($row['page_name'])) $page_name =$row['page_name'];
//                 if(isset($row['fan_count'])) $fan_count = $row['fan_count'];
//                 if(isset($row['date'])) $date = $row['date'];
//                 if(isset($row['reaction'])) $reaction =$row['reaction'];
          
//                  $data_followers[] =[
//                     'id' => $id,
//                     'page_name' =>$page_name,
//                     'fan_count' => $fan_count,
//                     'date' =>$date,
//                     'created_at' => now()->toDateTimeString(),
//                     'updated_at' => now()->toDateTimeString()
//                     ];                 

//   }
//  $path = storage_path('app/data_output/followers'.$project_id.'.csv');

// if (file_exists($path)) {
//     unlink($path) ;
// } 
// $this->doCSV($path,$data_followers);

// $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_followers FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,page_name,fan_count,date,created_at,updated_at)";

// $pdo = DB::connection()->getPdo();
//  $pdo->exec($query_load);
//  if (file_exists($path)) {
//     unlink($path) ;
// } 

//  $page_id_array = $this->get_table_id($project_id,"pages","page_name");
//  $pages_result=MongoPages::raw(function ($collection) use($my_pages,$page_id_array) {//print_r($filter);
//  return $collection->aggregate([
//         [
//         '$match' =>[
//              '$and'=> [ 
//           ['page_name' => ['$in' => $my_pages ] ] ,
//           [ 'page_name'=>[ '$nin'=> $page_id_array ] ],
            
//       ]

//         ]  
//            ]
    
//     ]);
// })->toArray();

//  $data_pages=[];
//  foreach ($pages_result as  $key => $row) {
//                 $page_name ='';
//                 $about ='';
//                 $name ='';
//                 $category ='';
//                 $sub_category ='';
//                 $profile='';
 
//                 if(isset($row['page_name'])) $page_name =$row['page_name'];
//                 if(isset($row['name'])) $name =$row['name'];
//                 if(isset($row['about'])) $about = $row['about'];
//                 if(isset($row['category'])) $category = $row['category'];
//                 if(isset($row['sub_category'])) $sub_category = $row['sub_category'];
//                 if(isset($row['profile'])) $profile = $row['profile'];
//               //  if(isset($row['reaction'])) $reaction =$row['reaction'];
          

//                   $data_pages[] =[
//                     'page_name' => $page_name,
//                     'name' =>$name,
//                     'about' => $about,
//                     'category' =>$category,
//                     'sub_category' =>$sub_category,
//                     'profile'=>$profile,
//                     'created_at' => now()->toDateTimeString(),
//                     'updated_at' => now()->toDateTimeString()
//                     ];                 

//   }
//  $path = storage_path('app/data_output/pages'.$project_id.'.csv');

// if (file_exists($path)) {
//     unlink($path) ;
// } 
// $this->doCSV($path,$data_pages);

// $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_pages FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (page_name,name,about,category,sub_category,profile,created_at,updated_at)";

// $pdo = DB::connection()->getPdo();
//  $pdo->exec($query_load);

//  if (file_exists($path)) {
//     unlink($path) ;
// } 

 return true;

 

}

public function insert_OtherInbound_Data($project_id,$other_pages)
    {
      //for edit
   $cmt_id_array=[]; $post_id_array=[];$follow_id_array=[];$page_id_array=[];
     
     $keyword_data = $this->getprojectkeywork($project_id);
     $filter['$or'] = $this->getkeywordfilter($keyword_data);

     //$pagename = $this->getpagename($project_id);
     //$page['$or']= $this->getpagefilter($pagename[0]["monitor_pages"]);
    
     $page['$or']= $this->getpagefilter($other_pages);
 
    /* dd($page);*/
 $inbound_post_id_array = $this->get_table_id($project_id,"inbound_posts","id",$other_pages);
 $inbound_post_result=MongodbData::raw(function ($collection) use($page,$inbound_post_id_array) {//print_r($filter);

    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             [ 'id'=>[ '$nin'=> $inbound_post_id_array ] ],
             $page
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();
 
/*
dd($inbound_post_result);*/
$data=[];
 $page_array=[];
 $data_message=[];

 foreach ($inbound_post_result as  $key => $row) {
                $id ='';
                $full_picture ='';
                $link ='';
                $name ='';
                $message ='';$type ='';
                $page_name ='';
                $share =0;
                $Like =0;$Love=0;$Wow=0;$Haha=0;$Sad=0;$Angry=0;$sentiment='';$emotion='';
                
                if(isset($row['message'])) $message = $row['message'];
                $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$message);

                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['full_picture'])) $full_picture = $row['full_picture'];
                if(isset($row['link'])) $link = $row['link'];
                if(isset($row['name'])) $name = $row['name'];
                
                if(isset($row['type'])) $type = $row['type'];
                if(isset($row['page_name'])) {$page_name = $row['page_name'];$page_array[]=$page_name;}
                if(isset($row['share'])) $share = $row['share'];
                if(isset($row['reaction']['Like'])) $Like =$this->convertKtoThousand($row['reaction']['Like']);
                if(isset($row['reaction']['Love'])) $Love =$this->convertKtoThousand($row['reaction']['Love']);
                if(isset($row['reaction']['Wow'])) $Wow =$this->convertKtoThousand($row['reaction']['Wow']);
                if(isset($row['reaction']['Haha'])) $Haha =$this->convertKtoThousand($row['reaction']['Haha']);
                if(isset($row['reaction']['Sad'])) $Sad =$this->convertKtoThousand($row['reaction']['Sad']);
                if(isset($row['reaction']['Angry'])) $Angry =$this->convertKtoThousand($row['reaction']['Angry']);
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];

                  $data[] =[
                    'id' => $id,
                    'full_picture' =>$full_picture,
                    'link' =>$link,
                    'name' =>$name,
                    'message' => $message,
                    'type' =>$type,
                    'wb_message' =>'',
                    'page_name' =>$page_name,
                    'shared' =>$share,
                    'Liked' =>$Like,
                    'Love' =>$Love,
                    'Wow' =>$Wow,
                    'Haha' =>$Haha,
                    'Sad' =>$Sad,
                    'Angry' =>$Angry,
                    'sentiment' =>$sentiment,
                    'emotion' =>$emotion,
                    'created_time' =>$datetime,
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'isBookMark' =>0,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                   ];                 

  }
  $path = storage_path('app/data_output/inbound_posts'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data);

 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_inbound_posts FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,full_picture,link,name,message,type,wb_message,page_name,shared,Liked,Love,Wow,Haha,Sad,Angry,sentiment,emotion,created_time,change_predict,checked_predict,isBookMark,created_at,updated_at)";


 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);


 $post_id = $this->getInboundPostId($project_id);/*dd( $filter);*/
// dd($post_id);
 
$inbound_cmt_id_array = $this->get_table_id($project_id,"inbound_comments","id");
  /*dd($inbound_cmt_id_array);*/
 $inbound_comment_result=Comment::raw(function ($collection) use($page, $filter,$inbound_cmt_id_array,$post_id) {//print_r($filter);
 
    return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
             ['id'=> ['$exists'=> true]],
             ['post_id'=>['$in'=>$post_id]],
             ['id'=>[ '$nin'=> $inbound_cmt_id_array ] ],
           
            
                      ]
        ]  
                   
       ],
        
    ]);
})->toArray();
 /*dd($inbound_comment_result);*/

  $data_comment=[];
 foreach ($inbound_comment_result as  $key => $row) {
                $id ='';
                $message ='';
                $post_id ='';
                $comment_count =0;
                $sentiment='';$emotion='';$parent='';
                $tags='';
                $utcdatetime = $row["created_time"];
                $datetime = $utcdatetime->toDateTime();
                $datetime=$datetime->setTimeZone(new \DateTimeZone('Asia/Rangoon'));
                $datetime = $datetime->format('Y-m-d H:i:s');
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['message'])) $message = $row['message'];
                if(isset($row['post_id'])) $post_id = $row['post_id'];
                if(isset($row['comment_count'])) $comment_count = $row['comment_count'];
                if(isset($row['sentiment'])) $sentiment = $row['sentiment'];
                if(isset($row['emotion'])) $emotion = $row['emotion'];
                if(isset($row['parent'])) $parent =  $row['parent'];
           

                  $data_comment[] =[
                    'id' => $id,
                    'created_time' =>$datetime,
                    'message' => $message,
                    'wb_message' =>'',
                    'post_id' =>$post_id,
                    'comment_count' =>$comment_count,
                    'sentiment' =>$sentiment,
                    'emotion' =>$emotion,
                    'interest' =>'',
                    'tags'=>'',
                    'change_predict'=>0,
                    'checked_predict'=>0,
                    'parent'=>$parent,
                    'isBookMark' =>0,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }

$path = storage_path('app/data_output/inbound_comment'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_comment);
 $query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_inbound_comments FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,created_time , message,wb_message,post_id,comment_count,sentiment,emotion,interest,tags,change_predict,checked_predict,parent,isBookMark,created_at,updated_at)";

 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);
// return;
 
 

$follow_id_array = $this->get_table_id($project_id,"followers","page_name");
$follower_result=MongoFollowers::raw(function ($collection) use($filter,$other_pages,$follow_id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
          ['page_name' => ['$in' => $other_pages ] ],
           [ 'page_name'=>[ '$nin'=> $follow_id_array ] ], 
            
      ]

        ]  
           ]
    
    ]);
})->toArray();
 
/* print_r($follower_result);
 return;*/
 $data_followers=[];
 foreach ($follower_result as  $key => $row) {
                $id ='';
                $page_name ='';
                $fan_count =0;
                $date ='';
             
   
                if(isset($row['id'])) $id =$row['id'];
                if(isset($row['page_name'])) $page_name =$row['page_name'];
                if(isset($row['fan_count'])) $fan_count = $row['fan_count'];
                if(isset($row['date'])) $date = $row['date'];
                if(isset($row['reaction'])) $reaction =$row['reaction'];
          
                 $data_followers[] =[
                    'id' => $id,
                    'page_name' =>$page_name,
                    'fan_count' => $fan_count,
                    'date' =>$date,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }
 $path = storage_path('app/data_output/followers'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_followers);

$query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_followers FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id,page_name,fan_count,date,created_at,updated_at)";

 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);

 $page_id_array = $this->get_table_id($project_id,"pages","page_name");
 $pages_result=MongoPages::raw(function ($collection) use($filter,$other_pages,$page_id_array) {//print_r($filter);
 return $collection->aggregate([
        [
        '$match' =>[
             '$and'=> [ 
          ['page_name' => ['$in' => $other_pages ] ] ,
          [ 'page_name'=>[ '$nin'=> $page_id_array ] ],
            
      ]

        ]  
           ]
    
    ]);
})->toArray();

 $data_pages=[];
 foreach ($pages_result as  $key => $row) {
                $page_name ='';
                $about ='';
                $name ='';
                $category ='';
                $sub_category ='';
                $profile='';
 
                if(isset($row['page_name'])) $page_name =$row['page_name'];
                if(isset($row['name'])) $name =$row['name'];
                if(isset($row['about'])) $about = $row['about'];
                if(isset($row['category'])) $category = $row['category'];
                if(isset($row['sub_category'])) $sub_category = $row['sub_category'];
                if(isset($row['profile'])) $profile = $row['profile'];
              //  if(isset($row['reaction'])) $reaction =$row['reaction'];
          

                  $data_pages[] =[
                    'page_name' => $page_name,
                    'name' =>$name,
                    'about' => $about,
                    'category' =>$category,
                    'sub_category' =>$sub_category,
                    'profile'=>$profile,
                    'created_at' => now()->toDateTimeString(),
                    'updated_at' => now()->toDateTimeString()
                    ];                 

  }
 $path = storage_path('app/data_output/pages'.$project_id.'.csv');

if (file_exists($path)) {
    unlink($path) ;
} 
$this->doCSV($path,$data_pages);

$query_load ="LOAD DATA LOCAL INFILE '".$path."' INTO TABLE temp_".$project_id."_pages FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (page_name,name,about,category,sub_category,profile,created_at,updated_at)";

 $pdo = DB::connection()->getPdo();
 $pdo->exec($query_load);


 return true;



 

}

function doJson($path,$json)
{
    $fp = fopen($path, 'w');
   fwrite($fp, $json);
   fclose($fp);
}
   function doCSV($path, $array)
{
   $fp = fopen($path, 'w');
$i = 0;
foreach ($array as $fields) {
    if($i == 0){
        fputcsv($fp, array_keys($fields));
    }
    fputcsv($fp, array_values($fields));
    $i++;
}

fclose($fp);
}

    function convertKtoThousand($s)
{
    if (strpos(strtoupper($s), "K") != false) {
    $s = rtrim($s, "kK");
    return floatval($s) * 1000;
  } else if (strpos(strtoupper($s), "M") != false) {
    $s = rtrim($s, "mM");
    return floatval($s) * 1000000;
  } else {
    return floatval($s);
  }
}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    public function TruncateTable($id)
  {
          if (Schema::hasTable('temp_'.$id.'_posts')) {
              DB::table('temp_'.$id.'_posts')->truncate();

             }
               if (Schema::hasTable('temp_'.$id.'_comments')) {
              DB::table('temp_'.$id.'_comments')->truncate();
             }
               if (Schema::hasTable('temp_'.$id.'_followers')) {
              DB::table('temp_'.$id.'_followers')->truncate();
             }
               if (Schema::hasTable('temp_'.$id.'_pages')) {
              DB::table('temp_'.$id.'_pages')->truncate();
             }
             if (Schema::hasTable('temp_'.$id.'_inbound_posts')) {
              DB::table('temp_'.$id.'_inbound_posts')->truncate();
             }
             if (Schema::hasTable('temp_'.$id.'_inbound_comments')) {
              DB::table('temp_'.$id.'_inbound_comments')->truncate();
             }
             return true;
  }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function brand_delete($id)
    {
    
        $prj = DB::table('projects')->where('id',$id)->delete();
        $keywords =  DB::table('project_keywords')->where('project_id', $id)->delete();
        // dd($id);
        $pages = InboundPages::where('brand_id',$id)->delete();
        $this->DropTable($id);
         return redirect('brandList')->with([
        'flash_message' => 'Deleted',
        'flash_message_important' => false
  ]);
    }

    public function DropTable($id)
  {
    Schema::dropIfExists('temp_'.$id.'_posts'); 
    Schema::dropIfExists('temp_'.$id.'_comments'); 
    Schema::dropIfExists('temp_'.$id.'_followers'); 
    Schema::dropIfExists('temp_'.$id.'_pages'); 
    Schema::dropIfExists('temp_'.$id.'_inbound_posts'); 
    Schema::dropIfExists('temp_'.$id.'_inbound_comments'); 

    return true;
  }

     public function getProjectData ()
   {
     $source='';
     if(!null==Input::get('source'))
     $source=Input::get('source');

     $login_user = auth()->user()->id;
     $permission_data = $this->getPermission();
     $projectlist = Project::select(['id', 'name', 'created_at'])->where('user_id',$login_user)->orderBy('id');

     // echo $source ;
     // return;

     return Datatables::of($projectlist)
       ->addColumn('action', function ($projectlist) use($permission_data,$source) {
        $html='';
                if($permission_data['setting'] === 1)
                {
                $html ='<a href="brandList/edit/'.$projectlist->id.'?source='.$source.'" class="btn btn-xs btn-primary"><i class="mdi mdi-table-edit"></i> Edit</a> 
            <button class="btn btn-xs btn-danger btn-delete" data-remote="' . route('brandList/delete', $projectlist->id) . '"><i class="mdi mdi-delete"></i>Delete</button> <a href="dashboard?pid='.$projectlist->id.'&source='.$source.'" class="btn btn-xs btn-secondary"><i class="fas fa-home"></i> Dashboard</a>';
                }
               return $html;

            })
       ->editColumn('created_at', function ($projectlist) {
                 if (isset($projectlist->created_at)) {
                return   $projectlist->created_at->format('d/m/Y');
            }
            })
       ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })

      ->make(true);
           // ->rawColumns(['image', 'action'])


 }
 function page_delete()
 {
   $pages = InboundPages::where('brand_id',77)->delete();
 }

}
