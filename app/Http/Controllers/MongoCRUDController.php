<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;

class MongoCRUDController extends Controller
{
    // 
    use GlobalController;
    public function UpdateBookMarkPost()
    {
          $bookmark_array = Input::post('bookmark_array');
          $brand_id = Input::post('id');
          $bookmark_remove_array = Input::post('bookmark_remove_array');

          if(isset($bookmark_array))
          {
          foreach ($bookmark_array as $key => $value) {
          	/* $update = MongodbData::where('id' , '=' , $value)->first();
			       $update->isBookMark = true;
			       $update->save();*/
             DB::table('temp_'.$brand_id.'_posts')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 1));  // update the record in the DB. 
          }
        }
        if(isset($bookmark_remove_array))
        {
          foreach ($bookmark_remove_array as $key => $value) {
            DB::table('temp_'.$brand_id.'_posts')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 0));  // update the record in the DB. 
          	
          }
        }

            return "success";

        }

        public function UpdateBookMarkInboundPost()
    {
          $bookmark_array = Input::post('bookmark_array');
          $brand_id = Input::post('id');
          $bookmark_remove_array = Input::post('bookmark_remove_array');

          if(isset($bookmark_array))
          {
          foreach ($bookmark_array as $key => $value) {
            /* $update = MongodbData::where('id' , '=' , $value)->first();
             $update->isBookMark = true;
             $update->save();*/
             DB::table('temp_'.$brand_id.'_inbound_posts')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 1));  // update the record in the DB. 
          }
        }
        if(isset($bookmark_remove_array))
        {
          foreach ($bookmark_remove_array as $key => $value) {
            DB::table('temp_'.$brand_id.'_inbound_posts')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 0));  // update the record in the DB. 
            
          }
        }

            return "success";

        }
        public function UpdateBookMarkInboundComment()
    {
          $bookmark_array = Input::post('bookmark_comment_array');
          $brand_id = Input::post('id');
          $bookmark_remove_array = Input::post('bookmark_comment_remove_array');

          if(isset($bookmark_array))
          {
          foreach ($bookmark_array as $key => $value) {
            /* $update = MongodbData::where('id' , '=' , $value)->first();
             $update->isBookMark = true;
             $update->save();*/
             DB::table('temp_'.$brand_id.'_inbound_comments')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 1));  // update the record in the DB. 
          }
        }
        if(isset($bookmark_remove_array))
        {
          foreach ($bookmark_remove_array as $key => $value) {
            DB::table('temp_'.$brand_id.'_inbound_comments')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 0));  // update the record in the DB. 
            
          }
        }

            return "success";

        }

         public function UpdateBookMarkComment()
    {
          $bookmark_array = Input::post('bookmark_comment_array');
          $brand_id = Input::post('id');
          $bookmark_remove_array = Input::post('bookmark_comment_remove_array');

          if(isset($bookmark_array))
          {
          foreach ($bookmark_array as $key => $value) {
            /* $update = MongodbData::where('id' , '=' , $value)->first();
             $update->isBookMark = true;
             $update->save();*/
             DB::table('temp_'.$brand_id.'_comments')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 1));  // update the record in the DB. 
          }
        }
        if(isset($bookmark_remove_array))
        {
          foreach ($bookmark_remove_array as $key => $value) {
            DB::table('temp_'.$brand_id.'_comments')
            ->where('id', $value)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('isBookMark' => 0));  // update the record in the DB. 
            
          }
        }

            return "success";

        }

//      public function commentUpdate()
//      { 
//          $comment_table = 'temp_59_inbound_comments';
//         $comments = DB::table($comment_table)->select('temp_id','wb_message')->where('sentiment','')->limit(10000)->get();
//         if(!$comments->isEmpty()){
//         foreach($comments as $comment)
//         {

//           $wb_message[] = $comment->wb_message;
//           $request['wb_message']= $comment->wb_message;
//           $request['id'] = $comment->temp_id;

//           $data[]=$request;
//         }

//         if(count($wb_message) > 0)
//         {
//           $client = new Client(['base_uri' => 'http://35.227.105.155:6000/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
//           $uri_sentiment = 'senti_emo_interest';
         
//             $formData = array(
//     'raw' =>  $wb_message,
   
// );

//            $formData = json_encode($formData);
// //            $path = storage_path('app/data_output/wunzinn_wb_msg_021018.json');
// // $this->doJson($path,$formData);
// // return;

//            $api_response = $client->post($uri_sentiment, [
//                                 'form_params' => [
//                                 'raw' =>  $formData,
//                                 ],
//                              ]);

//             $result = ($api_response->getBody()->getContents());

//             $json_result_array = json_decode($result, true);
  
//             $result = $json_result_array[0];

//             $count = (Int)count($data);

//             for($i=0;$i<$count;$i++)
//             {
              
//                 $id = $data[$i]['id'];
               
//                 $comment = DB::table($comment_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'interest'=> $result['interest'][$i],'updated_at' => now()->toDateTimeString()]);
//                  // $post = DB::table($comment_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'updated_at' => now()->toDateTimeString()]);
                 
//             }

//         }
//       }
//       else{
//         echo "Cron has nothing to do ";
//       }
      
//     // }
//   }

        public function commentUpdate()
        {
      // $projects = Project::all();
      // $data =  [];
      
      // foreach($projects as $project)
      // {
        $id = 17;
        $comment_table = "temp_".$id."_inbound_comments";
        $comments = DB::table($comment_table)->select('temp_id','wb_message')->where('sentiment','')->limit(10000)->get();
         if(!$comments->isEmpty()){
        foreach($comments as $comment)
        {

          $wb_message[] = $comment->wb_message;
          $request['wb_message']= $comment->wb_message;
          $request['id'] = $comment->temp_id;

          $data[]=$request;
        }
      
        if(count($wb_message) > 0)
        {
          $client = new Client(['base_uri' => 'http://35.227.105.155:6000/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
          $uri_sentiment = 'senti_emo_interest';
         
            $formData = array(
    'raw' =>  $wb_message,
   
);

           $formData = json_encode($formData);

           $api_response = $client->post($uri_sentiment, [
                                'form_params' => [
                                'raw' =>  $formData,
                                ],
                             ]);

            $result = ($api_response->getBody()->getContents());

            $json_result_array = json_decode($result, true);
  
            $result = $json_result_array[0];

            $count = (Int)count($data);

            for($i=0;$i<$count;$i++)
            {
              
                $id = $data[$i]['id'];
               
                $comment = DB::table($comment_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'interest'=> $result['interest'][$i],'updated_at' => now()->toDateTimeString()]);
                
                 
            }

        }
      }
      // }
        }
  function doJson($path,$json)
{
    $fp = fopen($path, 'w');
   fwrite($fp, $json);
   fclose($fp);
}

    function fontCheck($message)
    {

   
      $firstCharacter = substr(strtolower($message), 0, 8);
      if (strpos($firstCharacter, 'zawgyi') !== false) {
            
  		$split = explode("unicode",strtolower($message));
  		$z_msg = $split[0];
  		
  		$last_space_position = strrpos($z_msg, ' ');
  		$message = substr($z_msg, 0, $last_space_position);
  		$message = str_replace("zawgyi", "", strtolower($message));
  		$first2Characters = substr(strtolower($message), 0, 2);
  		$message = trim($message,$firstCharacter);

           
        }
        else
        {
       
          $split = explode("zawgyi",strtolower($message));
    
    		  $z_msg = $split[1];
     
    		  $firstCharacter = substr(strtolower($z_msg), 0, 1);
    		  
    		  $message = trim($z_msg,$firstCharacter);
    		
            }
         
     
    
    
     return $message;
   }

    
    function doTag()
    {
    
    // $query = DB::table('projects')->select('*')->get();
      // foreach($query as $project){
      // $project_id = $project->id;
      // $table = 'temp_'.$project_id.'_inbound_comments';
    $table='temp_17_inbound_comments';
      $comments = DB::table($table)->select('temp_id','message')->get();
      $tags =$this->gettags(); 
      // dd($comments);
      foreach ($comments as $comment) {
         $message = $comment->message;
        
         $id = $comment->temp_id;
         $tag_string = "";  
      
             foreach ($tags as $key => $value) {
              
              $kw = $value->keywords;
              $kw = explode(",",$kw);

               foreach ($kw as $key_kw => $value_kw) {

                  if (strpos($message, $value_kw) !== false)
                  {
                    
                 $tag_string.= ',' .$value->name;
       
                  }
        }
      }
      if($tag_string != ""){
         $tag_string = substr($tag_string, 1); 
         }     
         // dd($tag_string);

    $tag =  DB::table($table)->where('temp_id', $id)->update(['tags' => $tag_string,'updated_at' => now()->toDateTimeString()]);
  
       
      }
  // }
    }

   //  function doJson($path,$json)
   // {
   //  $fp = fopen($path, 'w');
   // fwrite($fp, $json);
   // fclose($fp);
   // }

        
    }

    
