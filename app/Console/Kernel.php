<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
       // 'App\Console\Commands\cronWordbreak_post',
       // 'App\Console\Commands\cronWordbreak_comment',
       // 'App\Console\Commands\cronPosts',
       // 'App\Console\Commands\cronComments',
       // 'App\Console\Commands\cronTag_comment',

        // 'App\Console\Commands\cronWordbreak_inboundPost',
        // 'App\Console\Commands\cronWordbreak_inboundComment',
        // 'App\Console\Commands\cronInboundPosts',
        // 'App\Console\Commands\cronInboundComments'
      
        // 'App\Console\Commands\cronTag_inboundComment'
        // 'App\Console\Commands\cronGettingTag'
        
    ];
		// protected $commands = [
		//         Commands\MakeView::class
		//     ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        //  $schedule->command('wordbreak_post:update')
        //           ->everyMinute();
        //           // ->cron('5 4 * * *');


        //   $schedule->command('wordbreak_comment:update')
        //           ->everyMinute()
        //           ->thenPing("https://cronhub.io/ping/beaeb360-c55a-11e8-9cc0-136987490eca");

        //  $schedule->command('post:update')
        //          ->everyMinute();

        //   $schedule->command('comment:update')
        //          ->everyMinute();

        // $schedule->command('comment_tag:update')
        //           ->everyMinute();




        //  $schedule->command('wordbreak_inboundPost:update')
        //           ->everyMinute();

        // $schedule->command('wordbreak_inboundComment:update')
        //           ->everyMinute();        

        //  $schedule->command('inbound_post:update')
        //           ->everyMinute()
        //           ->thenPing("https://cronhub.io/ping/a413d470-c7ab-11e8-9bec-510f13ed1aa9");

        //  $schedule->command('inbound_comment:update')
        //           ->everyMinute()
        //           ->thenPing("https://cronhub.io/ping/1c5e8cf0-c61c-11e8-a934-2d90837d4467"); 
                  
        
          // $schedule->command('inbound_comment_tag:update')
          //         ->everyMinute();
          // $schedule->command('listener:tag')
          //        ->everyMinute();
         
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
