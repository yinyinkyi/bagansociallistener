<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;

class cronInboundPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbound_post:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update inbound post every  minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $projects = Project::all();
      $data =  [];
      
      foreach($projects as $project)
      {
        $id = $project->id;
        $post_table = "temp_".$id."_inbound_posts";
        $posts = DB::table($post_table)->select('temp_id','wb_message')->where('sentiment','')->limit(3500)->get();
         if(!$posts->isEmpty()){
        foreach($posts as $post)
        {

          $wb_message[] = $post->wb_message;
          $request['wb_message']= $post->wb_message;
          $request['id'] = $post->temp_id;

          $data[]=$request;
        }
      
        if(count($wb_message) > 0)
        {
          $client = new Client(['base_uri' => 'http://35.185.97.177:5004/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
          $uri_sentiment = 'senti_emo_interest';
         
            $formData = array(
    'raw' =>  $wb_message,
    'threshold'=>0
   
       );

           // $formData = json_encode($formData);
           //$formData = htmlspecialchars(json_encode($formData), ENT_QUOTES, 'UTF-8');
             $formData = json_encode($formData);

           $api_response = $client->post($uri_sentiment, [
                                'form_params' => [
                                'raw' =>  $formData,
                                ],
                             ]);

            $result = ($api_response->getBody()->getContents());

            $json_result_array = json_decode($result, true);
  
            $result = $json_result_array[0];

            $count = (Int)count($data);

            for($i=0;$i<$count;$i++)
            {
              
                $id = $data[$i]['id'];
               
                $post = DB::table($post_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'updated_at' => now()->toDateTimeString()]);
                 
            }

        }
      }
      }
    }
}
