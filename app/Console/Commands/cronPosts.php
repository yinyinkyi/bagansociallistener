<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\MongodbData;
use DB;
use MongoDB;
use GuzzleHttp\Client;

class cronPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update post every  minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $projects = DB::table('projects')->select('*')->get();
      $data =  [];
      // \Log::info($projects);
      
      foreach($projects as $project)
      {
        $id = $project->id;
        $post_table = "temp_".$id."_posts";
        
          $posts = DB::table($post_table)->select('temp_id','wb_message')->where('sentiment','')->limit(3500)->get();
         if(!$posts->isEmpty()){
        foreach($posts as $post)
        {

          $wb_message[] = $post->wb_message;
          $request['wb_message']= $post->wb_message;
          $request['id'] = $post->temp_id;

          $data[]=$request;
        }
      
        if(count($wb_message) > 0)
        {
          $client = new Client(['base_uri' => 'http://35.227.105.155:6000/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
          $uri_sentiment = 'senti_emo_interest';
         
            $formData = array(
    'raw' =>  $wb_message,
   
);

           $formData = json_encode($formData);

           $api_response = $client->post($uri_sentiment, [
                                'form_params' => [
                                'raw' =>  $formData,
                                ],
                             ]);

            $result = ($api_response->getBody()->getContents());

            $json_result_array = json_decode($result, true);
  
            $result = $json_result_array[0];

            $count = (Int)count($data);

            for($i=0;$i<$count;$i++)
            {
              
                $id = $data[$i]['id'];
               
                $post = DB::table($post_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'updated_at' => now()->toDateTimeString()]);
                 
            }

        }
      }
      }
    }
  }

//         public function mongohandle()
//     {

//         $query_result = MongodbData::raw(function ($collection) {
//          return $collection->aggregate([
//         [
//         '$match' =>[
//              '$and'=> [ 
             
                 
//                  ['message'=> ['$exists'=> true]],
//                  ['id'=> ['$exists'=> true]],
//                  ['new'=>['$exists'=> false]]
//               ]

//               ]  
//               ]
//               ,

//              ['$limit' => 2000],
//               [
//             '$sort' =>['updated_time'=>-1]


//             ]
 
//     ]);
//    })->toArray();
// // dd($query_result);
//            // 
//          $data = [];
//          $data_message = [];
//          foreach($query_result as $row)
//          {
        
         
//           $request['id'] = $row["id"];
//           $request['message'] = $row["b_message"];
        
//           $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$row["message"]);
//           // $data[]=$data_message;
//           $data[] =$request;
          

//           //call API here with single message
   
//          }
//         if(count($data_message)>0)
//         {
         
//         $client = new Client(['base_uri' => 'http://35.227.105.155:6000/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
//       $uri_sentiment = 'senti_emo_interest';
           
//             $formData = array(
//     'raw' =>  $data_message,
   
// );
//         // dd($formData);
    
//            $formData = json_encode($formData);
//            // dd($formData);
//           $sentiment_response = $client->post($uri_sentiment, [
//                                 'form_params' => [
//                                 'raw' =>  $formData,
//                                 ],
//                              ]);

//           $emotion_response = $client->post($uri_emotion, [
//                               'form_params' => [
//                               'raw' =>  $formData,
//                                 ],
//                              ]);
//             // dd($sentiment_response);

//             $sentiment_result = ($sentiment_response->getBody()->getContents());

         

//             $json_sentiment_array = json_decode($sentiment_result, true);
  
//             $sentiment_result = $json_sentiment_array['raw'];
//             $emotion_result = ($emotion_response->getBody()->getContents());


//             $json_emotion_array = json_decode($emotion_result, true);
//             $emotion_result = $json_emotion_array['raw'];
//             $count = (Int)count($data);
//             for($i=0;$i<$count;$i++)
//             {
//                 $id = $data[$i]['id'];
//                 // dd($id);

//             $data['sentiment']= str_replace(" ", "",str_replace("'", "", $sentiment_result[$i])) ;
//              // dd($data['sentiment']);
//             $data['emotion']=str_replace(" ", "",str_replace("'", "",  $emotion_result[$i])) ;
//              // dd($data['sentiment']);
          
//              $post =MongodbData::where('id' , '=' ,$id)->first();
//              $post->sentiment = $data['sentiment'];
//              $post->emotion = $data['emotion'];
//              $post->new = 1;
//              $post->save();
           
//             }

            


// }

//     }

