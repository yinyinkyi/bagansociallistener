<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;

class cronWordbreak_inboundPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wordbreak_inboundPost:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update word break message in inbound post table every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $query = DB::table('projects')->select('*')->get();
      foreach($query as $project)
      {
        $post_table = 'temp_'.$project->id.'_inbound_posts';
        $query = "select temp_id,message from ".$post_table." where message <> '' and message is not null and ( wb_message is null or wb_message ='') LIMIT 1000 "; 
        $posts = DB::select($query);
        $data = [];
        $data_message = [];
        foreach($posts as $post)
        {
        
          $request['id'] = $post->temp_id;
          $message = $post->message;
          $z_count = substr_count(strtolower($message), 'zawgyi');
          $u_count = substr_count(strtolower($message), 'unicode');
           if ($z_count == 1 && $u_count == 1)
           {
            
                   $request['message'] = $this ->fontCheck($message);
                  
           }
          
           else{
            $request['message']  = $message;
          }

          $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$request['message']);
          $data[] =$request;
          
        }

        if(count($data_message)> 0)
        {
          $client = new Client(['base_uri' => 'http://35.185.97.177:5002/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
          $uri_wb = 'wb_posTag';
          
              $formData = array(
    'raw' =>  $data_message,
   
); 
    
           $formData = json_encode($formData);
           // dd($formData);
           $wb_response = $client->post($uri_wb, [
                                'form_params' => [
                                'raw' =>  $formData,
                                ],
                             ]);
       
            $wb_result = ($wb_response->getBody()->getContents());
           //
            $json_wb_array = json_decode($wb_result, true);
            $wb_result = $json_wb_array['raw'];
             // dd($wb_result);
            $count = (Int)count($data);
            for($i=0;$i<$count;$i++)
            {
                $id = $data[$i]['id'];
                
           $wb =  DB::table($post_table)->where('temp_id', $id)->update(['wb_message' => $wb_result[$i],'message' => $data[$i]['message'],'updated_at' => now()->toDateTimeString()]);
            }

   
        }
        
    }
  }

    function fontCheck($message)
    {

   
      $firstCharacter = substr(strtolower($message), 0, 8);
      if (strpos($firstCharacter, 'zawgyi') !== false) {
            
      $split = explode("unicode",strtolower($message));
      $z_msg = $split[0];
      
      $last_space_position = strrpos($z_msg, ' ');
      $message = substr($z_msg, 0, $last_space_position);
      $message = str_replace("zawgyi", "", strtolower($message));
      $first2Characters = substr(strtolower($message), 0, 2);
      $message = trim($message,$firstCharacter);

           
        }
        else
        {
       
          $split = explode("zawgyi",strtolower($message));
    
          $z_msg = $split[1];
     
          $firstCharacter = substr(strtolower($z_msg), 0, 1);
          
          $message = trim($z_msg,$firstCharacter);
        
            }
         
     
    
    
     return $message;
   }
}
