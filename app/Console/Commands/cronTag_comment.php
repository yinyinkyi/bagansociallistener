<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;


class cronTag_comment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'comment_tag:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update tags in comment table every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $projects = DB::table('projects')->select('*')->get();
      foreach($projects as $project){
      $project_id = $project->id;
      $table = 'temp_'.$project_id.'_comments';
      $comments = DB::table($table)->select('temp_id','message')->get();
      $tags =$this->gettags(); 
      // dd($comments);
      foreach ($comments as $comment) {
         $message = $comment->message;
        
         $id = $comment->temp_id;
         $tag_string = "";  
      
             foreach ($tags as $key => $value) {
              
              $kw = $value->keywords;
              $kw = explode(",",$kw);

               foreach ($kw as $key_kw => $value_kw) {

                  if (strpos($message, $value_kw) !== false)
                  {
                    
                 $tag_string.= ',' .$value->name;
       
                  }
        }
      }
      if($tag_string != ""){
         $tag_string = substr($tag_string, 1); 
         }     

    $tag =  DB::table($table)->where('temp_id', $id)->update(['tags' => $tag_string,'updated_at' => now()->toDateTimeString()]);
  
       
      }
  }
    }

    public function gettags()
    {
      $query="select name,keywords,id from tags";
      $result = DB::select($query);
      return $result;
    }
}
