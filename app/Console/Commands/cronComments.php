<?php

namespace App\Console\Commands;

use Illuminate\Console\Command as BaseCommand;

use Illuminate\Http\Request;
use App\MongodbData;
use App\Comment;
use App\Project;
use App\Http\Controllers\stdClass;
use DB;
use MongoDB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use App\ProjectKeyword;
use Yajra\Datatables\Datatables;



class cronComments extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'comment:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update comment every  minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
      $projects = DB::table('projects')->select('*')->get();
      $data =  [];
     
      
      foreach($projects as $project)
      {
     
        $id = $project->id;
        $comment_table = "temp_".$id."_comments";
        

          $comments = DB::table($comment_table)->select('temp_id','wb_message')->where('sentiment','')->limit(4500)->get();
          if(!$comments->isEmpty()){
        foreach($comments as $comment)

        {

          $wb_message[] = $comment->wb_message;
          $request['wb_message']= $comment->wb_message;
          $request['id'] = $comment->temp_id;

          $data[]=$request;
        }
      
        if(count($wb_message) > 0)
        {
          $client = new Client(['base_uri' => 'http://35.227.105.155:6000/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
          $uri_sentiment = 'senti_emo_interest';
         
            $formData = array(
    'raw' =>  $wb_message,
   
);

           $formData = json_encode($formData);

           $api_response = $client->post($uri_sentiment, [
                                'form_params' => [
                                'raw' =>  $formData,
                                ],
                             ]);

            $result = ($api_response->getBody()->getContents());

            $json_result_array = json_decode($result, true);
  
            $result = $json_result_array[0];

            $count = (Int)count($data);

            for($i=0;$i<$count;$i++)
            {
              
                $id = $data[$i]['id'];
               
                $comment = DB::table($comment_table)->where('temp_id',$id)->update(['sentiment' => $result['sentiment'][$i],'emotion'=> $result['emotion'][$i],'interest'=> $result['interest'][$i],'updated_at' => now()->toDateTimeString()]);
                 
            }

        }
      }
      }

    }

}
   
//     public function mongohandle()
//     {
//        $query_result = Comment::raw(function ($collection) {
//          return $collection->aggregate([
//         [
//         '$match' =>[
//              '$and'=> [ 
             
                 
//                  ['message'=> ['$exists'=> true]],
//                  ['id'=> ['$exists'=> true]],
//                  ['new'=>['$exists' => false]]
//               ]

//               ]  
//               ]
//               ,

//              ['$limit' => 2000],
//               [
//             '$sort' =>['updated_at'=>-1]


//             ]
 
//     ]);
//    })->toArray();
// // dd($query_result);
//            // 
//          $data = [];
//          $data_message = [];
//          foreach($query_result as $row)
//          {
        
         
//           $request['id'] = $row["id"];
//           $request['message'] = $row["message"];
        
//           $data_message[]=preg_replace('/(\r\n|\r|\n)+/', " ",$row["message"]);
//           // $data[]=$data_message;
//           $data[] =$request;
          

//           //call API here with single message
   
//          }
//         if(count($data_message)>0)
//         {
   
//          $client = new Client(['base_uri' => 'http://35.227.105.155:6000/','headers' => ['Content-type => application/x-www-form-urlencoded\r\n']]);
//           $uri_sentiment = 'sentiment_predict';
//           $uri_emotion = 'emotion_predict';
       
           
//             $formData = array(
//     'raw' =>  $data_message,
   
// );
       
    
//            $formData = json_encode($formData);
      
//           $sentiment_response = $client->post($uri_sentiment, [
//                                 'form_params' => [
//                                 'raw' =>  $formData,
//                                 ],
//                              ]);

//           $emotion_response = $client->post($uri_emotion, [
//                               'form_params' => [
//                               'raw' =>  $formData,
//                                 ],
//                              ]);
         

//             $sentiment_result = ($sentiment_response->getBody()->getContents());
    

//             $json_sentiment_array = json_decode($sentiment_result, true);
 
//             $sentiment_result = $json_sentiment_array['raw'];
//             $emotion_result = ($emotion_response->getBody()->getContents());

     
//             $json_emotion_array = json_decode($emotion_result, true);
//             $emotion_result = $json_emotion_array['raw'];
//             $count = (Int)count($data);
//             for($i=0;$i<$count;$i++)
//             {
//                 $id = $data[$i]['id'];
           

//             $data['sentiment']= str_replace(" ", "",str_replace("'", "", $sentiment_result[$i])) ;

//             $data['emotion']=str_replace(" ", "",str_replace("'", "",  $emotion_result[$i])) ;
  
          
//              $comment =Comment::where('id' , '=' ,$id)->first();
//              $comment->sentiment = $data['sentiment'];
//              $comment->emotion = $data['emotion'];
//              $comment->new = 1;
//              $comment->save();
            
//             }

            


// } 
//     }