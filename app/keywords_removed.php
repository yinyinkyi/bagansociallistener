<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class keywords_removed extends Model
{
  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
  protected $table = 'keywords_removed';
  protected $dates = ['created_at','updated_at'];
    protected $fillable = [
        'id','keyword','brand_id','created_at','updated_at'
    ];

 

}
