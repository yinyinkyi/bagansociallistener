<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MongoFan extends  Eloquent
{
    protected $connection = 'mongodb';
	protected $collection = 'followers';
    protected $dates = ['createdAt'];

  public function getDates() {
        return array();
  }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
}
